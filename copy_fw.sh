#!/bin/bash

#this script is setup to copy the fw images from my directory on volta to the sd card plugged into my laptop, you'll have to change all the paths

#fw files go on the first sd card partition, mount that partition locally to /mnt
#scp the files on volta to a local direcotyr to simplify permission issues.
#move those files to /fw
#IP=192.168.10.209
IP=192.168.10.227

scp ./images/BOOT.BIN ./images/image.ub root@$IP:/fw/
#scp ./images/BOOT.BIN ./images/image.ub root@192.168.10.201:/fw/
