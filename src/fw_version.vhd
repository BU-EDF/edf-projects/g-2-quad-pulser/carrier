library ieee;
use ieee.std_logic_1164.all;
-- timestamp package
package FW_TIMESTAMP is
  constant TS_CENT     : std_logic_vector(7 downto 0) := x"20";
  constant TS_YEAR     : std_logic_vector(7 downto 0) := x"21";
  constant TS_MONTH    : std_logic_vector(7 downto 0) := x"09";
  constant TS_DAY      : std_logic_vector(7 downto 0) := x"16";
  constant TS_HOUR     : std_logic_vector(7 downto 0) := x"12";
  constant TS_MIN      : std_logic_vector(7 downto 0) := x"45";
  constant TS_SEC      : std_logic_vector(7 downto 0) := x"16";
end package FW_TIMESTAMP;
