library IEEE;
use IEEE.std_logic_1164.all;
use work.i2c_ctrl.all;

package timing_system_CTRL is

  type timing_system_control_t is record  -- std_logic
    clk_source : std_logic;
    i2c        : i2c_control_t;
  end record timing_system_control_t;
  constant DEFAULT_TIMING_SYSTEM_CTRL : timing_system_control_t := (clk_source => '1',
                                                                    i2c        => DEFAULT_i2c_control);
  
  type timing_system_monitor_t is record  -- std_logic
    sys_locked     : std_logic;
    fmc_clk_locked : std_logic;
    clk_source     : std_logic;
    i2c            : i2c_monitor_t;
  end record timing_system_monitor_t;

end package timing_system_CTRL;
