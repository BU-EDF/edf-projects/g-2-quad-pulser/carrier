library ieee;
use ieee.std_logic_1164.all;

Library UNISIM;
use UNISIM.vcomponents.all;

use work.TIMING_SYSTEM_CTRL.all;

entity timing_system is
  
  port (
    ps_clk          : in  std_logic;
    sys_clk         : in  std_logic;
    clkTTC_in_p     : in  std_logic;
    clkTTC_in_n     : in  std_logic;
    sys_reset_async : in  std_logic;    
    reset_out_async : out std_logic;
    clk_200Mhz      : out std_logic;
    clk_40Mhz       : out std_logic;
    I2C_SDA         : inout std_logic;
    I2C_SCL         : inout std_logic;
    control         : in  timing_system_control_t;
    monitor         : out timing_system_monitor_t
);

end entity timing_system;

architecture behavioral of timing_system is

  component PL_sys_clock is
    port (
      clkPL          : in  std_logic;
      clkTTC         : in  std_logic;
      clk_in_sel     : in  std_logic;
      clk_200Mhz     : out std_logic;
      clk_40Mhz      : out std_logic;
      reset          : in  std_logic;
      locked         : out std_logic);
  end component PL_sys_clock;

  component PL_clk
    port (
      clkPL100         : in     std_logic;
      clkPL40          : out    std_logic;
      reset            : in     std_logic;
      locked           : out    std_logic
      );
  end component;

  component TTC_clk
    port (
      clkTTC_in_p         : in     std_logic;
      clkTTC_in_n         : in     std_logic;
      clkTTC          : out    std_logic;
      reset             : in     std_logic;
      locked            : out    std_logic
      );
  end component;

  component I2C_reg_master is
    generic (
      I2C_QUARTER_PERIOD_CLOCK_COUNT : integer;
      IGNORE_ACK                     : std_logic;
      REG_ADDR_BYTE_COUNT            : integer;
      USE_RESTART_FOR_READ_SEQUENCE  : std_logic);
    port (
      clk_sys     : in    std_logic;
      reset       : in    std_logic;
      I2C_Address : in    std_logic_vector(6 downto 0);
      run         : in    std_logic;
      rw          : in    std_logic;
      reg_addr    : in    std_logic_vector((REG_ADDR_BYTE_COUNT*8) -1 downto 0);
      rd_data     : out   std_logic_vector(31 downto 0);
      wr_data     : in    std_logic_vector(31 downto 0);
      byte_count  : in    std_logic_vector(2 downto 0);
      done        : out   std_logic := '0';
      error       : out   std_logic;
      SDA         : inout std_logic;
      SCLK        : inout std_logic);
  end component I2C_reg_master;
  
  signal locked : std_logic;  
  signal clkTTC_in : std_logic;
  signal clkPL_40Mhz : std_logic;
  signal clkTTC_40Mhz : std_logic;
  signal clk_200Mhz_local : std_logic;
  
begin  -- architecture behavioral

  -----------------------------------------------------------
  -- Master FPGA clock
  -----------------------------------------------------------
  monitor.sys_locked <= locked;
  reset_out_async <= not locked;


  
  PL_clk_1: entity work.PL_clk
    port map (
      clkPL100 => sys_clk,
      clkPL40  => clkPL_40Mhz,
      reset    => sys_reset_async,
      locked   => open);

--  TTC_clk_1: entity work.TTC_clk
--    port map (
--      clkTTC_in_p => clkTTC_in_p,
--      clkTTC_in_n => clkTTC_in_n,
--      clkTTC      => clkTTC_40Mhz,
--      reset       => sys_reset_async,
--      locked      => open);

  clkTTC : IBUFGDS
    port map (
      I  => clkTTC_in_p,
      IB => clkTTC_in_N,
      O  => clkTTC_40Mhz);    

  
--  clkTTC : IBUFDS
--    port map (
--      I  => clkTTC_in_p,
--      IB => clkTTC_in_N,
--      O  => clkTTC_in);
--  
--  clkTTC_BUFG : IBUFG
--    port map (
--      I => clkTTC_in,
--      O => clkTTC_40Mhz);    

  clk_200Mhz <= clk_200Mhz_local;
  PL_sys_clock_1: PL_sys_clock
    port map (
      clkPL          => clkPL_40Mhz,
      clkTTC         => clkTTC_40Mhz,
      clk_in_sel     => control.clk_source,
      clk_200Mhz     => clk_200Mhz_local,
      clk_40Mhz      => clk_40Mhz,
      reset          => sys_reset_async,
      locked         => locked);


  -----------------------------------------------------------
  -- g-2 clock
  -----------------------------------------------------------
  monitor.fmc_clk_locked <= locked; --for now
  monitor.clk_source <= control.clk_source; 


  
  monitor.I2C.reset      <= control.I2C.reset;
  monitor.I2C.rw         <= control.I2C.rw;
  monitor.I2C.addr       <= control.I2C.addr;
  monitor.I2C.reg_addr   <= control.I2C.reg_addr;
  monitor.I2C.wr_data    <= control.I2C.wr_data;
  monitor.I2C.byte_count <= control.I2C.byte_count;
  I2C_reg_master_1: entity work.I2C_reg_master
    generic map (
      I2C_QUARTER_PERIOD_CLOCK_COUNT => 250)
    port map (
      clk_sys     => ps_clk,
      reset       => control.I2C.reset,
      I2C_Address => control.I2C.addr,
      run         => control.I2C.run,
      rw          => control.I2C.rw,
      reg_addr    => control.I2C.reg_addr,
      rd_data     => monitor.I2C.rd_data,
      wr_data     => control.I2C.wr_data,
      byte_count  => control.I2C.byte_count,
      done        => monitor.I2C.done,
      error       => monitor.I2C.error,
      SDA         => I2C_SDA,
      SCLK        => I2C_SCL);
end architecture behavioral;
