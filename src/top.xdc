# ----------------------------------------------------------------------------
# PL User LEDs
# ----------------------------------------------------------------------------
# Bank 35 / JX1
set_property -dict { PACKAGE_PIN U13 IOSTANDARD LVCMOS25 } [get_ports {PL_LEDs[0]}]
# Bank 13 / JX2
set_property -dict { PACKAGE_PIN V6  IOSTANDARD LVCMOS33 } [get_ports {PL_LEDs[1]}]
set_property -dict { PACKAGE_PIN W6  IOSTANDARD LVCMOS33 } [get_ports {PL_LEDs[2]}]
set_property -dict { PACKAGE_PIN Y12 IOSTANDARD LVCMOS33 } [get_ports {PL_LEDs[3]}]
#set_property -dict { PACKAGE_PIN V6  IOSTANDARD LVCMOS25 } [get_ports {PL_LEDs[1]}]
#set_property -dict { PACKAGE_PIN W6  IOSTANDARD LVCMOS25 } [get_ports {PL_LEDs[2]}]
#set_property -dict { PACKAGE_PIN Y12 IOSTANDARD LVCMOS25 } [get_ports {PL_LEDs[3]}]

# ----------------------------------------------------------------------------
# PL Clock Input - Bank 13 / JX3
# ----------------------------------------------------------------------------
set_property -dict { PACKAGE_PIN Y7  IOSTANDARD LVCMOS33 } [get_ports pl_clk]
#set_property -dict { PACKAGE_PIN Y7  IOSTANDARD LVCMOS25 } [get_ports pl_clk]
create_clock -period 10.000 -name pl_clk -add [get_ports pl_clk]

# ----------------------------------------------------------------------------
# Ext clk PMODA
# ----------------------------------------------------------------------------
set_property -dict { PACKAGE_PIN T9  IOSTANDARD LVDS_25 DIFF_TERM FALSE} [get_ports ttc_clk_P]
set_property -dict { PACKAGE_PIN U10 IOSTANDARD LVDS_25 DIFF_TERM FALSE} [get_ports ttc_clk_N]
create_clock -period 25.000 -name ttc_clk_P -add [get_ports ttc_clk_P]


# ----------------------------------------------------------------------------
# Ext TTC spill
# ----------------------------------------------------------------------------
set_property -dict { PACKAGE_PIN W10 IOSTANDARD LVDS_25 DIFF_TERM FALSE} [get_ports ttc_spill_P]
set_property -dict { PACKAGE_PIN W9  IOSTANDARD LVDS_25 DIFF_TERM FALSE} [get_ports ttc_spill_N]

# ----------------------------------------------------------------------------
# spark detect swap
# ----------------------------------------------------------------------------
set_property -dict { PACKAGE_PIN U7  IOSTANDARD LVCMOS33 PULL_DOWN TRUE} [get_ports external_spark]
set_property -dict { PACKAGE_PIN V7  IOSTANDARD LVCMOS33 PULL_DOWN TRUE} [get_ports spark_out]



# ----------------------------------------------------------------------------
# FMC IO
# ----------------------------------------------------------------------------
create_clock -period 10.000 -name fmc_in_clk_pin0 -waveform {0.000 5.000} -add [get_ports fmc_in_clk_P[0]]
create_clock -period 10.000 -name fmc_in_clk_pin1 -waveform {0.000 5.000} -add [get_ports fmc_in_clk_P[1]]
create_clock -period 10.000 -name fmc_in_clk_pin2 -waveform {0.000 5.000} -add [get_ports fmc_in_clk_P[2]]
create_clock -period 10.000 -name fmc_in_clk_pin3 -waveform {0.000 5.000} -add [get_ports fmc_in_clk_P[3]]

# ----------------------------------------------------------------------------
# ADCBoard 0
# ----------------------------------------------------------------------------
set_property -dict { PACKAGE_PIN W15 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_data_N[0]}]
set_property -dict { PACKAGE_PIN V15 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_data_P[OA0]}]
set_property -dict { PACKAGE_PIN U17 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_clk_N[0]}]
set_property -dict { PACKAGE_PIN T16 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_clk_P[0]}]
set_property -dict { PACKAGE_PIN Y19 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_data_N[0]}]
set_property -dict { PACKAGE_PIN Y18 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_data_P[0]}]
set_property -dict { PACKAGE_PIN P20 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_clk_N[0]}]
set_property -dict { PACKAGE_PIN N20 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_clk_P[0]}]

set_property -dict { PACKAGE_PIN R16 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {uart_rx_P[0]}]
set_property -dict { PACKAGE_PIN R17 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {uart_rx_N[0]}]
set_property -dict { PACKAGE_PIN V16 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {uart_tx_P[0]}]
set_property -dict { PACKAGE_PIN W16 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {uart_tx_N[0]}]
set_property -dict { PACKAGE_PIN U14 IOSTANDARD LVCMOS25               } [get_ports {pwr_enable[0]}]

# ----------------------------------------------------------------------------
# ADCBoard 1
# ----------------------------------------------------------------------------
set_property -dict { PACKAGE_PIN R14 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_data_N[1]}]
set_property -dict { PACKAGE_PIN P14 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_data_P[1]}]
set_property -dict { PACKAGE_PIN Y17 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_clk_N[1]}]
set_property -dict { PACKAGE_PIN Y16 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_clk_P[1]}]
set_property -dict { PACKAGE_PIN Y14 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_data_N[1]}]
set_property -dict { PACKAGE_PIN W14 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_data_P[1]}]
set_property -dict { PACKAGE_PIN P19 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_clk_N[1]}]
set_property -dict { PACKAGE_PIN N18 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_clk_P[1]}]

set_property -dict { PACKAGE_PIN N17 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {uart_rx_P[1]}]
set_property -dict { PACKAGE_PIN P18 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {uart_rx_N[1]}]
set_property -dict { PACKAGE_PIN W18 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {uart_tx_P[1]}]
set_property -dict { PACKAGE_PIN W19 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {uart_tx_N[1]}]
set_property -dict { PACKAGE_PIN U19 IOSTANDARD LVCMOS25               } [get_ports {pwr_enable[1]}]

# ----------------------------------------------------------------------------
# ADCBoard 2
# ----------------------------------------------------------------------------
set_property -dict { PACKAGE_PIN L20 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_data_N[2]}]
set_property -dict { PACKAGE_PIN L19 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_data_P[2]}]
set_property -dict { PACKAGE_PIN F17 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_clk_N[2]}]
set_property -dict { PACKAGE_PIN F16 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_clk_P[2]}]
set_property -dict { PACKAGE_PIN G18 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_data_N[2]}]
set_property -dict { PACKAGE_PIN G17 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_data_P[2]}]
set_property -dict { PACKAGE_PIN H18 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_clk_N[2]}]
set_property -dict { PACKAGE_PIN J18 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_clk_P[2]}]

set_property -dict { PACKAGE_PIN H15 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {uart_rx_P[2]}]
set_property -dict { PACKAGE_PIN G15 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {uart_rx_N[2]}]
set_property -dict { PACKAGE_PIN K14 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {uart_tx_P[2]}]
set_property -dict { PACKAGE_PIN J14 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {uart_tx_N[2]}]
set_property -dict { PACKAGE_PIN M20 IOSTANDARD LVCMOS25  } [get_ports {pwr_enable[2]}]

# ----------------------------------------------------------------------------
# ADCBoard 3
# ----------------------------------------------------------------------------
set_property -dict { PACKAGE_PIN D20 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_data_N[3]}]
set_property -dict { PACKAGE_PIN D19 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_data_P[3]}]
set_property -dict { PACKAGE_PIN A20 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_clk_N[3]}]
set_property -dict { PACKAGE_PIN B19 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {fmc_out_clk_P[3]}]
set_property -dict { PACKAGE_PIN E19 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_data_N[3]}]
set_property -dict { PACKAGE_PIN E18 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_data_P[3]}]
set_property -dict { PACKAGE_PIN H17 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_clk_N[3]}]
set_property -dict { PACKAGE_PIN H16 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {fmc_in_clk_P[3]}]

set_property -dict { PACKAGE_PIN L14 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {uart_rx_P[3]}]
set_property -dict { PACKAGE_PIN L15 IOSTANDARD LVDS_25 DIFF_TERM TRUE  } [get_ports {uart_rx_N[3]}]
set_property -dict { PACKAGE_PIN N15 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {uart_tx_P[3]}]
set_property -dict { PACKAGE_PIN N16 IOSTANDARD LVDS_25 DIFF_TERM FALSE } [get_ports {uart_tx_N[3]}]
set_property -dict { PACKAGE_PIN U15 IOSTANDARD LVCMOS25               } [get_ports {pwr_enable[3]}]


#set_property -dict { PACKAGE_PIN U8 IOSTANDARD LVCMOS33 PULLUP TRUE } [get_ports {ttc_SDA}]
#set_property -dict { PACKAGE_PIN V9 IOSTANDARD LVCMOS33 PULLUP TRUE } [get_ports {ttc_SCL}]
#set_property -dict { PACKAGE_PIN U7 IOSTANDARD LVCMOS33} [get_ports {ttc_SDA}]
#set_property -dict { PACKAGE_PIN V7 IOSTANDARD LVCMOS33} [get_ports {ttc_SCL}]
#set_property -dict { PACKAGE_PIN U7 IOSTANDARD LVCMOS33} [get_ports {ttc_SCL}]
#set_property -dict { PACKAGE_PIN V7 IOSTANDARD LVCMOS33} [get_ports {ttc_SDA}]


set_clock_groups -asynchronous \
		 -group [get_clocks  clk_fpga_0      -include_generated_clocks] \
		 -group [get_clocks  pl_clk          -include_generated_clocks] \
 		 -group [get_clocks  ttc_clk_P       -include_generated_clocks] \
		 -group [get_clocks  fmc_in_clk_pin0 -include_generated_clocks] \
	 	 -group [get_clocks  fmc_in_clk_pin1 -include_generated_clocks] \
 		 -group [get_clocks  fmc_in_clk_pin2 -include_generated_clocks] \
		 -group [get_clocks  fmc_in_clk_pin3 -include_generated_clocks]
