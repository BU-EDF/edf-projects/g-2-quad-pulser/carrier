library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

use work.FMC_COMM_CTRL.all;
use work.PMOD_COMM_CTRL.all;
use work.carrier_CTRL.all;
use work.GLOBAL_CTRL.all;
use work.event_builder_CTRL.all;
use work.types.all;
use work.TIMING_SYSTEM_CTRL.all;
use work.hs_message_CTRL.all;


library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity top is
  
  port (
    --Zync crap
    DDR_addr          : inout STD_LOGIC_VECTOR (14 downto 0);
    DDR_ba            : inout STD_LOGIC_VECTOR (2 downto 0);
    DDR_cas_n         : inout STD_LOGIC;
    DDR_ck_n          : inout STD_LOGIC;
    DDR_ck_p          : inout STD_LOGIC;
    DDR_cke           : inout STD_LOGIC;
    DDR_cs_n          : inout STD_LOGIC;
    DDR_dm            : inout STD_LOGIC_VECTOR (3 downto 0);
    DDR_dq            : inout STD_LOGIC_VECTOR (31 downto 0);
    DDR_dqs_n         : inout STD_LOGIC_VECTOR (3 downto 0);
    DDR_dqs_p         : inout STD_LOGIC_VECTOR (3 downto 0);
    DDR_odt           : inout STD_LOGIC;
    DDR_ras_n         : inout STD_LOGIC;
    DDR_reset_n       : inout STD_LOGIC;
    DDR_we_n          : inout STD_LOGIC;
    FIXED_IO_ddr_vrn  : inout STD_LOGIC;
    FIXED_IO_ddr_vrp  : inout STD_LOGIC;
    FIXED_IO_mio      : inout STD_LOGIC_VECTOR (53 downto 0);
    FIXED_IO_ps_clk   : inout STD_LOGIC;
    FIXED_IO_ps_porb  : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;

    --User stuff
    pl_clk            : in std_logic;

    ttc_clk_P         : in std_logic;
    ttc_clk_N         : in std_logic;    
    ttc_spill_P       : in std_logic;
    ttc_spill_N       : in std_logic;
--    ttc_SDA           : inout std_logic;
--    ttc_SCL           : inout std_logic;

    external_spark    : in std_logic;
    spark_out         : out std_logic;
    
    fmc_in_clk_P      : in  std_logic_vector(3 downto 0);
    fmc_in_clk_N      : in  std_logic_vector(3 downto 0);
    fmc_in_data_P     : in  std_logic_vector(3 downto 0);
    fmc_in_data_N     : in  std_logic_vector(3 downto 0);
    fmc_out_data_P    : out std_logic_vector(3 downto 0);
    fmc_out_data_N    : out std_logic_vector(3 downto 0);
    fmc_out_clk_P     : out std_logic_vector(3 downto 0);
    fmc_out_clk_N     : out std_logic_vector(3 downto 0);

    uart_rx_P         : in  std_logic_vector(3 downto 0);
    uart_rx_N         : in  std_logic_vector(3 downto 0);
    uart_tx_P         : out std_logic_vector(3 downto 0);
    uart_tx_N         : out std_logic_vector(3 downto 0);
    
    pwr_enable        : out std_logic_vector(3 downto 0);
    
    PL_LEDs           : out std_logic_vector(3 downto 0)    
    );

end entity top;

architecture behavioral of top is

  component zynq_wrapper is
    port (
      DDR_addr          : inout STD_LOGIC_VECTOR (14 downto 0);
      DDR_ba            : inout STD_LOGIC_VECTOR (2 downto 0);
      DDR_cas_n         : inout STD_LOGIC;
      DDR_ck_n          : inout STD_LOGIC;
      DDR_ck_p          : inout STD_LOGIC;
      DDR_cke           : inout STD_LOGIC;
      DDR_cs_n          : inout STD_LOGIC;
      DDR_dm            : inout STD_LOGIC_VECTOR (3 downto 0);
      DDR_dq            : inout STD_LOGIC_VECTOR (31 downto 0);
      DDR_dqs_n         : inout STD_LOGIC_VECTOR (3 downto 0);
      DDR_dqs_p         : inout STD_LOGIC_VECTOR (3 downto 0);
      DDR_odt           : inout STD_LOGIC;
      DDR_ras_n         : inout STD_LOGIC;
      DDR_reset_n       : inout STD_LOGIC;
      DDR_we_n          : inout STD_LOGIC;
      FIXED_IO_ddr_vrn  : inout STD_LOGIC;
      FIXED_IO_ddr_vrp  : inout STD_LOGIC;
      FIXED_IO_mio      : inout STD_LOGIC_VECTOR (53 downto 0);
      FIXED_IO_ps_clk   : inout STD_LOGIC;
      FIXED_IO_ps_porb  : inout STD_LOGIC;
      FIXED_IO_ps_srstb : inout STD_LOGIC;
      PS_clk            : out   STD_LOGIC;
      PS_reset_n        : out   std_logic_vector(0 downto 0);
      event_data        : in    STD_LOGIC_VECTOR (31 downto 0);
      event_data_ready  : out   STD_LOGIC;
      event_data_fifo_count  : out   STD_LOGIC_VECTOR(31 downto 0);
      event_data_write  : in    STD_LOGIC;                    
      event_last_word   : in    STD_LOGIC;                    
      data_in           : in    STD_LOGIC_VECTOR (31 downto 0);
      data_out          : out   STD_LOGIC_VECTOR (31 downto 0);
      reg_addr          : out   STD_LOGIC_VECTOR (15 downto 0);
      reg_op_done       : in    STD_LOGIC;
      reg_rw            : out   STD_LOGIC_VECTOR (1 downto 0));
  end component zynq_wrapper;

  component timing_system is
    port (
      ps_clk          : in  std_logic;
      sys_clk         : in  std_logic;
      clkTTC_in_p     : in  std_logic;
      clkTTC_in_n     : in  std_logic;
      sys_reset_async : in  std_logic;
      reset_out_async : out std_logic;
      clk_200Mhz      : out std_logic;
      clk_40Mhz       : out std_logic;
      I2C_SDA         : inout std_logic;
      I2C_SCL         : inout std_logic;
      control         : in  timing_system_control_t;
      monitor         : out timing_system_monitor_t);
  end component timing_system;
  
  component register_map is
    generic (
      FIRMWARE_VERSION : std_logic_vector(31 downto 0));
    port (
      clk_sys           : in  std_logic;
      locked_sys        : in  std_logic;
      reset             : in  std_logic;
      data_in           : in  std_logic_vector(31 downto 0);
      WR_address        : in  std_logic_vector(15 downto 0);
      RD_address        : in  std_logic_vector(15 downto 0);
      WR_strb           : in  std_logic;
      RD_strb           : in  std_logic;
      data_out          : out std_logic_vector(31 downto 0);
      data_out_valid    : out std_logic;
      clk_200Mhz        : in  std_logic;
      locked_200Mhz     : in  std_logic;
      carrier_control   : out carrier_control_t;
      carrier_monitor   : in  carrier_control_t;
      fmc_comm_controls : out FMC_Comm_Control_array_t;
      fmc_comm_monitors : in  FMC_Comm_Monitor_array_t;
      global_control    : in  GLOBAL_Control_t;
      global_monitor    : out GLOBAL_Monitor_t;
      pmod_comm_control : out pmod_comm_control_t;
      pmod_comm_monitor : in  pmod_comm_monitor_t;
      eb_control            : out EB_Control_t;
      eb_monitor            : in  EB_Monitor_t;
      timing_system_control : out timing_system_control_t;
      timing_system_monitor : in  timing_system_monitor_t;
      hsm_control           : out hsm_control_t;
      hsm_monitor           : in  hsm_monitor_t
    );
  end component register_map;
  
  component fmc_comm is
    generic (
      CLK_IN_TYPE     : character;
      INVERT_DATA_IN  : boolean;
      INVERT_CLK_IN   : boolean;
      INVERT_DATA_OUT : boolean;
      INVERT_CLK_OUT  : boolean);
    port (
      clk_40Mhz         : in  std_logic;
      clk_200Mhz        : in  std_logic;
      clk_locked        : in  std_logic;
      reset             : in  std_logic;
      bit_clk_P         : in  std_logic;
      bit_clk_N         : in  std_logic;
      data_in_P         : in  std_logic;
      data_in_N         : in  std_logic;
      data_out_P        : out std_logic;
      data_out_N        : out std_logic;
      data_out_clk_P    : out std_logic;
      data_out_clk_N    : out std_logic;
      hs_data_in        : in  std_logic_vector(3 downto 0);
      hs_data_in_valid  : in  std_logic;
      hs_data_empty     : out std_logic;
      hs_data_out       : out std_logic_vector(7 downto 0);
      hs_data_start     : out std_logic;
      hs_data_end       : out std_logic;
      hs_data_out_valid : out std_logic;
      hs_rx_mesg_data       : out std_logic_vector(6 downto 0);
      hs_rx_mesg_data_valid : out std_logic;
      control           : in  FMC_Comm_Control_t;
      monitor           : out FMC_Comm_Monitor_t);
  end component fmc_comm;

  component hs_message_encoder is
    port (
      clk                : in  std_logic;
      new_spill          : in  std_logic;
      new_spill_and_rst  : in  std_logic;
      OOB_trigger        : in  std_logic;
      shutdown           : in  std_logic;
      quad_pulser_trigger: in  std_logic;
      hs_mesg_data       : out std_logic_vector(3 downto 0);
      hs_mesg_data_valid : out std_logic;
      hs_mesg_empty      : in  std_logic;
      control            : in  hsm_TX_control_t;
      monitor            : out hsm_TX_monitor_t);
  end component hs_message_encoder;
  

  component GLOBAL is
    port (
      clk         : in  std_logic;
      rst         : in  std_logic;
      ext_trigger : in  std_logic;
      vac_fail    : in  std_logic;
      permit      : in  std_logic;
      adc_trigger : out std_logic;
      reset_spill  : out std_logic;
      spill_number : out std_logic_vector(31 downto 0);
      control     : in  GLOBAL_Control_t;
      monitor     : out GLOBAL_Monitor_t);
  end component GLOBAL;

  component pmod_comm is
    port (
      clk_200Mhz    : in  std_logic;
      uart_local_rx : in  std_logic_vector(3 downto 0);
      uart_local_tx : out std_logic_vector(3 downto 0);
      control       : in  pmod_comm_control_t;
      monitor       : out pmod_comm_monitor_t);
  end component pmod_comm;

  component event_builder is
    generic (
      ADCBOARD_COUNT       : integer;
      DMA_BLOCK_PWR_OF_TWO : integer);
    port (
      clk_EB           : in  std_logic;
      reset_async      : in  std_logic;
      ADC_data         : in  slv8_array_t(ADCBOARD_COUNT-1 downto 0);
      ADC_data_start   : in  std_logic_vector(ADCBOARD_COUNT-1 downto 0);
      ADC_data_end     : in  std_logic_vector(ADCBOARD_COUNT-1 downto 0);
      ADC_data_valid   : in  std_logic_vector(ADCBOARD_COUNT-1 downto 0);
      new_spill        : in  std_logic;
      spill_number     : in  std_logic_vector(31 downto 0);
      clk_dma          : in  std_logic;
      reset_dma        : in  std_logic;
      event_data       : out STD_LOGIC_VECTOR(31 downto 0);
      event_data_ready : in  STD_LOGIC;
      event_data_open  : in  std_logic_vector(31 downto 0);
      event_data_write : out STD_LOGIC;
      event_last_word  : out STD_LOGIC;
      control          : in  EB_Control_t;
      monitor          : out EB_Monitor_t);
  end component event_builder;

  component hs_messsage_decoder is
    port (
      clk           : in  std_logic;
      data          : in  slv7_array_t(3 downto 0);
      data_valid    : in  std_logic_vector(3 downto 0);
      spark_trigger : out std_logic_vector(0 downto 0);
      monitor       : out HSM_RX_Monitor_t);
  end component hs_messsage_decoder;
  
  component resetter is
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      reset       : out std_logic);
  end component resetter;


  signal clk_200Mhz        : std_logic;
  signal clk_40Mhz         : std_logic;
  signal locked_200Mhz     : std_logic;

--  signal ttc_80Mhzclk : std_logic;
  type boolean_array_t is array (3 downto 0) of boolean;
  constant fmc_invert_data_in  : boolean_array_t := (true,true,true,true);
  constant fmc_invert_clk_in   : boolean_array_t := (true,true,true,true);
  constant fmc_invert_data_out : boolean_array_t := (true,true,true,true);
  constant fmc_invert_clk_out  : boolean_array_t := (true,true,true,true);
  constant fmc_needs_idelayctrl  : boolean_array_t := (false,false,false,true);
  type char_array_t is array (3 downto 0) of character;
  constant fmc_clock_source : char_array_t := ('G','R','G','R');

  signal reset_fmc_sr : std_logic_vector(4 downto 0);
  signal reset_fmc : std_logic;

  signal uart_rx           : std_logic_vector(3 downto 0);
  signal uart_tx           : std_logic_vector(3 downto 0);
  
  signal data_in           :  STD_LOGIC_VECTOR (31 downto 0) := x"00000000";
  signal data_out          :  STD_LOGIC_VECTOR (31 downto 0) := x"00000000";
  signal data_out_valid    :  std_logic := '0';
  signal reg_addr          :  STD_LOGIC_VECTOR (15 downto 0) := x"0000";
  signal reg_op_done       :  STD_LOGIC := '0';
  signal reg_rw            :  STD_LOGIC_VECTOR (1 downto 0);
  signal last_reg_rw            :  STD_LOGIC_VECTOR (1 downto 0);
  signal ps_clk            : std_logic := '0';
  signal rd_strobe : std_logic := '0';
  signal wr_strobe : std_logic := '0';
  
  signal leds : std_logic_vector(3 downto 0) := x"0";

  signal ttc_spill : std_logic;
  signal new_spill : std_logic_vector(4 downto 0);
  signal new_spill_and_rst : std_logic_vector(4 downto 0);
  signal oob_trigger : std_logic_vector(3 downto 0);
  signal spark_trigger : std_logic_vector(0 downto 0);
  signal spill_ID : std_logic_vector(31 downto 0);
 
  signal hs_message_from_ADCBoard       : slv7_array_t(3 downto 0);      --HSM from ADC
  signal hs_message_from_ADCBoard_valid : std_logic_vector(3 downto 0);  --HSM from ADC

  signal hs_message_to_ADCBoard         : slv4_array_t(3 downto 0);      --HSM to ADC
  signal hs_message_to_ADCBoard_valid   : std_logic_vector(3 downto 0);  --HSM to ADC
  signal hs_message_to_ADCBoard_empty   : std_logic_vector(3 downto 0);  --HSM to ADC
  
  signal hs_daq_from_ADCBoard           : slv8_array_t(3 downto 0);      --DAQ stream data from ADC
  signal hs_daq_from_ADCBoard_valid     : std_logic_vector(3 downto 0);  --DAQ stream data from ADC
  signal hs_daq_from_ADCBoard_start     : std_logic_vector(3 downto 0);  --DAQ stream data from ADC
  signal hs_daq_from_ADCBoard_end       : std_logic_vector(3 downto 0);  --DAQ stream data from ADC


  --HS wfm test
  signal ps_reset         : std_logic;
  signal ps_reset_n       : std_logic;
  signal event_data       : STD_LOGIC_VECTOR (31 downto 0);
  signal event_data_ready : STD_LOGIC;
  signal event_data_fifo_count : STD_LOGIC_VECTOR (31 downto 0);
  signal event_data_open  : STD_LOGIC_VECTOR (31 downto 0);
  constant event_data_fifo_size : unsigned(31 downto 0) := x"00008002"; -- it would
                                                                  -- be nice to
                                                                  -- know how
                                                                  -- to get
                                                                  -- this automatically.
  signal event_data_write : STD_LOGIC;
  signal event_last_word  : STD_LOGIC;

  
  -- CTRL records
  signal carrier_control         : carrier_control_t  := DEFAULT_CARRIER_CONTROL;
  signal carrier_monitor         : carrier_monitor_t;
  signal fmc_comm_controls       : FMC_Comm_Control_array_t := (others => DEFAULT_FMC_COMM_CONTROL);
  signal fmc_comm_monitors       : FMC_Comm_Monitor_array_t;
  signal global_timing_control   : GLOBAL_Control_t;
  signal global_timing_monitor   : GLOBAL_Monitor_t;
  signal pmod_comm_control       : pmod_comm_control_t;
  signal pmod_comm_monitor       : pmod_comm_monitor_t;
  signal eb_control              : EB_Control_t;
  signal eb_monitor              : EB_Monitor_t;
  signal timing_system_control   : timing_system_control_t;
  signal timing_system_monitor   : timing_system_monitor_t;
  signal hsm_control             : hsm_control_t;
  signal hsm_monitor             : hsm_monitor_t;


  signal reset_all : std_logic;

  --external spark trigger swapping
  signal external_spark_sr : std_logic_vector(19 downto 0);
  signal external_spark_pulse : std_logic;
  signal spark_trigger_remote : std_logic_vector(0 downto 0);
  signal spark_out_sr : std_logic_vector(31 downto 0);
  
begin  -- architecture behavioral

  -----------------------------------------------------------
  -- Zynq PS wrapper (Here be dragons)
  -----------------------------------------------------------
  zynq_wrapper_1: entity work.zynq_wrapper
    port map (
      DDR_addr          => DDR_addr,
      DDR_ba            => DDR_ba,
      DDR_cas_n         => DDR_cas_n,
      DDR_ck_n          => DDR_ck_n,
      DDR_ck_p          => DDR_ck_p,
      DDR_cke           => DDR_cke,
      DDR_cs_n          => DDR_cs_n,
      DDR_dm            => DDR_dm,
      DDR_dq            => DDR_dq,
      DDR_dqs_n         => DDR_dqs_n,
      DDR_dqs_p         => DDR_dqs_p,
      DDR_odt           => DDR_odt,
      DDR_ras_n         => DDR_ras_n,
      DDR_reset_n       => DDR_reset_n,
      DDR_we_n          => DDR_we_n,
      FIXED_IO_ddr_vrn  => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp  => FIXED_IO_ddr_vrp,
      FIXED_IO_mio      => FIXED_IO_mio,
      FIXED_IO_ps_clk   => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb  => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      PS_clk            => ps_clk,
      PS_reset_n(0)     => PS_reset_n,
      event_data        => event_data,
      event_data_ready  => event_data_ready,
      event_data_fifo_count => event_data_fifo_count,
      event_data_write  => event_data_write,
      event_last_word   => event_last_word,
      data_in           => data_in,
      data_out          => data_out,
      reg_addr          => reg_addr,
      reg_op_done       => reg_op_done,
      reg_rw            => reg_rw);



  -----------------------------------------------------------
  -- FPGA timing
  -----------------------------------------------------------  
  locked_200Mhz <= timing_system_monitor.sys_locked;

  timing_system_1: entity work.timing_system
    port map (
      ps_clk          => ps_clk,
      sys_clk         => pl_clk,
      clkTTC_in_p     => ttc_clk_P,
      clkTTC_in_n     => ttc_clk_N,
      sys_reset_async => ps_reset,
      reset_out_async => open,--reset_async,
      clk_200Mhz      => clk_200Mhz,
      clk_40Mhz       => clk_40Mhz,
      I2C_SDA         => open,--ttc_SDA,
      I2C_SCL         => open,--ttc_SCL,
      control         => timing_system_control,
      monitor         => timing_system_monitor);
  
  -----------------------------------------------------------
  -- Basic carrier board level CTRL
  -----------------------------------------------------------
--  adc_power_enable                <= carrier_control.adc_power_en;
  
  PL_LEDs <= LEDs;
  carrier_monitor.LEDs <= LEDs;
  carrier_monitor.LED_mode <= carrier_control.LED_mode;
  carrier_monitor.LED_GPO  <= carrier_control.LED_GPO;
  LED_proc: process (ps_clk) is
  begin  -- process LED_proc
    if ps_clk'event and ps_clk = '1' then  -- rising clock edge
      case carrier_control.LED_mode is
        when x"0"   => LEDs <= reg_rw & locked_200Mhz & '1';
        when x"1"   => LEDs <= carrier_control.LED_GPO;
        when others => LEDs <= x"0";
      end case;
    end if;
  end process LED_proc;

  -----------------------------------------------------------
  -- FPGA register map
  -----------------------------------------------------------
  --convert PS rd/wr states to PL strobes
  rd_strobe <= '1' when last_reg_rw(0) = '0' and reg_rw(0) = '1' else '0';
  wr_strobe <= '1' when last_reg_rw(1) = '0' and reg_rw(1) = '1' else '0';
  reg_strobes: process (ps_clk) is
  begin  -- process reg_strobes
    if ps_clk'event and ps_clk = '1' then  -- rising clock edge
      -- monitor rw bits to build strobes
      last_reg_rw <= reg_rw;

      -- set op done when read finsihes
      if rd_strobe = '1' then 
        reg_op_done <= '0';
      elsif wr_strobe = '1' then
        reg_op_done <= '1';
      else
        if data_out_valid = '1' then
          reg_op_done <= '1';
        end if;
      end if;
      
    end if;
  end process reg_strobes;

  resetter_1: entity work.resetter
    port map (
      clk         => ps_clk,
      reset_async => ps_reset,
      reset_sync  => carrier_control.reset,
      reset       => reset_all);
  
  register_map_1: entity work.register_map
    generic map (
      FIRMWARE_VERSION => x"00000103")
    port map (
      clk_sys          => ps_clk,
      locked_sys       => '1',
      reset            => reset_all,
      data_in          => data_out,
      WR_address       => reg_addr,
      RD_address       => reg_addr,
      WR_strb          => WR_strobe,
      RD_strb          => RD_strobe,
      data_out         => data_in,
      data_out_valid   => data_out_valid,
      clk_200Mhz       => clk_200Mhz,
      locked_200Mhz    => locked_200Mhz,
      carrier_control  => carrier_control,
      carrier_monitor  => carrier_monitor,
      fmc_comm_controls => fmc_comm_controls,
      fmc_comm_monitors => fmc_comm_monitors,
      global_control    => global_timing_control,
      global_monitor    => global_timing_monitor,
      pmod_comm_control => pmod_comm_control, 
      pmod_comm_monitor => pmod_comm_monitor,
      eb_control        => eb_control,
      eb_monitor        => eb_monitor,
      timing_system_control => timing_system_control,
      timing_system_monitor => timing_system_monitor,
      hsm_control       => hsm_control,
      hsm_monitor       => hsm_monitor
      );
  
  -----------------------------------------------------------
  -- FMC To ADC board communication
  -----------------------------------------------------------
  -----------------------------------------------------------
  fmc_reset: process (clk_200Mhz, timing_system_monitor.fmc_clk_locked) is
  begin  -- process fmc_reset
    if timing_system_monitor.fmc_clk_locked = '0' then -- asynchronous reset (active high)
      reset_fmc_sr <= (others => '1');
      reset_fmc <= '1';
    elsif clk_200Mhz'event and clk_200Mhz = '1' then   -- rising clock edge
      --Sychronize reset release to the 200Mhz clock
      reset_fmc <= reset_fmc_sr(0);      
      reset_fmc_sr <= reset_fmc_sr(4 downto 1) & '0';
    end if;    
  end process fmc_reset;

  
  fmc_connections: for iBoard in 3 downto 0 generate
    fmc_comm_2: entity work.fmc_comm
      generic map (
        CLK_IN_TYPE     => fmc_clock_source(iBoard),        
        INVERT_DATA_IN  => fmc_invert_data_in(iBoard),
        INVERT_CLK_IN   => fmc_invert_clk_in(iBoard),
        INVERT_DATA_OUT => fmc_invert_data_out(iBoard),
        INVERT_CLK_OUT  => fmc_invert_clk_out(iBoard),
        NEEDS_IDELAYCTRL => fmc_needs_idelayctrl(iBoard))
      port map (
        clk_40Mhz         => clk_40Mhz,
        clk_200Mhz        => clk_200Mhz,
        clk_locked        => locked_200Mhz,
        reset             => '0',
        bit_clk_P         => fmc_in_clk_P(iBoard),
        bit_clk_N         => fmc_in_clk_N(iBoard),
        data_in_P         => fmc_in_data_P(iBoard),
        data_in_N         => fmc_in_data_N(iBoard),
        data_out_P        => fmc_out_data_P(iBoard),
        data_out_N        => fmc_out_data_N(iBoard),
        data_out_clk_P    => fmc_out_clk_P(iBoard),
        data_out_clk_N    => fmc_out_clk_N(iBoard),
        hs_data_in        => hs_message_to_ADCBoard(iBoard),        
        hs_data_in_valid  => hs_message_to_ADCBoard_valid(iBoard),
        hs_data_empty     => hs_message_to_ADCBoard_empty(iBoard),            
        hs_data_out       => hs_daq_from_ADCBoard(iBoard),
        hs_data_start     => hs_daq_from_ADCBoard_start(iBoard),
        hs_data_end       => hs_daq_from_ADCBoard_end(iBoard),
        hs_data_out_valid => hs_daq_from_ADCBoard_valid(iBoard),
        hs_rx_mesg_data       => hs_message_from_ADCBoard(iBoard),
        hs_rx_mesg_data_valid => hs_message_from_ADCBoard_valid(iBoard),
        control           => FMC_comm_controls(iBoard),
        monitor           => FMC_comm_monitors(iBoard));

    uart_rx_input : IBUFDS
      port map (
        I  => uart_rx_P(iBoard),
        IB => uart_rx_N(iBoard),
        O  => uart_rx(iBoard));

    uart_tx_input : OBUFDS
      port map (
        I  => uart_tx(iBoard),
        O  => uart_tx_P(iBoard),
        OB => uart_tx_N(iBoard));

    carrier_monitor.adc_power_en(iBoard) <= carrier_control.adc_power_en(iBoard);
    pwr_enable(iBoard) <= carrier_control.adc_power_en(iBoard);
    
  end generate fmc_connections;


  hs_messsage_decoder_1: entity work.hs_messsage_decoder
    port map (
      clk           => clk_200Mhz,
      data          => hs_message_from_ADCBoard,
      data_valid    => hs_message_from_ADCBoard_valid,
      spark_trigger => spark_trigger_remote,
      monitor       => hsm_monitor.RX);

  spark_out <= spark_out_sr(0);
  generate_long_spark_out: process (clk_200Mhz) is
  begin  -- process generate_long_spark_out
    if locked_200Mhz = '0' then
      spark_out_sr <= x"00000000";
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      if spark_trigger_remote(0) = '1' then
        spark_out_sr <= x"FFFFFFFF";
      else
        spark_out_sr <= "0" & spark_out_sr(31 downto 1);
      end if;
    end if;
  end process generate_long_spark_out;
  counter_1: entity work.counter
    generic map (
      roll_over   => '0',
      DATA_WIDTH  => 32)
    port map (
      clk         => clk_200Mhz,
      reset_async => '0',
      reset_sync  => '0',
      enable      => '1',
      event       => spark_trigger_remote(0),
      count       => carrier_monitor.external_spark_sent_count,
      at_max      => open);   
  
  
  generate_global_spark_trigger: process (clk_200Mhz) is
  begin  -- process generate_global_spark_trigger
    if locked_200Mhz = '0' then
      external_spark_pulse <= '0';
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      external_spark_sr <= external_spark_sr(18 downto 0) & external_spark;
      external_spark_pulse <= '0';
      if external_spark_sr(19 downto 0) = x"0FFFF" then
        external_spark_pulse <= '1';
      end if;
    end if;
    spark_trigger(0) <= spark_trigger_remote(0) or external_spark_pulse;
  end process generate_global_spark_trigger;
  counter_2: entity work.counter
    generic map (
      roll_over   => '0',
      DATA_WIDTH  => 32)
    port map (
      clk         => clk_200Mhz,
      reset_async => '0',
      reset_sync  => '0',
      enable      => '1',
      event       => external_spark_pulse,
      count       => carrier_monitor.external_spark_recv_count,
      at_max      => open);   

  message_encoders: for iBoard in 0 to 3 generate
    

--  hs_message_to_ADCBoard(3 downto 1)       <= (others => hs_message_to_ADCBoard(0));
--  hs_message_to_ADCBoard_valid(3 downto 1) <= (others => hs_message_to_ADCBoard_valid(0));
--  hs_message_to_ADCBoard_empty(3 downto 1) <= (others => hs_message_to_ADCBoard_empty(0));  
    hs_message_encoder_1: entity work.hs_message_encoder
      port map (
        clk                => clk_200Mhz,
        new_spill          => new_spill(iBoard+1),
        new_spill_and_rst  => new_spill_and_rst(iBoard+1),
        OOB_trigger        => oob_trigger(iBoard),
        shutdown           => spark_trigger(0),
        quad_pulser_trigger => '0',
        hs_mesg_data       => hs_message_to_ADCBoard(iBoard),
        hs_mesg_data_valid => hs_message_to_ADCBoard_valid(iBoard),
        hs_mesg_empty      => and_reduce(hs_message_to_ADCBoard_empty),
        control            => hsm_control.TX(iBoard),
        monitor            => hsm_monitor.TX(iBoard));
  end generate message_encoders;
  
  
  pmod_comm_1: entity work.pmod_comm
    port map (
      clk_200Mhz    => clk_200Mhz,
      uart_local_rx => uart_rx,
      uart_local_tx => uart_tx,
      control       => pmod_comm_control,
      monitor       => pmod_comm_monitor);
  

  
  ttcSpillBUF : IBUFDS
    port map (
      I  => TTC_spill_P,
      IB => TTC_spill_N,
      O  => TTC_spill);

--  ttc_SDA <= 'Z';
--  ttc_SCL <= 'Z';
  
  GLOBAL_1: entity work.GLOBAL
    port map (
      clk          => clk_200Mhz,
      rst          => '0',
      ext_trigger  => TTC_spill,
      vac_fail     => '0',
      permit       => '0',
      adc_trigger  => new_spill,
      reset_spill  => new_spill_and_rst,
      spill_number => spill_ID,
      oob_trigger(0)  => open,
      oob_trigger(4 downto 1)  => oob_trigger,
      control      => global_timing_control,
      monitor      => global_timing_monitor);

  ps_reset <= not ps_reset_n;

  fifo_count_proc: process (ps_clk) is
  begin  -- process fifo_count_proc
    if ps_clk'event and ps_clk = '1' then  -- rising clock edge
      event_data_open <= std_logic_vector(event_data_fifo_size -
                                          unsigned(event_data_fifo_count));
    end if;
  end process fifo_count_proc;
  event_builder_1: entity work.event_builder
    generic map (
      ADCBOARD_COUNT       => 4,
      DMA_BLOCK_PWR_OF_TWO => 8)
    port map (
      clk_EB           => clk_200Mhz,
      reset_async      => ps_reset,
      ADC_data         => hs_daq_from_ADCBoard,
      ADC_data_start   => hs_daq_from_ADCBoard_start,
      ADC_data_end     => hs_daq_from_ADCBoard_end,
      ADC_data_valid   => hs_daq_from_ADCBoard_valid,
      new_spill        => new_spill(0),
      spill_number     => spill_ID,
      clk_dma          => ps_clk,
      reset_dma        => ps_reset,
      event_data       => event_data,
      event_data_ready => event_data_ready,
      event_data_open  => event_data_open,
      event_data_write => event_data_write,
      event_last_word  => event_last_word,
      control          => eb_control,
      monitor          => eb_monitor);

  
end architecture behavioral;
