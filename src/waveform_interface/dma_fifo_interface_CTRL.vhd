library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

package DMA_FIFO_Interface_CTRL is

  type DMA_FIFO_Control_t is record
    test : std_logic;
  end record DMA_FIFO_Control_t;
  constant DEFAULT_DMA_FIFO_Control : DMA_FIFO_Control_t := (test => '0');

  type DMA_FIFO_Monitor_t is record
    block_rate   : std_logic_vector(31 downto  0);
    data_rate    : std_logic_vector(31 downto  0);
  end record DMA_FIFO_Monitor_t;
  
end package DMA_FIFO_Interface_CTRL;
