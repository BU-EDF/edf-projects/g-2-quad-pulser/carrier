library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

use work.TEST_CTRL.all;
use work.DMA_FIFO_Interface_CTRL.all;

entity interface_test is
  
  port (
    ps_clk           : in std_logic;
    reset_async      : in std_logic;
    event_data       : out STD_LOGIC_VECTOR ( 31 downto 0 );
    event_data_ready : in  STD_LOGIC;
    event_data_write : out STD_LOGIC;
    event_last_word  : out STD_LOGIC;    
    control          : in WFM_INT_Control_t;
    monitor          : out WFM_INT_Monitor_t
);

end entity interface_test;

architecture behavioral of interface_test is

  component dma_fifo_interface is
    port (
      ps_clk              : in  std_logic;
      reset_async         : in  std_logic;
      event_data          : out STD_LOGIC_VECTOR(31 downto 0);
      event_data_ready    : in  STD_LOGIC;
      event_data_write    : out STD_LOGIC;
      event_last_word     : out STD_LOGIC;
      block_start_ready   : out std_logic;
      block_size          : in  std_logic_vector(15 downto 0);
      block_start         : in  std_logic;
      block_sending       : out std_logic;
      block_write_ready   : out std_logic;
      block_data_in       : in  std_logic_vector(31 downto 0);
      block_data_in_valid : in  std_logic;
      control             : in  DMA_FIFO_Control_t;
      monitor             : out DMA_FIFO_Monitor_t);
  end component dma_fifo_interface;

-------------------------------------------------------------------------------
  --state machine process
  -------------------------------------------------------------------------------
  type SM_t is (SM_RESET,
                SM_NEW_PACKET,
                SM_SEND_PACKET,
                SM_END_PACKET,
                SM_IDLE);

  signal state : SM_t;

  -------------------------------------------------------------------------------
  -- packet counter process
  -------------------------------------------------------------------------------
  signal packet_counter     : unsigned(15 downto 0);
  signal packet_counter_end : unsigned(15 downto 0);
  signal data_word          : unsigned(15 downto 0);
  
  -------------------------------------------------------------------------------
  -- packet counter process
  -------------------------------------------------------------------------------
  signal data_write_buffer : std_logic;
  signal data_buffer : std_logic_vector(31 downto 0);


  signal block_start_ready   : std_logic;
  signal block_size          : std_logic_vector(15 downto 0);
  signal block_start         : std_logic;
  signal block_sending       : std_logic;
  signal block_write_ready   : std_logic;
  signal block_data_in       : std_logic_vector(31 downto 0);
  signal block_data_in_valid : std_logic;

  
  signal DMA_FIFO_control : DMA_FIFO_Control_t;
  signal DMA_FIFO_monitor : DMA_FIFO_Monitor_t;

  signal enable_counter : unsigned(31 downto 0);
begin  -- architecture behavioral



  dma_fifo_interface_1: entity work.dma_fifo_interface
    port map (
      ps_clk              => ps_clk,
      reset_async         => reset_async,
      event_data          => event_data,
      event_data_ready    => event_data_ready,
      event_data_write    => event_data_write,
      event_last_word     => event_last_word,
      block_start_ready   => block_start_ready,
      block_size          => block_size,
      block_start         => block_start,
      block_sending       => block_sending,
      block_write_ready   => block_write_ready,
      block_data_in       => block_data_in,
      block_data_in_valid => block_data_in_valid,
      control             => DMA_FIFO_control,
      monitor             => DMA_FIFO_monitor);

  monitor.streaming_enable <= control.streaming_enable;
  monitor.packet_counter_end <= control.packet_counter_end;
  monitor.stream_period <= control.stream_period;
  monitor.DMA_FIFO <= DMA_FIFO_monitor;
  
  enable_counter_proc: process (ps_clk) is
  begin  -- process enable_counter
    if ps_clk'event and ps_clk = '1' then  -- rising clock edge
      enable_counter <= enable_counter + 1;
      if state = SM_NEW_PACKET then
        enable_counter <= x"00000000";
      end if;
    end if;
  end process enable_counter_proc;

  monitor.axi_ready <= block_start_ready;
  state_machine: process (ps_clk,reset_async) is
  begin  -- process state_machine
    if reset_async = '1' then
      state <= SM_RESET;
    elsif ps_clk'event and ps_clk = '1' then  -- rising clock edge
      case state is
        when SM_RESET =>
          state <= SM_IDLE;
        when SM_IDLE =>
          if block_start_ready = '1' and
            ((control.streaming_enable = '1' and enable_counter > unsigned(control.stream_period)) or
             control.streaming_oneshot = '1') then
            state <= SM_NEW_PACKET;
          end if;
        when SM_NEW_PACKET =>
          state <= SM_SEND_PACKET;
        when SM_SEND_PACKET =>
          if packet_counter = packet_counter_end then
            state <= SM_END_PACKET;
          end if;
        when SM_END_PACKET =>
          state <= SM_IDLE;
        when others =>
          state <= SM_RESET;
      end case;
    end if;
  end process state_machine;

  --Generate counter for 
  packet_counter_proc: process (ps_clk) is
  begin  -- process packet_counter_proc
    if ps_clk'event and ps_clk = '1' then  -- rising clock edge
      case state is
        when SM_RESET       =>
          packet_counter <= x"0000";
          data_word <= x"1234";
        when SM_NEW_PACKET  =>
          packet_counter <= x"0001";
          packet_counter_end <= control.packet_counter_end;
        when SM_IDLE | SM_SEND_PACKET | SM_END_PACKET =>
          if packet_counter /= x"0000" then
            if block_write_ready = '1' then              
              packet_counter <= packet_counter + 1;
              data_word <= data_word + 1;
            end if;
          end if;
        when others => null;
      end case;
    end if;
  end process packet_counter_proc;
  
  
  packet_gen: process (ps_clk,reset_async) is
  begin  -- process ps_clk
    if reset_async = '1' then
      --Zero and writes or pending writes
      block_data_in_valid <= '0';
      data_write_buffer <= '0';
    elsif ps_clk'event and ps_clk = '1' then  -- rising clock edge
      block_data_in       <= data_buffer;
      block_data_in_valid <= data_write_buffer;
      block_start <= '0';
      
      data_write_buffer <= '0';
      case state is
        when SM_NEW_PACKET  =>
          block_size  <= std_logic_vector(control.packet_counter_end);
          block_start <= '1';
          
        when SM_SEND_PACKET =>
          if block_write_ready = '1' then
            --Delayed data by one word so end packet can update last word
            data_buffer(31 downto 16) <= std_logic_vector(data_word);
            data_buffer(15 downto  0) <= std_logic_vector(data_word);
            data_write_buffer <= '1';            
          end if;
        when others => null;
      end case;
    end if;
  end process packet_gen;

end architecture behavioral;
