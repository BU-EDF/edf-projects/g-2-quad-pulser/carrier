library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use work.DMA_FIFO_Interface_CTRL.all;

package TEST_CTRL is

  type WFM_INT_Control_t is record
    streaming_enable   : std_logic;
    streaming_oneshot  : std_logic;
    packet_counter_end : unsigned(15 downto 0);
    stream_period      : std_logic_vector(31 downto 0);
  end record WFM_INT_Control_t;
  constant DEFAULT_WFM_INT_Control : WFM_INT_Control_t := (streaming_enable => '0',
                                                           streaming_oneshot => '0',
                                                           packet_counter_end => x"001f",
                                                           stream_period => x"0fffffff");

  type WFM_INT_Monitor_t is record
    streaming_enable   : std_logic;
    packet_counter_end : unsigned(15 downto 0);
    axi_ready          : std_logic;
    stream_period      : std_logic_vector(31 downto 0);
    DMA_FIFO           : DMA_FIFO_Monitor_t;
  end record WFM_INT_Monitor_t;
  
end package TEST_CTRL;
