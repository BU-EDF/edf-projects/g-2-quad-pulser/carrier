library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;


use work.DMA_FIFO_Interface_CTRL.all;

entity dma_fifo_interface is
  
  port (
    ps_clk              : in std_logic; 
    reset_async         : in std_logic;
    --Interface to AXI system (ps_clk Domain)
    event_data          : out STD_LOGIC_VECTOR(31 downto  0);
    event_data_ready    : in  STD_LOGIC;
    event_data_write    : out STD_LOGIC;
    event_last_word     : out STD_LOGIC;
    --Interface to local logic (ps_clk domain)
    block_start_ready   : out std_logic;
    block_size          : in  std_logic_vector(15 downto  0);
    block_start         : in  std_logic; -- start of packet
                                         -- (only works if block_send_ready is 1)
    block_sending       : out std_logic;
    block_write_ready   : out std_logic; -- data can be sent
    block_data_in       : in  std_logic_vector(31 downto  0);
    block_data_in_valid : in  std_logic;
    control             : in  DMA_FIFO_Control_t;
    monitor             : out DMA_FIFO_Monitor_t
);

end entity dma_fifo_interface;

architecture behavioral of dma_fifo_interface is
  component timed_counter is
    generic (
      timer_count : std_logic_vector;
      DATA_WIDTH  : integer);
    port (
      clk          : in  std_logic;
      reset_async  : in  std_logic;
      reset_sync   : in  std_logic;
      enable       : in  std_logic;
      event        : in  std_logic;
      update_pulse : out std_logic;
      timed_count  : out std_logic_vector(DATA_WIDTH-1 downto 0));
  end component timed_counter;

  -------------------------------------------------------------------------------
  --state machine process
  -------------------------------------------------------------------------------
  type SM_t is (SM_RESET,
                SM_START,
                SM_SEND,
                SM_END,
                SM_IDLE);

  signal state : SM_t;

  -------------------------------------------------------------------------------
  -- word counter process
  -------------------------------------------------------------------------------
  signal word_counter     : unsigned(15 downto 0);
  signal word_counter_end : unsigned(15 downto 0);
  
  -------------------------------------------------------------------------------
  -- word counter process
  -------------------------------------------------------------------------------
  signal data_write_buffer : std_logic;
  signal data_buffer       : std_logic_vector(31 downto 0);
  signal data_checksum     : unsigned(31 downto  0);
  signal data_last_word    : std_logic;
  
  -------------------------------------------------------------------------------
  -- monitoring
  -------------------------------------------------------------------------------
  signal block_start_pulse : std_logic;
  
begin  -- architecture behavioral

  

  -- block_write_ready output
  block_write_ready_comb: process (event_data_ready,state) is
  begin  -- process block_send_ready_comb
    case state is
      --We are not sending a block, so we can start sending a block if
      --the axi fifo can accept data
      when SM_START | SM_SEND => block_write_ready <= event_data_ready;
      -- All other states you can't write in
      when others             => block_write_ready <= '0';                       
    end case;
  end process block_write_ready_comb;

  -- block_sending output
  block_sending_comb: process (state,block_start) is
  begin  -- process block_sending_comb
    case state is
      when SM_IDLE                     => block_sending <= block_start;
      when SM_START | SM_SEND | SM_END => block_sending <= '1';
      when others                      => block_sending <= '0';
    end case;
  end process block_sending_comb;

  -- block_start_ready
  block_start_ready_comb: process (state,event_data_ready,block_start) is
  begin  -- process block_start_ready_comb
    case state is
      when SM_IDLE => block_start_ready <= event_data_ready and (not block_start);
      when others  => block_start_ready  <= '0';
    end case;
  end process block_start_ready_comb;

  --State machine for sending control
  state_machine: process (ps_clk,reset_async) is
  begin  -- process state_machine
    if reset_async = '1' then
      state <= SM_RESET;

    elsif ps_clk'event and ps_clk = '1' then  -- rising clock edge

      case state is
        when SM_RESET =>
          state <= SM_IDLE;
        when SM_IDLE =>
          if event_data_ready = '1' then
            --We can write to the AXI DMA fifo
            if block_start = '1' then
              --latch the block size
              word_counter_end <= unsigned(block_size);
              --move to processing the data
              state <= SM_START;              
            end if;
          end if;
        when SM_START =>
          state <= SM_SEND;
        when SM_SEND =>
          if word_counter = word_counter_end then
            state <= SM_END;
          end if;
        when SM_END =>
          state <= SM_IDLE;
        when others =>
          state <= SM_RESET;
      end case;
    end if;
  end process state_machine;

  --Generate counter for 
  word_counter_proc: process (ps_clk) is
  begin  -- process word_counter_proc
    if ps_clk'event and ps_clk = '1' then  -- rising clock edge
      case state is
        when SM_IDLE             =>
          word_counter <= x"0000";
        when SM_START | SM_SEND  =>
          if data_write_buffer = '1' then
            word_counter <= word_counter + 1;
          end if;          
        when others => null;
      end case;
    end if;
  end process word_counter_proc;
  
  
  word_gen: process (ps_clk,reset_async) is
  begin  -- process ps_clk
    if reset_async = '1' then
      --Zero last word
      event_last_word   <= '0';
      data_last_word    <= '0';
      
      --Zero and writes or pending writes
      event_data_write  <= '0';
      data_write_buffer <= '0';
      
    elsif ps_clk'event and ps_clk = '1' then  -- rising clock edge
      --write buffered data
      event_data       <= data_buffer;
      event_data_write <= data_write_buffer;
      event_last_word  <= data_last_word;
      
      --zero write bit and last word bit
      data_last_word    <= '0';
      data_write_buffer <= '0';
      
      case state is
        when SM_START =>
          --being able to start a block means we can just write
          data_buffer <= x"0000" & std_logic_vector(word_counter_end);
          data_write_buffer <= '1';
          --checksum
          data_checksum <= x"00000000";
        when SM_SEND =>
          --Delayed data by one word so end word can update last word
          if event_data_ready = '1' and block_data_in_valid = '1' then
            data_buffer <= block_data_in;
            data_write_buffer <= '1';
            --checksum
            data_checksum <= data_checksum + unsigned(block_data_in);
          end if;
        when SM_END =>
          data_buffer <= std_logic_vector(0 - data_checksum);
          data_write_buffer <= '1';
          data_last_word <= '1';
        when others => null;
      end case;
    end if;
  end process word_gen;

  block_start_pulse <= '1' when state = SM_START else '0';
  block_rate: entity work.timed_counter
    generic map (
      timer_count => x"05F5E100")
    port map (
      clk          => ps_clk,
      reset_async  => reset_async,
      reset_sync   => '0',
      enable       => '1',
      event        => block_start_pulse,
      update_pulse => open,
      timed_count  => monitor.block_rate);

  --
  word_rate: entity work.timed_counter
    generic map (
      timer_count => x"05F5E100",
      DATA_WIDTH  => 30)
    
    port map (
      clk          => ps_clk,
      reset_async  => reset_async,
      reset_sync   => '0',
      enable       => '1',
      event        => data_write_buffer,
      update_pulse => open,
      timed_count(29 downto  0) => monitor.data_rate(31 downto 2)
      );
  --Each word is 4 bytes, so we bitshift by two to multiply by four
  monitor.data_rate(1 downto 0) <= "00";
  
end architecture behavioral;
