---------------------------------------------------------------------------------
-- fmc data io
---------------------------------------------------------------------------------
-- Notes
-- clk_200Mhz and the bit_clk must be fundamentally derived from the same
-- source. 
---------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.FMC_COMM_CTRL.all;


library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity fmc_io is
  generic (
    CLK_IN_TYPE     : character := 'R';
    INVERT_DATA_IN  : boolean   := false;
    INVERT_CLK_IN   : boolean   := false;
    INVERT_DATA_OUT : boolean   := false;
    INVERT_CLK_OUT  : boolean   := false;
    NEEDS_IDELAYCTRL : boolean  := false);
  port (
--    clk_40Mhz  : in std_logic; --data clock
    clk_200Mhz : in std_logic; --bit DDR clock
    clk_locked : in std_logic;
    reset      : in std_logic;
    
    --DDR links
    bit_clk_P           : in  std_logic;
    bit_clk_N           : in  std_logic;
    data_in_P           : in  std_logic;
    data_in_N           : in  std_logic;     
    data_out_P          : out std_logic;
    data_out_N          : out std_logic;
    data_out_clk_P      : out std_logic;
    data_out_clk_N      : out std_logic;

    rx_data_8b          : out std_logic_vector(8 downto 0);
    rx_data_8b_valid    : out std_logic;
    rx_locked           : out std_logic;
    rx_ce_20Mhz         : out std_logic;
    
    tx_data_8b          : in  std_logic_vector(8 downto 0);
    tx_ce_40Mhz         : out std_logic;
    
    control             : in  FMC_IO_Control_t;
    monitor             : out FMC_IO_Monitor_t
    );

end entity fmc_io;

architecture behavioral of fmc_io is

  component clock_monitor is
    generic (
      SYS_CLK_MHZ     : integer;
      WATCHED_CLK_MHZ : integer;
      GOOD_COUNT      : integer);
    port (
      sys_clk        : in  std_logic;
      sys_reset      : in  std_logic;
      watch_clk      : in  std_logic;
      locked         : out std_logic);
  end component clock_monitor;
  
  component resetter is
    generic (
      PIPELINE_CLOCK_TICKS : integer;
      FULL_ASYNC_RESET     : std_logic);
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      reset       : out std_logic);
  end component resetter;

  component pacd is
    port (
      iPulseA : IN  std_logic;
      iClkA   : IN  std_logic;
      iRSTAn  : IN  std_logic;
      iClkB   : IN  std_logic;
      iRSTBn  : IN  std_logic;
      oPulseB : OUT std_logic);
  end component pacd;

  component register_map_CDC is
    generic (
      WIDTH : integer);
    port (
      clkA       : in  std_logic;
      clkB       : in  std_logic;
      inA        : in  std_logic_vector(WIDTH-1 downto 0);
      inA_valid  : in  std_logic;
      outB       : out std_logic_vector(WIDTH-1 downto 0);
      outB_valid : out std_logic);
  end component register_map_CDC;
    
  component lockon_10b is
    generic (
      LOCKON_SYMBOL_P     : std_logic_vector(9 downto 0);
      LOCKON_SYMBOL_N     : std_logic_vector(9 downto 0);
      COUNT_START_SEARCH  : integer;
      COUNT_SEARCH_FAILED : integer);
    port (
      clk                  : in  std_logic;
      clk_en               : in  std_logic;
      reset                : in  std_logic;
      data_in              : in  std_logic_vector(9 downto 0);
      symbol_error         : in  std_logic;
      locked               : out std_logic;
      lockon_failed_strobe : out std_logic);
  end component lockon_10b;

  component shift_register_delay is
    generic (
      DEPTH : integer);
    port (
      clk         : in  std_logic;
      clk_en      : in  std_logic := '1';
      reset_sync  : in  std_logic := '0';
      reset_async : in  std_logic := '0';
      pulse       : out std_logic);
  end component shift_register_delay;
  
  component decode_8b10b_top is
    generic (
      C_DECODE_TYPE  : INTEGER;
      C_HAS_BPORTS   : INTEGER;
      C_HAS_CE       : INTEGER;
      C_HAS_CODE_ERR : INTEGER;
      C_HAS_DISP_ERR : INTEGER;
      C_HAS_DISP_IN  : INTEGER;
      C_HAS_ND       : INTEGER;
      C_HAS_RUN_DISP : INTEGER;
      C_HAS_SINIT    : INTEGER;
      C_HAS_SYM_DISP : INTEGER;
      C_SINIT_VAL    : STRING(1 TO 10);
      C_SINIT_VAL_B  : STRING(1 TO 10));
    port (
      CLK        : IN  STD_LOGIC                    := '0';
      DIN        : IN  STD_LOGIC_VECTOR(9 DOWNTO 0) := (OTHERS => '0');
      DOUT       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      KOUT       : OUT STD_LOGIC;
      CE         : IN  STD_LOGIC                    := '0';
      CE_B       : IN  STD_LOGIC                    := '0';
      CLK_B      : IN  STD_LOGIC                    := '0';
      DIN_B      : IN  STD_LOGIC_VECTOR(9 DOWNTO 0) := (OTHERS => '0');
      DISP_IN    : IN  STD_LOGIC                    := '0';
      DISP_IN_B  : IN  STD_LOGIC                    := '0';
      SINIT      : IN  STD_LOGIC                    := '0';
      SINIT_B    : IN  STD_LOGIC                    := '0';
      CODE_ERR   : OUT STD_LOGIC                    := '0';
      CODE_ERR_B : OUT STD_LOGIC                    := '0';
      DISP_ERR   : OUT STD_LOGIC                    := '0';
      DISP_ERR_B : OUT STD_LOGIC                    := '0';
      DOUT_B     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      KOUT_B     : OUT STD_LOGIC;
      ND         : OUT STD_LOGIC                    := '0';
      ND_B       : OUT STD_LOGIC                    := '0';
      RUN_DISP   : OUT STD_LOGIC;
      RUN_DISP_B : OUT STD_LOGIC;
      SYM_DISP   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      SYM_DISP_B : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
  end component decode_8b10b_top;

  component encode_8b10b_top is
    generic (
      C_ENCODE_TYPE       : INTEGER;
      C_FORCE_CODE_DISP   : INTEGER;
      C_FORCE_CODE_DISP_B : INTEGER;
      C_FORCE_CODE_VAL    : STRING;
      C_FORCE_CODE_VAL_B  : STRING;
      C_HAS_BPORTS        : INTEGER;
      C_HAS_CE            : INTEGER;
      C_HAS_DISP_OUT      : INTEGER;
      C_HAS_DISP_IN       : INTEGER;
      C_HAS_FORCE_CODE    : INTEGER;
      C_HAS_KERR          : INTEGER;
      C_HAS_ND            : INTEGER);
    port (
      CLK          : IN  STD_LOGIC                    := '0';
      DIN          : IN  STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
      KIN          : IN  STD_LOGIC                    := '0';
      DOUT         : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
      CE           : IN  STD_LOGIC                    := '0';
      FORCE_CODE   : IN  STD_LOGIC                    := '0';
      FORCE_DISP   : IN  STD_LOGIC                    := '0';
      DISP_IN      : IN  STD_LOGIC                    := '0';
      DISP_OUT     : OUT STD_LOGIC;
      ND           : OUT STD_LOGIC                    := '0';
      KERR         : OUT STD_LOGIC                    := '0';
      CLK_B        : IN  STD_LOGIC                    := '0';
      DIN_B        : IN  STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
      KIN_B        : IN  STD_LOGIC                    := '0';
      DOUT_B       : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
      CE_B         : IN  STD_LOGIC                    := '0';
      FORCE_CODE_B : IN  STD_LOGIC                    := '0';
      FORCE_DISP_B : IN  STD_LOGIC                    := '0';
      DISP_IN_B    : IN  STD_LOGIC                    := '0';
      DISP_OUT_B   : OUT STD_LOGIC;
      ND_B         : OUT STD_LOGIC                    := '0';
      KERR_B       : OUT STD_LOGIC                    := '0');
  end component encode_8b10b_top;

  component pipeline_delay is
    generic (
      WIDTH : integer;
      DELAY : integer);
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      data_in     : in  std_logic_vector(WIDTH-1 downto 0);
      data_out    : out std_logic_vector(WIDTH-1 downto 0));
  end component pipeline_delay;

  component counter is
    generic (
      roll_over   : std_logic;
      end_value   : std_logic_vector(31 downto 0);
      start_value : std_logic_vector(31 downto 0);
      DATA_WIDTH  : integer);
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      enable      : in  std_logic;
      event       : in  std_logic;
      count       : out std_logic_vector(DATA_WIDTH-1 downto 0);
      at_max      : out std_logic);
  end component counter;
  -------------------------------------------------------------------------------  
  --signals
  -------------------------------------------------------------------------------
  -------------------------------------------------------------------------------

  constant SIZE_10B : integer := 10;

  ---------------------------------------
  -- Data in capture
  signal clk_link_in          : std_logic;
  signal clk_link_in_int      : std_logic;
  signal clk_link_in_locked   : std_logic;
  signal data_in              : std_logic;
  signal data_in_delayed      : std_logic;
  signal data_in_Q1           : std_logic;
  signal data_in_Q2           : std_logic;
  signal data_in_Q1_buffer    : std_logic;
  signal data_in_Q2_buffer    : std_logic;
  signal data_in_sr           : std_logic_vector(2*SIZE_10B-1 downto 0);
  signal rx_io_reset          : std_logic;
  signal rx_io_reset_buffer   : std_logic;
  
  ---------------------------------------
  -- Data in alignment
  signal bit_slip_in           : integer range SIZE_10B-1 downto 0;
  signal bit_slip_out          : integer range SIZE_10B-1 downto 0;
  signal bit_slip_incr         : std_logic;
  signal latch_data_in         : std_logic;
  signal latch_data_in_request : std_logic;
  signal latched_data_in       : std_logic_vector(SIZE_10B-1 downto 0);
  signal latched_data_in_valid : std_logic;
  signal data_in_locked        : std_logic;
  signal lockon_failed_strobe  : std_logic;  
  signal shift_phase           : std_logic;
  signal lockon_failed_sr      : std_logic_vector(2*SIZE_10B -1 downto 0);
  
  ---------------------------------------
  -- Data in decoding
  signal data_in_10b          : std_logic_vector(9 downto 0);
  signal data_in_10b_valid    : std_logic;
  signal data_in_symbol_error : std_logic;
  signal data_in_code_error   : std_logic                    := '0';
  signal data_in_disp_error   : std_logic                    := '0';  
  signal data_in_8b           : std_logic_vector(8 downto 0);
  signal clk_en_20Mhz         : std_logic := '0';
  
  

  
  
  ---------------------------------------
  -- Data out encoding
  signal data_out_ce          : std_logic;
  signal data_out_10b         : std_logic_vector(SIZE_10B -1 downto 0);
  signal data_out_10b_valid   : std_logic;
  
  ---------------------------------------
  -- Data out serializing
  signal data_out_10b_sr      : std_logic_vector(SIZE_10B-1 downto 0);
  signal data_out_D1          : std_logic;
  signal data_out_D2          : std_logic;
  signal data_out             : std_logic;
  signal data_out_clk         : std_logic;
  
  --debug
--  attribute mark_debug : string;
--  attribute mark_debug of rx_data_8b            : signal is "true";
  
  attribute ASYNC_REG : string;
  attribute ASYNC_REG of rx_io_reset : signal is "yes";
  
begin  -- architecture behavioral

  monitor.rx_clk_reset  <= control.rx_clk_reset;
  monitor.rx_io_reset   <= control.rx_io_reset; 
  monitor.tx_clk_reset  <= control.tx_clk_reset;
  monitor.tx_io_reset   <= control.tx_io_reset;

  -------------------------------------------------------------------------------
  -- Clock and DDR data capture 
  -------------------------------------------------------------------------------  

  clock_type_global: if CLK_IN_TYPE = 'G' generate
    --clk in
    in_clk_normal: if INVERT_CLK_IN = false generate
      fmc_clk_in : IBUFGDS_DIFF_OUT
        port map (
          I  => bit_clk_p,
          IB => bit_clk_n,
          O  => clk_link_in);    
    end generate in_clk_normal;
    in_clk_invert: if INVERT_CLK_IN = true generate
      fmc_clk_in : IBUFGDS_DIFF_OUT
        port map (
          I  => bit_clk_p,
          IB => bit_clk_n,
          OB => clk_link_in);    
    end generate in_clk_invert;    
  end generate clock_type_global;

  clock_type_regional: if CLK_IN_TYPE = 'R' generate    
    in_clk_normal: if INVERT_CLK_IN = false generate
      fmc_clk_in : IBUFDS_DIFF_OUT
        port map (
          I  => bit_clk_p,
          IB => bit_clk_n,
          O  => clk_link_in_int);
    end generate in_clk_normal;
    in_clk_invert: if INVERT_CLK_IN = true generate
      fmc_clk_in : IBUFDS_DIFF_OUT
        port map (
          I  => bit_clk_p,
          IB => bit_clk_n,
          OB => clk_link_in_int);
    end generate in_clk_invert;
    fmc_clk_in_buf : BUFR
      generic map (
        BUFR_DIVIDE => "1")
      port map (
        O   => clk_link_in,
        CE  => '1',
        CLR => '0',
        I   => clk_link_in_int);
    
  end generate clock_type_regional;

  

  
  --monitor this clock with the clk_200Mhz system clock for locked
  clock_monitor_1: entity work.clock_monitor
    generic map (
      SYS_CLK_MHZ     => 200,
      WATCHED_CLK_MHZ => 100,
      GOOD_COUNT      => 10)
    port map (
      sys_clk        => clk_200Mhz,
      sys_reset      => '0',
      watch_clk      => clk_link_in,
      locked         => clk_link_in_locked);
  
  --Data in
  in_data_normal: if INVERT_DATA_IN = false generate
    fmc_data_in : IBUFDS_DIFF_OUT
      port map (
        I  => data_in_p,
        IB => data_in_n,
        O  => data_in);
  end generate in_data_normal;
  in_data_invert: if INVERT_DATA_IN = true generate
    fmc_data_in : IBUFDS_DIFF_OUT
      port map (
        I  => data_in_p,
        IB => data_in_n,
        OB => data_in);
  end generate in_data_invert;

  
  --DDR capture of data in
  include_idelayctrl: if NEEDS_IDELAYCTRL generate
    fmc_data_idelayctrl : idelayctrl
      port map (
        REFCLK => clk_200Mhz,
        RST    => reset,
        RDY    => monitor.delay_locked);
  end generate include_idelayctrl;

  monitor.delay_inc <= control.delay_inc;  
  fmc_data_idelaye2 : IDELAYE2
    generic map (
      IDELAY_TYPE      => "VARIABLE",
      DELAY_SRC        => "IDATAIN",
      REFCLK_FREQUENCY => 200.0)
    port map (
      C           => clk_200Mhz,
      REGRST      => '0',
      LD          => control.delay_reset,
      CE          => control.delay_ce or shift_phase,--control.shift_phase,
      CINVCTRL    => '0',
      cntvaluein => (others => '0'),
      INC         => control.delay_inc,--control.phase_shift_direction,
      IDATAIN     => data_in,
      datain      => '0',
      ldpipeen    => '0',
      DATAOUT     => data_in_delayed,
      CNTVALUEOUT => monitor.delay_tap_out);--monitor.phase_shift);

  fmc_data_ddr : iddr
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE_PIPELINED")
    port map (
      Q1 => data_in_Q1,
      Q2 => data_in_Q2,
      C  => clk_link_in,
      CE => '1',
      D  => data_in_delayed,--data_in,
      R  => rx_io_reset,
      S  => '0');

  -- add DDR data to the shift register
  iddr_sr: process (clk_link_in) is
  begin  -- process iddr_sr
    if clk_link_in'event and clk_link_in = '1' then  -- rising clock edge      
--      data_in_sr <= data_in_sr(data_in_sr'left -2 downto 0) & data_in_Q1 & data_in_Q2;
      data_in_Q1_buffer <= data_in_Q1;
      data_in_Q2_buffer <= data_in_Q2;      
      data_in_sr <= data_in_Q2_buffer & data_in_Q1_buffer & data_in_sr(data_in_sr'left downto 2);
    end if;
  end process iddr_sr;

  -- this process cycles through the possible bit slips
  data_latch_bitslip: process (clk_link_in, clk_link_in_locked) is
  begin  -- process data_latch_bitslip
    if clk_link_in_locked = '0' then              -- asynchronous reset (active high)
      bit_slip_in <= 0;
    elsif clk_link_in'event and clk_link_in = '1' then  -- rising clock edge
      if bit_slip_incr = '1' then
        if bit_slip_in = 9 then
          bit_slip_in <= 0;
        else
          bit_slip_in <= bit_slip_in + 1;
        end if;
      end if;
    end if;
  end process data_latch_bitslip;


  shift_register_delay_1: entity work.shift_register_delay
    generic map (
      DEPTH => (SIZE_10B/2)-1)
    port map (
      clk         => clk_link_in,
      clk_en      => '1',
      reset_sync  => '0',
      reset_async => '0',
      pulse       => latch_data_in);
  
  --This latches the data from the raw shift register using the current bit
  --slip position  
  data_latch: process (clk_link_in) is
  begin  -- process data_latch
    if clk_link_in'event and clk_link_in = '1' then  -- rising clock edge
      bit_slip_out <= bit_slip_in;
      
      --Mark the data as valid if we got a request and the clk_link_in is locked
      latched_data_in_valid <= '0';      
      if clk_link_in_locked = '1' and latch_data_in = '1' then
        latched_data_in_valid <= '1';
      else
        latched_data_in_valid <= '0';        
      end if;

      --latch the data when requested
      if latch_data_in = '1' then
        latched_data_in <= data_in_sr(data_in_sr'left - bit_slip_out downto data_in_sr'left - (SIZE_10B-1) - bit_slip_out);
      end if;
    end if;
  end process data_latch;




















  -------------------------------------------------------------------------------
  -- CLOCK DOMAIN CROSSING between the DDR clock domain and the local clock domain
  -------------------------------------------------------------------------------

--  --Pass request to latch the parallel data
--  pacd_1: entity work.pacd
--    port map (
--      iPulseA => latch_data_in_request,
--      iClkA   => clk_200Mhz,
--      iRSTAn  => '1',
--      iClkB   => clk_link_in,
--      iRSTBn  => '1',
--      oPulseB => latch_data_in);

  -- tell the DDR clock domain to do a bitslip
  pacd_2: entity work.pacd
    port map (
      iPulseA => lockon_failed_strobe,
      iClkA   => clk_200Mhz,
      iRSTAn  => '1',
      iClkB   => clk_link_in,
      iRSTBn  => '1',
      oPulseB => bit_slip_incr);

  --Pass latched data from the DDR clock domain to the local clock domain.
  register_map_CDC_1: entity work.register_map_CDC
    generic map (
      WIDTH => 10)
    port map (
      clkA       => clk_link_in,
      clkB       => clk_200Mhz,
      inA        => latched_data_in,
      inA_valid  => latched_data_in_valid,
      outB       => data_in_10b,
      outB_valid => data_in_10b_valid);


  pipeline_delay_1: entity work.pipeline_delay
    generic map (
      WIDTH => 1,
      DELAY => 1)
    port map (
      clk         => clk_200Mhz,
      reset_async => '0',
      data_in(0)  => control.rx_io_reset,
      data_out(0) => rx_io_reset_buffer);
  resetter_1: entity work.resetter
    generic map (
      PIPELINE_CLOCK_TICKS => 10,
      FULL_ASYNC_RESET     => '1')
    port map (
      clk         => clk_link_in,
      reset_async => rx_io_reset_buffer,--control.rx_io_reset,
      reset_sync  => '0',
      reset       => rx_io_reset);
















  -------------------------------------------------------------------------------
  -- Local clock domain from here on
  -------------------------------------------------------------------------------

  --Generate a pulse at the parallel data rate to request data from the DDR
  --clock domain.
  -- This only works if the clk_200Mhz and the DDR clock are fundamentally related
--  shift_register_delay_3: entity work.shift_register_delay
--    generic map (
--      DEPTH => (SIZE_10B)-1)
--    port map (
--      clk         => clk_200Mhz,
--      clk_en      => '1',
--      reset_sync  => '0',
--      reset_async => '0',
--      pulse       => latch_data_in_request);
  
  --8b10b lockon and bit-slip search
  rx_locked <= data_in_locked;
  lockon_10b_1: entity work.lockon_10b
    generic map (
      LOCKON_SYMBOL_P     => "1001111100",
      LOCKON_SYMBOL_N     => "0110000011",
      COUNT_START_SEARCH  => 3,
      COUNT_SEARCH_FAILED => 15)
    port map (
      clk                  => clk_200Mhz,
      clk_en               => data_in_10b_valid,
      reset                => '0',
      data_in              => data_in_10b,
      symbol_error         => data_in_symbol_error,
      locked               => data_in_locked,
      lockon_failed_strobe => lockon_failed_strobe);

  --reset the phase of the incomming channel after 20 lockon_failed_strobes
  data_in_phase_shifter: process (clk_200Mhz) is
  begin  -- process data_in_phase_shifter
    if clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      --pulse zeroing
      shift_phase <= '0';
      --shift register
      if lockon_failed_strobe = '1' then
        lockon_failed_sr <= lockon_failed_sr(lockon_failed_sr'left-1 downto 0) & '1';
      end if;
      --check to see if we've had enough lockon errors to request a shift
      if lockon_failed_sr(lockon_failed_sr'left) = '1' then
        shift_phase <= '1';
        lockon_failed_sr <= (others => '0');
      end if;
    end if;
  end process data_in_phase_shifter;
  
  --monitor the data coming in for debugging
  data_in_monitor: process (clk_200Mhz) is
  begin  -- process data_in_monitor
    if clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      if data_in_10b_valid = '1' then
        monitor.rx_10b <= data_in_10b;            
      end if;
    end if;
  end process data_in_monitor;

  
  -- decode 8b10b data     
  data_in_symbol_error <= data_in_code_error or data_in_disp_error;
  decode_8b10b_top_1: entity work.decode_8b10b_top
    generic map (
      C_DECODE_TYPE  => 0,
      C_HAS_CE       => 1,
      C_HAS_CODE_ERR => 1,
      C_HAS_DISP_ERR => 1,
      C_HAS_RUN_DISP => 1)
    port map (
      CLK        => clk_200Mhz,
      DIN        => data_in_10b,
      DOUT       => data_in_8b(7 downto 0),
      KOUT       => data_in_8b(8),
      CE         => data_in_10b_valid,
      CODE_ERR   => data_in_code_error,
      DISP_ERR   => data_in_disp_error);


  counter_1: entity work.counter
    port map (
      clk         => clk_200Mhz,
      reset_async => control.reset_counter_rx_code_error,
      reset_sync  => '0',
      enable      => '1',
      event       => data_in_code_error,
      count       => monitor.counter_rx_code_error,
      at_max      => open);
  counter_2: entity work.counter
    port map (
      clk         => clk_200Mhz,
      reset_async => control.reset_counter_rx_disp_error,
      reset_sync  => '0',
      enable      => '1',
      event       => data_in_disp_error,
      count       => monitor.counter_rx_disp_error,
      at_max      => open);
  
  rx_ce_20Mhz <= clk_en_20Mhz;
  clk_20_toggle_lockon: process (clk_200Mhz) is
  begin  -- process clk_40_toggle_lockon
    if clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      clk_en_20Mhz <= data_in_10b_valid;
    end if;
  end process clk_20_toggle_lockon;

  --Latch the 8b data and send it out
  local_data_in_latch: process (clk_200Mhz) is
  begin  -- process local_data_in_latch
    if clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      rx_data_8b_valid <= '0';
      
      if (clk_en_20Mhz         = '1' and
          data_in_locked       = '1' and
          data_in_symbol_error = '0' )then

        rx_data_8b <= data_in_8b;
        rx_data_8b_valid <= '1';
      end if;
    end if;
  end process local_data_in_latch;












  
  -------------------------------------------------------------------------------
  -- Data out DDR
  -------------------------------------------------------------------------------

  tx_ce_40Mhz <= data_out_ce;
  shift_register_delay_2: entity work.shift_register_delay
    generic map (
      DEPTH => SIZE_10B/2 -1)
    port map (
      clk         => clk_200Mhz,
      clk_en      => '1',
      reset_sync  => '0',
      reset_async => '0',
      pulse       => data_out_ce);
  
  encode_8b10b_top_1: entity work.encode_8b10b_top
    generic map (
      C_ENCODE_TYPE       => 0,
      C_HAS_CE            => 1)
    port map (
      CLK          => clk_200Mhz,
      DIN          => tx_data_8b(7 downto 0),
      KIN          => tx_data_8b(8),
      DOUT         => data_out_10b,
      CE           => data_out_ce);

  tx_serializer: process (clk_200Mhz) is
  begin  -- process tx_serializer
    if clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      --pass the MSB two bits on to the ODDR
      if INVERT_DATA_OUT = false then
        data_out_D1 <= data_out_10b_sr(0);
        data_out_D2 <= data_out_10b_sr(1);
      else
        data_out_D1 <= not data_out_10b_sr(0);
        data_out_D2 <= not data_out_10b_sr(1);        
      end if;

      
      --delay data_out_ce by one clock tick so the encoder can do it's magic
      data_out_10b_valid <= data_out_ce;
      --If fresh data is ready, latch it
      if data_out_10b_valid = '1' then
        data_out_10b_sr <= data_out_10b;
      else
        --shift through old data.
        data_out_10b_sr <= "00" & data_out_10b_sr(data_out_10b_sr'left downto 2);
      end if;
    end if;
  end process tx_serializer;

  tx_ddr : ODDR
    generic map (
      DDR_CLK_EDGE => "SAME_EDGE")
    port map (
      Q  => data_out,
      C  => clk_200Mhz,
      CE => '1',
      D1 => data_out_D1,
      D2 => data_out_D2,
      S  => '0',
      R  => control.tx_io_reset);

  
  out_clk_normal: if INVERT_CLK_OUT = false generate
    tx_clock : ODDR
      generic map (
        DDR_CLK_EDGE => "SAME_EDGE")
      port map (
        Q  => data_out_clk,
        C  => clk_200Mhz,
        CE => '1',
        D1 => '1',
        D2 => '0',
        S  => '0',
        R  => control.tx_clk_reset);
  end generate out_clk_normal;
  out_clk_invert: if INVERT_CLK_OUT = true generate
    tx_clock : ODDR
      generic map (
        DDR_CLK_EDGE => "SAME_EDGE")
      port map (
        Q  => data_out_clk,
        C  => clk_200Mhz,
        CE => '1',
        D1 => '0',
        D2 => '1',
        S  => '0',
        R  => control.tx_clk_reset);
  end generate out_clk_invert;

  
  data_out_buf : OBUFDS
    port map (
      I  => data_out,
      O  => data_out_p,
      OB => data_out_n);
  clk_out_buf : OBUFDS
    port map (
      I  => data_out_clk,
      O  => data_out_clk_p,
      OB => data_out_clk_n);
  




end architecture behavioral;
