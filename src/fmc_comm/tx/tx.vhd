-------------------------------------------------------------------------------
-- Multiplexer of TX UART and TX high-speed(HS) data
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

entity tx is
  
  port (
    reset              : in  std_logic;
    clk_200Mhz         : in  std_logic;
    clk_en_40Mhz       : in  std_logic;
    locked             : in  std_logic;
    tx_data            : out std_logic_vector(8 downto 0);

    --Uart TX
    uart_data          : in  std_logic_vector(6 downto 0);
    uart_data_valid    : in  std_logic;
    uart_empty         : out std_logic;
    uart_sending_pulse : out std_logic;
    
    --High-speed Tx
    hs_data            : in  std_logic_vector(3 downto 0);
    hs_data_valid      : in  std_logic;
    hs_empty           : out std_logic;
    hs_sending_pulse   : out std_logic);

end entity tx;

architecture behavioral of tx is

  constant IDLE_CHAR       : std_logic_vector(8 downto 0) := '1'&x"3C";

  signal clk_en      : std_logic;
  signal hs_tx_ack   : std_logic;
  signal uart_tx_ack : std_logic;
  
  --hs data
  signal hs_phase_counter : unsigned(2 downto 0) := "001";
  signal local_hs_empty : std_logic := '0';
  signal latched_hs_data : std_logic_vector(6 downto 0) := "0000000";
  signal local_hs_data_valid : std_logic := '0';
 
  
  --Uart
  signal local_uart_empty : std_logic := '1';
  signal latched_uart_data : std_logic_vector(6 downto 0) := "0000000";
  signal local_uart_data_valid : std_logic := '0';

--  --debug
--  attribute mark_debug : string;
--  attribute mark_debug of clk_en                : signal is "true";
--  attribute mark_debug of hs_tx_ack             : signal is "true";
--  attribute mark_debug of uart_tx_ack           : signal is "true";
--  attribute mark_debug of hs_phase_counter      : signal is "true";
--  attribute mark_debug of local_hs_empty        : signal is "true";
--  attribute mark_debug of latched_hs_data       : signal is "true";
--  attribute mark_debug of local_hs_data_valid   : signal is "true";
--  attribute mark_debug of local_uart_empty      : signal is "true";
--  attribute mark_debug of latched_uart_data     : signal is "true";
--  attribute mark_debug of local_uart_data_valid : signal is "true";

  
begin  -- architecture behavioral

  comb_helper_signals: process (locked,
                                clk_en_40Mhz,
                                uart_data_valid,
                                hs_data_valid) is
  begin  -- process comb_helper_signals
    clk_en      <= '0';
    hs_tx_ack   <= '0';
    uart_tx_ack <= '0';
    
    if locked = '1' and clk_en_40Mhz = '1' then
      clk_en <= '1';
      if local_hs_empty = '0' then
        hs_tx_ack   <= '1';
      elsif local_uart_empty = '0' then
        uart_tx_ack <= '1';            
      end if;
    end if;    
  end process comb_helper_signals;
  
  tx_machine: process (clk_200Mhz, reset) is
  begin  -- process tx_machine
    if reset = '1' then                 -- asynchronous reset (active high)
      
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      hs_sending_pulse   <= '0';
      uart_sending_pulse <= '0';
      if clk_en = '1' then         
        if local_hs_empty = '0' then
          --Send HS data
          tx_data <= "01" & latched_hs_data;
          hs_sending_pulse <= '1';
        elsif local_uart_empty = '0' then
          --Send UART data
          tx_data <= "00" & latched_uart_data;
          uart_sending_pulse <= '1';
        else
          -- idle tx_data
          tx_data <= IDLE_CHAR;
        end if;
      end if;
    end if;
  end process tx_machine;


  -------------------------------------------------------------------------------
  -- Handle uart tx capture
  -------------------------------------------------------------------------------
  --Quickly block any new data coming in by resetting uart_tx_empty with the
  --tx_data valid signal.  We will update local_empty in the process, but this
  --stops 
  uart_empty <= local_uart_empty and (not uart_data_valid);
  -- Capture data that will be sent out, then send it out on our clk_en pulse
  uart_tx_capture: process (clk_200Mhz) is
  begin  -- process tx_proc
    if clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      if locked = '1' then
        if local_uart_empty = '1' then      
          -- latch of capture of tx and store for 40Mhz clock en
          if uart_data_valid = '1' then
            --latch
            latched_uart_data <= uart_data;
            local_uart_empty <= '0';
          end if;     
        else
          -- Wait until we send the data and then mark the buffer as empty
          if uart_tx_ack = '1' then
            local_uart_empty <= '1';
          end if;
        end if;
      else
        local_uart_empty <= '0';
      end if;
    end if;
  end process uart_tx_capture;


  -------------------------------------------------------------------------------
  -- Handle hs tx
  -------------------------------------------------------------------------------
  --hs_phase_counter give the phase of the 200Mhz hs signal relative to the
  --40Mhz clock
  hs_phase_proc: process (clk_200Mhz) is
  begin  -- process hs_phase_proc
    if clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      if clk_en_40Mhz = '1' then
        hs_phase_counter <= "001";
      elsif hs_phase_counter = "100" then
        hs_phase_counter <= "000";
      else        
        hs_phase_counter <= hs_phase_counter + 1;
      end if;
    end if;
  end process hs_phase_proc;
  --Quickly block any new data coming in by resetting uart_tx_empty with the
  --tx_data valid signal.  We will update local_empty in the process, but this
  --stops 
  hs_empty <= local_hs_empty and (not hs_data_valid);
  -- Capture data that will be sent out, then send it out on our clk_en pulse
  hs_tx_capture: process (clk_200Mhz) is
  begin  -- process tx_proc
    if clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      if locked = '1' then
        if local_hs_empty = '1' then      
          -- latch of capture of tx and store for 40Mhz clock en
          if hs_data_valid = '1' then
            --latch the 3 bits of phase and 4 bits of hs data
            latched_hs_data <= std_logic_vector(hs_phase_counter) & hs_data;
            local_hs_empty <= '0';
          end if;     
        else
          -- Wait until we send the data and then mark the buffer as empty
          if hs_tx_ack = '1'  then
            local_hs_empty <= '1';
          end if;
        end if;
      else
        local_hs_empty <= '0';
      end if;
    end if;
  end process hs_tx_capture;

  
  

  
end architecture behavioral;
