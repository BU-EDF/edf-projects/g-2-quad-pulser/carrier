#open_wave_config {../src/fmc_comm/tx/tb/testbench_top_behav.wcfg}
restart
add_force {/tb_tx/clk_200Mhz} -radix hex {1 0ns} {0 2500ps} -repeat_every 5000ps
add_force {/tb_tx/tb_reset} -radix hex 1
run 20ns
add_force {/tb_tx/tb_reset} -radix hex 0
run 5us
