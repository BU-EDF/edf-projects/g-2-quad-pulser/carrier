library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;


entity tb_tx is
  port (
    clk_200Mhz   : in std_logic;
    tb_reset : in std_logic);
end entity tb_tx;

architecture behavioral of tb_tx is

  component tx is
    port (
      reset              : in  std_logic;
      clk_200Mhz         : in  std_logic;
      clk_en_40Mhz       : in  std_logic;
      locked             : in  std_logic;
      tx_data            : out std_logic_vector(8 downto 0);
      uart_data       : in  std_logic_vector(6 downto 0);
      uart_data_valid : in  std_logic;
      uart_empty      : out std_logic;
      hs_data            : in  std_logic_vector(3 downto 0);
      hs_data_valid      : in  std_logic;
      hs_empty           : out std_logic);
  end component tx;
  
  signal reset              : std_logic;
--  signal clk_200Mhz         : std_logic;
  signal clk_en_40Mhz       : std_logic;
  signal locked             : std_logic;
  signal tx_data            : std_logic_vector(8 downto 0);
  signal uart_data       : std_logic_vector(6 downto 0);
  signal uart_data_valid : std_logic;
  signal uart_empty      : std_logic;
  signal hs_data            : std_logic_vector(3 downto 0);
  signal hs_data_valid      : std_logic;
  signal hs_empty           : std_logic;



  signal clk_en_sr : std_logic_vector(4 downto 0) := "10000";
  signal counter : unsigned(31 downto 0) := x"00000000";
begin  -- architecture behavioral

  input: process (clk_200Mhz,tb_reset) is
  begin  -- process input
    if tb_reset = '1' then
      clk_en_sr <= "10000";
      clk_en_40Mhz <= '0';
      reset <= '1';
      counter <= x"00000000";
      locked <= '0';

      uart_data <= "0000000";
      uart_data_valid <= '0';

      hs_data       <= x"0";
      hs_data_valid <= '0';
      
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      clk_en_40Mhz <= clk_en_sr(0);
      clk_en_sr <= clk_en_sr(0)&clk_en_sr(4 downto 1);        
      
      counter <= counter + 1;

      uart_data_valid <= '0';
      hs_data_valid      <= '0';
      
      if counter > x"0000000a" then
        reset <= '0';
      end if;
      
      if counter > x"00000080" then
        locked <= '1';
      end if;
      
      if (counter > x"00000100" and counter < x"00000200") or
         (counter > x"00000205" and counter < x"00000230") then
        if uart_empty = '1' then
          uart_data_valid <= '1';
          uart_data <= std_logic_vector(counter(6 downto 0));          
        end if;
      end if;

      if counter = x"00000200" then
        hs_data <= x"4";
        hs_data_valid <= '1';
      elsif counter = x"00000210" then
        hs_data <= x"6";
        hs_data_valid <= '1';
      end if;
    end if;
  end process input;


  tx_1: tx
    port map (
      reset              => reset,
      clk_200Mhz         => clk_200Mhz,
      clk_en_40Mhz       => clk_en_40Mhz,
      locked             => locked,
      tx_data            => tx_data,
      uart_data       => uart_data,
      uart_data_valid => uart_data_valid,
      uart_empty      => uart_empty,
      hs_data            => hs_data,
      hs_data_valid      => hs_data_valid,
      hs_empty           => hs_empty);
  
end architecture behavioral;
