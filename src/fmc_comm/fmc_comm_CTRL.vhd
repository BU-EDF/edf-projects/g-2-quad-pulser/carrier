library IEEE;
use IEEE.std_logic_1164.all;

package FMC_COMM_CTRL is

  type FMC_IO_Control_t is record
    rx_clk_reset                : std_logic;
    rx_io_reset                 : std_logic;
    tx_clk_reset                : std_logic;
    tx_io_reset                 : std_logic;
    delay_reset                 : std_logic;
    delay_ce                    : std_logic;
    delay_inc                   : std_logic;
    delay_tap_in                : std_logic_vector(4 downto 0);
    reset_counter_rx_code_error : std_logic;
    reset_counter_rx_disp_error : std_logic;    
  end record FMC_IO_Control_t;
  
  constant DEFAULT_FMC_IO_Control : FMC_IO_Control_t := (rx_clk_reset          => '0',
                                                         rx_io_reset           => '0',
                                                         tx_clk_reset          => '0',
                                                         tx_io_reset           => '0',
                                                         delay_reset           => '0',
                                                         delay_ce              => '0',
                                                         delay_inc             => '0',
                                                         delay_tap_in          => "00000",
                                                         reset_counter_rx_code_error => '0',
                                                         reset_counter_rx_disp_error => '0');
  type FMC_Comm_Control_t is record

    tx_comm_data          : std_logic_vector(6 downto 0);
    tx_comm_data_valid    : std_logic;
    tx_reset_uart_counter : std_logic;

    tx_hs_data            : std_logic_vector(3 downto 0);
    tx_hs_data_valid      : std_logic;
    tx_reset_hs_counter   : std_logic;
                          
    rx_comm_read          : std_logic;
    rx_reset_uart_counter : std_logic;
    rx_reset_hs_mesg_counter   : std_logic;
    rx_reset_hs_counter   : std_logic;
    
    IO                    : FMC_IO_Control_t;
  end record FMC_Comm_Control_t;

  constant DEFAULT_FMC_Comm_Control : FMC_Comm_Control_t := (tx_comm_data          => "0000000",
                                                             tx_comm_data_valid    => '0',
                                                             tx_reset_uart_counter => '1',
                                                             tx_hs_data            => x"0",
                                                             tx_hs_data_valid      => '0',
                                                             tx_reset_hs_counter   => '1',
                                                             rx_comm_read          => '0',
                                                             rx_reset_uart_counter => '0',
                                                             rx_reset_hs_mesg_counter   => '0',
                                                             rx_reset_hs_counter   => '0',
                                                             IO                    => DEFAULT_FMC_IO_Control);


  type FMC_Comm_control_array_t is array (3 downto 0) of FMC_Comm_Control_t;
  
  
  type FMC_IO_Monitor_t is record
    rx_clk_reset          : std_logic;
    rx_io_reset           : std_logic;
    tx_clk_reset          : std_logic;
    tx_io_reset           : std_logic;
    delay_reset           : std_logic;
    delay_inc             : std_logic;    
    delay_locked          : std_logic;
    delay_tap_in          : std_logic_vector(4 downto 0);
    delay_tap_out         : std_logic_vector(4 downto 0);
    rx_10b                : std_logic_vector(9 downto 0);
    counter_rx_code_error : std_logic_vector(31 downto 0);
    counter_rx_disp_error : std_logic_vector(31 downto 0);
  end record FMC_IO_Monitor_t;
  
  type FMC_Comm_Monitor_t is record
    tx_comm_empty       : std_logic;
    tx_hs_empty         : std_logic;
    tx_uart_counter     : std_logic_vector(15 downto 0);
    tx_hs_counter       : std_logic_vector(15 downto 0);
    tx_hs_data_rate     : std_logic_vector(31 downto 0);
    tx_comm_rate        : std_logic_vector(31 downto 0);
    
    rx_locked           : std_logic;
    rx_comm             : std_logic_vector(6 downto 0);
    rx_comm_valid       : std_logic;
    rx_uart_counter     : std_logic_vector(15 downto 0);
    rx_hs_mesg_counter  : std_logic_vector(15 downto 0);
    rx_hs_counter       : std_logic_vector(15 downto 0);
    rx_hs_data_rate     : std_logic_vector(31 downto 0);
    rx_comm_rate        : std_logic_vector(31 downto 0);

    reg_rx_mode         : std_logic_vector(7  downto  0);
    reg_rx_addr         : std_logic_vector(15 downto  0);
    reg_rx_data         : std_logic_vector(31 downto  0);
    reg_rx_valid        : std_logic;
    
    IO                  : FMC_IO_Monitor_t;
    
  end record FMC_Comm_Monitor_t;

  type FMC_Comm_Monitor_array_t is array (3 downto 0) of FMC_Comm_Monitor_t;
  
end package FMC_COMM_CTRL;
