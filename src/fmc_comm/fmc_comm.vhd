library IEEE;
use IEEE.std_logic_1164.all;
use work.FMC_COMM_CTRL.all;


library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity fmc_comm is
  generic (
    CLK_IN_TYPE     : character  := 'R';
    INVERT_DATA_IN  : boolean    := false;
    INVERT_CLK_IN   : boolean    := false;
    INVERT_DATA_OUT : boolean    := false;
    INVERT_CLK_OUT  : boolean    := false;
    NEEDS_IDELAYCTRL : boolean  := false);

  port (
    clk_40Mhz  : in std_logic; --locally generated data clock
    clk_200Mhz : in std_logic; --locally generated bit DDR clock
    clk_locked : in std_logic;
    reset      : in std_logic;
    
    --DDR links
    bit_clk_P           : in  std_logic;
    bit_clk_N           : in  std_logic;
    data_in_P           : in  std_logic;
    data_in_N           : in  std_logic;     
    data_out_P          : out std_logic;
    data_out_N          : out std_logic;
    data_out_clk_P      : out std_logic;
    data_out_clk_N      : out std_logic;

    -- HS data in
    hs_data_in          : in  std_logic_vector(3 downto 0);
    hs_data_in_valid    : in  std_logic;
    hs_data_empty       : out std_logic;
    
    -- HS data rx
    hs_data_out         : out std_logic_vector(7 downto 0);
    hs_data_start       : out std_logic;
    hs_data_end         : out std_logic;
    hs_data_out_valid   : out std_logic;

    -- HS message rx
    hs_rx_mesg_data       : out std_logic_vector(6 downto 0);
    hs_rx_mesg_data_valid : out std_logic;

    
    control             : in  FMC_Comm_Control_t;
    monitor             : out FMC_Comm_Monitor_t
    );

end entity fmc_comm;

architecture behavioral of fmc_comm is

  component fmc_io is
    generic (
      CLK_IN_TYPE     : string;
      INVERT_DATA_IN  : boolean;
      INVERT_CLK_IN   : boolean;
      INVERT_DATA_OUT : boolean;
      INVERT_CLK_OUT  : boolean;
      NEEDS_IDELAYCTRL : boolean);
    port (
      clk_200Mhz       : in  std_logic;
      clk_locked       : in  std_logic;
      reset            : in  std_logic;
      bit_clk_P        : in  std_logic;
      bit_clk_N        : in  std_logic;
      data_in_P        : in  std_logic;
      data_in_N        : in  std_logic;
      data_out_P       : out std_logic;
      data_out_N       : out std_logic;
      data_out_clk_P   : out std_logic;
      data_out_clk_N   : out std_logic;
      rx_data_8b       : out std_logic_vector(8 downto 0);
      rx_data_8b_valid : out std_logic;
      rx_locked        : out std_logic;
      rx_ce_20Mhz      : out std_logic;
      tx_data_8b       : in  std_logic_vector(8 downto 0);
      tx_ce_40Mhz      : out std_logic;
      control          : in  FMC_IO_Control_t;
      monitor          : out FMC_IO_Monitor_t);
  end component fmc_io;
  
  component rx is
    port (
      reset              : in  std_logic;
      clk_200Mhz         : in  std_logic;
      clk_en_20Mhz       : in  std_logic;
      locked             : in  std_logic;
      data               : in  std_logic_vector(8 downto 0);
      data_valid         : in  std_logic;
      uart_rx_data       : out std_logic_vector(6 downto 0);
      uart_rx_data_valid : out std_logic;
--      reg_mode           : out std_logic_vector(7  downto  0);
--      reg_addr           : out std_logic_vector(15 downto  0);
--      reg_data           : out std_logic_vector(31 downto  0);
--      reg_valid          : out std_logic;
      hs_mesg_data       : out std_logic_vector(6 downto 0);
      hs_mesg_data_valid : out std_logic;
      hs_data_out        : out std_logic_vector(7 downto 0);
      hs_data_start      : out std_logic;
      hs_data_end        : out std_logic;
      hs_data_out_valid  : out std_logic);
  end component rx;

  component tx is
    port (
      reset              : in  std_logic;
      clk_200Mhz         : in  std_logic;
      clk_en_40Mhz       : in  std_logic;
      locked             : in  std_logic;
      tx_data            : out std_logic_vector(8 downto 0);
      uart_data          : in  std_logic_vector(6 downto 0);
      uart_data_valid    : in  std_logic;
      uart_empty         : out std_logic;
      uart_sending_pulse : out std_logic;
      hs_data            : in  std_logic_vector(3 downto 0);
      hs_data_valid      : in  std_logic;
      hs_empty           : out std_logic;
      hs_sending_pulse   : out std_logic_vector);
  end component tx;

  component timed_counter is
    generic (
      timer_count : std_logic_vector;
      DATA_WIDTH  : integer);
    port (
      clk          : in  std_logic;
      clk_en       : in  std_logic;
      reset_async  : in  std_logic;
      reset_sync   : in  std_logic;
      enable       : in  std_logic;
      event        : in  std_logic;
      update_pulse : out std_logic;
      timed_count  : out std_logic_vector(DATA_WIDTH-1 downto 0));
  end component timed_counter;
  
  component counter is
    generic (
      roll_over   : std_logic;
      end_value   : std_logic_vector;
      start_value : std_logic_vector;
      DATA_WIDTH  : integer);
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      enable      : in  std_logic;
      event       : in  std_logic;
      count       : out std_logic_vector(DATA_WIDTH-1 downto 0);
      at_max      : out std_logic);
  end component counter;

  component FMC_COMM_FIFO is
    port (
      clk   : IN  STD_LOGIC;
      srst  : IN  STD_LOGIC;
      din   : IN  STD_LOGIC_VECTOR(6 DOWNTO 0);
      wr_en : IN  STD_LOGIC;
      rd_en : IN  STD_LOGIC;
      dout  : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
      full  : OUT STD_LOGIC;
      empty : OUT STD_LOGIC);
  end component FMC_COMM_FIFO;
  
  ---------------------------------------
  -- io signals
  signal rx_locked          :  std_logic := '0';
  
  ---------------------------------------
  -- rx side
  signal rx_data_8b         : std_logic_vector(8 downto 0);
  signal rx_data_8b_valid   : std_logic;
  signal clk_en_20Mhz       : std_logic;
  
  signal comm_data_in       : std_logic_vector(6 downto 0);
  signal comm_data_in_valid : std_logic;

  signal rx_comm_present    : std_logic;
  signal rx_comm_empty      : std_logic;
  
  signal rx_hs_data_valid   : std_logic;

  signal local_hs_rx_mesg_data       : std_logic_vector(6 downto 0);
  signal local_hs_rx_mesg_data_valid : std_logic;

  ---------------------------------------
  -- tx side
  signal tx_data_8b            : std_logic_vector(8 downto 0);
  signal clk_en_40Mhz          : std_logic;

  signal tx_uart_sending_pulse : std_logic;
  signal tx_hs_sending_pulse   : std_logic;

  signal hs_tx_data           :  std_logic_vector(3 downto 0);
  signal hs_tx_data_valid     :  std_logic;
  signal hs_tx_data_empty     :  std_logic;

begin  -- architecture behavioral

  -------------------------------------------------------------------------------
  -- Serdes code
  -------------------------------------------------------------------------------  
  monitor.rx_locked <= rx_locked;
  fmc_io_1: entity work.fmc_io
    generic map (
      CLK_IN_TYPE    => CLK_IN_TYPE,
      INVERT_DATA_IN   => INVERT_DATA_IN,--true,--false,
      INVERT_CLK_IN    => INVERT_CLK_IN,--true,--false,
      INVERT_DATA_OUT  => INVERT_DATA_OUT,--true,--false,
      INVERT_CLK_OUT   => INVERT_CLK_OUT,--)--true)--false)
      NEEDS_IDELAYCTRL => NEEDS_IDELAYCTRL)
    port map (
--      clk_40Mhz        => clk_40Mhz,
      clk_200Mhz       => clk_200Mhz,
      clk_locked       => clk_locked,
      reset            => reset,
      bit_clk_P        => bit_clk_P,
      bit_clk_N        => bit_clk_N,
      data_in_P        => data_in_P,
      data_in_N        => data_in_N,
      data_out_P       => data_out_P,
      data_out_N       => data_out_N,
      data_out_clk_P   => data_out_clk_P,
      data_out_clk_N   => data_out_clk_N,
      rx_data_8b       => rx_data_8b,
      rx_data_8b_valid => rx_data_8b_valid,
      rx_locked        => rx_locked,
      rx_ce_20Mhz      => clk_en_20Mhz,
      tx_data_8b       => tx_data_8b,
      tx_ce_40Mhz      => clk_en_40Mhz,
      control          => control.IO,
      monitor          => monitor.IO);

  -------------------------------------------------------------------------------
  -- Tx generation
  -------------------------------------------------------------------------------    
  hs_tx_multiplexer: process (clk_200Mhz) is
  begin  -- process hs_tx_multiplexer
    if clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      hs_tx_data_valid <= '0';
      if hs_tx_data_empty = '1' then
        if hs_data_in_valid = '1' then
          hs_tx_data       <= hs_data_in;
          hs_tx_data_valid <= '1';
        elsif control.tx_hs_data_valid = '1' then
          hs_tx_data       <= control.tx_hs_data;
          hs_tx_data_valid <= '1';
        end if;
      end if;
    end if;
  end process hs_tx_multiplexer;

  hs_data_empty <= hs_tx_data_empty;
  monitor.tx_hs_empty <= hs_tx_data_empty;
  tx_1: entity work.tx
    port map (
      reset              => '0',
      clk_200Mhz         => clk_200Mhz,
      clk_en_40Mhz       => clk_en_40Mhz,
      locked             => '1',
      tx_data            => tx_data_8b,
      uart_data          => control.tx_comm_data,
      uart_data_valid    => control.tx_comm_data_valid,
      uart_empty         => monitor.tx_comm_empty,
      uart_sending_pulse => tx_uart_sending_pulse,
      hs_data            => hs_tx_data,
      hs_data_valid      => hs_tx_data_valid,
      hs_empty           => hs_tx_data_empty,
      hs_sending_pulse   => tx_hs_sending_pulse);

  timed_counter_tx_hs: entity work.timed_counter
    generic map (
      timer_count => x"0BEBC200")
    port map (
      clk          => clk_200Mhz,
      reset_async  => '0',
      reset_sync   => '0',
      enable       => '1',
      event        => tx_hs_sending_pulse,
      update_pulse => open,
      timed_count  => monitor.tx_hs_data_rate);

  timed_counter_tx_comm: entity work.timed_counter
    generic map (
      timer_count => x"0BEBC200")
    port map (
      clk          => clk_200Mhz,
      reset_async  => '0',
      reset_sync   => '0',
      enable       => '1',
      event        => tx_uart_sending_pulse,
      update_pulse => open,
      timed_count  => monitor.tx_comm_rate);

  
  tx_uart_counter: entity work.counter
    generic map (
      roll_over   => '1',
      DATA_WIDTH  => 16)
    port map (
      clk         => clk_200Mhz,
      reset_async => '0',
      reset_sync  => control.tx_reset_uart_counter,
      enable      => '1',
      event       => tx_uart_sending_pulse,
      count       => monitor.tx_uart_counter,
      at_max      => open);
  tx_hs_counter: entity work.counter
    generic map (
      roll_over   => '1',
      DATA_WIDTH  => 16)
    port map (
      clk         => clk_200Mhz,
      reset_async => '0',
      reset_sync  => control.tx_reset_hs_counter,
      enable      => '1',
      event       => tx_hs_sending_pulse,
      count       => monitor.tx_hs_counter,
      at_max      => open);

  -------------------------------------------------------------------------------
  -- Rx generation
  -------------------------------------------------------------------------------    
  hs_data_out_valid <= rx_hs_data_valid;
  hs_rx_mesg_data       <= local_hs_rx_mesg_data;
  hs_rx_mesg_data_valid <= local_hs_rx_mesg_data_valid;
  rx_1: entity work.rx
    port map (
      reset              => '0',
      clk_200Mhz         => clk_200Mhz,
      clk_en_20Mhz       => clk_en_20Mhz,
      locked             => rx_locked,
      data               => rx_data_8b,
      data_valid         => rx_data_8b_valid,
      uart_rx_data       => comm_data_in,
      uart_rx_data_valid => comm_data_in_valid,
--      reg_mode           => monitor.reg_rx_mode,
--      reg_addr           => monitor.reg_rx_addr,
--      reg_data           => monitor.reg_rx_data,
--      reg_valid          => monitor.reg_rx_valid,
      hs_mesg_data       => local_hs_rx_mesg_data,
      hs_mesg_data_valid => local_hs_rx_mesg_data_valid,
      hs_data_out        => hs_data_out,
      hs_data_start      => hs_data_start,
      hs_data_end        => hs_data_end,
      hs_data_out_valid  => rx_hs_data_valid);

  timed_counter_rx_hs: entity work.timed_counter
    generic map (
      timer_count => x"0BEBC200")
    port map (
      clk          => clk_200Mhz,
      reset_async  => '0',
      reset_sync   => '0',
      enable       => '1',
      event        => rx_hs_data_valid,
      update_pulse => open,
      timed_count  => monitor.rx_hs_data_rate);

  timed_counter_rx_comm: entity work.timed_counter
    generic map (
      timer_count => x"0BEBC200")
    port map (
      clk          => clk_200Mhz,
      reset_async  => '0',
      reset_sync   => '0',
      enable       => '1',
      event        => comm_data_in_valid,
      update_pulse => open,
      timed_count  => monitor.rx_comm_rate);

  monitor.rx_comm_valid <= not rx_comm_empty;
  FMC_COMM_FIFO_1: FMC_COMM_FIFO
    port map (
      clk   => clk_200Mhz,
      srst  => '0',
      din   => comm_data_in,
      wr_en => comm_data_in_valid,
      rd_en => control.rx_comm_read,
      dout  => monitor.rx_comm,
      full  => open,
      empty => rx_comm_empty);
  

  rx_uart_counter: entity work.counter
    generic map (
      roll_over   => '1',
      DATA_WIDTH  => 16)
    port map (
      clk         => clk_200Mhz,
      reset_async => '0',
      reset_sync  => control.rx_reset_uart_counter,
      enable      => '1',
      event       => comm_data_in_valid,
      count       => monitor.rx_uart_counter,
      at_max      => open);
  rx_hs_mesg_counter: entity work.counter
    generic map (
      roll_over   => '1',
      DATA_WIDTH  => 16)
    port map (
      clk         => clk_200Mhz,
      reset_async => '0',
      reset_sync  => control.rx_reset_hs_mesg_counter,
      enable      => '1',
      event       => local_hs_rx_mesg_data_valid,
      count       => monitor.rx_hs_mesg_counter,
      at_max      => open);
  rx_hs_counter: entity work.counter
    generic map (
      roll_over   => '1',
      DATA_WIDTH  => 16)
    port map (
      clk         => clk_200Mhz,
      reset_async => '0',
      reset_sync  => control.rx_reset_hs_counter,
      enable      => '1',
      event       => rx_hs_data_valid,
      count       => monitor.rx_hs_counter,
      at_max      => open);


  
  
end architecture behavioral;
