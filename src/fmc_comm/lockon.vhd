
library IEEE;
use IEEE.STD_LOGIC_1164.all;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;


entity lockon_10b is
  generic (
    LOCKON_SYMBOL_P    : std_logic_vector(9 downto 0) := "1001111100"; --K28.1p
    LOCKON_SYMBOL_N    : std_logic_vector(9 downto 0) := "0110000011"; --K28.1n
    COUNT_START_SEARCH  : integer := 3;
    COUNT_SEARCH_FAILED : integer := 15
    );
  port (
    clk                  : in std_logic;
    clk_en               : in std_logic;
    reset                : in std_logic;
    data_in              : in std_logic_vector(9 downto 0);
    symbol_error         : in std_logic;
    locked               : out std_logic;
    lockon_failed_strobe : out std_logic
    );
  
end entity lockon_10b;

architecture behavioral of lockon_10b is

  --lockon
  constant COUNT_START_SEQUENCE : integer := 0;
  signal lockon_counter           : integer := COUNT_START_SEARCH;

  signal local_locked : std_logic := '0';
begin  -- architecture behavioral
  
  locked <= local_locked;
  lockon_10b: process (clk, reset) is
  begin  -- process lockon_8b10b
    if reset = '1' then            -- asynchronous reset (active low)
      lockon_failed_strobe <= '0';
      lockon_counter <= COUNT_START_SEARCH;
      local_locked <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      lockon_failed_strobe <= '0';

      if clk_en = '1' then      
        if local_locked = '0' then
          --We are searching for the lockon symbols
          lockon_counter <= lockon_counter + 1;
          
          if lockon_counter = COUNT_START_SEQUENCE then
            --Start the search by telling the source to try a new setting
            lockon_failed_strobe <= '1';
          elsif lockon_counter = COUNT_SEARCH_FAILED then
            --The search has failed with this parameter, go back to the begging
            lockon_counter <= COUNT_START_SEQUENCE;
          elsif lockon_counter > COUNT_START_SEARCH then
            if (data_in = LOCKON_SYMBOL_P) or (data_in = LOCKON_SYMBOL_N) then
              -- If we found a lockon character, we are locked
              local_locked <= '1';
            end if;
          end if;
        else
          -- we are locked on to the stream, look for bad characters
          if symbol_error = '1' then
            local_locked <= '0';
            lockon_counter <= COUNT_START_SEQUENCE;
          end if;
        end if;
      end if;   
    end if;
  end process lockon_10b;
  
end architecture behavioral;
