#open_wave_config {../src/fmc_comm/rx/tb/testbench_top_behav.wcfg}
restart
add_force {/tb_rx/clk_200Mhz} -radix hex {1 0ns} {0 2500ps} -repeat_every 5000ps
add_force {/tb_rx/tb_reset} -radix hex 1
run 20ns
add_force {/tb_rx/tb_reset} -radix hex 0
run 5us
