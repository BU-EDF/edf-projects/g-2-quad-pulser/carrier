-------------------------------------------------------------------------------
-- Rx from ADCBoard
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
--use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

entity rx is
  
  port (
    reset              : in  std_logic;
    clk_200Mhz         : in  std_logic;
    clk_en_20Mhz       : in  std_logic;
    locked             : in  std_logic;
    data               : in  std_logic_vector(8 downto 0);
    data_valid         : in  std_logic;    

    --Uart RX
    uart_rx_data       : out std_logic_vector(6 downto 0);
    uart_rx_data_valid : out std_logic;

--    --REG TX    
--    reg_mode           : out std_logic_vector(7  downto  0);
--    reg_addr           : out std_logic_vector(15 downto  0);
--    reg_data           : out std_logic_vector(31 downto  0);
--    reg_valid          : out std_logic;
    
    --High-speed messages
    hs_mesg_data       : out std_logic_vector(6 downto 0);
    hs_mesg_data_valid : out std_logic;
    
    --High-speed Rx    
    hs_data_out        : out std_logic_vector(7 downto 0);
    hs_data_start      : out std_logic;
    hs_data_end        : out std_logic;
    hs_data_out_valid  : out std_logic);

end entity rx;

architecture behavioral of rx is

  --RX state machine
  type RX_state_t is (RXS_IDLE,
                      RXS_UART,RXS_UART_END,
                      RXS_HSD,RXS_HS_END,
                      RXS_WAIT,
                      RXS_ERROR);--,
--                      RXS_REG,RXS_REG_END);
  signal RX_state : RX_state_t := RXS_WAIT;
  signal error_state_pulse : std_logic := '0';
  signal in_hs_frame : std_logic := '0';
  
  -- DDR data path
  constant IDLE_CHAR       : std_logic_vector(8 downto 0) := '1'&x"3C";
  constant UART_START_CHAR : std_logic_vector(8 downto 0) := '1'&x"FB";
  constant UART_END_CHAR   : std_logic_vector(8 downto 0) := '1'&x"FD";
  constant HS_START_CHAR   : std_logic_vector(8 downto 0) := '1'&x"BC";
  constant HS_END_CHAR     : std_logic_vector(8 downto 0) := '1'&x"DC";
--  constant REG_START_CHAR  : std_logic_vector(8 downto 0) := '1'&x"7C";
--  constant REG_END_CHAR    : std_logic_vector(8 downto 0) := '1'&x"9C";

--  signal reg_state_sr : std_logic_vector(6 downto 0);

  signal hs_data_out_buffer : std_logic_vector(7 downto 0);
  signal hs_data_out_valid_buffer : std_logic;
  signal hs_waiting_for_first_word : std_logic;
  
  --debug
  attribute mark_debug : string;
  attribute mark_debug of RX_state            : signal is "true";
  attribute mark_debug of in_hs_frame         : signal is "true";
  attribute mark_debug of hs_data_out         : signal is "true";
  attribute mark_debug of hs_data_out_valid   : signal is "true";
  attribute mark_debug of data                : signal is "true";
  attribute mark_debug of data_valid          : signal is "true";
  attribute mark_debug of hs_data_end         : signal is "true";
  attribute mark_debug of hs_data_start       : signal is "true";
  attribute mark_debug of uart_rx_data        : signal is "true";
  attribute mark_debug of uart_rx_data_valid  : signal is "true";


  
begin  -- architecture behavioral

  -------------------------------------------------------------------------------
  -- Rx capture
  -------------------------------------------------------------------------------
  rx_state_machine: process (clk_200Mhz, reset) is
  begin  -- process rx_capture
    if reset = '1' then                 -- asynchronous reset (active high)
      rx_state <= RXS_WAIT;
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      error_state_pulse <= '0';
--      reg_valid <= '0';
      
      if locked = '0' then
        rx_state <= RXS_WAIT;
      else
        
        if data_valid = '1' then
          case RX_state is
            --------------------------------------------------------------------------
            when RXS_IDLE =>
              -- We are waiting for a state change from idle
              case data is
                when IDLE_CHAR       => rx_state <= RXS_IDLE;
                                        in_hs_frame <= '0';
                when UART_START_CHAR => rx_state <= RXS_UART;
--                when REG_START_CHAR  => rx_state <= RXS_REG;
--                                        reg_state_sr <= "1000000";
                when HS_START_CHAR   => rx_state <= RXS_HSD;
                                        in_hs_frame <= '1';
                when others          => rx_state <= RXS_ERROR;
              end case;
            --------------------------------------------------------------------------
            when RXS_UART =>
              if data(8) = '0' then
                -- we got our data character, wait for the UART end word
                rx_state <= RXS_UART_END;
              else
                -- anything else is an error
                rx_state <= RXS_ERROR;
              end if;
            --------------------------------------------------------------------------
            when RXS_UART_END =>
              if data = UART_END_CHAR then
                if in_hs_frame = '1' then
                  rx_state <= RXS_HSD;
                else
                  rx_state <= RXS_IDLE;
                end if;
              else
                -- we got the wrong char here, go to error
                rx_state <= RXS_ERROR;
              end if;
----            --------------------------------------------------------------------------
----            when RXS_REG =>
----              reg_state_sr <= '0'&reg_state_sr(reg_state_sr'left downto 1);
----              case reg_state_sr is
----                when "1000000" => reg_mode               <= data(7 downto 0);
----                when "0100000" => reg_addr(15 downto  8) <= data(7 downto 0);
----                when "0010000" => reg_addr( 7 downto  0) <= data(7 downto 0);
----                when "0001000" => reg_data(31 downto 24) <= data(7 downto 0);
----                when "0000100" => reg_data(23 downto 16) <= data(7 downto 0);
----                when "0000010" => reg_data(15 downto  8) <= data(7 downto 0);
----                when "0000001" => reg_data( 7 downto  0) <= data(7 downto 0);
----                                  reg_valid <= '1';
----                                  rx_state <= RXS_REG_END;
----                when others => null;
----              end case;
----            --------------------------------------------------------------------------
----            when RXS_REG_END =>
----              if data = REG_END_CHAR then
----                if in_hs_frame = '1' then
----                  rx_state <= RXS_HSD;
----                else
----                  rx_state <= RXS_IDLE;
----                end if;
----              else
----                -- we got the wrong char here, go to error
----                rx_state <= RXS_ERROR;
----              end if;
            --------------------------------------------------------------------------
            when RXS_HSD =>
              if data = UART_START_CHAR then
                -- switch to UART data processing
                rx_state <= RXS_UART;
              elsif data = HS_END_CHAR then
                -- packet ended, go back to idle
                in_hs_frame <= '0';
                rx_state <= RXS_IDLE;
              elsif data(8) = '1' then
                -- this is a k-char where there shouldn't be one
                rx_state <= RXS_ERROR;
              end if;
            --------------------------------------------------------------------------
            when RXS_ERROR =>            
              rx_state <= RXS_WAIT;
              error_state_pulse <= '1';
            --------------------------------------------------------------------------
            when RXS_WAIT =>
              if data = IDLE_CHAR then
                rx_state <= RXS_IDLE;
              end if;
            --------------------------------------------------------------------------
            when others => rx_state <= RXS_ERROR;
          end case;
        end if;                          
      end if;
    end if;
  end process rx_state_machine;
  
  rx_proc: process (clk_200Mhz) is
  begin  -- process rx_proc
    if clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      uart_rx_data_valid <= '0';
      hs_mesg_data_valid <= '0';

      if data_valid = '1' then
        hs_data_out_valid_buffer <= '0';
        case RX_state is
          when RXS_UART =>            
            if data(8) = '0' then
              -- non-control char
              if data(7) = '0' then
                --uart char data
                uart_rx_data_valid <= '1';
                uart_rx_data <= data(6 downto 0);
              else
                --hs mesg data
                hs_mesg_data_valid <= '1';
                hs_mesg_data <= data(6 downto 0);
              end if;
            end if;
          when RXS_HSD =>            
            --Latch the current HS data to be delayed by one character so we
            --can mark it as the last
            hs_data_out_buffer <= data(7 downto 0);
            if data(8) = '0' then
              hs_data_out_valid_buffer <= '1';
            end if;
          when others => null;
        end case;
      end if;
    end if;
  end process rx_proc;

  hs_out_proc: process (clk_200Mhz) is
  begin  -- process hs_out_proc
    if clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      -- Output the HS data after one word of buffer so we can mark the last
      -- valid word as the last word
      hs_data_out_valid <= '0';
      hs_data_end       <= '0';
      hs_data_start     <= '0';

      
      if data_valid = '1' then
        
        if data = HS_START_CHAR or data = HS_END_CHAR then
          --Make sure we are ready for the first word by monitoring data_in
          hs_waiting_for_first_word <= '1';
        end if;

        if hs_data_out_valid_buffer = '1' then
          --output the hs data
          hs_data_out <= hs_data_out_buffer;
          hs_data_out_valid <= '1';

          --Check if this is the first word
          if hs_waiting_for_first_word = '1' then
            hs_data_start <= '1';
            hs_waiting_for_first_word <= '0';
          end if;
          
          --CHeck if this is the last word
          if data = HS_END_CHAR then
            hs_data_end <= '1';
            hs_waiting_for_first_word <= '1';
          end if;
        end if;
      end if;
    end if;
  end process hs_out_proc;
  
  
end architecture behavioral;

