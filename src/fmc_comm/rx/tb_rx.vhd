library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;


entity tb_rx is
  port (
    clk_200Mhz   : in std_logic;
    tb_reset : in std_logic);
end entity tb_rx;

architecture behavioral of tb_rx is

  component rx is
    port (
      reset              : in  std_logic;
      clk_200Mhz         : in  std_logic;
      clk_en_20Mhz       : in  std_logic;
      locked             : in  std_logic;
      data               : in  std_logic_vector(8 downto 0);
      data_valid         : in  std_logic;
      uart_rx_data       : out std_logic_vector(6 downto 0);
      uart_rx_data_valid : out std_logic;
      hs_data_out        : out std_logic_vector(7 downto 0);
      hs_data_out_valid  : out std_logic);
  end component rx;


  signal reset              : std_logic;
  signal clk_en_20Mhz       : std_logic;
  signal locked             : std_logic;
  signal data               : std_logic_vector(8 downto 0);
  signal data_valid         : std_logic;
  signal uart_rx_data       : std_logic_vector(6 downto 0);
  signal uart_rx_data_valid : std_logic;
  signal hs_data_out        : std_logic_vector(7 downto 0);
  signal hs_data_out_valid  : std_logic;

  signal clk_en_sr : std_logic_vector(9 downto 0) := "1000000000";
  signal counter : unsigned(31 downto 0) := x"00000000";
begin  -- architecture behavioral

  input: process (clk_200Mhz,tb_reset) is
  begin  -- process input
    if tb_reset = '1' then
      clk_en_sr <= "1000000000";
      clk_en_20Mhz <= '0';
      reset <= '1';
      counter <= x"00000000";
      locked <= '0';

      data_valid <= '0';
      data <= '0'&x"00";
      
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      clk_en_20Mhz <= clk_en_sr(0);
      clk_en_sr <= clk_en_sr(0)&clk_en_sr(9 downto 1);        
      

      if clk_en_sr(0) = '1' then
        counter <= counter + 1;
        
        if counter = x"00000003" then
          reset <= '0';
        end if;
        
        if counter = x"00000008" then
          locked <= '1';
        end if;

        if counter > x"00000008" then
          data_valid <= '1';
        end if;
        
        if counter < x"00000020" then
          data <= '1'&x"3C";
        elsif counter = x"00000020" then
          data <= '1'&x"FB";
        elsif counter = x"00000021" then
          data <= '0'&x"01";
        elsif counter = x"00000022" then
          data <= '1'&x"FD";
        else
          data <= '1'&x"3C";
        end if;
      else
        data_valid <= '0';
      end if;
      
    end if;
  end process input;

  rx_1: entity work.rx
    port map (
      reset              => reset,
      clk_200Mhz         => clk_200Mhz,
      clk_en_20Mhz       => clk_en_20Mhz,
      locked             => locked,
      data               => data,
      data_valid         => data_valid,
      uart_rx_data       => uart_rx_data,
      uart_rx_data_valid => uart_rx_data_valid,
      hs_data_out        => hs_data_out,
      hs_data_out_valid  => hs_data_out_valid);
  
end architecture behavioral;
