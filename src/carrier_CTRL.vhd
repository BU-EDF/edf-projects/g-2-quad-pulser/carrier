library IEEE;
use IEEE.std_logic_1164.all;

package carrier_CTRL is

  type carrier_control_t is record
    LED_GPO         : std_logic_vector(3 downto 0);
    LED_mode        : std_logic_vector(3 downto 0);
    reset_pl_clock  : std_logic;
    adc_power_en    : std_logic_vector(3 downto 0);
    reset           : std_logic;
  end record carrier_control_t;
  constant DEFAULT_CARRIER_CONTROL : carrier_control_t := (LED_GPO        => x"0",
                                                           LED_mode       => x"0",
                                                           reset_pl_clock => '0',
                                                           adc_power_en   => x"0",
                                                           reset          => '0');

  type carrier_monitor_t is record
    LEDs            : std_logic_vector(3 downto 0);
    LED_GPO         : std_logic_vector(3 downto 0);
    LED_mode        : std_logic_vector(3 downto 0);
    reset_pl_clock  : std_logic;
    locked_pl_clock : std_logic;
    adc_power_en    : std_logic_vector(3 downto 0);
    external_spark_sent_count : std_logic_vector(31 downto 0);
    external_spark_recv_count : std_logic_vector(31 downto 0);
  end record carrier_monitor_t;

end package carrier_CTRL;
