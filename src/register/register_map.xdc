#Register read data given two clocks to get data to to the CDC
set_multicycle_path 2 -setup -to [get_pins -hier -filter NAME=~*register_map*read_data*[*][*]/D]
set_multicycle_path 1 -hold -end -to [get_pins -hier -filter NAME=~*register_map*read_data*[*][*]/D]

#Register read address given two clocks to get data to the case statement
set_multicycle_path 2 -setup -from [get_pins -hier -filter NAME=~*register_map*read_address_cap*[*][*]/Q]
set_multicycle_path 1 -hold -end -from [get_pins -hier -filter NAME=~*register_map*read_address_cap*[*][*]/Q]
#special helpers
set_multicycle_path 2 -setup -from [get_pins -hier -filter NAME=~*register_map*fmc_read_index*[*][*]/Q]
set_multicycle_path 1 -hold -end -from [get_pins -hier -filter NAME=~*register_map*fmc_read_index*[*][*]/Q]
set_multicycle_path 2 -setup -from [get_pins -hier -filter NAME=~*register_map*pmod_read_index*[*][*]/Q]
set_multicycle_path 1 -hold -end -from [get_pins -hier -filter NAME=~*register_map*pmod_read_index*[*][*]/Q]


#Register write address/data given two clocks to get data to the case statement
set_multicycle_path 2 -setup -from [get_pins -hier -filter NAME=~*register_map*write_addr_cap*[*][*]/Q]
set_multicycle_path 1 -hold -end -from [get_pins -hier -filter NAME=~*register_map*write_addr_cap*[*][*]/Q]
set_multicycle_path 2 -setup -from [get_pins -hier -filter NAME=~*register_map*write_data_cap*[*][*]/Q]
set_multicycle_path 1 -hold -end -from [get_pins -hier -filter NAME=~*register_map*write_data_cap*[*][*]/Q]
#special helpers
set_multicycle_path 2 -setup -from [get_pins -hier -filter NAME=~*register_map*fmc_wr_index*[*][*]/Q]
set_multicycle_path 1 -hold -end -from [get_pins -hier -filter NAME=~*register_map*fmc_wr_index*[*][*]/Q]
set_multicycle_path 2 -setup -from [get_pins -hier -filter NAME=~*register_map*pmod_wr_index*[*][*]/Q]
set_multicycle_path 1 -hold -end -from [get_pins -hier -filter NAME=~*register_map*pmod_wr_index*[*][*]/Q]
