library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

use work.types.all;

use work.FW_TIMESTAMP.all;
use work.FMC_COMM_CTRL.all;
use work.PMOD_COMM_CTRL.all;
use work.carrier_ctrl.all;
use work.GLOBAL_CTRL.all;
use work.timing_system_CTRL.all;
use work.event_builder_CTRL.all;
use work.hs_message_CTRL.all;

entity register_map is

  generic (
    FIRMWARE_VERSION : std_logic_vector(31 downto 0) := x"00000000");
  

  port (
    clk_sys               : in  std_logic;
    locked_sys            : in  std_logic;
    reset                 : in  std_logic;          -- state machine reset
                          
    data_in               : in  std_logic_vector(31 downto 0);
    WR_address            : in  std_logic_vector(15 downto 0);
    RD_address            : in  std_logic_vector(15 downto 0);
    WR_strb               : in  std_logic;
    RD_strb               : in  std_logic;
    data_out              : out std_logic_vector(31 downto 0);
    data_out_valid        : out std_logic;
    
    -- register clock domains    
    clk_200Mhz            : in  std_logic;
    locked_200Mhz         : in  std_logic;

    -- intefaces to data
    carrier_control       : out carrier_control_t;
    carrier_monitor       : in  carrier_monitor_t;
    fmc_comm_controls     : out FMC_Comm_Control_array_t;
    fmc_comm_monitors     : in  FMC_Comm_Monitor_array_t;
    global_control        : out GLOBAL_Control_t;
    global_monitor        : in  GLOBAL_Monitor_t;
    pmod_comm_control     : out pmod_comm_control_t;
    pmod_comm_monitor     : in  pmod_comm_monitor_t;
    eb_control            : out EB_Control_t;
    eb_monitor            : in  EB_Monitor_t;
    timing_system_control : out timing_system_control_t;
    timing_system_monitor : in  timing_system_monitor_t;
    hsm_control           : out hsm_control_t;
    hsm_monitor           : in  hsm_monitor_t
    );

end entity register_map;

architecture Behavioral of register_map is

  component register_map_bridge is
    generic (
      CLOCK_DOMAINS : integer);
    port (
      clk_reg_map           : in  std_logic;
      reset                 : in  std_logic;
      WR_strobe             : in  std_logic;
      RD_strobe             : in  std_logic;
      WR_address            : in  std_logic_vector(15 downto 0);
      RD_address            : in  std_logic_vector;
      data_in               : in  std_logic_vector(31 downto 0);
      data_out              : out std_logic_vector(31 downto 0);
      rd_ack                : out std_logic;
      wr_ack                : out std_logic;
      clk_domain            : in  std_logic_vector(CLOCK_DOMAINS-1 downto 0);
      clk_domain_locked     : in  std_logic_vector(CLOCK_DOMAINS-1 downto 0);
      read_address_valid    : out std_logic_vector(CLOCK_DOMAINS-1 downto 0);
      read_address_ack      : in  std_logic_vector(CLOCK_DOMAINS-1 downto 0);
      read_address          : out slv16_array_t(CLOCK_DOMAINS-1 downto 0);
      read_data_wr          : in  std_logic_vector(CLOCK_DOMAINS-1 downto 0);
      read_data             : in  slv36_array_t(CLOCK_DOMAINS-1 downto 0);
      write_addr_data_valid : out std_logic_vector(CLOCK_DOMAINS-1 downto 0);
      write_addr_data_ack   : in  std_logic_vector(CLOCK_DOMAINS-1 downto 0);
      write_addr            : out slv16_array_t(CLOCK_DOMAINS-1 downto 0);
      write_data            : out slv32_array_t(CLOCK_DOMAINS-1 downto 0));
  end component register_map_bridge;

  component resetter is
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      reset       : out std_logic);
  end component resetter;

  component counter is
    generic (
      roll_over   : std_logic;
      end_value   : std_logic_vector;
      start_value : std_logic_vector;
      DATA_WIDTH  : integer);
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      enable      : in  std_logic;
      event       : in  std_logic;
      count       : out std_logic_vector(DATA_WIDTH-1 downto 0);
      at_max      : out std_logic);
  end component counter;
  
  -----------------------------------------------------------------------------
  -- Address space
  -----------------------------------------------------------------------------


  ----------------------------
  -- Status
  ----------------------------
  constant CARRIER_STATUS     : unsigned(15 downto 0) := x"0000";
  -- 0 (a)   global reset
  -- 4 (r/w) reset PL master clock
  -- 5 (r)   PL master clock locked
  
  ----------------------------
  -- Status
  ----------------------------
  constant CARRIER_LEDS      : unsigned(15 downto 0) := x"0001";
  --  3..0 (r/w) LED mode (0x1:GPO, 0x0 clocks)
  --  7..4 (r/w) Current LED output value
  -- 11..8 (r/w) GPO mode LED values

  ----------------------------
  -- remote triggers rx
  ----------------------------
  constant CARRIER_EXT_TRIG_RX : unsigned(15 downto 0) := x"0002";
  -- 31..0 (r)  Count
  ----------------------------
  -- remote triggers tx
  ----------------------------
  constant CARRIER_EXT_TRIG_TX : unsigned(15 downto 0) := x"0003";
  -- 31..0 (r)  Count
  
  ----------------------------
  -- CARRIER FW Version
  ----------------------------
  constant CARRIER_FW_VERSION : unsigned(15 downto 0) := x"0005";
  --   7..0 (r) rev   (1-99) Manually updated
  --  15..8 (r) day   (1-31) Manually updated
  -- 23..16 (r) month (1-12) Manually updated
  -- 31..24 (r) year  (20XX) Manually updated

  ----------------------------
  -- CARRIER Synthesis Date
  ----------------------------
  constant CARRIER_SYNTH_DATE : unsigned(15 downto 0) := x"0006";
  --   7..0 (r) day     (1-31)  automatically updated
  --  15..8 (r) month   (1-12)  automatically updated
  -- 23..16 (r) year    (00-99) automatically updated
  -- 31..24 (r) century (20)    automatically updated

  ----------------------------
  -- CARRIER Synthesis Time
  ----------------------------
  constant CARRIER_SYNTH_TIME : unsigned(15 downto 0) := x"0007";
  --   7..0 (r) second  (00-59)  automatically updated
  --  15..8 (r) minute  (00-59)  automatically updated
  -- 23..16 (r) hour    (00-23) automatically updated

  ----------------------------
  -- ADC Board Power Enable
  ----------------------------
  constant ADC_BOARD_POWER_ENABLE : unsigned(15 downto 0) := x"0010";
  -- 3..0 (r/w) ADC Board power enable


  ----------------------------
  -- Timing system control
  ----------------------------
  constant TIMING_SYSTEM : unsigned(15 downto 0) := x"0050";
  -- 0 (r)   system clock locked
  -- 1 (r)   external clock locked
  -- 8 (r/w) fmc clock source

  ----------------------------
  -- TIMING I2c Control
  ----------------------------
  constant TIMING_I2C_Control : unsigned(15 downto 0) := x"0051";
  -- 0 (a) run
  -- 1 (r/w) r/w
  -- 3 (r) available
  -- 4 (r/w) reset
  -- 5 (r) error
  -- 10..8 (r/w) byte count
  -- 23..16 (r/w) address
  -- 30..24 (r/w) i2c addr
  
  ----------------------------
  -- TIMING I2c write
  ----------------------------
  constant TIMING_I2C_WR_DATA : unsigned(15 downto 0) := x"0052";
  -- 31..0 (r/w) write data

  ----------------------------
  -- TIMING I2C read
  ----------------------------
  constant TIMING_I2C_RD_DATA : unsigned(15 downto 0) := x"0053";
  -- 31..0 (r) read data

  
  ----------------------------
  -- HSM1 control
  ----------------------------
  constant HSM1_USER : unsigned(15 downto 0) := x"0060";
  -- 15..0 (a)   trigger message of type
  -- 16    (a)   reset counters

  ----------------------------
  -- HSM2 control
  ----------------------------
  constant HSM2_USER : unsigned(15 downto 0) := x"0061";
  -- 15..0 (a)   trigger message of type
  -- 16    (a)   reset counters

  ----------------------------
  -- HSM3 control
  ----------------------------
  constant HSM3_USER : unsigned(15 downto 0) := x"0062";
  -- 15..0 (a)   trigger message of type
  -- 16    (a)   reset counters

  ----------------------------
  -- HSM4 control
  ----------------------------
  constant HSM4_USER : unsigned(15 downto 0) := x"0063";
  -- 15..0 (a)   trigger message of type
  -- 16    (a)   reset counters

  ----------------------------
  -- HSM1 counter
  ----------------------------
  constant HSM1_COUNTER_1 : unsigned(15 downto 0) := x"0070";
  constant HSM1_COUNTER_2 : unsigned(15 downto 0) := x"0071";
  constant HSM1_COUNTER_3 : unsigned(15 downto 0) := x"0072";
  constant HSM1_COUNTER_4 : unsigned(15 downto 0) := x"0073";
  constant HSM1_COUNTER_5 : unsigned(15 downto 0) := x"0074";
  constant HSM1_COUNTER_6 : unsigned(15 downto 0) := x"0075";
  constant HSM1_COUNTER_7 : unsigned(15 downto 0) := x"0076";
  constant HSM1_COUNTER_8 : unsigned(15 downto 0) := x"0077";
  constant HSM1_COUNTER_9 : unsigned(15 downto 0) := x"0078";
  constant HSM1_COUNTER_10 : unsigned(15 downto 0) := x"0079";
  constant HSM1_COUNTER_11 : unsigned(15 downto 0) := x"007a";
  constant HSM1_COUNTER_12 : unsigned(15 downto 0) := x"007b";
  constant HSM1_COUNTER_13 : unsigned(15 downto 0) := x"007c";
  constant HSM1_COUNTER_14 : unsigned(15 downto 0) := x"007d";
  constant HSM1_COUNTER_15 : unsigned(15 downto 0) := x"007e";
  constant HSM1_COUNTER_16 : unsigned(15 downto 0) := x"007f";
  -- 31..0 (r)   message count

  ----------------------------
  -- HSM1 RX counter
  ----------------------------
  constant HSM_RX_1_CNT_1   : unsigned(15 downto 0) := x"0080";
  constant HSM_RX_1_CNT_2   : unsigned(15 downto 0) := x"0081";
  constant HSM_RX_1_CNT_3   : unsigned(15 downto 0) := x"0082";
  constant HSM_RX_1_CNT_4   : unsigned(15 downto 0) := x"0083";
  constant HSM_RX_1_CNT_5   : unsigned(15 downto 0) := x"0084";
  constant HSM_RX_1_CNT_6   : unsigned(15 downto 0) := x"0085";
  constant HSM_RX_1_CNT_7   : unsigned(15 downto 0) := x"0086";
  constant HSM_RX_1_CNT_8   : unsigned(15 downto 0) := x"0087";
  constant HSM_RX_1_CNT_9   : unsigned(15 downto 0) := x"0088";
  constant HSM_RX_1_CNT_10  : unsigned(15 downto 0) := x"0089";
  constant HSM_RX_1_CNT_11  : unsigned(15 downto 0) := x"008A";
  constant HSM_RX_1_CNT_12  : unsigned(15 downto 0) := x"008B";
  constant HSM_RX_1_CNT_13  : unsigned(15 downto 0) := x"008C";
  constant HSM_RX_1_CNT_14  : unsigned(15 downto 0) := x"008D";
  constant HSM_RX_1_CNT_15  : unsigned(15 downto 0) := x"008E";
  constant HSM_RX_1_CNT_16  : unsigned(15 downto 0) := x"008F";


  ----------------------------
  -- HSM2 counter
  ----------------------------
  constant HSM2_COUNTER_1 : unsigned(15 downto 0) := x"0090";
  constant HSM2_COUNTER_2 : unsigned(15 downto 0) := x"0091";
  constant HSM2_COUNTER_3 : unsigned(15 downto 0) := x"0092";
  constant HSM2_COUNTER_4 : unsigned(15 downto 0) := x"0093";
  constant HSM2_COUNTER_5 : unsigned(15 downto 0) := x"0094";
  constant HSM2_COUNTER_6 : unsigned(15 downto 0) := x"0095";
  constant HSM2_COUNTER_7 : unsigned(15 downto 0) := x"0096";
  constant HSM2_COUNTER_8 : unsigned(15 downto 0) := x"0097";
  constant HSM2_COUNTER_9 : unsigned(15 downto 0) := x"0098";
  constant HSM2_COUNTER_10 : unsigned(15 downto 0) := x"0099";
  constant HSM2_COUNTER_11 : unsigned(15 downto 0) := x"009a";
  constant HSM2_COUNTER_12 : unsigned(15 downto 0) := x"009b";
  constant HSM2_COUNTER_13 : unsigned(15 downto 0) := x"009c";
  constant HSM2_COUNTER_14 : unsigned(15 downto 0) := x"009d";
  constant HSM2_COUNTER_15 : unsigned(15 downto 0) := x"009e";
  constant HSM2_COUNTER_16 : unsigned(15 downto 0) := x"009f";
  -- 31..0 (r)   message count

  ----------------------------
  -- HSM2 RX counter
  ----------------------------
  constant HSM_RX_2_CNT_1   : unsigned(15 downto 0) := x"00a0";
  constant HSM_RX_2_CNT_2   : unsigned(15 downto 0) := x"00a1";
  constant HSM_RX_2_CNT_3   : unsigned(15 downto 0) := x"00a2";
  constant HSM_RX_2_CNT_4   : unsigned(15 downto 0) := x"00a3";
  constant HSM_RX_2_CNT_5   : unsigned(15 downto 0) := x"00a4";
  constant HSM_RX_2_CNT_6   : unsigned(15 downto 0) := x"00a5";
  constant HSM_RX_2_CNT_7   : unsigned(15 downto 0) := x"00a6";
  constant HSM_RX_2_CNT_8   : unsigned(15 downto 0) := x"00a7";
  constant HSM_RX_2_CNT_9   : unsigned(15 downto 0) := x"00a8";
  constant HSM_RX_2_CNT_10  : unsigned(15 downto 0) := x"00a9";
  constant HSM_RX_2_CNT_11  : unsigned(15 downto 0) := x"00aA";
  constant HSM_RX_2_CNT_12  : unsigned(15 downto 0) := x"00aB";
  constant HSM_RX_2_CNT_13  : unsigned(15 downto 0) := x"00aC";
  constant HSM_RX_2_CNT_14  : unsigned(15 downto 0) := x"00aD";
  constant HSM_RX_2_CNT_15  : unsigned(15 downto 0) := x"00aE";
  constant HSM_RX_2_CNT_16  : unsigned(15 downto 0) := x"00aF";

  ----------------------------
  -- HSM3 counter
  ----------------------------
  constant HSM3_COUNTER_1 : unsigned(15 downto 0) := x"00b0";
  constant HSM3_COUNTER_2 : unsigned(15 downto 0) := x"00b1";
  constant HSM3_COUNTER_3 : unsigned(15 downto 0) := x"00b2";
  constant HSM3_COUNTER_4 : unsigned(15 downto 0) := x"00b3";
  constant HSM3_COUNTER_5 : unsigned(15 downto 0) := x"00b4";
  constant HSM3_COUNTER_6 : unsigned(15 downto 0) := x"00b5";
  constant HSM3_COUNTER_7 : unsigned(15 downto 0) := x"00b6";
  constant HSM3_COUNTER_8 : unsigned(15 downto 0) := x"00b7";
  constant HSM3_COUNTER_9 : unsigned(15 downto 0) := x"00b8";
  constant HSM3_COUNTER_10 : unsigned(15 downto 0) := x"00b9";
  constant HSM3_COUNTER_11 : unsigned(15 downto 0) := x"00ba";
  constant HSM3_COUNTER_12 : unsigned(15 downto 0) := x"00bb";
  constant HSM3_COUNTER_13 : unsigned(15 downto 0) := x"00bc";
  constant HSM3_COUNTER_14 : unsigned(15 downto 0) := x"00bd";
  constant HSM3_COUNTER_15 : unsigned(15 downto 0) := x"00be";
  constant HSM3_COUNTER_16 : unsigned(15 downto 0) := x"00bf";
  -- 31..0 (r)   message count

  ----------------------------
  -- HSM3 RX counter
  ----------------------------
  constant HSM_RX_3_CNT_1   : unsigned(15 downto 0) := x"00c0";
  constant HSM_RX_3_CNT_2   : unsigned(15 downto 0) := x"00c1";
  constant HSM_RX_3_CNT_3   : unsigned(15 downto 0) := x"00c2";
  constant HSM_RX_3_CNT_4   : unsigned(15 downto 0) := x"00c3";
  constant HSM_RX_3_CNT_5   : unsigned(15 downto 0) := x"00c4";
  constant HSM_RX_3_CNT_6   : unsigned(15 downto 0) := x"00c5";
  constant HSM_RX_3_CNT_7   : unsigned(15 downto 0) := x"00c6";
  constant HSM_RX_3_CNT_8   : unsigned(15 downto 0) := x"00c7";
  constant HSM_RX_3_CNT_9   : unsigned(15 downto 0) := x"00c8";
  constant HSM_RX_3_CNT_10  : unsigned(15 downto 0) := x"00c9";
  constant HSM_RX_3_CNT_11  : unsigned(15 downto 0) := x"00cA";
  constant HSM_RX_3_CNT_12  : unsigned(15 downto 0) := x"00cB";
  constant HSM_RX_3_CNT_13  : unsigned(15 downto 0) := x"00cC";
  constant HSM_RX_3_CNT_14  : unsigned(15 downto 0) := x"00cD";
  constant HSM_RX_3_CNT_15  : unsigned(15 downto 0) := x"00cE";
  constant HSM_RX_3_CNT_16  : unsigned(15 downto 0) := x"00cF";

    ----------------------------
  -- HSM4 counter
  ----------------------------
  constant HSM4_COUNTER_1 : unsigned(15 downto 0) := x"00d0";
  constant HSM4_COUNTER_2 : unsigned(15 downto 0) := x"00d1";
  constant HSM4_COUNTER_3 : unsigned(15 downto 0) := x"00d2";
  constant HSM4_COUNTER_4 : unsigned(15 downto 0) := x"00d3";
  constant HSM4_COUNTER_5 : unsigned(15 downto 0) := x"00d4";
  constant HSM4_COUNTER_6 : unsigned(15 downto 0) := x"00d5";
  constant HSM4_COUNTER_7 : unsigned(15 downto 0) := x"00d6";
  constant HSM4_COUNTER_8 : unsigned(15 downto 0) := x"00d7";
  constant HSM4_COUNTER_9 : unsigned(15 downto 0) := x"00d8";
  constant HSM4_COUNTER_10 : unsigned(15 downto 0) := x"00d9";
  constant HSM4_COUNTER_11 : unsigned(15 downto 0) := x"00da";
  constant HSM4_COUNTER_12 : unsigned(15 downto 0) := x"00db";
  constant HSM4_COUNTER_13 : unsigned(15 downto 0) := x"00dc";
  constant HSM4_COUNTER_14 : unsigned(15 downto 0) := x"00dd";
  constant HSM4_COUNTER_15 : unsigned(15 downto 0) := x"00de";
  constant HSM4_COUNTER_16 : unsigned(15 downto 0) := x"00df";
  -- 31..0 (r)   message count

  ----------------------------
  -- HSM4 RX counter
  ----------------------------
  constant HSM_RX_4_CNT_1   : unsigned(15 downto 0) := x"00e0";
  constant HSM_RX_4_CNT_2   : unsigned(15 downto 0) := x"00e1";
  constant HSM_RX_4_CNT_3   : unsigned(15 downto 0) := x"00e2";
  constant HSM_RX_4_CNT_4   : unsigned(15 downto 0) := x"00e3";
  constant HSM_RX_4_CNT_5   : unsigned(15 downto 0) := x"00e4";
  constant HSM_RX_4_CNT_6   : unsigned(15 downto 0) := x"00e5";
  constant HSM_RX_4_CNT_7   : unsigned(15 downto 0) := x"00e6";
  constant HSM_RX_4_CNT_8   : unsigned(15 downto 0) := x"00e7";
  constant HSM_RX_4_CNT_9   : unsigned(15 downto 0) := x"00e8";
  constant HSM_RX_4_CNT_10  : unsigned(15 downto 0) := x"00e9";
  constant HSM_RX_4_CNT_11  : unsigned(15 downto 0) := x"00eA";
  constant HSM_RX_4_CNT_12  : unsigned(15 downto 0) := x"00eB";
  constant HSM_RX_4_CNT_13  : unsigned(15 downto 0) := x"00eC";
  constant HSM_RX_4_CNT_14  : unsigned(15 downto 0) := x"00eD";
  constant HSM_RX_4_CNT_15  : unsigned(15 downto 0) := x"00eE";
  constant HSM_RX_4_CNT_16  : unsigned(15 downto 0) := x"00eF";
  -- 31..0 (r)   message count
  
  
  ----------------------------
  -- ADC Board # IO STATUS
  ----------------------------
  constant ADC_BOARD_4_IO_STATUS : unsigned(15 downto 0) := x"0400";
  constant ADC_BOARD_3_IO_STATUS : unsigned(15 downto 0) := x"0300";
  constant ADC_BOARD_2_IO_STATUS : unsigned(15 downto 0) := x"0200";
  constant ADC_BOARD_1_IO_STATUS : unsigned(15 downto 0) := x"0100";
  -- 0 (r/w) rx clock reset
  -- 1 (r/w) rx io reset 
  -- 2 (r/w) tx clock reset
  -- 3 (r/w) tx io reset 
  -- 4 (r)   rx locked

  ----------------------------
  -- ADC Board # Comm Rx 
  ----------------------------
  constant ADC_BOARD_4_COMM_RX : unsigned(15 downto 0) := x"0410";
  constant ADC_BOARD_3_COMM_RX : unsigned(15 downto 0) := x"0310";
  constant ADC_BOARD_2_COMM_RX : unsigned(15 downto 0) := x"0210";
  constant ADC_BOARD_1_COMM_RX : unsigned(15 downto 0) := x"0110";
  -- 6..0 (r/i) rx 7b ASCII char
  -- 8    (r/i) rx valid
  
  ----------------------------
  -- ADC Board # RX_DELAY
  ----------------------------
  constant ADC_BOARD_4_COMM_RX_DELAY : unsigned(15 downto 0) := x"0411";
  constant ADC_BOARD_3_COMM_RX_DELAY : unsigned(15 downto 0) := x"0311";
  constant ADC_BOARD_2_COMM_RX_DELAY : unsigned(15 downto 0) := x"0211";
  constant ADC_BOARD_1_COMM_RX_DELAY : unsigned(15 downto 0) := x"0111";
  -- 0 (r/w) reset
  -- 1 (a)   ce
  -- 2 (r/w) incr
  -- 3 (r)   locked
  -- 8..4 (r) tap out

  ----------------------------
  -- ADC Board # RX_DELAY_TAP
  ----------------------------
  constant ADC_BOARD_4_COMM_RX_DELAY_TAP : unsigned(15 downto 0) := x"0412";
  constant ADC_BOARD_3_COMM_RX_DELAY_TAP : unsigned(15 downto 0) := x"0312";
  constant ADC_BOARD_2_COMM_RX_DELAY_TAP : unsigned(15 downto 0) := x"0212";
  constant ADC_BOARD_1_COMM_RX_DELAY_TAP : unsigned(15 downto 0) := x"0112";
  -- 4..0 (r/w) tap in

  ----------------------------
  -- ADC Board # rx 10b char
  ----------------------------
  constant ADC_BOARD_4_COMM_RX_10B_DATA : unsigned(15 downto 0) := x"0413";
  constant ADC_BOARD_3_COMM_RX_10B_DATA : unsigned(15 downto 0) := x"0313";
  constant ADC_BOARD_2_COMM_RX_10B_DATA : unsigned(15 downto 0) := x"0213";
  constant ADC_BOARD_1_COMM_RX_10B_DATA : unsigned(15 downto 0) := x"0113";
  -- 9..0 (r) rx 10b data
  
  ----------------------------
  -- ADC Board # Comm Rx 
  ----------------------------
  constant ADC_BOARD_4_COMM_RX_STATS : unsigned(15 downto 0) := x"0415";
  constant ADC_BOARD_3_COMM_RX_STATS : unsigned(15 downto 0) := x"0315";
  constant ADC_BOARD_2_COMM_RX_STATS : unsigned(15 downto 0) := x"0215";
  constant ADC_BOARD_1_COMM_RX_STATS : unsigned(15 downto 0) := x"0115";
  -- 15..0  (r/a) rx uart counter (reset on write)
  -- 31..16 (r/a) rx hs counter (reset on write)

  ----------------------------
  -- ADC Board # rx char errors
  ----------------------------
  constant ADC_BOARD_4_COMM_RX_CODE_ERROR : unsigned(15 downto 0) := x"0416";
  constant ADC_BOARD_3_COMM_RX_CODE_ERROR : unsigned(15 downto 0) := x"0316";
  constant ADC_BOARD_2_COMM_RX_CODE_ERROR : unsigned(15 downto 0) := x"0216";
  constant ADC_BOARD_1_COMM_RX_CODE_ERROR : unsigned(15 downto 0) := x"0116";
  -- 31..0 (r/a) code error counter / reset
  
  ----------------------------
  -- ADC Board # rx disp errors
  ----------------------------
  constant ADC_BOARD_4_COMM_RX_DISP_ERROR : unsigned(15 downto 0) := x"0417";
  constant ADC_BOARD_3_COMM_RX_DISP_ERROR : unsigned(15 downto 0) := x"0317";
  constant ADC_BOARD_2_COMM_RX_DISP_ERROR : unsigned(15 downto 0) := x"0217";
  constant ADC_BOARD_1_COMM_RX_DISP_ERROR : unsigned(15 downto 0) := x"0117";
  -- 31..0 (r/a) disp error counter / reset

  ----------------------------
  -- ADC Board # Comm Rx Data rate
  ----------------------------
  constant ADC_BOARD_4_COMM_RX_DATA_RATE : unsigned(15 downto 0) := x"0418";
  constant ADC_BOARD_3_COMM_RX_DATA_RATE : unsigned(15 downto 0) := x"0318";
  constant ADC_BOARD_2_COMM_RX_DATA_RATE : unsigned(15 downto 0) := x"0218";
  constant ADC_BOARD_1_COMM_RX_DATA_RATE : unsigned(15 downto 0) := x"0118";
  -- 31..0  (r) HS data rate

  ----------------------------
  -- ADC Board # Comm Rx Comm rate
  ----------------------------
  constant ADC_BOARD_4_COMM_RX_COMM_RATE : unsigned(15 downto 0) := x"0419";
  constant ADC_BOARD_3_COMM_RX_COMM_RATE : unsigned(15 downto 0) := x"0319";
  constant ADC_BOARD_2_COMM_RX_COMM_RATE : unsigned(15 downto 0) := x"0219";
  constant ADC_BOARD_1_COMM_RX_COMM_RATE : unsigned(15 downto 0) := x"0119";
  -- 31..0  (r) HS data rate

  ----------------------------
  -- ADC Board # Comm Rx HS message
  ----------------------------
  constant ADC_BOARD_4_COMM_RX_MESG_STATS : unsigned(15 downto 0) := x"041A";
  constant ADC_BOARD_3_COMM_RX_MESG_STATS : unsigned(15 downto 0) := x"031A";
  constant ADC_BOARD_2_COMM_RX_MESG_STATS : unsigned(15 downto 0) := x"021A";
  constant ADC_BOARD_1_COMM_RX_MESG_STATS : unsigned(15 downto 0) := x"011A";
  -- 15..0  (r/a) rx hs message counter (reset on write)
  
  
  ----------------------------
  -- ADC Board # Comm Tx 
  ----------------------------
  constant ADC_BOARD_4_COMM_TX : unsigned(15 downto 0) := x"0420";
  constant ADC_BOARD_3_COMM_TX : unsigned(15 downto 0) := x"0320";
  constant ADC_BOARD_2_COMM_TX : unsigned(15 downto 0) := x"0220";
  constant ADC_BOARD_1_COMM_TX : unsigned(15 downto 0) := x"0120";
  -- 6..0 (a) tx 7b ASCII char
  -- 8    (r) tx empty

  ----------------------------
  -- ADC Board # HS Tx 
  ----------------------------
  constant ADC_BOARD_4_HS_TX : unsigned(15 downto 0) := x"0421";
  constant ADC_BOARD_3_HS_TX : unsigned(15 downto 0) := x"0321";
  constant ADC_BOARD_2_HS_TX : unsigned(15 downto 0) := x"0221";
  constant ADC_BOARD_1_HS_TX : unsigned(15 downto 0) := x"0121";
  -- 3..0 (a) tx 4b hs data
  -- 8    (r) tx empty

  ----------------------------
  -- ADC Board # Comm Tx 
  ----------------------------
  constant ADC_BOARD_4_COMM_TX_STATS : unsigned(15 downto 0) := x"0425";
  constant ADC_BOARD_3_COMM_TX_STATS : unsigned(15 downto 0) := x"0325";
  constant ADC_BOARD_2_COMM_TX_STATS : unsigned(15 downto 0) := x"0225";
  constant ADC_BOARD_1_COMM_TX_STATS : unsigned(15 downto 0) := x"0125";
  -- 15..0  (r/a) tx uart counter (reset on write)
  -- 31..16 (r/a) tx hs counter (reset on write)

  ----------------------------
  -- ADC Board # Comm Tx Data rate
  ----------------------------
  constant ADC_BOARD_4_COMM_TX_DATA_RATE : unsigned(15 downto 0) := x"0428";
  constant ADC_BOARD_3_COMM_TX_DATA_RATE : unsigned(15 downto 0) := x"0328";
  constant ADC_BOARD_2_COMM_TX_DATA_RATE : unsigned(15 downto 0) := x"0228";
  constant ADC_BOARD_1_COMM_TX_DATA_RATE : unsigned(15 downto 0) := x"0128";
  -- 31..0  (r) HS data rate

  ----------------------------
  -- ADC Board # Comm Tx Comm rate
  ----------------------------
  constant ADC_BOARD_4_COMM_TX_COMM_RATE : unsigned(15 downto 0) := x"0429";
  constant ADC_BOARD_3_COMM_TX_COMM_RATE : unsigned(15 downto 0) := x"0329";
  constant ADC_BOARD_2_COMM_TX_COMM_RATE : unsigned(15 downto 0) := x"0229";
  constant ADC_BOARD_1_COMM_TX_COMM_RATE : unsigned(15 downto 0) := x"0129";
  -- 31..0  (r) HS data rate


  ----------------------------
  -- Global
  ----------------------------  
  constant GLOBAL_STATUS : unsigned(15 downto 0) := x"5000";
  -- 0 (r/w) enable all pulsers
  -- 1 (r/w) enable all WFDs
  -- 2 (r/w) disable fault shutdown
  -- 4 (a)   overall reset
  -- 5 (a)   reset after fault
  -- 8 (r)   fault seen
  -- 16 (r/w) allow ext triggers
  -- 17 (r/w) allow free run triggers


  ----------------------------
  -- Global trigger rate
  ----------------------------  
  constant GLOBAL_TRIG_RATE : unsigned(15 downto 0) := x"5001";
  -- 31..0 (r) rate (Hz)

  ----------------------------
  -- Global trigger count
  ----------------------------  
  constant GLOBAL_TRIG_COUNT : unsigned(15 downto 0) := x"5002";
  -- 31..0 (r/a) count

  ----------------------------
  -- Global trigger count control
  ----------------------------  
  constant GLOBAL_TRIG_COUNT_CONTROL : unsigned(15 downto 0) := x"5003";
  -- 0 (r/w) reset trig count
  
  ----------------------------
  -- Global Free-run status
  ----------------------------  
  constant GLOBAL_FR_STATUS : unsigned(15 downto 0) := x"5010";
  -- 0 (r/w) enable free-running mode 
  -- 1 (r/w) select burst mode        
  -- 4 (a) force trigger              

  ----------------------------
  -- Global Free-run period
  ----------------------------  
  constant GLOBAL_FR_PER : unsigned(15 downto 0) := x"5011";
  -- 15..0 (r/w) delay between triggers/bursts (us)
  
  ----------------------------
  -- Global Free-run burst mask
  ----------------------------  
  constant GLOBAL_FR_BURST_MASK : unsigned(15 downto 0) := x"5012";
  -- 31..0 (r/w) burst mask 1-32 bursts

  ----------------------------
  -- Global Free-run burst spacing
  ----------------------------  
  constant GLOBAL_FR_BURST_SPACING : unsigned(15 downto 0) := x"5013";
  -- 15..0 (r/w) burst spacing

  ----------------------------
  -- Global extra pulses CTRL
  ----------------------------  
  constant GLOBAL_EXTRA_PULSER1_CTRL : unsigned(15 downto 0) := x"5020";
  -- 23..16 (r/w) pulse enables
  --     0 (r/w)  bypass extra pulses

  ---------------------------
  -- Global extra pulses config
  ----------------------------  
  constant GLOBAL_EXTRA_PULSER1_0    : unsigned(15 downto 0) := x"5021";
  constant GLOBAL_EXTRA_PULSER1_1    : unsigned(15 downto 0) := x"5022";
  constant GLOBAL_EXTRA_PULSER1_2    : unsigned(15 downto 0) := x"5023";
  constant GLOBAL_EXTRA_PULSER1_3    : unsigned(15 downto 0) := x"5024";
  constant GLOBAL_EXTRA_PULSER1_4    : unsigned(15 downto 0) := x"5025";
  constant GLOBAL_EXTRA_PULSER1_5    : unsigned(15 downto 0) := x"5026";
  constant GLOBAL_EXTRA_PULSER1_6    : unsigned(15 downto 0) := x"5027";
  constant GLOBAL_EXTRA_PULSER1_7    : unsigned(15 downto 0) := x"5028";
  -- 31..0 (r/w) pulser counter time

  ----------------------------
  -- Global extra pulses CTRL
  ----------------------------  
  constant GLOBAL_EXTRA_PULSER2_CTRL : unsigned(15 downto 0) := x"5030";
  -- 24..16 (r/w) pulse enables
  --     0 (r/w)  bypass extra pulses

  ---------------------------
  -- Global extra pulses config
  ----------------------------  
  constant GLOBAL_EXTRA_PULSER2_0    : unsigned(15 downto 0) := x"5031";
  constant GLOBAL_EXTRA_PULSER2_1    : unsigned(15 downto 0) := x"5032";
  constant GLOBAL_EXTRA_PULSER2_2    : unsigned(15 downto 0) := x"5033";
  constant GLOBAL_EXTRA_PULSER2_3    : unsigned(15 downto 0) := x"5034";
  constant GLOBAL_EXTRA_PULSER2_4    : unsigned(15 downto 0) := x"5035";
  constant GLOBAL_EXTRA_PULSER2_5    : unsigned(15 downto 0) := x"5036";
  constant GLOBAL_EXTRA_PULSER2_6    : unsigned(15 downto 0) := x"5037";
  constant GLOBAL_EXTRA_PULSER2_7    : unsigned(15 downto 0) := x"5038";
  -- 31..0 (r/w) pulser counter time

  ----------------------------
  -- Global extra pulses CTRL
  ----------------------------  
  constant GLOBAL_EXTRA_PULSER3_CTRL : unsigned(15 downto 0) := x"5040";
  -- 24..16 (r/w) pulse enables
  --     0 (r/w)  bypass extra pulses

  ---------------------------
  -- Global extra pulses config
  ----------------------------  
  constant GLOBAL_EXTRA_PULSER3_0    : unsigned(15 downto 0) := x"5041";
  constant GLOBAL_EXTRA_PULSER3_1    : unsigned(15 downto 0) := x"5042";
  constant GLOBAL_EXTRA_PULSER3_2    : unsigned(15 downto 0) := x"5043";
  constant GLOBAL_EXTRA_PULSER3_3    : unsigned(15 downto 0) := x"5044";
  constant GLOBAL_EXTRA_PULSER3_4    : unsigned(15 downto 0) := x"5045";
  constant GLOBAL_EXTRA_PULSER3_5    : unsigned(15 downto 0) := x"5046";
  constant GLOBAL_EXTRA_PULSER3_6    : unsigned(15 downto 0) := x"5047";
  constant GLOBAL_EXTRA_PULSER3_7    : unsigned(15 downto 0) := x"5048";
  -- 31..0 (r/w) pulser counter time

    ----------------------------
  -- Global extra pulses CTRL
  ----------------------------  
  constant GLOBAL_EXTRA_PULSER4_CTRL : unsigned(15 downto 0) := x"5050";
  -- 24..16 (r/w) pulse enables
  --     0 (r/w)  bypass extra pulses

  ---------------------------
  -- Global extra pulses config
  ----------------------------  
  constant GLOBAL_EXTRA_PULSER4_0    : unsigned(15 downto 0) := x"5051";
  constant GLOBAL_EXTRA_PULSER4_1    : unsigned(15 downto 0) := x"5052";
  constant GLOBAL_EXTRA_PULSER4_2    : unsigned(15 downto 0) := x"5053";
  constant GLOBAL_EXTRA_PULSER4_3    : unsigned(15 downto 0) := x"5054";
  constant GLOBAL_EXTRA_PULSER4_4    : unsigned(15 downto 0) := x"5055";
  constant GLOBAL_EXTRA_PULSER4_5    : unsigned(15 downto 0) := x"5056";
  constant GLOBAL_EXTRA_PULSER4_6    : unsigned(15 downto 0) := x"5057";
  constant GLOBAL_EXTRA_PULSER4_7    : unsigned(15 downto 0) := x"5058";
  -- 31..0 (r/w) pulser counter time

  ----------------------------
  -- ADC Board # PMOD Comm Rx 
  ----------------------------
  constant ADC_BOARD_4_PMOD_COMM_RX : unsigned(15 downto 0) := x"6410";
  constant ADC_BOARD_3_PMOD_COMM_RX : unsigned(15 downto 0) := x"6310";
  constant ADC_BOARD_2_PMOD_COMM_RX : unsigned(15 downto 0) := x"6210";
  constant ADC_BOARD_1_PMOD_COMM_RX : unsigned(15 downto 0) := x"6110";
  -- 7..0 (r/i) rx 7b ASCII char
  -- 8    (r/i) rx valid

  ----------------------------
  -- ADC Board # PMOD Comm Tx 
  ----------------------------
  constant ADC_BOARD_4_PMOD_COMM_TX : unsigned(15 downto 0) := x"6420";
  constant ADC_BOARD_3_PMOD_COMM_TX : unsigned(15 downto 0) := x"6320";
  constant ADC_BOARD_2_PMOD_COMM_TX : unsigned(15 downto 0) := x"6220";
  constant ADC_BOARD_1_PMOD_COMM_TX : unsigned(15 downto 0) := x"6120";
  -- 7..0 (a) tx 7b ASCII char
  -- 8    (r) tx empty


  ----------------------------
  -- EventBuilder
  ----------------------------
  constant EB_STATUS : unsigned(15 downto 0) := x"7000";
  -- 3..0 (r/w) board enable mask
  -- 7..4 (r)   dma state
  -- 29   (a)   reset event builder state machine
  -- 30   (r)   dma ready
  -- 31   (r/w) event builder enable
  
  ----------------------------
  -- DMA BLOCK RATE
  ----------------------------
  constant DMA_BLOCK_RATE : unsigned(15 downto 0) := x"7002";
  -- 31..0 (r) dma block rate (Hz)

  ----------------------------
  -- DMA data RATE
  ----------------------------
  constant DMA_DATA_RATE : unsigned(15 downto 0) := x"7003";
  -- 31..0 (r) dma data rate (Hz)

  ----------------------------
  -- DMA BLOCK COUNT
  ----------------------------
  constant DMA_BLOCK_COUNT : unsigned(15 downto 0) := x"7004";
  -- 31..0 (r/a) dma block count

  ----------------------------
  -- DMA BYTE COUNT
  ----------------------------
  constant DMA_BYTE_COUNT : unsigned(15 downto 0) := x"7005";
  -- 31..0 (r/a) dma byte count

  ----------------------------
  -- DMA IDLE COUNT
  ----------------------------
  constant DMA_IDLE_COUNT : unsigned(15 downto 0) := x"7006";
  -- 31..0 (r) dma idle block count

  ----------------------------
  -- DMA FIFO OPEN
  ----------------------------
  constant DMA_FIFO_OPEN : unsigned(15 downto 0) := x"7007";
  -- 31..0 (r) dma idle block count

  ----------------------------
  -- ADC FRAME 1 FIFO
  ----------------------------
  constant DMA_FRAME_FIFO_DMA_1 : unsigned(15 downto 0) := x"7010";
  constant DMA_FRAME_FIFO_DMA_2 : unsigned(15 downto 0) := x"7020";
  constant DMA_FRAME_FIFO_DMA_3 : unsigned(15 downto 0) := x"7030";
  constant DMA_FRAME_FIFO_DMA_4 : unsigned(15 downto 0) := x"7040";
  -- 0      (r) fifo empty
  -- 20..4  (r) fifo out count
  
  ----------------------------
  -- ADC FRAME 1 DMA
  ----------------------------
  constant DMA_FRAME_DMA_1 : unsigned(15 downto 0) := x"7011";
  constant DMA_FRAME_DMA_2 : unsigned(15 downto 0) := x"7021";
  constant DMA_FRAME_DMA_3 : unsigned(15 downto 0) := x"7031";
  constant DMA_FRAME_DMA_4 : unsigned(15 downto 0) := x"7041";  
  -- 3..0   (r) state

  ----------------------------
  -- ADC FRAME 1 FIFO
  ----------------------------
  constant DMA_FRAME_FIFO_EB_1 : unsigned(15 downto 0) := x"7012";
  constant DMA_FRAME_FIFO_EB_2 : unsigned(15 downto 0) := x"7022";
  constant DMA_FRAME_FIFO_EB_3 : unsigned(15 downto 0) := x"7032";
  constant DMA_FRAME_FIFO_EB_4 : unsigned(15 downto 0) := x"7042";
  -- 0      (r) fifo full
  -- 20..4  (r) fifo in count

  ----------------------------
  -- ADC FRAME 1 EB START
  ----------------------------
  constant DMA_FRAME_EB_START_1 : unsigned(15 downto 0) := x"7013";
  constant DMA_FRAME_EB_START_2 : unsigned(15 downto 0) := x"7023";
  constant DMA_FRAME_EB_START_3 : unsigned(15 downto 0) := x"7033";
  constant DMA_FRAME_EB_START_4 : unsigned(15 downto 0) := x"7043";
  -- 31..0   (r) unexpected starts

  ----------------------------
  -- ADC FRAME 1 EB END
  ----------------------------
  constant DMA_FRAME_EB_END_1 : unsigned(15 downto 0) := x"7014";
  constant DMA_FRAME_EB_END_2 : unsigned(15 downto 0) := x"7024";
  constant DMA_FRAME_EB_END_3 : unsigned(15 downto 0) := x"7034";
  constant DMA_FRAME_EB_END_4 : unsigned(15 downto 0) := x"7044";
  -- 31..0   (r) unexpected ends

  ----------------------------
  -- ADC FRAME 1 FRAME SIZE
  ----------------------------
  constant DMA_FRAME_FRAME_SIZE_1 : unsigned(15 downto 0) := x"7015";
  constant DMA_FRAME_FRAME_SIZE_2 : unsigned(15 downto 0) := x"7025";
  constant DMA_FRAME_FRAME_SIZE_3 : unsigned(15 downto 0) := x"7035";
  constant DMA_FRAME_FRAME_SIZE_4 : unsigned(15 downto 0) := x"7045";
  -- 23..0   (r) most recent frame size
  -- 31      (r) "has data flag"
  
  ----------------------------
  -- ADC FRAME 1 BUFFER COUNT
  ----------------------------
  constant DMA_FRAME_BUFFER_COUNT_1 : unsigned(15 downto 0) := x"7016";
  constant DMA_FRAME_BUFFER_COUNT_2 : unsigned(15 downto 0) := x"7026";
  constant DMA_FRAME_BUFFER_COUNT_3 : unsigned(15 downto 0) := x"7036";
  constant DMA_FRAME_BUFFER_COUNT_4 : unsigned(15 downto 0) := x"7046";
  -- 31..0   (r) buffer count
  
  ----------------------------
  -- ADC FRAME 1 Size error count
  ----------------------------
  constant DMA_FRAME_SIZE_ERRORS_1 : unsigned(15 downto 0) := x"7017";
  constant DMA_FRAME_SIZE_ERRORS_2 : unsigned(15 downto 0) := x"7027";
  constant DMA_FRAME_SIZE_ERRORS_3 : unsigned(15 downto 0) := x"7037";
  constant DMA_FRAME_SIZE_ERRORS_4 : unsigned(15 downto 0) := x"7047";
  -- 31..0   (r) size error count


  ----------------------------
  -- ADC Frame 1 count
  ----------------------------
  constant DMA_FRAME_COUNT_1 : unsigned(15 downto 0) := x"7018";
  constant DMA_FRAME_COUNT_2 : unsigned(15 downto 0) := x"7028";
  constant DMA_FRAME_COUNT_3 : unsigned(15 downto 0) := x"7038";
  constant DMA_FRAME_COUNT_4 : unsigned(15 downto 0) := x"7048";
  -- 31..0   (r) size error count

  ----------------------------
  -- ADC Frame 1 rate
  ----------------------------
  constant DMA_FRAME_RATE_1 : unsigned(15 downto 0) := x"7019";
  constant DMA_FRAME_RATE_2 : unsigned(15 downto 0) := x"7029";
  constant DMA_FRAME_RATE_3 : unsigned(15 downto 0) := x"7039";
  constant DMA_FRAME_RATE_4 : unsigned(15 downto 0) := x"7049";
  -- 31..0   (r) size error count

  
  ----------------------------
  -- Test counter clk 0
  ----------------------------  
  constant REG_TEST_COUNTER_0 : unsigned(15 downto 0) := x"FFE0";
  -- 31..0 (r/w) test

  ----------------------------
  -- Test counter clk 1
  ----------------------------  
  constant REG_TEST_COUNTER_1 : unsigned(15 downto 0) := x"FFE1";
  -- 31..0 (r/w) test

  ----------------------------
  -- Test register clk 0
  ----------------------------  
  constant REG_TEST_0 : unsigned(15 downto 0) := x"FFF0";
  -- 31..0 (r/w) test

  ----------------------------
  -- Test register clk 1
  ----------------------------  
  constant REG_TEST_1 : unsigned(15 downto 0) := x"FFF1";
  -- 31..0 (r/w) test

  ----------------------------
  -- clk locked
  ----------------------------  
  constant REG_LOCKED : unsigned(15 downto 0) := x"FFFC";
  -- 1..0 (r) clock domain locked
  
  ----------------------------
  -- reg read count
  ----------------------------  
  constant REG_RD_COUNT : unsigned(15 downto 0) := x"FFFD";
  -- 31..0 (r/w) register reads

  ----------------------------
  -- reg write count
  ----------------------------  
  constant REG_WR_COUNT : unsigned(15 downto 0) := x"FFFE";
  -- 31..0 (r/w) register write 



  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------
  constant CLOCK_DOMAINS       : integer := 2;
  signal read_address_valid    : std_logic_vector(CLOCK_DOMAINS-1 downto 0);
  signal read_address_ack      : std_logic_vector(CLOCK_DOMAINS-1 downto 0);
  signal read_address          : slv16_array_t(CLOCK_DOMAINS-1 downto 0);
  signal read_address_cap      : u16_array_t(CLOCK_DOMAINS-1 downto 0);
  signal read_data_wr          : std_logic_vector(CLOCK_DOMAINS-1 downto 0);
  signal read_data             : slv36_array_t(CLOCK_DOMAINS-1 downto 0);
  signal write_addr_data_valid : std_logic_vector(CLOCK_DOMAINS-1 downto 0);
  signal write_addr_data_ack   : std_logic_vector(CLOCK_DOMAINS-1 downto 0);
  signal write_addr            : slv16_array_t(CLOCK_DOMAINS-1 downto 0);
  signal write_addr_cap        : u16_array_t(CLOCK_DOMAINS-1 downto 0);
  signal write_data            : slv32_array_t(CLOCK_DOMAINS-1 downto 0);
  signal write_data_cap        : slv32_array_t(CLOCK_DOMAINS-1 downto 0);

  signal clk_domain            : std_logic_vector(CLOCK_DOMAINS-1 downto 0);
  signal clk_domain_locked     : std_logic_vector(CLOCK_DOMAINS-1 downto 0);
  signal reset_sync_domain     : std_logic_vector(CLOCK_DOMAINS-1 downto 0);      
                               
  signal rd_ack                : std_logic;
  signal wr_ack                : std_logic;
  
  --state machines
  type READ_STATE_t is (READ_STATE_IDLE, READ_STATE_READ, READ_STATE_CAPTURE_ADDRESS, READ_STATE_ADDRESS_HELPERS, READ_STATE_WRITE);
  type READ_STATE_array_t is array (CLOCK_DOMAINS-1 downto 0) of READ_STATE_t;
  signal READ_state            : READ_STATE_array_t := (others => READ_STATE_IDLE);

  type WRITE_STATE_t is (WRITE_STATE_IDLE, WRITE_STATE_CAPTURE_ADDRESS,WRITE_STATE_ADDRESS_HELPERS, WRITE_STATE_WRITE);
  type WRITE_STATE_array_t is array (CLOCK_DOMAINS-1 downto 0) of WRITE_STATE_t;
  signal WRITE_state           : WRITE_STATE_array_t := (others => WRITE_STATE_IDLE);

  --helpers
  signal fmc_read_index        : integer range 3 downto 0;
  signal fmc_wr_index          : integer range 3 downto 0;
  signal pmod_read_index       : integer range 3 downto 0;
  signal pmod_wr_index         : integer range 3 downto 0;
  
  --testing
  type test_reg_t is array (CLOCK_DOMAINS-1 downto 0) of std_logic_vector(31 downto 0);
  signal test_reg              : test_reg_t := (others => x"deadbeef");
  signal test_counter          : test_reg_t;
  
  signal read_count            : std_logic_vector(31 downto 0) := x"00000000";
  signal write_count           : std_logic_vector(31 downto 0) := x"00000000";

begin  -- architecture Behavioral

  -------------------------------------------------------------------------------------
  -- clock domain bridge interface
  -------------------------------------------------------------------------------------
  clk_domain(0)        <= clk_sys;
  clk_domain(1)        <= clk_200Mhz;
  clk_domain_locked(0) <= locked_sys;
  clk_domain_locked(1) <= locked_200Mhz;

  counter_proc: for iClk in CLOCK_DOMAINS-1 downto 0 generate
    counter_17: entity work.counter
      generic map (
        roll_over   => '1')
      port map (
        clk         => clk_domain(iClk),
        reset_async => reset,
        reset_sync  => '0',
        enable      => '1',
        event       => '1',
        count       => test_counter(iClk),
        at_max      => open);
  end generate counter_proc;
  
  reset_proc: for iClk in CLOCK_DOMAINS-1 downto 0 generate
    resetter_2: entity work.resetter
      port map (
        clk         => clk_domain(iClk),
        reset_async => reset,
        reset_sync  => '0',
        reset       => reset_sync_domain(iClk));
  end generate reset_proc;

  
  data_valid_proc: process (clk_sys,reset) is
  begin  -- process data_valid_proc
    if reset = '1' then
      data_out_valid <= '0';
    elsif clk_sys'event and clk_sys = '1' then  -- rising clock edge
      if WR_strb = '1' or RD_strb = '1' then
        data_out_valid <= '0';
      elsif rd_ack = '1' then
        data_out_valid <= '1';
      end if;
    end if;
  end process data_valid_proc;
  
  register_map_bridge_1 : entity work.register_map_bridge
    generic map (
      CLOCK_DOMAINS => CLOCK_DOMAINS)
    port map (
      clk_reg_map           => clk_sys,
      reset                 => reset,
      WR_strobe             => WR_strb,
      RD_strobe             => RD_strb,
      WR_address            => WR_address,
      RD_address            => RD_address,
      data_in               => data_in,
      data_out              => data_out,
      rd_ack                => rd_ack,
      wr_ack                => wr_ack,
      clk_domain            => clk_domain,
      clk_domain_locked     => clk_domain_locked,
      read_address_valid    => read_address_valid,
      read_address_ack      => read_address_ack,
      read_address          => read_address,
      read_data_wr          => read_data_wr,
      read_data             => read_data,
      write_addr_data_valid => write_addr_data_valid,
      write_addr_data_ack   => write_addr_data_ack,
      write_addr            => write_addr,
      write_data            => write_data);



  -------------------------------------------------------------------------------------
  -- SYS clk 100Mhz
  -------------------------------------------------------------------------------------
  REGS_sys_clk_100Mhz : process (clk_domain(0)) is
  begin  -- process REGS_sys_clk_100Mhz
    if clk_domain(0)'event and clk_domain(0) = '1' then  -- rising clock edge
      if reset_sync_domain(0) = '1' then 
        test_reg(0)     <= x"deadbeef";
        carrier_control <= DEFAULT_CARRIER_CONTROL;
        eb_control      <= DEFAULT_EB_CONTROL;
      else      
        -------------------------------------------------------
        -- Read
        -------------------------------------------------------
        read_address_ack(0) <= '0';
        read_data_wr(0)     <= '0';
        
        case READ_state(0) is
          when READ_STATE_IDLE =>
            if read_address_valid(0) = '1' then
              read_address_ack(0) <= '1';
              READ_state(0)       <= READ_STATE_CAPTURE_ADDRESS;

              -- capture the address we will be using
              read_address_cap(0) <= unsigned(read_address(0));
            end if;
          when READ_STATE_CAPTURE_ADDRESS =>
            -- extra clock tick for multicycle path of address
            READ_state(0)       <= READ_STATE_READ;
          when READ_STATE_READ =>
            READ_state(0) <= READ_STATE_WRITE;

            read_data(0) <= x"100000000";  --Mark the data as zeros and the
                                           --register found                
            case read_address_cap(0) is
              when CARRIER_STATUS        =>
                read_data(0)(4)            <= carrier_monitor.reset_pl_clock;
                read_data(0)(5)            <= carrier_monitor.locked_pl_clock;
              when CARRIER_LEDS          =>
                read_data(0)( 3 downto  0) <= carrier_monitor.LED_mode;
                read_data(0)( 7 downto  4) <= carrier_monitor.LEDs;
                read_data(0)(11 downto  8) <= carrier_monitor.LED_GPO;
              when CARRIER_EXT_TRIG_RX =>
                read_data(0)(31 downto  0) <= carrier_monitor.external_spark_recv_count;
              when CARRIER_EXT_TRIG_TX =>
                read_data(0)(31 downto  0) <= carrier_monitor.external_spark_sent_count;
              when CARRIER_FW_VERSION    =>
                read_data(0)(31 downto  0) <= FIRMWARE_VERSION;
              when CARRIER_SYNTH_DATE    =>
                read_data(0)(31 downto  0) <= TS_CENT & TS_YEAR & TS_MONTH & TS_DAY;
              when CARRIER_SYNTH_TIME    =>
                read_data(0)(31 downto  0) <= x"00" & TS_HOUR & TS_MIN & TS_SEC;
              when TIMING_SYSTEM =>
                read_data(0)(0)            <= timing_system_monitor.sys_locked;
                read_data(0)(1)            <= timing_system_monitor.fmc_clk_locked;
                read_data(0)(8)            <= timing_system_monitor.clk_source;
              when TIMING_I2C_CONTROL      =>
                read_data(0)(1)               <= timing_system_monitor.i2c.rw;
                read_data(0)(3)               <= timing_system_monitor.i2c.done;
                read_data(0)(4)               <= timing_system_monitor.i2c.reset;
                read_data(0)(5)               <= timing_system_monitor.i2c.error;
                read_data(0)(10 downto 8)     <= timing_system_monitor.i2c.byte_count;
                read_data(0)(23 downto 16)    <= timing_system_monitor.i2c.reg_addr;
                read_data(0)(30 downto 24)    <= timing_system_monitor.i2c.addr;
              when TIMING_I2C_WR_DATA      =>
                read_data(0)(31 downto 0)     <= timing_system_monitor.i2c.wr_data;
              when TIMING_I2C_RD_DATA      =>
                read_data(0)(31 downto 0)     <= timing_system_monitor.i2c.rd_data;

              when ADC_BOARD_POWER_ENABLE =>
                read_data(0)( 3 downto  0) <= carrier_monitor.adc_power_en;
              when EB_STATUS =>
                read_data(0)( 3 downto  0) <= eb_monitor.board_enable;
                read_data(0)( 7 downto  4) <= eb_monitor.state;
                read_data(0)(30)           <= eb_monitor.dma_ready;
                read_data(0)(31)           <= eb_monitor.enable;
              when DMA_BLOCK_RATE =>
                read_data(0)(31 downto  0) <= eb_monitor.block_rate;
              when DMA_DATA_RATE  =>
                read_data(0)(31 downto  0) <= eb_monitor.data_rate;
              when DMA_BLOCK_COUNT  =>
                read_data(0)(31 downto  0) <= eb_monitor.block_count;
              when DMA_BYTE_COUNT  =>
                read_data(0)(31 downto  0) <= eb_monitor.byte_count;
              when DMA_IDLE_COUNT  =>
                read_data(0)(31 downto  0) <= eb_monitor.idle_block_count;
              when DMA_FIFO_OPEN   =>
                read_data(0)(31 downto  0) <= eb_monitor.dma_fifo_open;
              when DMA_FRAME_FIFO_DMA_1 =>
                read_data(0)(0)            <= eb_monitor.ADCFrame(0).DAQ_FIFO_empty;
                read_data(0)(20 downto  4) <= eb_monitor.ADCFrame(0).DAQ_FIFO_out_count;
              when DMA_FRAME_DMA_1 =>
                read_data(0)(3 downto 0)            <= eb_monitor.ADCFrame(0).DMA.state;                
              when DMA_FRAME_FIFO_DMA_2 =>
                read_data(0)(0)            <= eb_monitor.ADCFrame(1).DAQ_FIFO_empty;
                read_data(0)(20 downto  4) <= eb_monitor.ADCFrame(1).DAQ_FIFO_out_count;
              when DMA_FRAME_DMA_2 =>
                read_data(0)(3 downto 0)            <= eb_monitor.ADCFrame(1).DMA.state;                
              when DMA_FRAME_FIFO_DMA_3 =>
                read_data(0)(0)            <= eb_monitor.ADCFrame(2).DAQ_FIFO_empty;
                read_data(0)(20 downto  4) <= eb_monitor.ADCFrame(2).DAQ_FIFO_out_count;
              when DMA_FRAME_DMA_3 =>
                read_data(0)(3 downto 0)            <= eb_monitor.ADCFrame(2).DMA.state;                
              when DMA_FRAME_FIFO_DMA_4 =>
                read_data(0)(0)            <= eb_monitor.ADCFrame(3).DAQ_FIFO_empty;
                read_data(0)(20 downto  4) <= eb_monitor.ADCFrame(3).DAQ_FIFO_out_count;
              when DMA_FRAME_DMA_4 =>
                read_data(0)(3 downto 0)            <= eb_monitor.ADCFrame(3).DMA.state;                
              when REG_TEST_COUNTER_0      =>
                read_data(0)(31 downto 0)  <= test_counter(0);
              when REG_TEST_0            =>
                read_data(0)(31 downto 0)  <= test_reg(0);
              when REG_LOCKED            =>
                read_data(0)(1 downto 0)   <= clk_domain_locked;
              when REG_RD_COUNT          =>
                read_data(0)(31 downto 0)  <= read_count;
              when REG_WR_COUNT          =>
                read_data(0)(31 downto 0)  <= write_count;
              when others =>
                --mark data as not found
                read_data(0) <= x"000000000";
            end case;
          when READ_STATE_WRITE =>
            read_data_wr(0) <= '1';
            READ_state(0)   <= READ_STATE_IDLE;
          when others =>
            READ_state(0) <= READ_STATE_IDLE;
        end case;

        -------------------------------------------------------
        -- Write
        -------------------------------------------------------
        -------------------------------------------------------
        eb_control.reset_block_count <= '0';
        eb_control.reset_byte_count  <= '0';
        eb_control.reset_sync        <= '0';

        carrier_control.reset        <= '0';
        
        timing_system_control.i2c.run           <= '0';
        
        write_addr_data_ack(0) <= '0';
        case WRITE_state(0) is
          when WRITE_STATE_IDLE =>
            -- Write/action switch
            if write_addr_data_valid(0) = '1' then
              WRITE_STATE(0) <= WRITE_STATE_CAPTURE_ADDRESS;

              -- buffer address
              write_addr_cap(0)      <= unsigned(write_addr(0));
              write_data_cap(0)      <= write_data(0);

            end if;
          when WRITE_STATE_CAPTURE_ADDRESS =>
            --extra clock tick for multicycle addr/cap signals
            WRITE_STATE(0)         <= WRITE_STATE_WRITE;
            write_addr_data_ack(0) <= '1';
          when WRITE_STATE_WRITE =>
            -- process write and ack
            WRITE_STATE(0) <= WRITE_STATE_IDLE;
            case write_addr_cap(0) is
              when CARRIER_STATUS        =>
                carrier_control.reset          <= write_data_cap(0)(0);
                carrier_control.reset_pl_clock <= write_data_cap(0)(4);
              when CARRIER_LEDS          =>
                carrier_control.LED_mode       <= write_data_cap(0)( 3 downto  0);
                carrier_control.LED_GPO        <= write_data_cap(0)(11 downto  8);                
              when ADC_BOARD_POWER_ENABLE =>
                carrier_control.adc_power_en   <= write_data_cap(0)( 3 downto  0);
              when TIMING_SYSTEM =>
                timing_system_control.clk_source <= write_data_cap(0)(8);
              when TIMING_I2C_CONTROL      =>
                timing_system_control.i2c.run           <= write_data_cap(0)(0);
                timing_system_control.i2c.rw            <= write_data_cap(0)(1);
                timing_system_control.i2c.reset         <= write_data_cap(0)(4);
                timing_system_control.i2c.byte_count    <= write_data_cap(0)(10 downto 8);
                timing_system_control.i2c.reg_addr      <= write_data_cap(0)(23 downto 16);
                timing_system_control.i2c.addr          <= write_data_cap(0)(30 downto 24);
              when TIMING_I2C_WR_DATA      =>
                timing_system_control.i2c.wr_data       <= write_data_cap(0)(31 downto 0) ;
              when EB_STATUS              =>
                eb_control.board_enable        <= write_data_cap(0)(3 downto 0);
                eb_control.reset_sync          <= write_data_cap(0)(29);
                eb_control.enable              <= write_data_cap(0)(31);
              when DMA_BLOCK_COUNT  =>
                eb_control.reset_block_count <= '1';
              when DMA_BYTE_COUNT  =>
                eb_control.reset_byte_count  <= '1';
              when REG_TEST_0            =>
                test_reg(0)                    <= write_data_cap(0);

              when others => null;
            end case;
          when others => WRITE_STATE(0) <= WRITE_STATE_IDLE;
        end case;
      end if;
    end if;
  end process REGS_sys_clk_100Mhz;



  -------------------------------------------------------------------------------------
  -- 200Mhz clk domain
  -------------------------------------------------------------------------------------
  REGS_clk_200Mhz : process (clk_domain(1)) is
  begin  -- process REGS_fmc_clk
    if clk_domain(1)'event and clk_domain(1) = '1' then  -- rising clock edge
      if reset_sync_domain(1) = '1' then 
        test_reg(1) <= x"deadbeef";
        fmc_comm_controls <= (others => DEFAULT_FMC_COMM_CONTROL);
        global_control    <= DEFAULT_GLOBAL_Control;
        pmod_comm_control <= DEFAULT_PMOD_COMM_Control;
      else      
        -------------------------------------------------------
        -- Read
        -------------------------------------------------------
        read_address_ack(1) <= '0';
        read_data_wr(1)     <= '0';

        -- read actions
        fmc_comm_controls(0).rx_comm_read <= '0';
        fmc_comm_controls(1).rx_comm_read <= '0';
        fmc_comm_controls(2).rx_comm_read <= '0';
        fmc_comm_controls(3).rx_comm_read <= '0';
        pmod_comm_control.board(0).rx_comm_read <= '0';
        pmod_comm_control.board(1).rx_comm_read <= '0';
        pmod_comm_control.board(2).rx_comm_read <= '0';
        pmod_comm_control.board(3).rx_comm_read <= '0';
        
        case READ_state(1) is
          when READ_STATE_IDLE =>
            if read_address_valid(1) = '1' then
              read_address_ack(1) <= '1';
              READ_state(1)       <= READ_STATE_CAPTURE_ADDRESS;

              -- capture the address we will be using
              read_address_cap(1) <= unsigned(read_address(1));
              case read_address(1)(11 downto 8) is
                when x"1"  => fmc_read_index <= 0;
                when x"2"  => fmc_read_index <= 1;
                when x"3"  => fmc_read_index <= 2;
                when x"4"  => fmc_read_index <= 3;
                when others => null;
              end case;
              case read_address(1)(11 downto  8) is
                when x"1"  => pmod_read_index <= 0;
                when x"2"  => pmod_read_index <= 1;
                when x"3"  => pmod_read_index <= 2;
                when x"4"  => pmod_read_index <= 3;
                when others => null;
              end case;
              
            end if;
          when READ_STATE_CAPTURE_ADDRESS =>
            --extra clock tick for address multicycle timing
            READ_state(1)       <= READ_STATE_ADDRESS_HELPERS;
          when READ_STATE_ADDRESS_HELPERS =>
            READ_state(1)       <= READ_STATE_READ;
          when READ_STATE_READ =>
            READ_state(1) <= READ_STATE_WRITE;

            read_data(1) <= x"100000000";  --Mark the data as zeros and the
                                           --register found                
            case read_address_cap(1) is
              when HSM1_COUNTER_1 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(0);
              when HSM1_COUNTER_2 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(1);
              when HSM1_COUNTER_3 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(2);
              when HSM1_COUNTER_4 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(3);
              when HSM1_COUNTER_5 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(4);
              when HSM1_COUNTER_6 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(5);
              when HSM1_COUNTER_7 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(6);
              when HSM1_COUNTER_8 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(7);
              when HSM1_COUNTER_9 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(8);
              when HSM1_COUNTER_10 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(9);
              when HSM1_COUNTER_11 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(10);
              when HSM1_COUNTER_12 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(11);
              when HSM1_COUNTER_13 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(12);
              when HSM1_COUNTER_14 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(13);
              when HSM1_COUNTER_15 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(14);
              when HSM1_COUNTER_16 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(0).counter(15);
              when HSM2_COUNTER_1 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(0);
              when HSM2_COUNTER_2 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(1);
              when HSM2_COUNTER_3 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(2);
              when HSM2_COUNTER_4 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(3);
              when HSM2_COUNTER_5 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(4);
              when HSM2_COUNTER_6 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(5);
              when HSM2_COUNTER_7 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(6);
              when HSM2_COUNTER_8 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(7);
              when HSM2_COUNTER_9 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(8);
              when HSM2_COUNTER_10 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(9);
              when HSM2_COUNTER_11 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(10);
              when HSM2_COUNTER_12 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(11);
              when HSM2_COUNTER_13 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(12);
              when HSM2_COUNTER_14 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(13);
              when HSM2_COUNTER_15 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(14);
              when HSM2_COUNTER_16 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(1).counter(15);
              when HSM3_COUNTER_1 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(0);
              when HSM3_COUNTER_2 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(1);
              when HSM3_COUNTER_3 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(2);
              when HSM3_COUNTER_4 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(3);
              when HSM3_COUNTER_5 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(4);
              when HSM3_COUNTER_6 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(5);
              when HSM3_COUNTER_7 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(6);
              when HSM3_COUNTER_8 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(7);
              when HSM3_COUNTER_9 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(8);
              when HSM3_COUNTER_10 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(9);
              when HSM3_COUNTER_11 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(10);
              when HSM3_COUNTER_12 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(11);
              when HSM3_COUNTER_13 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(12);
              when HSM3_COUNTER_14 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(13);
              when HSM3_COUNTER_15 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(14);
              when HSM3_COUNTER_16 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(2).counter(15);
              when HSM4_COUNTER_1 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(0);
              when HSM4_COUNTER_2 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(1);
              when HSM4_COUNTER_3 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(2);
              when HSM4_COUNTER_4 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(3);
              when HSM4_COUNTER_5 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(4);
              when HSM4_COUNTER_6 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(5);
              when HSM4_COUNTER_7 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(6);
              when HSM4_COUNTER_8 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(7);
              when HSM4_COUNTER_9 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(8);
              when HSM4_COUNTER_10 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(9);
              when HSM4_COUNTER_11 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(10);
              when HSM4_COUNTER_12 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(11);
              when HSM4_COUNTER_13 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(12);
              when HSM4_COUNTER_14 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(13);
              when HSM4_COUNTER_15 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(14);
              when HSM4_COUNTER_16 =>
                read_data(1)(31 downto  0) <= hsm_monitor.TX(3).counter(15);

                
              when HSM_RX_1_CNT_1  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg( 0);
              when HSM_RX_1_CNT_2  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg( 1);
              when HSM_RX_1_CNT_3  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg( 2);
              when HSM_RX_1_CNT_4  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg( 3);
              when HSM_RX_1_CNT_5  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg( 4);
              when HSM_RX_1_CNT_6  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg( 5);
              when HSM_RX_1_CNT_7  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg( 6);
              when HSM_RX_1_CNT_8  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg( 7);
              when HSM_RX_1_CNT_9  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg( 8);
              when HSM_RX_1_CNT_10 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg( 9);
              when HSM_RX_1_CNT_11 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg(10);
              when HSM_RX_1_CNT_12 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg(11);
              when HSM_RX_1_CNT_13 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg(12);
              when HSM_RX_1_CNT_14 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg(13);
              when HSM_RX_1_CNT_15 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg(14);
              when HSM_RX_1_CNT_16 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(0).Mesg(15);
              when HSM_RX_2_CNT_1  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg( 0);
              when HSM_RX_2_CNT_2  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg( 1);
              when HSM_RX_2_CNT_3  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg( 2);
              when HSM_RX_2_CNT_4  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg( 3);
              when HSM_RX_2_CNT_5  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg( 4);
              when HSM_RX_2_CNT_6  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg( 5);
              when HSM_RX_2_CNT_7  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg( 6);
              when HSM_RX_2_CNT_8  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg( 7);
              when HSM_RX_2_CNT_9  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg( 8);
              when HSM_RX_2_CNT_10 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg( 9);
              when HSM_RX_2_CNT_11 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg(10);
              when HSM_RX_2_CNT_12 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg(11);
              when HSM_RX_2_CNT_13 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg(12);
              when HSM_RX_2_CNT_14 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg(13);
              when HSM_RX_2_CNT_15 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg(14);
              when HSM_RX_2_CNT_16 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(1).Mesg(15);
              when HSM_RX_3_CNT_1  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg( 0);
              when HSM_RX_3_CNT_2  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg( 1);
              when HSM_RX_3_CNT_3  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg( 2);
              when HSM_RX_3_CNT_4  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg( 3);
              when HSM_RX_3_CNT_5  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg( 4);
              when HSM_RX_3_CNT_6  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg( 5);
              when HSM_RX_3_CNT_7  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg( 6);
              when HSM_RX_3_CNT_8  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg( 7);
              when HSM_RX_3_CNT_9  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg( 8);
              when HSM_RX_3_CNT_10 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg( 9);
              when HSM_RX_3_CNT_11 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg(10);
              when HSM_RX_3_CNT_12 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg(11);
              when HSM_RX_3_CNT_13 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg(12);
              when HSM_RX_3_CNT_14 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg(13);
              when HSM_RX_3_CNT_15 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg(14);
              when HSM_RX_3_CNT_16 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(2).Mesg(15);
              when HSM_RX_4_CNT_1  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg( 0);
              when HSM_RX_4_CNT_2  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg( 1);
              when HSM_RX_4_CNT_3  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg( 2);
              when HSM_RX_4_CNT_4  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg( 3);
              when HSM_RX_4_CNT_5  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg( 4);
              when HSM_RX_4_CNT_6  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg( 5);
              when HSM_RX_4_CNT_7  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg( 6);
              when HSM_RX_4_CNT_8  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg( 7);
              when HSM_RX_4_CNT_9  => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg( 8);
              when HSM_RX_4_CNT_10 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg( 9);
              when HSM_RX_4_CNT_11 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg(10);
              when HSM_RX_4_CNT_12 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg(11);
              when HSM_RX_4_CNT_13 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg(12);
              when HSM_RX_4_CNT_14 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg(13);
              when HSM_RX_4_CNT_15 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg(14);
              when HSM_RX_4_CNT_16 => read_data(1)(31 downto  0) <= hsm_monitor.RX.ADC(3).Mesg(15);
                
              when ADC_BOARD_1_IO_STATUS | ADC_BOARD_2_IO_STATUS | ADC_BOARD_3_IO_STATUS | ADC_BOARD_4_IO_STATUS =>
                read_data(1)(0)            <= fmc_comm_monitors(fmc_read_index).IO.rx_clk_reset;
                read_data(1)(1)            <= fmc_comm_monitors(fmc_read_index).IO.rx_io_reset;
                read_data(1)(2)            <= fmc_comm_monitors(fmc_read_index).IO.tx_clk_reset;
                read_data(1)(3)            <= fmc_comm_monitors(fmc_read_index).IO.tx_io_reset;
                read_data(1)(4)            <= fmc_comm_monitors(fmc_read_index).rx_locked;
                
              when ADC_BOARD_1_COMM_RX | ADC_BOARD_2_COMM_RX | ADC_BOARD_3_COMM_RX | ADC_BOARD_4_COMM_RX   =>
                read_data(1)( 6 downto  0) <= fmc_comm_monitors(fmc_read_index).rx_comm;
                read_data(1)(8)            <= fmc_comm_monitors(fmc_read_index).rx_comm_valid;
                --Ack the read to clear it
                fmc_comm_controls(fmc_read_index).rx_comm_read <= fmc_comm_monitors(fmc_read_index).rx_comm_valid;
                
              when ADC_BOARD_1_COMM_RX_DELAY | ADC_BOARD_2_COMM_RX_DELAY | ADC_BOARD_3_COMM_RX_DELAY | ADC_BOARD_4_COMM_RX_DELAY  =>
                read_data(1)(0)               <= fmc_comm_monitors(fmc_read_index).IO.delay_reset;
                read_data(1)(2)               <= fmc_comm_monitors(fmc_read_index).IO.delay_inc;
                read_data(1)(3)               <= fmc_comm_monitors(fmc_read_index).IO.delay_locked;
                read_data(1)( 8 downto  4)    <= fmc_comm_monitors(fmc_read_index).IO.delay_tap_out;
                
              when ADC_BOARD_1_COMM_RX_DELAY_TAP | ADC_BOARD_2_COMM_RX_DELAY_TAP | ADC_BOARD_3_COMM_RX_DELAY_TAP | ADC_BOARD_4_COMM_RX_DELAY_TAP =>
                read_data(1)( 4 downto  0)    <= fmc_comm_monitors(fmc_read_index).IO.delay_tap_in;
                
              when ADC_BOARD_1_COMM_RX_10B_DATA | ADC_BOARD_2_COMM_RX_10B_DATA | ADC_BOARD_3_COMM_RX_10B_DATA | ADC_BOARD_4_COMM_RX_10B_DATA =>
                read_data(1)( 9 downto  0)    <= fmc_comm_monitors(fmc_read_index).IO.rx_10b;
                
              when ADC_BOARD_1_COMM_RX_STATS | ADC_BOARD_2_COMM_RX_STATS | ADC_BOARD_3_COMM_RX_STATS | ADC_BOARD_4_COMM_RX_STATS  =>
                read_data(1)(15 downto  0)    <= fmc_comm_monitors(fmc_read_index).rx_uart_counter;
                read_data(1)(31 downto 16)    <= fmc_comm_monitors(fmc_read_index).rx_hs_counter;

              when ADC_BOARD_1_COMM_RX_DATA_RATE | ADC_BOARD_2_COMM_RX_DATA_RATE | ADC_BOARD_3_COMM_RX_DATA_RATE | ADC_BOARD_4_COMM_RX_DATA_RATE  =>
                read_data(1)(31 downto  0)    <= fmc_comm_monitors(fmc_read_index).rx_hs_data_rate;

              when ADC_BOARD_1_COMM_RX_COMM_RATE | ADC_BOARD_2_COMM_RX_COMM_RATE | ADC_BOARD_3_COMM_RX_COMM_RATE | ADC_BOARD_4_COMM_RX_COMM_RATE  =>
                read_data(1)(31 downto  0)    <= fmc_comm_monitors(fmc_read_index).rx_comm_rate;

              when ADC_BOARD_1_COMM_RX_CODE_ERROR | ADC_BOARD_2_COMM_RX_CODE_ERROR | ADC_BOARD_3_COMM_RX_CODE_ERROR | ADC_BOARD_4_COMM_RX_CODE_ERROR  =>
                read_data(1)(31 downto 0)     <= fmc_comm_monitors(fmc_read_index).IO.counter_rx_code_error;

              when ADC_BOARD_1_COMM_RX_DISP_ERROR | ADC_BOARD_2_COMM_RX_DISP_ERROR | ADC_BOARD_3_COMM_RX_DISP_ERROR | ADC_BOARD_4_COMM_RX_DISP_ERROR  =>
                read_data(1)(31 downto 0)     <= fmc_comm_monitors(fmc_read_index).IO.counter_rx_disp_error;

              when ADC_BOARD_1_COMM_RX_MESG_STATS | ADC_BOARD_2_COMM_RX_MESG_STATS | ADC_BOARD_3_COMM_RX_MESG_STATS | ADC_BOARD_4_COMM_RX_MESG_STATS  =>
                read_data(1)(15 downto  0)    <= fmc_comm_monitors(fmc_read_index).rx_hs_mesg_counter;

              when ADC_BOARD_1_COMM_TX | ADC_BOARD_2_COMM_TX | ADC_BOARD_3_COMM_TX | ADC_BOARD_4_COMM_TX        =>
                read_data(1)(8)               <= fmc_comm_monitors(fmc_read_index).tx_comm_empty;

              when ADC_BOARD_1_HS_TX | ADC_BOARD_2_HS_TX | ADC_BOARD_3_HS_TX | ADC_BOARD_4_HS_TX        =>
                read_data(1)(8)               <= fmc_comm_monitors(fmc_read_index).tx_hs_empty;
                
              when ADC_BOARD_1_COMM_TX_STATS | ADC_BOARD_2_COMM_TX_STATS | ADC_BOARD_3_COMM_TX_STATS | ADC_BOARD_4_COMM_TX_STATS  =>
                read_data(1)(15 downto  0)    <= fmc_comm_monitors(fmc_read_index).tx_uart_counter;
                read_data(1)(31 downto 16)    <= fmc_comm_monitors(fmc_read_index).tx_hs_counter;

              when ADC_BOARD_1_COMM_TX_DATA_RATE | ADC_BOARD_2_COMM_TX_DATA_RATE | ADC_BOARD_3_COMM_TX_DATA_RATE | ADC_BOARD_4_COMM_TX_DATA_RATE  =>
                read_data(1)(31 downto  0)    <= fmc_comm_monitors(fmc_read_index).tx_hs_data_rate;

              when ADC_BOARD_1_COMM_TX_COMM_RATE | ADC_BOARD_2_COMM_TX_COMM_RATE | ADC_BOARD_3_COMM_TX_COMM_RATE | ADC_BOARD_4_COMM_TX_COMM_RATE  =>
                read_data(1)(31 downto  0)    <= fmc_comm_monitors(fmc_read_index).tx_comm_rate;

                
              when GLOBAL_STATUS =>
                read_data(1)(0)  <= global_monitor.pulser_ena;
                read_data(1)(1)  <= global_monitor.wfd_ena;
                read_data(1)(2)  <= global_monitor.fault_override;
                read_data(1)(8)  <= global_monitor.fault_seen;
                read_data(1)(16) <= global_monitor.ext_en;
                read_data(1)(17) <= global_monitor.fr_en;
                
              when GLOBAL_TRIG_RATE =>
                read_data(1)(31 downto 0) <= global_monitor.trigger_rate;

              when GLOBAL_TRIG_COUNT =>
                read_data(1)(31 downto 0) <= global_monitor.trigger_count;

              when GLOBAL_TRIG_COUNT_CONTROL =>
                read_data(1)(0)           <= global_monitor.enable_trigger_count;

                
              when GLOBAL_FR_STATUS =>
                read_data(1)(0)  <= global_monitor.fr_settings.enable;
                read_data(1)(1)  <= global_monitor.fr_settings.burst_ena;

              when GLOBAL_FR_PER =>
                read_data(1)(15 downto  0) <= global_monitor.fr_settings.periodic_delay;                

              when GLOBAL_FR_BURST_MASK =>
                read_data(1)(31 downto  0) <= global_monitor.fr_settings.burst_mask;

              when GLOBAL_FR_BURST_SPACING =>
                read_data(1)(15 downto  0) <= global_monitor.fr_settings.burst_spacing;

              when GLOBAL_EXTRA_PULSER1_CTRL =>
                read_data(1)(0)  <= global_monitor.extra_pulses(0).bypass;
                read_data(1)(23 downto 16)  <= global_monitor.extra_pulses(0).pulse_en;
              when GLOBAL_EXTRA_PULSER1_0 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(0).pulse(0);
              when GLOBAL_EXTRA_PULSER1_1 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(0).pulse(1);
              when GLOBAL_EXTRA_PULSER1_2 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(0).pulse(2);
              when GLOBAL_EXTRA_PULSER1_3 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(0).pulse(3);
              when GLOBAL_EXTRA_PULSER1_4 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(0).pulse(4);
              when GLOBAL_EXTRA_PULSER1_5 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(0).pulse(5);
              when GLOBAL_EXTRA_PULSER1_6 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(0).pulse(6);
              when GLOBAL_EXTRA_PULSER1_7 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(0).pulse(7);

              when GLOBAL_EXTRA_PULSER2_CTRL =>
                read_data(1)(0)  <= global_monitor.extra_pulses(1).bypass;
                read_data(1)(23 downto 16)  <= global_monitor.extra_pulses(1).pulse_en;
              when GLOBAL_EXTRA_PULSER2_0 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(1).pulse(0);
              when GLOBAL_EXTRA_PULSER2_1 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(1).pulse(1);
              when GLOBAL_EXTRA_PULSER2_2 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(1).pulse(2);
              when GLOBAL_EXTRA_PULSER2_3 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(1).pulse(3);
              when GLOBAL_EXTRA_PULSER2_4 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(1).pulse(4);
              when GLOBAL_EXTRA_PULSER2_5 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(1).pulse(5);
              when GLOBAL_EXTRA_PULSER2_6 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(1).pulse(6);
              when GLOBAL_EXTRA_PULSER2_7 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(1).pulse(7);

              when GLOBAL_EXTRA_PULSER3_CTRL =>
                read_data(1)(0)  <= global_monitor.extra_pulses(2).bypass;
                read_data(1)(23 downto 16)  <= global_monitor.extra_pulses(2).pulse_en;
              when GLOBAL_EXTRA_PULSER3_0 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(2).pulse(0);
              when GLOBAL_EXTRA_PULSER3_1 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(2).pulse(1);
              when GLOBAL_EXTRA_PULSER3_2 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(2).pulse(2);
              when GLOBAL_EXTRA_PULSER3_3 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(2).pulse(3);
              when GLOBAL_EXTRA_PULSER3_4 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(2).pulse(4);
              when GLOBAL_EXTRA_PULSER3_5 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(2).pulse(5);
              when GLOBAL_EXTRA_PULSER3_6 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(2).pulse(6);
              when GLOBAL_EXTRA_PULSER3_7 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(2).pulse(7);

              when GLOBAL_EXTRA_PULSER4_CTRL =>
                read_data(1)(0)  <= global_monitor.extra_pulses(3).bypass;
                read_data(1)(23 downto 16)  <= global_monitor.extra_pulses(3).pulse_en;
              when GLOBAL_EXTRA_PULSER4_0 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(3).pulse(0);
              when GLOBAL_EXTRA_PULSER4_1 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(3).pulse(1);
              when GLOBAL_EXTRA_PULSER4_2 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(3).pulse(2);
              when GLOBAL_EXTRA_PULSER4_3 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(3).pulse(3);
              when GLOBAL_EXTRA_PULSER4_4 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(3).pulse(4);
              when GLOBAL_EXTRA_PULSER4_5 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(3).pulse(5);
              when GLOBAL_EXTRA_PULSER4_6 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(3).pulse(6);
              when GLOBAL_EXTRA_PULSER4_7 =>
                read_data(1)(31 downto 0)  <= global_monitor.extra_pulses(3).pulse(7);

                
              when ADC_BOARD_4_PMOD_COMM_RX | ADC_BOARD_3_PMOD_COMM_RX | ADC_BOARD_2_PMOD_COMM_RX | ADC_BOARD_1_PMOD_COMM_RX =>
                read_data(1)( 7 downto  0) <= pmod_comm_monitor.board(pmod_read_index).rx_comm;
                read_data(1)(8)            <= pmod_comm_monitor.board(pmod_read_index).rx_comm_valid;
                --Ack the read to clear it
                pmod_comm_control.board(pmod_read_index).rx_comm_read <= pmod_comm_monitor.board(pmod_read_index).rx_comm_valid;

              when ADC_BOARD_4_PMOD_COMM_TX | ADC_BOARD_3_PMOD_COMM_TX | ADC_BOARD_2_PMOD_COMM_TX | ADC_BOARD_1_PMOD_COMM_TX =>
                read_data(1)(8)            <= pmod_comm_monitor.board(pmod_read_index).tx_comm_empty;

              when DMA_FRAME_FIFO_EB_1 =>
                read_data(1)(0)            <= eb_monitor.ADCFrame(0).DAQ_FIFO_full;
                read_data(1)(20 downto  4) <= eb_monitor.ADCFrame(0).DAQ_FIFO_in_Count;
              when DMA_FRAME_EB_START_1 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(0).EB.unexpected_start_count;
              when DMA_FRAME_EB_END_1 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(0).EB.unexpected_end_count;
              when DMA_FRAME_FRAME_SIZE_1 =>                
                read_data(1)(23 downto  0) <= eb_monitor.ADCFrame(0).DMA.frame_size;
                read_data(1)(31)            <= eb_monitor.board_has_data(0);
              when DMA_FRAME_BUFFER_COUNT_1 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(0).DMA.buffer_count;
              when DMA_FRAME_SIZE_ERRORS_1 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(0).DMA.size_error_count;
              when DMA_FRAME_COUNT_1 =>
                read_data(1)(31 downto  0) <= eb_monitor.board_frame_count(0);
              when DMA_FRAME_RATE_1 =>
                read_data(1)(31 downto  0) <= eb_monitor.board_frame_rate(0);
              when DMA_FRAME_FIFO_EB_2 =>
                read_data(1)(0)            <= eb_monitor.ADCFrame(1).DAQ_FIFO_full;
                read_data(1)(20 downto  4) <= eb_monitor.ADCFrame(1).DAQ_FIFO_in_Count;
              when DMA_FRAME_EB_START_2 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(1).EB.unexpected_start_count;
              when DMA_FRAME_EB_END_2 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(1).EB.unexpected_end_count;
              when DMA_FRAME_FRAME_SIZE_2 =>                
                read_data(1)(23 downto  0) <= eb_monitor.ADCFrame(1).DMA.frame_size;
                read_data(1)(31)            <= eb_monitor.board_has_data(1);
              when DMA_FRAME_BUFFER_COUNT_2 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(1).DMA.buffer_count;
              when DMA_FRAME_SIZE_ERRORS_2 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(1).DMA.size_error_count;
              when DMA_FRAME_COUNT_2 =>
                read_data(1)(31 downto  0) <= eb_monitor.board_frame_count(1);
              when DMA_FRAME_RATE_2 =>
                read_data(1)(31 downto  0) <= eb_monitor.board_frame_rate(1);

              when DMA_FRAME_FIFO_EB_3 =>
                read_data(1)(0)            <= eb_monitor.ADCFrame(2).DAQ_FIFO_full;
                read_data(1)(20 downto  4) <= eb_monitor.ADCFrame(2).DAQ_FIFO_in_Count;
              when DMA_FRAME_EB_START_3 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(2).EB.unexpected_start_count;
              when DMA_FRAME_EB_END_3 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(2).EB.unexpected_end_count;
              when DMA_FRAME_FRAME_SIZE_3 =>                
                read_data(1)(23 downto  0) <= eb_monitor.ADCFrame(2).DMA.frame_size;
                read_data(1)(31)           <= eb_monitor.board_has_data(2);
              when DMA_FRAME_BUFFER_COUNT_3 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(2).DMA.buffer_count;
              when DMA_FRAME_SIZE_ERRORS_3 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(2).DMA.size_error_count;
              when DMA_FRAME_COUNT_3 =>
                read_data(1)(31 downto  0) <= eb_monitor.board_frame_count(2);
              when DMA_FRAME_RATE_3 =>
                read_data(1)(31 downto  0) <= eb_monitor.board_frame_rate(2);

              when DMA_FRAME_FIFO_EB_4 =>
                read_data(1)(0)            <= eb_monitor.ADCFrame(3).DAQ_FIFO_full;
                read_data(1)(20 downto  4) <= eb_monitor.ADCFrame(3).DAQ_FIFO_in_Count;
              when DMA_FRAME_EB_START_4 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(3).EB.unexpected_start_count;
              when DMA_FRAME_EB_END_4 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(3).EB.unexpected_end_count;
              when DMA_FRAME_FRAME_SIZE_4 =>                
                read_data(1)(23 downto  0) <= eb_monitor.ADCFrame(3).DMA.frame_size;
                read_data(1)(31)           <= eb_monitor.board_has_data(3);
              when DMA_FRAME_BUFFER_COUNT_4 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(3).DMA.buffer_count;
              when DMA_FRAME_SIZE_ERRORS_4 =>
                read_data(1)(31 downto  0) <= eb_monitor.ADCFrame(3).DMA.size_error_count;
              when DMA_FRAME_COUNT_4 =>
                read_data(1)(31 downto  0) <= eb_monitor.board_frame_count(3);
              when DMA_FRAME_RATE_4 =>
                read_data(1)(31 downto  0) <= eb_monitor.board_frame_rate(3);

                
              when REG_TEST_COUNTER_1    =>
                read_data(1)(31 downto 0)  <= test_counter(1);
              when REG_TEST_1 =>
                read_data(1)(31 downto  0)    <= test_reg(1);
              when others =>
                --mark data as not found
                read_data(1) <= x"000000000";
            end case;
          when READ_STATE_WRITE =>
            read_data_wr(1) <= '1';
            READ_state(1)   <= READ_STATE_IDLE;
          when others =>
            READ_state(1) <= READ_STATE_IDLE;
        end case;

        -------------------------------------------------------
        -- Write
        -------------------------------------------------------        
        
        for iBoard in 3 downto 0 loop
          hsm_control.TX(iBoard).send_message <= x"0000";
          hsm_control.TX(iBoard).reset_mesg_counters <= '0';

          fmc_comm_controls(iBoard).tx_comm_data_valid             <= '0';
          fmc_comm_controls(iBoard).tx_hs_data_valid               <= '0';
          fmc_comm_controls(iBoard).IO.delay_ce                    <= '0';
          fmc_comm_controls(iBoard).tx_reset_hs_counter            <= '0';
          fmc_comm_controls(iBoard).tx_reset_uart_counter          <= '0';
          fmc_comm_controls(iBoard).rx_reset_hs_counter            <= '0';
          fmc_comm_controls(iBoard).rx_reset_uart_counter          <= '0';
          fmc_comm_controls(iBoard).rx_reset_hs_mesg_counter       <= '0';
          fmc_comm_controls(iBoard).IO.reset_counter_rx_code_error <= '0';
          fmc_comm_controls(iBoard).IO.reset_counter_rx_disp_error <= '0';          

          pmod_comm_control.board(iBoard).tx_comm_data_valid       <= '0';
        end loop;  -- iBoard

        global_control.global_reset   <= '0';
        global_control.fault_reset    <= '0';
        global_control.fr_settings.single_trigger <= '0';
        global_control.reset_trigger_count <= '0';
        
        write_addr_data_ack(1) <= '0';
        case WRITE_state(1) is
          when WRITE_STATE_IDLE =>
            -- Write/action switch
            if write_addr_data_valid(1) = '1' then
              WRITE_STATE(1) <= WRITE_STATE_CAPTURE_ADDRESS;
              -- buffer address
              write_addr_cap(1)      <= unsigned(write_addr(1));
              case write_addr(1)(11 downto  8) is
                when x"1"  => fmc_wr_index <= 0;
                when x"2"  => fmc_wr_index <= 1;
                when x"3"  => fmc_wr_index <= 2;
                when x"4"  => fmc_wr_index <= 3;
                when others => null;
              end case;
              case write_addr(1)(11 downto  8) is
                when x"1"  => pmod_wr_index <= 0;
                when x"2"  => pmod_wr_index <= 1;
                when x"3"  => pmod_wr_index <= 2;
                when x"4"  => pmod_wr_index <= 3;
                when others => null;
              end case;

            end if;
          when WRITE_STATE_CAPTURE_ADDRESS =>
            --extra clock tick for multicycle addr/data signals
            write_data_cap(1)      <= write_data(1);
            WRITE_STATE(1)         <= WRITE_STATE_WRITE;
            write_addr_data_ack(1) <= '1';
          when WRITE_STATE_WRITE =>
            -- process write and ack
            WRITE_STATE(1) <= WRITE_STATE_IDLE;
            case write_addr_cap(1) is

              when GLOBAL_TRIG_COUNT =>
                global_control.reset_trigger_count <= '1';
              when GLOBAL_TRIG_COUNT_CONTROL =>
                global_control.enable_trigger_count <= write_data_cap(1)(0);

              when HSM1_USER =>
                hsm_control.TX(0).send_message                        <= write_data_cap(1)(15 downto 0);
                hsm_control.TX(0).reset_mesg_counters                 <= write_data_cap(1)(16);
              when HSM2_USER =>
                hsm_control.TX(1).send_message                        <= write_data_cap(1)(15 downto 0);
                hsm_control.TX(1).reset_mesg_counters                 <= write_data_cap(1)(16);
              when HSM3_USER =>
                hsm_control.TX(2).send_message                        <= write_data_cap(1)(15 downto 0);
                hsm_control.TX(2).reset_mesg_counters                 <= write_data_cap(1)(16);
              when HSM4_USER =>
                hsm_control.TX(3).send_message                        <= write_data_cap(1)(15 downto 0);
                hsm_control.TX(3).reset_mesg_counters                 <= write_data_cap(1)(16);

              when ADC_BOARD_1_IO_STATUS | ADC_BOARD_2_IO_STATUS | ADC_BOARD_3_IO_STATUS | ADC_BOARD_4_IO_STATUS         =>
                fmc_comm_controls(fmc_wr_index).IO.rx_clk_reset <= write_data_cap(1)(0);
                fmc_comm_controls(fmc_wr_index).IO.rx_io_reset  <= write_data_cap(1)(1);
                fmc_comm_controls(fmc_wr_index).IO.tx_clk_reset <= write_data_cap(1)(2);
                fmc_comm_controls(fmc_wr_index).IO.tx_io_reset  <= write_data_cap(1)(3);
                
              when ADC_BOARD_1_COMM_RX | ADC_BOARD_2_COMM_RX | ADC_BOARD_3_COMM_RX | ADC_BOARD_4_COMM_RX           => null;
                                                    
              when ADC_BOARD_1_COMM_RX_DELAY | ADC_BOARD_2_COMM_RX_DELAY | ADC_BOARD_3_COMM_RX_DELAY | ADC_BOARD_4_COMM_RX_DELAY     =>
                fmc_comm_controls(fmc_wr_index).IO.delay_reset  <= write_data_cap(1)(0);
                fmc_comm_controls(fmc_wr_index).IO.delay_ce     <= write_data_cap(1)(1);
                fmc_comm_controls(fmc_wr_index).IO.delay_inc    <= write_data_cap(1)(2);
                
              when ADC_BOARD_1_COMM_RX_DELAY_TAP | ADC_BOARD_2_COMM_RX_DELAY_TAP | ADC_BOARD_3_COMM_RX_DELAY_TAP | ADC_BOARD_4_COMM_RX_DELAY_TAP =>
                fmc_comm_controls(fmc_wr_index).IO.delay_tap_in <= write_data_cap(1)( 4 downto  0);

              when ADC_BOARD_1_COMM_RX_STATS | ADC_BOARD_2_COMM_RX_STATS | ADC_BOARD_3_COMM_RX_STATS | ADC_BOARD_4_COMM_RX_STATS     =>
                fmc_comm_controls(fmc_wr_index).rx_reset_uart_counter <= or_reduce(write_data_cap(1)(15 downto  0));
                fmc_comm_controls(fmc_wr_index).rx_reset_hs_counter   <= or_reduce(write_data_cap(1)(31 downto 16));

              when ADC_BOARD_1_COMM_RX_CODE_ERROR | ADC_BOARD_2_COMM_RX_CODE_ERROR | ADC_BOARD_3_COMM_RX_CODE_ERROR | ADC_BOARD_4_COMM_RX_CODE_ERROR     =>
                fmc_comm_controls(fmc_wr_index).IO.reset_counter_rx_code_error <= '1';

              when ADC_BOARD_1_COMM_RX_DISP_ERROR | ADC_BOARD_2_COMM_RX_DISP_ERROR | ADC_BOARD_3_COMM_RX_DISP_ERROR | ADC_BOARD_4_COMM_RX_DISP_ERROR     =>
                fmc_comm_controls(fmc_wr_index).IO.reset_counter_rx_disp_error <= '1';

              when ADC_BOARD_1_COMM_RX_MESG_STATS | ADC_BOARD_2_COMM_RX_MESG_STATS | ADC_BOARD_3_COMM_RX_MESG_STATS | ADC_BOARD_4_COMM_RX_MESG_STATS  =>
                fmc_comm_controls(fmc_wr_index).rx_reset_hs_mesg_counter <= '1';

                
              when ADC_BOARD_1_COMM_TX | ADC_BOARD_2_COMM_TX | ADC_BOARD_3_COMM_TX | ADC_BOARD_4_COMM_TX           =>
                fmc_comm_controls(fmc_wr_index).tx_comm_data    <= write_data_cap(1)( 6 downto  0);
                -- for safetly, only write when empty is '1'
                fmc_comm_controls(fmc_wr_index).tx_comm_data_valid <= fmc_comm_monitors(fmc_wr_index).tx_comm_empty;

              when ADC_BOARD_1_HS_TX | ADC_BOARD_2_HS_TX | ADC_BOARD_3_HS_TX | ADC_BOARD_4_HS_TX        =>
                fmc_comm_controls(fmc_wr_index).tx_hs_data    <= write_data_cap(1)( 3 downto  0);
                -- for safetly, only write when empty is '1'
                fmc_comm_controls(fmc_wr_index).tx_hs_data_valid <= fmc_comm_monitors(fmc_wr_index).tx_hs_empty;
                                
              when ADC_BOARD_1_COMM_TX_STATS | ADC_BOARD_2_COMM_TX_STATS | ADC_BOARD_3_COMM_TX_STATS | ADC_BOARD_4_COMM_TX_STATS     =>
                fmc_comm_controls(fmc_wr_index).tx_reset_uart_counter <= or_reduce(write_data_cap(1)(15 downto  0));
                fmc_comm_controls(fmc_wr_index).tx_reset_hs_counter   <= or_reduce(write_data_cap(1)(31 downto 16));

              when GLOBAL_STATUS =>
                global_control.pulser_ena     <= write_data_cap(1)(0);
                global_control.wfd_ena        <= write_data_cap(1)(1);
                global_control.fault_override <= write_data_cap(1)(2);
                global_control.global_reset   <= write_data_cap(1)(4);
                global_control.fault_reset    <= write_data_cap(1)(5);
                global_control.ext_en         <= write_data_cap(1)(16);
                global_control.fr_en          <= write_data_cap(1)(17);


              when GLOBAL_FR_STATUS =>
                global_control.fr_settings.enable         <= write_data_cap(1)(0);
                global_control.fr_settings.burst_ena      <= write_data_cap(1)(1);
                global_control.fr_settings.single_trigger <= write_data_cap(1)(4);

              when GLOBAL_FR_PER =>
                global_control.fr_settings.periodic_delay <= write_data_cap(1)(15 downto  0);

              when GLOBAL_FR_BURST_MASK =>
                global_control.fr_settings.burst_mask     <= write_data_cap(1)(31 downto  0);

              when GLOBAL_FR_BURST_SPACING =>
                global_control.fr_settings.burst_spacing  <= write_data_cap(1)(15 downto  0);

              when GLOBAL_EXTRA_PULSER1_CTRL =>
                global_control.extra_pulses(0).bypass   <= write_data_cap(1)(0);
                global_control.extra_pulses(0).pulse_en <= write_data_cap(1)(23 downto 16);
              when GLOBAL_EXTRA_PULSER1_0 =>
                global_control.extra_pulses(0).pulse(0) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER1_1 =>
                global_control.extra_pulses(0).pulse(1) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER1_2 =>
                global_control.extra_pulses(0).pulse(2) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER1_3 =>
                global_control.extra_pulses(0).pulse(3) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER1_4 =>
                global_control.extra_pulses(0).pulse(4) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER1_5 =>
                global_control.extra_pulses(0).pulse(5) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER1_6 =>
                global_control.extra_pulses(0).pulse(6) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER1_7 =>
                global_control.extra_pulses(0).pulse(7) <= write_data_cap(1)(31 downto 0);

              when GLOBAL_EXTRA_PULSER2_CTRL =>
                global_control.extra_pulses(1).bypass <= write_data_cap(1)(0);
                global_control.extra_pulses(1).pulse_en <= write_data_cap(1)(23 downto 16);
              when GLOBAL_EXTRA_PULSER2_0 =>
                global_control.extra_pulses(1).pulse(0) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER2_1 =>
                global_control.extra_pulses(1).pulse(1) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER2_2 =>
                global_control.extra_pulses(1).pulse(2) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER2_3 =>
                global_control.extra_pulses(1).pulse(3) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER2_4 =>
                global_control.extra_pulses(1).pulse(4) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER2_5 =>
                global_control.extra_pulses(1).pulse(5) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER2_6 =>
                global_control.extra_pulses(1).pulse(6) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER2_7 =>
                global_control.extra_pulses(1).pulse(7) <= write_data_cap(1)(31 downto 0);

              when GLOBAL_EXTRA_PULSER3_CTRL =>
                global_control.extra_pulses(2).bypass <= write_data_cap(1)(0);
                global_control.extra_pulses(2).pulse_en <= write_data_cap(1)(23 downto 16);
              when GLOBAL_EXTRA_PULSER3_0 =>
                global_control.extra_pulses(2).pulse(0) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER3_1 =>
                global_control.extra_pulses(2).pulse(1) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER3_2 =>
                global_control.extra_pulses(2).pulse(2) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER3_3 =>
                global_control.extra_pulses(2).pulse(3) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER3_4 =>
                global_control.extra_pulses(2).pulse(4) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER3_5 =>
                global_control.extra_pulses(2).pulse(5) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER3_6 =>
                global_control.extra_pulses(2).pulse(6) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER3_7 =>
                global_control.extra_pulses(2).pulse(7) <= write_data_cap(1)(31 downto 0);

              when GLOBAL_EXTRA_PULSER4_CTRL =>
                global_control.extra_pulses(3).bypass <= write_data_cap(1)(0);
                global_control.extra_pulses(3).pulse_en <= write_data_cap(1)(23 downto 16);
              when GLOBAL_EXTRA_PULSER4_0 =>
                global_control.extra_pulses(3).pulse(0) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER4_1 =>
                global_control.extra_pulses(3).pulse(1) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER4_2 =>
                global_control.extra_pulses(3).pulse(2) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER4_3 =>
                global_control.extra_pulses(3).pulse(3) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER4_4 =>
                global_control.extra_pulses(3).pulse(4) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER4_5 =>
                global_control.extra_pulses(3).pulse(5) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER4_6 =>
                global_control.extra_pulses(3).pulse(6) <= write_data_cap(1)(31 downto 0);
              when GLOBAL_EXTRA_PULSER4_7 =>
                global_control.extra_pulses(3).pulse(7) <= write_data_cap(1)(31 downto 0);


                
              when ADC_BOARD_1_PMOD_COMM_RX | ADC_BOARD_2_PMOD_COMM_RX | ADC_BOARD_3_PMOD_COMM_RX | ADC_BOARD_4_PMOD_COMM_RX           => null;

              when ADC_BOARD_1_PMOD_COMM_TX | ADC_BOARD_2_PMOD_COMM_TX | ADC_BOARD_3_PMOD_COMM_TX | ADC_BOARD_4_PMOD_COMM_TX           =>
                pmod_comm_control.board(pmod_wr_index).tx_comm_data    <= write_data_cap(1)( 7 downto  0);
                -- for safetly, only write when empty is '1'
                pmod_comm_control.board(pmod_wr_index).tx_comm_data_valid <= pmod_comm_monitor.board(pmod_wr_index).tx_comm_empty;
                
              when REG_TEST_1 =>
                test_reg(1) <= write_data_cap(1);

              when others => null;
            end case;
          when others => WRITE_STATE(1) <= WRITE_STATE_IDLE;
        end case;
      end if;
    end if;
  end process REGS_clk_200Mhz;


  
  
  counter_reads: entity work.counter
    generic map (
      DATA_WIDTH  => 32)
    port map (
      clk         => clk_sys,
      reset_async => '0',
      reset_sync  => reset,
      enable      => '1',
      event       => RD_strb,
      count       => read_count,
      at_max      => open);
  
  counter_writes: entity work.counter
    generic map (
      DATA_WIDTH  => 32)
    port map (
      clk         => clk_sys,
      reset_async => '0',
      reset_sync  => reset,
      enable      => '1',
      event       => WR_strb,
      count       => write_count,
      at_max      => open);


end architecture Behavioral;

