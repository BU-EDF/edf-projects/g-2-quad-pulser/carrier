#clock domain crossing
set_false_path -from [get_cells -hier -filter NAME=~*register_map_CDC*/captureA_reg[*]] -to [get_cells -hier -filter {NAME=~*register_map_CDC*/captureB_reg[0][*]}]
