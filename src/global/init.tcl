add_force clk {0 0ns} {1 5ns} -repeat_every 10ns
add_force set.periodic_delay -radix dec 20
add_force set.burst_ena 0
add_force set.enable 1
add_force set.single_trigger 0
add_force set.burst_mask 11001010
add_force set.burst_spacing -radix dec 4
add_force rst_n 0
run 10ns
add_force rst_n 1
run 10ns
# all disabled
add_force set.burst_ena 0
add_force set.enable 0
add_force set.single_trigger 0
run 3us
# single trigger
add_force set.single_trigger 1
run 10ns
add_force set.single_trigger 0
run 3us
# single burst
add_force set.burst_ena 1
add_force set.single_trigger 1
run 10ns
add_force set.single_trigger 0
run 3us
