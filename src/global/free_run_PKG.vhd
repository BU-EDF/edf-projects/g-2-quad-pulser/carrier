--
-- settings for the free-run pulser mode
--
library ieee;
use ieee.std_logic_1164.all;

package FREE_RUN_PKG is

  type FREE_RUN_t is record
    -- free-running pulser control
    periodic_delay : std_logic_vector(15 downto 0);  -- (R/W) delay between triggers/bursts
    burst_mask     : std_logic_vector(31 downto 0);  -- (R/W)  burst mask 1-12 bursts
    -- spacing between pulses burst in 1us units
    burst_spacing  : std_logic_vector(15 downto 0);  -- (R/W)
    enable         : std_logic;         -- (R/W) enable free-running mode
    burst_ena      : std_logic;         -- (R/W) select burst mode
    single_trigger : std_logic;         -- (A) force trigger              
  end record FREE_RUN_t;
  constant DEFAULT_FREE_RUN : FREE_RUN_t := (periodic_delay => x"000A",
                                             burst_mask => x"00000001",
                                             burst_spacing => x"0001",
                                             enable => '0',
                                             burst_ena => '0',
                                             single_trigger => '0');
end package FREE_RUN_PKG;
