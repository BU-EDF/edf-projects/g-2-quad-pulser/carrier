library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.GLOBAL_CTRL.all;
use work.FREE_RUN_PKG.all;


entity GLOBAL is
  
  port (
    clk          : in  std_logic;
    rst          : in  std_logic;
    -- 3 logic inputs from one of the ADC boards
    ext_trigger  : in  std_logic;        -- trigger input
    vac_fail     : in  std_logic;        -- vacuum fail input
    permit       : in  std_logic;        -- accelerator permit input
    -- output to all ADC boards
    adc_trigger  : out std_logic_vector(4 downto 0);  -- broadcast trigger signal to all ADC boards
    reset_spill  : out std_logic_vector(4 downto 0);
    spill_number : out std_logic_vector(31 downto 0);
    oob_trigger  : out std_logic_vector(4 downto 0);
    -- registers
    control      : in  GLOBAL_Control_t;
    monitor      : out GLOBAL_Monitor_t);

end entity GLOBAL;

architecture arch of GLOBAL is

  component timed_counter is
    generic (
      timer_count : std_logic_vector;
      DATA_WIDTH  : integer);
    port (
      clk          : in  std_logic;
      reset_async  : in  std_logic;
      reset_sync   : in  std_logic;
      enable       : in  std_logic;
      event        : in  std_logic;
      update_pulse : out std_logic;
      timed_count  : out std_logic_vector(DATA_WIDTH-1 downto 0));
  end component timed_counter;

  component counter is
    generic (
      roll_over   : std_logic;
      end_value   : std_logic_vector(31 downto 0);
      start_value : std_logic_vector(31 downto 0);
      DATA_WIDTH  : integer);
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      enable      : in  std_logic;
      event       : in  std_logic;
      count       : out std_logic_vector(DATA_WIDTH-1 downto 0);
      at_max      : out std_logic);
  end component counter;
  
  component free_run is
    port (
      clk            : in  std_logic;
      rstn          : in  std_logic;
      set            : in  Free_Run_t;
      trigger        : out std_logic);
  end component free_run;

  signal reset_n : std_logic;
  signal local_adc_trigger : std_logic_vector(4 downto 0);
  signal free_run_trigger : std_logic;
  
  signal free_run_settings : Free_Run_t;
  signal control_buffer : GLOBAL_Control_t;

  signal reset_on_next_spill : std_logic_vector(4 downto 0);
  signal local_spill_count : std_logic_vector(31 downto 0);

  signal ext_trigger_sr  : std_logic_vector(3 downto 0);
  signal ext_trig_gated : std_logic;
  signal in_ext_trigger : std_logic;
  signal ext_trig_debounce_counter : integer range 0 to 255;
  signal ext_trig  : std_logic;

  constant TRIG_MAX_LENGTH : integer range 0 to 255 := 9;
  
begin  -- architecture arch

  reset_n <= not rst;
  monitor.pulser_ena     <= control_buffer.pulser_ena;
  monitor.wfd_ena        <= control_buffer.wfd_ena;
  monitor.fault_override <= control_buffer.fault_override;
  monitor.fr_settings    <= control_buffer.fr_settings;
--  monitor.pulser_ena     <= control.pulser_ena;
--  monitor.wfd_ena        <= control.wfd_ena;
--  monitor.fault_override <= control.fault_override;
--  monitor.fr_settings    <= control.fr_settings;
--
  control_buffer_proc: process (clk) is
  begin  -- process control_buffer
    if clk'event and clk = '1' then  -- rising clock edge
      control_buffer <= control;
    end if;
  end process control_buffer_proc;
  
  adc_trigger(0) <= local_adc_trigger(0);
  free_run_1 : entity work.free_run
    port map (
      clk            => clk,
      rst_n          => '1',
      set            => control_buffer.fr_settings,--control.fr_settings,
      trigger        => free_run_trigger);

  --Gate the external trigger using the ext_trig_gate_sr
  ext_trig_debounce: process (clk,rst) is    
  begin  -- process ext_trig_debounce
    if rst = '1' then
      in_ext_trigger <= '0';
      ext_trig_gated <= '0';
      ext_trigger_sr  <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      ext_trig_gated <= '0';
      ext_trigger_sr <= ext_trigger_sr(ext_trigger_sr'left -1 downto 0) & ext_trigger;
      
      if in_ext_trigger = '1' then
        ext_trig_debounce_counter <= ext_trig_debounce_counter + 1;
        if ext_trig_debounce_counter > TRIG_MAX_LENGTH then
          if ext_trigger = '1' then
            --this is a long trigger, so ignore it. 
            ext_trig <= '0';
          end if;
          if ext_trig_debounce_counter = 25 then
            ext_trig_gated <= ext_trig;
          end if;
          if ext_trig_debounce_counter = 100 then
            in_ext_trigger <= '0';
          end if;
        end if;
      elsif ext_trigger_sr(1 downto 0) = "01" then
        in_ext_trigger <= '1';
        ext_trig <= '1';
        ext_trig_debounce_counter <= 0;
      end if;

      
    end if;
  end process ext_trig_debounce;


  monitor.fr_en <= control.fr_en;
  monitor.ext_en <= control.ext_en;
  local_adc_trigger <=(others => control.pulser_ena and ((free_run_trigger and control.fr_en) or
                                                         (ext_trig_gated and control.ext_en)));
  reset_spill <= reset_on_next_spill and local_adc_trigger;
  trigger_number_reset_helper : process (clk) is
  begin  -- process
    if clk'event and clk = '1' then  -- rising clock edge      



      if local_spill_count(15 downto 0) = x"FFFF" then
        reset_on_next_spill <= (others => '1');
      else
        --reset this on a per ADCBoad basis
        reset_on_next_spill <= reset_on_next_spill and (not local_adc_trigger);        
      end if;
    end if;
  end process;

  spill_number <= local_spill_count;
  monitor.trigger_count <= local_spill_count;
  monitor.enable_trigger_count <= control_buffer.enable_trigger_count;
  counter_2: entity work.counter
    generic map (
      roll_over   => '0')
    port map (
      clk         => clk,
      reset_async => rst,
      reset_sync  => control_buffer.reset_trigger_count,--control.reset_trigger_count,
      enable      => control_buffer.enable_trigger_count,--control.enable_trigger_count,
      event       => local_adc_trigger(0),
      count       => local_spill_count,
      at_max      => open);
  
  timed_counter_1: entity work.timed_counter
    generic map (
      timer_count => x"0BEBC200",
      DATA_WIDTH  => 32)
    port map (
      clk          => clk,
      reset_async  => '0',
      reset_sync   => '0',
      enable       => '1',
      event        => local_adc_trigger(0),
      update_pulse => open,
      timed_count  => monitor.trigger_rate);


  oob_trigger(0) <= '0';
  extra_pulsers: for iBoard in 0 to 3 generate
    monitor.extra_pulses(iBoard) <= control.extra_pulses(iBoard);
    extra_pulsers_1: entity work.extra_pulsers
      generic map (
        START_WIDTH => control.extra_pulses(iBoard).pulse(0)'length)
      port map (
        clk             => clk,
        reset           => '0',
        trigger_in      => local_adc_trigger(iBoard+1),
        bypass          => control.extra_pulses(iBoard).bypass,
        pulse_en        => control.extra_pulses(iBoard).pulse_en,
        pulse_0         => control.extra_pulses(iBoard).pulse(0),
        pulse_1         => control.extra_pulses(iBoard).pulse(1),
        pulse_2         => control.extra_pulses(iBoard).pulse(2),
        pulse_3         => control.extra_pulses(iBoard).pulse(3),
        pulse_4         => control.extra_pulses(iBoard).pulse(4),
        pulse_5         => control.extra_pulses(iBoard).pulse(5),
        pulse_6         => control.extra_pulses(iBoard).pulse(6),
        pulse_7         => control.extra_pulses(iBoard).pulse(7),
        trigger_out     => adc_trigger(iBoard+1),
        oob_trigger_out => oob_trigger(iBoard+1));
  end generate extra_pulsers;
end architecture arch;
