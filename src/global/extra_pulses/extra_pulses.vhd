library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity extra_pulsers is
  
  generic (
    START_WIDTH : integer := 12);

  port (
    clk        : in std_logic;
    reset      : in std_logic;
    trigger_in : in std_logic;
    bypass     : in std_logic;
    pulse_en   : in std_logic_vector(7 downto 0);
    pulse_0    : in std_logic_vector(START_WIDTH-1 downto 0);
    pulse_1    : in std_logic_vector(START_WIDTH-1 downto 0);
    pulse_2    : in std_logic_vector(START_WIDTH-1 downto 0);
    pulse_3    : in std_logic_vector(START_WIDTH-1 downto 0);
    pulse_4    : in std_logic_vector(START_WIDTH-1 downto 0);
    pulse_5    : in std_logic_vector(START_WIDTH-1 downto 0);
    pulse_6    : in std_logic_vector(START_WIDTH-1 downto 0);
    pulse_7    : in std_logic_vector(START_WIDTH-1 downto 0);    
    trigger_out : out std_logic;
    oob_trigger_out : out std_logic
    );

end entity extra_pulsers;

architecture behavioral of extra_pulsers is
  signal counter : unsigned(START_WIDTH downto 0);
  type pulser_array is array (0 to 7) of std_logic_vector(START_WIDTH-1 downto 0);
  signal pulse_count : pulser_array;
  signal pulse : std_logic_vector(7 downto 0);
begin  -- architecture behavioral

  --pulse(0) goes to the trigger signal (bypass allows old operation)
  trigger_out <= pulse(0) when bypass = '0' else trigger_in;
  oob_trigger_out <= or_reduce(pulse(7 downto 1)) when bypass = '0' else '0';
                                       
                                               
  counter_proc: process (clk, reset) is
  begin  -- process counter_proc
    if reset = '1' then                 -- asynchronous reset (active high)
      counter <= (others => '1');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if bypass = '0' then
        if counter(START_WIDTH) = '1' then
          counter <= counter;     
        else
          counter <= counter + 1;
        end if;
        
        if trigger_in = '1' and bypass = '0' then
          counter <= (others => '0');
        end if;
      else
        counter <= (others => '1');
      end if;
    end if;
  end process counter_proc;

  pulser_latch: process (clk, reset) is
  begin  -- process pulser_latch
    if reset = '1' then                 -- asynchronous reset (active high)
      pulse_count(0) <= (others => '0');
      pulse_count(1) <= (others => '0');
      pulse_count(2) <= (others => '0');
      pulse_count(3) <= (others => '0');
      pulse_count(4) <= (others => '0');
      pulse_count(5) <= (others => '0');
      pulse_count(6) <= (others => '0');
      pulse_count(7) <= (others => '0');      
    elsif clk'event and clk = '1' then  -- rising clock edge
      pulse_count(0) <= pulse_0;
      pulse_count(1) <= pulse_1;
      pulse_count(2) <= pulse_2;
      pulse_count(3) <= pulse_3;
      pulse_count(4) <= pulse_4;
      pulse_count(5) <= pulse_5;
      pulse_count(6) <= pulse_6;
      pulse_count(7) <= pulse_7;
    end if;
  end process pulser_latch;
  
  pulsers: for iPulser in 0 to 7 generate   
    pulser: process (clk, reset) is
    begin  -- process pulser
      if reset = '1' then               -- asynchronous reset (active high)
        pulse(iPulser) <= '0';
      elsif clk'event and clk = '1' then  -- rising clock edge
        pulse(iPulser) <= '0';
        if ( pulse_en(iPulser) = '1' and
             counter(START_WIDTH) = '0' and
             (std_logic_vector(counter(START_WIDTH-1 downto 0)) = pulse_count(iPulser))) then
          pulse(iPulser) <= '1';
        end if;
      end if;
    end process pulser;
  end generate pulsers;
end architecture behavioral;
