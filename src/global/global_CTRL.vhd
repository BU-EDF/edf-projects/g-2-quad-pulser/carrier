--
-- define the registers for the quad pulser global control
--

library ieee;
use ieee.std_logic_1164.all;
use work.FREE_RUN_PKG.all;

package GLOBAL_CTRL is

  type pulser_counter_array_t is array (0 to 7) of std_logic_vector(32-1 downto 0);
  type extra_pulses_t is record
    bypass                : std_logic;
    pulse_en              : std_logic_vector(7 downto 0);
    pulse                 : pulser_counter_array_t;
  end record extra_pulses_t;
  constant DEFAULT_EXTRA_PULSES_t : extra_pulses_t := (bypass => '1',
                                                       pulse_en => (others => '0'),
                                                       pulse    => (others => (others => '0')));
  type extra_pulses_array_t is array (0 to 3) of extra_pulses_t;
  
  
  type GLOBAL_Control_t is record
    -- overall box control
    pulser_ena            : std_logic;         -- (R/W) enable all pulsers
    wfd_ena               : std_logic;         -- (R/W) enable all WFDs
    fault_override        : std_logic;         -- (R/W) disable fault shutdown
    global_reset          : std_logic;         -- (A) overall reset
    fault_reset           : std_logic;         -- (A) reset after fault
    fr_settings           : FREE_RUN_t;        -- settings for free-running mode
    reset_trigger_count   : std_logic;
    enable_trigger_count  : std_logic;
    fr_en                 : std_logic;
    ext_en                : std_logic;
    extra_pulses          : extra_pulses_array_t;
  end record GLOBAL_Control_t;
  constant DEFAULT_GLOBAL_Control : GLOBAL_Control_t := (pulser_ena => '0',
                                                         wfd_ena   => '0',
                                                         fault_override => '0',
                                                         global_reset   => '0',
                                                         fault_reset    => '0',
                                                         fr_settings    => DEFAULT_FREE_RUN,
                                                         reset_trigger_count => '0',
                                                         enable_trigger_count => '1',
                                                         fr_en          => '0',
                                                         ext_en          => '0',
                                                         extra_pulses   => (others => DEFAULT_EXTRA_PULSES_t));

  type GLOBAL_Monitor_t is record
    pulser_ena            : std_logic;
    wfd_ena               : std_logic;
    fault_override        : std_logic;
    fault_seen            : std_logic;
    fr_settings           : FREE_RUN_t;        -- settings for free-running mode
    trigger_rate          : std_logic_vector(31 downto 0);
    enable_trigger_count  : std_logic;
    trigger_count         : std_logic_vector(31 downto 0);
    fr_en                 : std_logic;
    ext_en                : std_logic;
    extra_pulses          : extra_pulses_array_t;
  end record GLOBAL_Monitor_t;

end package GLOBAL_CTRL;

