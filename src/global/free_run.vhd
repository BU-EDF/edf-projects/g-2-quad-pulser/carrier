library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.FREE_RUN_PKG.all;

--
-- Free-running trigger for g-2 quad pulser
-- Generate periodic triggers at programmable rate or Bursts
--
-- 2018-04-16 (gastler) -- removed std_logic_unsigned since it isn't standardized
--   converted counters to unsigned types
--   removed subtraction on counters by starting counting at 1
-- 2017-11-26 (hazen) -- fix single trigger, now works for
-- either burst or periodic mode.
-- 2017-11-22 (hazen) -- started over from scratch :(
-- More or less working.  Burst mask not implemented
-- Delay of set.periodic_delay on single trigger

entity free_run is

  generic (
    USEC_C  : integer := 200;           -- clocks per uSEC (nom 100)
    MSEC_C  : integer := 1000;          -- usec per msec (nom 1000)
    BURST_C : integer := 32             -- pulses in a burst
    );

  port (
    clk     : in  std_logic;            -- 100MHz system clock
    rst_n   : in  std_logic;            -- system-wide active low reset
    set     : in  FREE_RUN_t;           -- settings
    trigger : out std_logic);           -- output trigger
end entity free_run;

architecture behavioral of free_run is

  constant US_COUNT_START : unsigned(7 downto 0) := x"01";
  signal us_count : unsigned(7 downto 0);  -- count to one usec
  constant MS_COUNT_START : unsigned(9 downto 0) := "00"&x"01";
  signal ms_count : unsigned(9 downto 0);  -- count to one msec
  signal us_tick  : std_logic;                     -- single usec tick
  signal ms_tick  : std_logic;                     -- single msec tick

  constant PER_COUNT_START : unsigned(15 downto 0) := x"0001";
  signal per_count : unsigned(15 downto 0);  -- count between triggers in ms
  signal per_trig  : std_logic;         -- periodic trigger or burst due
  signal bur_trig  : std_logic;         -- burst trigger output

  signal last_ena  : std_logic;         -- detect changes on enable
  signal pend_trig : std_logic;         -- trigger pending
  signal in_burst  : std_logic;         -- burst is active

  constant BURST_GAP_COUNT_START : unsigned(15 downto 0) := x"0001";
  signal burst_gap_count   : unsigned(15 downto 0);  -- count us between burst pulses
  signal burst_pulse_count : unsigned(5 downto 0);  -- burst pulse number
  signal burst_bit         : std_logic_vector(BURST_C-1 downto 0);  -- burst bit mask

  signal mode : std_logic_vector(1 downto 0);  -- input mode


begin

  -- multiplex output based on mode
  mode <= set.enable & set.burst_ena;

  with mode select
    trigger <=
    per_trig           when "10",       -- periodic mode
    set.single_trigger when "00",       -- single software trigger
    bur_trig           when others;      -- burst software/periodic

  process (clk, rst_n) is
  begin  -- process
    if rst_n = '0' then                 -- asynchronous reset (active low)

      us_count          <= US_COUNT_START;
      ms_count          <= MS_COUNT_START;
      per_count         <= PER_COUNT_START;
      burst_gap_count   <= BURST_GAP_COUNT_START;
      burst_pulse_count <= (others => '0');
      pend_trig         <= '0';
      in_burst          <= '0';

    elsif clk'event and clk = '1' then  -- rising clock edge

      -- update registers for edge detection
      last_ena <= set.enable;

      -- reset counters on changes
      if (set.enable = '1' and last_ena = '0') or (set.single_trigger = '1')
      then
        us_count          <= US_COUNT_START;       
        ms_count          <= MS_COUNT_START;       
        per_count         <= PER_COUNT_START;      
        burst_gap_count   <= BURST_GAP_COUNT_START;
        burst_pulse_count <= (others => '0');
        pend_trig         <= '0';
        in_burst          <= '0';
      end if;

      -- start trigger cycle for software burst mode
      if set.single_trigger = '1' and set.burst_ena = '1' then
        pend_trig <= '1';
      end if;

      ---- us and ms counters run all the time
      ---- (though are reset on trigger or reset)
      -- count microseconds
      us_tick <= '0';
      if us_count = USEC_C then
        us_count <= US_COUNT_START;
        us_tick  <= '1';
      else
        us_count <= us_count + 1;
      end if;

      -- count milliseconds
      ms_tick <= '0';
      if us_tick = '1' then
        if ms_count = MSEC_C then
          ms_tick  <= '1';
          ms_count <= MS_COUNT_START;
        else
          ms_count <= ms_count + 1;
        end if;
      end if;

      -- generate periodic or burst trigger 
      per_trig <= '0';                  -- default: no trigger
      -- evaluate trigger when pend_trig is set or we're in periodic mode
      if (set.enable = '1' or pend_trig = '1') and ms_tick = '1' then
        if per_count = unsigned(set.periodic_delay) then  -- time to 
          per_count <= PER_COUNT_START;           -- yes, reset counter
          per_trig  <= '1';             -- assert trigger
          if pend_trig = '1' then       -- reset pending flag
            pend_trig <= '0';
          end if;
        else
          per_count <= per_count + 1;   -- not yet time, increment count
        end if;
      end if;

      -- start a burst on per_trig or software trig
      if per_trig = '1' then                      -- pending trigger?
        in_burst          <= '1';                 -- set flat in_burst
        burst_gap_count   <= BURST_GAP_COUNT_START;  -- reset gap/pulse counters
        burst_pulse_count <= (others    => '0');
        -- create mask with '1' in upper bit position
        burst_bit         <= (BURST_C-1 => '1', others => '0');
      end if;

      -- deal with running burst (only every us)
      bur_trig <= '0';                  -- default: no burst trigger
      if in_burst = '1' and us_tick = '1' then
        -- time for a new pulse?
        if burst_gap_count = unsigned(set.burst_spacing) then
          burst_gap_count <= BURST_GAP_COUNT_START;
          -- done with pulsing?
          if burst_pulse_count = to_unsigned(BURST_C,burst_pulse_count'length) then
            in_burst <= '0';
          else
            burst_pulse_count <= burst_pulse_count + 1;
            -- test against mask
            if or_reduce(burst_bit and set.burst_mask(BURST_C-1 downto 0)) = '1' then
              bur_trig <= '1';
            end if;
          end if;
          burst_bit <= '0' & burst_bit(BURST_C-1 downto 1);  -- shift mask right
        else
          burst_gap_count <= burst_gap_count + 1;  -- increment gap counter
        end if;
      end if;

    end if;
  end process;

end architecture behavioral;
