library ieee;
use ieee.std_logic_1164.all;

entity resetter is
  generic (
    PIPELINE_CLOCK_TICKS : integer := 1;
    FULL_ASYNC_RESET     : std_logic := '0'
);
  port (
    clk         : in  std_logic;
    reset_async : in  std_logic;
    reset_sync  : in  std_logic;
    reset       : out std_logic);

end entity resetter;

architecture Behavioral of resetter is

  attribute ASYNC_REG : string;
  attribute SHREG_EXTRACT : string;

  signal reset_buffer : std_logic_vector(PIPELINE_CLOCK_TICKS downto 0) := (others => '1');
  attribute ASYNC_REG     of reset_buffer : signal is "yes";
  attribute SHREG_EXTRACT of reset_buffer : signal is "no";
begin  -- architecture Behavioral

  reset <= reset_buffer(reset_buffer'left);     
  reset_proc: process (clk, reset_async) is
  begin  -- process reset_proc
    if reset_async = '1' then             -- asynchronous reset (active high)
      reset_buffer(PIPELINE_CLOCK_TICKS-1 downto 0) <= (others => '1');
      if FULL_ASYNC_RESET = '1' then
        reset_buffer(PIPELINE_CLOCK_TICKS) <= '1';
      end if;
    elsif clk'event and clk = '1' then    -- rising clock edge

      -- reset delay pipeline
      reset_buffer(PIPELINE_CLOCK_TICKS downto 0) <= reset_buffer(PIPELINE_CLOCK_TICKS-1 downto 0) & '0';      

      --Handle the sync reset pipeline
      if reset_sync = '1' then
        reset_buffer <= (others => '1');
      end if;

      
      --buffered reset output
      reset_buffer(PIPELINE_CLOCK_TICKS) <= reset_buffer(PIPELINE_CLOCK_TICKS-1);
    end if;
  end process reset_proc;

  
end architecture Behavioral;

