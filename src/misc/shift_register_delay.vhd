-------------------------------------------------------------------------------
-- Use a shift register to generate a pulse every N clock ticks
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity shift_register_delay is
  
  generic (
    DEPTH : integer := 10);

  port (
    clk          : in  std_logic;
    clk_en       : in  std_logic := '1';
    reset_sync   : in  std_logic := '0';
    reset_async  : in  std_logic := '0';
    pulse        : out std_logic);

end entity shift_register_delay;

architecture behavioral of shift_register_delay is

  signal sr : std_logic_vector(DEPTH-1 downto 0) := (others => '0');
begin  -- architecture behavioral

  sr_proc: process (clk, reset_async) is
  begin  -- process sr_proc
    if reset_async = '1' then               -- asynchronous reset (active low)
      sr <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      pulse <= '0'; -- pulse reset

      -- sync reset for sr
      if reset_sync = '1' then
        sr <= (others => '0');
      elsif clk_en = '1' then
        -- shift '1's into the MS end of the shift register
        sr(DEPTH - 1) <= '1';
        sr(DEPTH - 2 downto 0) <= sr(DEPTH-1 downto 1);

        -- When the LSB is a '1', reset the shift register and output a pulse
        if sr(0) = '1' then
          sr <= (others => '0');
          pulse <= '1';
        end if;
      end if;
    end if;
  end process sr_proc;
end architecture behavioral;
