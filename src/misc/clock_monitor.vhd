library ieee;
use ieee.std_logic_1164.all;

entity clock_monitor is
  
  generic (
    SYS_CLK_MHZ     : integer := 100;
    WATCHED_CLK_MHZ : integer := 100;
    GOOD_COUNT      : integer := 10);

  port (
    sys_clk        : in std_logic;
    sys_reset      : in std_logic;
    watch_clk      : in std_logic;
    locked         : out std_logic);

end entity clock_monitor;

architecture behavioral of clock_monitor is
  attribute ASYNC_REG : string;
  
  component pacd is
    port (
      iPulseA : IN  std_logic;
      iClkA   : IN  std_logic;
      iRSTAn  : IN  std_logic;
      iClkB   : IN  std_logic;
      iRSTBn  : IN  std_logic;
      oPulseB : OUT std_logic);
  end component pacd;
  
  constant LOCKED_SR_SIZE : integer := GOOD_COUNT;
  signal   locked_sr      : std_logic_vector(2*LOCKED_SR_SIZE -1 downto 0);
  attribute ASYNC_REG of locked_sr : signal is "yes";
  
  signal   watched_heart_beat_response : std_logic;
  signal   watched_heart_beat          : std_logic;
  
  signal   lock_failed : std_logic;
  attribute ASYNC_REG of lock_failed : signal is "yes";

  
  signal   sys_heart_beat_response : std_logic;
  signal   sys_heart_beat          : std_logic;

  constant RESET_SR_SIZE : integer := (SYS_CLK_MHZ*LOCKED_SR_SIZE)/WATCHED_CLK_MHZ;
  signal   reset_sr      : std_logic_vector(RESET_SR_SIZE -1 downto 0);
begin  -- architecture behavioral

  --output the MSB of the shift register as the locked signal
  locked <= locked_sr(locked_sr'left);

  sr_proc: process (watch_clk, lock_failed) is
  begin  -- process sr_proc
    if lock_failed = '1' then           -- asynchronous reset (active high)
      locked_sr <= (others => '0');
    elsif watch_clk'event and watch_clk = '1' then  -- rising clock edge
      --shift in good values into locked sr
      locked_sr <= locked_sr(locked_sr'left -1 downto 0) &'1';
      --respond to a hear beat pulse
      watched_heart_beat_response <= '0';
      if watched_heart_beat = '1' then
        watched_heart_beat_response <= '1';
      end if;
--      watched_heart_beat_response <= watched_heart_beat;
    end if;
  end process sr_proc;

  pacd_1: entity work.pacd
    port map (
      iPulseA => sys_heart_beat,
      iClkA   => sys_clk,
      iRSTAn  => '1',
      iClkB   => watch_clk,
      iRSTBn  => '1',
      oPulseB => watched_heart_beat);

  pacd_2: entity work.pacd
    port map (
      iPulseA => watched_heart_beat_response,
      iClkA   => watch_clk,
      iRSTAn  => '1',
      iClkB   => sys_clk,
      iRSTBn  => '1',
      oPulseB => sys_heart_beat_response);
  
  watch_dog: process (sys_clk, sys_reset) is
  begin  -- process watch_dog
    if sys_reset = '1' then                 -- asynchronous reset (active high)
      lock_failed <= '1';
    elsif sys_clk'event and sys_clk = '1' then  -- rising clock edge
      assert RESET_SR_SIZE /= 0 report "BAD SR SIZE IN " severity error;

      --reset pulses
      sys_heart_beat <= '0';
      lock_failed <= '0';
      
      if sys_heart_beat_response = '1' then
        -- We got a heart beat response, so the watched clock domain is
        -- working and we can zero out our reset_sr
        reset_sr <= (others => '0');
        --send another heart beat to watch the other clock domain
        sys_heart_beat <= '1';
      elsif reset_sr(reset_sr'left) = '1' then
        --too many clock ticks have gone by without a response so we should
        --reset the other clock domain process.
        lock_failed <= '1';
        --start waiting again.
        reset_sr <= (others => '0');
        --send another heart beat to watch the other clock domain
        sys_heart_beat <= '1';
      else
        --keep track of how many clock ticks we've missed
        reset_sr <= reset_sr(reset_sr'left-1 downto 0) & '1';
      end if;
    end if;
  end process watch_dog;
  
end architecture behavioral;
