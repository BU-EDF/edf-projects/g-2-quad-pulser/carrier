library IEEE;
use IEEE.std_logic_1164.all;


entity pipeline_delay is
  
  generic (
    WIDTH : integer := 32;
    DELAY : integer := 1);

  port (
    clk         : in  std_logic;
    clk_en      : in  std_logic := '1';  --default allows us to ignore the
                                         --signal when not needed. This is
                                         --IGNORED when DELAY is zero
    reset_async : in  std_logic;
    data_in     : in  std_logic_vector(WIDTH-1 downto 0);
    data_out    : out std_logic_vector(WIDTH-1 downto 0));

end entity pipeline_delay;

architecture behavioral of pipeline_delay is

  type pipeline_t is array (DELAY downto 0) of std_logic_vector(WIDTH-1 downto 0);
  signal pipeline : pipeline_t;
begin  -- architecture behavioral

  --Handles zero delay well (negative is a syntax error)
  pipeline(0) <= data_in;
  data_out    <= pipeline(DELAY);

  --only generate a pipeline if we really need one
  pipeline_needed: if DELAY > 0 generate   
    pipeline_proc: process (clk,reset_async) is
    begin  -- process pipeline_proc
      if reset_async = '1' then
        pipeline(DELAY downto 1) <= (others => (others => '0'));
      elsif clk'event and clk = '1' then  -- rising clock edge
        if clk_en = '1' then
          pipeline(DELAY downto 1) <= pipeline(DELAY-1 downto 0);          
        end if;
      end if;
    end process pipeline_proc;    
  end generate pipeline_needed;

end architecture behavioral;
