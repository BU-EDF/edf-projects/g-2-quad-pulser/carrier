---------------------------------------------------------------------------------
-- high speed message encoder
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;


use work.HS_Message_package.all;
use work.hs_message_CTRL.all;

entity hs_message_encoder is
  
  port (
    clk                : in  std_logic;
    new_spill          : in  std_logic;
    new_spill_and_rst  : in  std_logic;
    OOB_trigger        : in  std_logic;
    shutdown           : in  std_logic;
    quad_pulser_trigger: in  std_logic;
    hs_mesg_data       : out std_logic_vector(3 downto 0);
    hs_mesg_data_valid : out std_logic;
    hs_mesg_empty      : in  std_logic;
    control            : in  hsm_TX_control_t;
    monitor            : out hsm_TX_monitor_t);

end entity hs_message_encoder;

architecture behavioral of hs_message_encoder is
  component counter is
    generic (
      roll_over   : std_logic;
      end_value   : std_logic_vector(31 downto 0);
      start_value : std_logic_vector(31 downto 0);
      DATA_WIDTH  : integer);
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      enable      : in  std_logic;
      event       : in  std_logic;
      count       : out std_logic_vector(DATA_WIDTH-1 downto 0);
      at_max      : out std_logic);
  end component counter;

  component pipeline_delay is
    generic (
      WIDTH : integer;
      DELAY : integer);
    port (
      clk         : in  std_logic;
      clk_en      : in  std_logic := '1';
      reset_async : in  std_logic;
      data_in     : in  std_logic_vector(WIDTH-1 downto 0);
      data_out    : out std_logic_vector(WIDTH-1 downto 0));
  end component pipeline_delay;
  
  constant MESSAGE_COUNT : integer := 16;  
  signal message_sent : std_logic_vector(MESSAGE_COUNT-1 downto 0);
  signal send_message : std_logic_vector(MESSAGE_COUNT-1 downto 0);

  signal reset_counter : std_logic_vector(MESSAGE_COUNT-1 downto 0);
begin  -- architecture behavioral
 
  new_message_proc: process (clk) is
  begin  -- process new_message_proc
    if clk'event and clk = '1' then  -- rising clock edge      
      if new_spill           = '1' or control.send_message(to_integer(unsigned(HSM_TX_Trigger))) = '1' then
        send_message(to_integer(unsigned(HSM_TX_Trigger)))          <= '1';
      end if;
      if new_spill_and_rst   = '1' or control.send_message(to_integer(unsigned(HSM_TX_Spill_reset))) = '1' then
        send_message(to_integer(unsigned(HSM_TX_Spill_reset)))      <= '1';
      end if;
      if oob_trigger         = '1' or control.send_message(to_integer(unsigned(HSM_TX_OOB_Trigger))) = '1' then
        send_message(to_integer(unsigned(HSM_TX_OOB_Trigger)))      <=  '1';
      end if;
      if shutdown            = '1' or control.send_message(to_integer(unsigned(HSM_TX_SHUTDOWN))) = '1' then
        send_message(to_integer(unsigned(HSM_TX_SHUTDOWN)))         <= '1';
      end if;
      if quad_pulser_trigger = '1' or control.send_message(to_integer(unsigned(HSM_TX_quad_pulser_trig))) = '1' then
        send_message(to_integer(unsigned(HSM_TX_quad_pulser_trig))) <= '1';
      end if;

      if message_sent(to_integer(unsigned(HSM_TX_Trigger))) = '1' then
        send_message(to_integer(unsigned(HSM_TX_Trigger)))          <= '0';
      end if;
      if message_sent(to_integer(unsigned(HSM_TX_Spill_reset))) = '1' then
        send_message(to_integer(unsigned(HSM_TX_Spill_reset)))          <= '0';
      end if;
      if message_sent(to_integer(unsigned(HSM_TX_OOB_Trigger))) = '1' then
        send_message(to_integer(unsigned(HSM_TX_OOB_Trigger)))          <= '0';
      end if;
      if message_sent(to_integer(unsigned(HSM_TX_SHUTDOWN))) = '1' then
        send_message(to_integer(unsigned(HSM_TX_SHUTDOWN)))          <= '0';
      end if;
      if message_sent(to_integer(unsigned(HSM_TX_quad_pulser_trig))) = '1' then
        send_message(to_integer(unsigned(HSM_TX_quad_pulser_trig)))          <= '0';
      end if;

      
    end if;
  end process new_message_proc;

  hs_mesg_data_valid <= or_reduce(message_sent);
  process (clk) is
  begin  -- process
    if clk'event and clk = '1' then  -- rising clock edge
      message_sent <= (others => '0');
      if hs_mesg_empty = '1' then
        --An if-else tree was used here to give priority
        
        if send_message(to_integer(unsigned(HSM_TX_SHUTDOWN))) = '1' and message_sent(to_integer(unsigned(HSM_TX_SHUTDOWN))) = '0' then
          --trigger window message
          hs_mesg_data <= HSM_TX_SHUTDOWN;
          message_sent(to_integer(unsigned(HSM_TX_SHUTDOWN))) <= '1';
        elsif send_message(to_integer(unsigned(HSM_TX_Spill_reset))) = '1' and message_sent(to_integer(unsigned(HSM_TX_Spill_reset))) = '0' then
          --trigger window message
          hs_mesg_data <= HSM_TX_Spill_reset;
          message_sent(to_integer(unsigned(HSM_TX_Spill_reset))) <= '1';
        elsif send_message(to_integer(unsigned(HSM_TX_Trigger))) = '1' and message_sent(to_integer(unsigned(HSM_TX_Trigger))) = '0' then
          --trigger window message
          hs_mesg_data <= HSM_TX_Trigger;
          message_sent(to_integer(unsigned(HSM_TX_Trigger))) <= '1';
        elsif send_message(to_integer(unsigned(HSM_TX_OOB_Trigger))) = '1' and message_sent(to_integer(unsigned(HSM_TX_OOB_Trigger))) = '0' then
          --trigger window message
          hs_mesg_data <= HSM_TX_OOB_Trigger;
          message_sent(to_integer(unsigned(HSM_TX_OOB_Trigger))) <= '1';
        elsif send_message(to_integer(unsigned(HSM_TX_quad_pulser_trig))) = '1' and message_sent(to_integer(unsigned(HSM_TX_quad_pulser_trig))) = '0' then
          --trigger window message
          hs_mesg_data <= HSM_TX_quad_pulser_trig;
          message_sent(to_integer(unsigned(HSM_TX_quad_pulser_trig))) <= '1';
        else
          --unspecified messages are here and are orderd to put 15 having
          --lowest priority.
          for iVal in 15 downto 5 loop
            if send_message(iVal) = '1' then
              hs_mesg_data <= std_logic_vector(to_unsigned(iVal,4));
              message_sent(iVal) <= '1';              
            end if;
          end loop;  -- iVal
        end if;        
      end if;
    end if;
  end process;


  pipeline_delay_1: entity work.pipeline_delay
    generic map (
      WIDTH => MESSAGE_COUNT,
      DELAY => 1)
    port map (
      clk         => clk,
      clk_en      => '1',
      reset_async => '0',
      data_in     => (others => control.reset_mesg_counters),
      data_out    => reset_counter);
  
  counters: for iMesgVal in MESSAGE_COUNT-1 downto 0 generate
    counter_1: entity work.counter
      generic map (
        roll_over   => '0',
        DATA_WIDTH  => 32)
      port map (
        clk         => clk,
        reset_async => '0',
        reset_sync  => reset_counter(iMesgVal),
        enable      => '1',
        event       => message_sent(iMesgVal),
        count       => monitor.counter(iMesgVal),
        at_max      => open);   
  end generate counters;
  
end architecture behavioral;
