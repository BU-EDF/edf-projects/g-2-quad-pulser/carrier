-------------------------------------------------------------------------------
-- Carrier -> ADCBoard HS message package
-- Dan Gastler
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


package HS_Message_Package is

  constant HSM_TX_Trigger          : std_logic_vector(3 downto 0) := x"0";
  constant HSM_TX_Spill_reset      : std_logic_vector(3 downto 0) := x"1"; --also
                                                                        --causes a spill
  constant HSM_TX_OOB_Trigger      : std_logic_vector(3 downto 0) := x"2";
  constant HSM_TX_SHUTDOWN         : std_logic_vector(3 downto 0) := x"3"; 

  constant HSM_TX_quad_pulser_trig : std_logic_vector(3 downto 0) := x"4";



  constant HSM_RX_SPARK_1 : std_logic_vector(3 downto 0) := x"0";
end package HS_Message_Package;
