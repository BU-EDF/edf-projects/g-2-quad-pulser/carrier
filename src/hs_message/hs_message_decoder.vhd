-------------------------------------------------------------------------------
-- Decode high speed messages 
-- Dan Gastler
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_misc.all;
USE ieee.numeric_std.ALL;


use work.types.all;
use work.HS_Message_Package.all;
use work.hs_message_CTRL.all;


library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity hs_messsage_decoder is
  
  port (
    clk                : in  std_logic;
    data               : in  slv7_array_t(3 downto 0);
    data_valid         : in  std_logic_vector(3 downto 0);
    spark_trigger      : out std_logic_vector(0 downto 0);
    monitor            : out HSM_RX_Monitor_t
    );

end entity hs_messsage_decoder;

architecture behavioral of hs_messsage_decoder is
  constant MESSAGE_COUNT : integer := 16;
  signal message         : std_logic_vector((MESSAGE_COUNT*4)-1 downto 0);
begin  -- architecture behavioral
  
  decoder: process (clk) is
  begin  -- process decoder
    if clk'event and clk = '1' then  -- rising clock edge
      --pulse resets
      spark_trigger <= (others => '0');
      message <= (others => '0');      
      -- decoder
      for iADC in 3 downto 0 loop
        if data_valid(iADC) = '1' then
          message(iADC*4 + to_integer(unsigned(data(iADC)(3 downto 0)))) <= '1';
          case data(iADC)(3 downto 0) is
            when HSM_RX_SPARK_1 => spark_trigger(0) <= '1';
            when others => null;
          end case;
        end if;
      end loop;  -- iADC
    end if;
  end process decoder;
 
  ADC_BOARD_loop: for iADC in 3 downto 0 generate
    counters: for iMesgVal in MESSAGE_COUNT-1 downto 0 generate      
      counter_1: entity work.counter
        generic map (
          roll_over   => '0',
          DATA_WIDTH  => 32)
        port map (
          clk         => clk,
          reset_async => '0',
          reset_sync  => '0',
          enable      => '1',
          event       => message((iADC*4)+iMesgVal),
          count       => monitor.ADC(iADC).Mesg(iMesgVal),
          at_max      => open);   
    end generate counters;    
  end generate ADC_BOARD_loop;


  
end architecture behavioral;
