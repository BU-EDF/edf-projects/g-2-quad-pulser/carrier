library IEEE;
use IEEE.std_logic_1164.all;

use work.types.all;

package hs_message_CTRL is

  type hsm_TX_control_t is record
    send_message             : std_logic_vector(15 downto 0);
    reset_mesg_counters      : std_logic;
  end record hsm_TX_control_t;
  type hsm_tx_control_array_t is array (3 downto 0) of hsm_tx_control_t;
  
  type hsm_TX_monitor_t is record
    counter                  : slv32_array_t(15 downto 0);
  end record hsm_TX_monitor_t;
  type hsm_tx_monitor_array_t is array (3 downto 0) of hsm_tx_monitor_t;
  
  type HSM_RX_CNT_t is record
    Mesg                     : slv32_array_t(15 downto 0);
  end record HSM_RX_CNT_t;
  type HSM_RX_CNT_array_t is array (3 downto 0) of HSM_RX_CNT_t;
  
  type HSM_RX_Monitor_t is record
   ADC                       : HSM_RX_CNT_array_t;
  end record HSM_RX_Monitor_t;



  
  type hsm_control_t is record
    TX                       : hsm_TX_control_array_t;
  end record hsm_control_t;
  
  type hsm_monitor_t is record
    TX                       : hsm_TX_monitor_array_t;
    RX                       : HSM_RX_Monitor_T;
  end record hsm_monitor_t;

  
end package hs_message_CTRL;
