library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.event_builder_CTRL.all;

entity adc_frame_dma is
  
  generic (
    ADC_NUMBER           : integer := 0;
    FIFO_SIZE_PWR_OF_TWO : integer := 12;
    DMA_BLOCK_PWR_OF_TWO : integer := 8);--;
--    PREFETCH_WIDTH       : integer := 1);

  port (
    clk                    : in  std_logic;
    reset_async            : in  std_logic;
    fifo_data              : in  std_logic_vector(33 downto 0);
    fifo_data_valid        : in  std_logic;
    fifo_rd                : out std_logic;
    buffered_count         : in  STD_LOGIC_VECTOR(FIFO_SIZE_PWR_OF_TWO-1 DOWNTO 0);
    frame_spill_id         : out std_logic_vector(15 downto 0);
    frame_size_left        : out std_logic_vector(23 downto 0);
    dma_block_request      : in  std_logic;
    dma_block_request_size : in  std_logic_vector(DMA_BLOCK_PWR_OF_TWO downto 0);
    dma_block_data         : out std_logic_vector(31 downto 0);
    dma_block_data_valid   : out std_logic;
    dma_block_data_last    : out std_logic;
    monitor                : out ADCFrameDMA_Monitor_t
    );

end entity adc_frame_dma;

architecture behavioral of adc_frame_dma is
  component counter is
    generic (
      roll_over   : std_logic;
      end_value   : std_logic_vector(31 downto 0);
      start_value : std_logic_vector(31 downto 0);
      DATA_WIDTH  : integer);
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      enable      : in  std_logic;
      event       : in  std_logic;
      count       : out std_logic_vector(DATA_WIDTH-1 downto 0);
      at_max      : out std_logic);
  end component counter;
  
  type ro_state_t is (ROS_INIT,
                      ROS_WAIT_FOR_NEXT_FRAME,
                      ROS_GET_SIZE,
                      ROS_GET_SPILL_ID,
                      ROS_WAIT_FOR_FRAME,
                      ROS_WAIT_FOR_REQUEST,
                      ROS_SEND_DATA,
                      ROS_ERROR);            
  signal ro_state : ro_state_t;

  signal frame_size : unsigned(23 downto 0);
  signal size_left  : unsigned(23 downto 0);
  signal readout_frame_size_m2 : unsigned(23 downto 0);
  signal block_send_count : unsigned(DMA_BLOCK_PWR_OF_TWO downto 0);
  signal block_done : std_logic;

  constant PREFETCH_WIDTH       : integer := 1;
  type word_sr_t is array (2**PREFETCH_WIDTH -1 downto 0) of std_logic_vector(31 downto 0);
  signal buffered_data : word_sr_t;


  signal data_out_valid : std_logic;
  signal fifo_in_rd     : std_logic;
  signal block_write_count : std_logic_vector(31 downto 0);
  signal fifo_read_count : std_logic_vector(31 downto 0);

  signal size_error : std_logic;
  
begin  -- architecture behavioral

  state_mon_proc: process (ro_state) is
  begin  -- process state_mon_proc
    case ro_state is
      when ROS_INIT                => monitor.state <= x"0";                  
      when ROS_WAIT_FOR_NEXT_FRAME => monitor.state <= x"1";   
      when ROS_GET_SIZE            => monitor.state <= x"2";              
      when ROS_GET_SPILL_ID        => monitor.state <= x"3";          
      when ROS_WAIT_FOR_FRAME      => monitor.state <= x"4";        
      when ROS_WAIT_FOR_REQUEST    => monitor.state <= x"5";      
      when ROS_SEND_DATA           => monitor.state <= x"6";
      when ROS_ERROR               => monitor.state <= x"7";                                      
      when others                  => monitor.state <= x"F";
    end case;
  end process state_mon_proc;

  
  dma_interface_state_machine: process (clk, reset_async) is
  begin  -- process dma_data_present_proc
    if reset_async = '1' then           -- asynchronous reset (active high)
      ro_state <= ROS_INIT;
    elsif clk'event and clk = '1' then  -- rising clock edge
      size_error <= '0';
      case ro_state is
        when ROS_INIT =>
          ro_state <= ROS_WAIT_FOR_NEXT_FRAME;
        when ROS_WAIT_FOR_NEXT_FRAME =>
          if or_reduce(buffered_count(buffered_count'left downto 2)) = '1' then
            --we have more than four words, wo we can grab the first two words
            --of the frame
            ro_state <= ROS_GET_SIZE;
          end if;
        when ROS_GET_SIZE =>
          if fifo_data_valid = '1' then
            --save the frame size and get the spill ID
            ro_state <= ROS_GET_SPILL_ID;
          end if;
        when ROS_GET_SPILL_ID =>
          if fifo_data_valid = '1' then
            --save the spill id and wait for the frame to be in the buffer
            ro_state <= ROS_WAIT_FOR_FRAME;
          end if;
        when ROS_WAIT_FOR_FRAME =>
          if unsigned(buffered_count) >= readout_frame_size_m2(FIFO_SIZE_PWR_OF_TWO-1 downto 0) then
            --full frame is in the buffer, wait for readout request
            ro_state <= ROS_WAIT_FOR_REQUEST;
          elsif or_reduce(std_logic_vector(readout_frame_size_m2(readout_frame_size_m2'left downto FIFO_SIZE_PWR_OF_TWO))) = '1' then
            ro_state <= ROS_ERROR;
          end if;
        when ROS_WAIT_FOR_REQUEST =>
          if dma_block_request = '1' then
            --send up to block_size words
            ro_state <= ROS_SEND_DATA;
          end if;
        when ROS_SEND_DATA =>
          --sending data
          --if or_reduce(std_logic_vector(size_left)) = '0' then
          if block_send_count = 0 and or_reduce(std_logic_vector(size_left)) = '0' then
            --we are done sending out this frame. 
            ro_state <= ROS_WAIT_FOR_NEXT_FRAME;
          elsif block_done = '1' then
            ro_state <= ROS_WAIT_FOR_REQUEST;
          end if;
        when ROS_ERROR =>
          size_error <= '1';
        when others => ro_state <= ROS_INIT;
      end case;
    end if;
  end process dma_interface_state_machine;

  dma_block_data_last   <= block_done;
  --frame_size_left       <= std_logic_vector(size_left);
  dma_block_data_valid  <= data_out_valid;
  fifo_rd               <= fifo_in_rd;
  dma_interface_proc: process (clk, reset_async) is
  begin  -- process dma_interface_proc
    if reset_async = '1' then           -- asynchronous reset (active high)
      
    elsif clk'event and clk = '1' then  -- rising clock edge
      fifo_in_rd <= '0';
      block_done <= '0';
      data_out_valid <= '0';

      monitor.frame_size <= std_logic_vector(frame_size);
      monitor.buffer_count(FIFO_SIZE_PWR_OF_TWO -1 downto 0) <= buffered_count;
      
      case ro_state is
        -----------------------------------------------------
        when ROS_INIT =>
          size_left <= (others => '0');
        -----------------------------------------------------
        when ROS_WAIT_FOR_NEXT_FRAME =>
          if or_reduce(buffered_count(buffered_count'left downto 2)) = '1' then
            fifo_in_rd <= '1';
          end if;
        -----------------------------------------------------
        when ROS_GET_SIZE =>
          if fifo_data_valid = '1' then
            fifo_in_rd <= '1';
            --cache the frame size
--            frame_size <= unsigned(fifo_data(23 downto 0));
            frame_size <= unsigned(fifo_data(31 downto 8));
            --we still want this word, so we buffer it. 
--            buffered_data <= fifo_data(31 downto 0) & buffered_data(1);
            buffered_data <= fifo_data(31 downto 0) & buffered_data(buffered_data'left downto 1);
          end if;
        -----------------------------------------------------
        when ROS_GET_SPILL_ID =>
          if fifo_data_valid = '1' then
            -- compute the frame size minus two since we need that many more
            -- words from the FIFO
            readout_frame_size_m2 <= unsigned(frame_size) - to_unsigned(2,frame_size'length);
              
            -- output the frame's spill ID
            frame_spill_ID <= fifo_data(23 downto 8);
            --we still want this word, so we buffer it. 
--            buffered_data(1) <= x"0" & std_logic_vector(to_unsigned(ADC_NUMBER,4)) & fifo_data(23 downto 0);
--            buffered_data(0) <= buffered_data(1);
            buffered_data(buffered_data'left) <= x"0" & std_logic_vector(to_unsigned(ADC_NUMBER,4)) & fifo_data(23 downto 0);
            buffered_data(buffered_data'left -1 downto 0) <= buffered_data(buffered_data'left downto 1);
          end if;
        -----------------------------------------------------
        when ROS_WAIT_FOR_FRAME =>
          if unsigned(buffered_count) >= readout_frame_size_m2(FIFO_SIZE_PWR_OF_TWO-1 downto 0) then            --new
            size_left       <= frame_size;
            frame_size_left <= std_logic_vector(frame_size);          
          end if;                                   
        -----------------------------------------------------
        when ROS_WAIT_FOR_REQUEST =>
          if dma_block_request = '1' then
            --Cache the number of reads we are suppose to do.
            block_send_count <= unsigned(dma_block_request_size);
            block_done <= '0';

            -- Get the next word from the fifo
            if or_reduce(dma_block_request_size(DMA_BLOCK_PWR_OF_TWO downto PREFETCH_WIDTH)) = '1' then
              fifo_in_rd <= '1';
            end if;
          end if;
        when ROS_SEND_DATA =>                    
          if or_reduce(std_logic_vector(block_send_count)) = '1' then
            -- still have words to write

            if size_left <= 2**PREFETCH_WIDTH then
              --we are at the last two words to be read, but they are already
              --in the buffered_data shift register, so we dont' want to read
              --from the FIFO
              --
              dma_block_data <= buffered_data(0); -- output data
              data_out_valid <= '1';

              --Shift in empty data (DEADBEEF for debugging)              
              buffered_data  <= x"DEADBEEF" & buffered_data(buffered_data'left downto 1);

              -- count down this DMA block
              block_send_count <= block_send_count -1;
              if( or_reduce(std_logic_vector(size_left)) = '1') then
                size_left        <= size_left -1;
              end if;
              --Note the end of the block
              if block_send_count = 1 then
                frame_size_left       <= std_logic_vector(size_left);
                block_done <= '1';
              end if;
              
            elsif fifo_data_valid = '1' then
              --we are in the bulk of the data and reading out the FIFO, so we
              --process data when it gets out of the fifo and stuff it in our buffer.
              --
              dma_block_data <= buffered_data(0); -- output data
              data_out_valid <= '1';

              --Shift in empty data ( we now lose the two bits of first/last)
              buffered_data  <= fifo_data(31 downto 0) & buffered_data(buffered_data'left downto 1);

              -- count down this DMA block
              block_send_count <= block_send_count -1;
              size_left        <= size_left -1;
              -- Note the end of the block
              if block_send_count = 1 then
                frame_size_left       <= std_logic_vector(size_left);
                block_done <= '1';
              end if;

              -- Get the next word from the fifo
              if ((size_left /= (2**PREFETCH_WIDTH + 1)) and ( block_send_count /= 1)) then
                fifo_in_rd <= '1';                
              end if;
            end if;            
          end if;         
        when others => NULL;
      end case;
          
    end if;
  end process dma_interface_proc;

  counter_1: entity work.counter
    port map (
      clk         => clk,
      reset_async => reset_async,
      reset_sync  => dma_block_request,
      enable      => '1',
      event       => data_out_valid,
      count       => block_write_count,
      at_max      => open);

  counter_2: entity work.counter
    port map (
      clk         => clk,
      reset_async => reset_async,
      reset_sync  => block_done,
      enable      => '1',
      event       => fifo_in_rd,
      count       => fifo_read_count,
      at_max      => open);

    counter_3: entity work.counter
    port map (
      clk         => clk,
      reset_async => reset_async,
      reset_sync  => block_done,
      enable      => '1',
      event       => size_error,
      count       => monitor.size_error_count,
      at_max      => open);

end architecture behavioral;
