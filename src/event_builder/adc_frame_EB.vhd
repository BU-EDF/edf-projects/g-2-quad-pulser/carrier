library ieee;
use ieee.std_logic_1164.all;
use work.event_builder_CTRL.all;

entity adc_frame_EB is
  
  port (
    clk_EB        : in std_logic;
    reset_async   : in  std_logic;
    data_in       : in  std_logic_vector(7 downto 0);
    data_in_start : in  std_logic;
    data_in_end   : in  std_logic;
    data_in_valid : in  std_logic;
    fifo_word     : out std_logic_vector(33 downto 0);
    fifo_wr       : out std_logic;
    monitor       : out ADCFrameEB_Monitor_t);

end entity adc_frame_EB;

architecture behavioral of adc_frame_EB is

  signal capturing_frame     : std_logic;
  signal delay_data_in       : std_logic_vector(7 downto 0);
  signal delay_data_in_start : std_logic;
  signal delay_data_in_end   : std_logic;
  signal delay_data_in_valid : std_logic;

  signal word_byte_sr        : std_logic_vector(3 downto 0);
  signal capturing_frame_end : std_logic;
  signal write_word          : std_logic_vector(33 downto 0);
  
  signal error_unexpected_end   : std_logic;
  signal error_unexpected_start : std_logic;

  component counter is
    generic (
      roll_over   : std_logic;
      end_value   : std_logic_vector(31 downto 0);
      start_value : std_logic_vector(31 downto 0);
      DATA_WIDTH  : integer);
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      enable      : in  std_logic;
      event       : in  std_logic;
      count       : out std_logic_vector(DATA_WIDTH-1 downto 0);
      at_max      : out std_logic);
  end component counter;
  
begin  -- architecture behavioral

  fill_fifo_proc: process (clk_EB, reset_async) is
  begin  -- process fill_fifo_proc
    if reset_async = '1' then               -- asynchronous reset (active high)
      capturing_frame <= '0';
      capturing_frame_end <= '0';
    elsif clk_EB'event and clk_EB = '1' then  -- rising clock edge
      --error pulses
      error_unexpected_end   <= '0';
      error_unexpected_start <= '0';
      
      --Buffer the input siganls for the fifo write process
      delay_data_in       <= data_in;       
      delay_data_in_start <= data_in_start; 
      delay_data_in_end   <= data_in_end;   
      delay_data_in_valid <= data_in_valid; 

      --Capturing frame can be set by a start bit directly, or delayed by one
      --clock if we are ending the frame. (so that the other proc can handle
      --the last word
      if capturing_frame_end = '1' then
        capturing_frame     <= '0';
        capturing_frame_end <= '0';
      end if;
      
      --new data
      if data_in_valid = '1' then
        if capturing_frame = '1' then
          --we are in a frame capture

          --CHeck for invalid start during capture
          if data_in_start = '1' then
            error_unexpected_start <= '1';
          end if;

          if data_in_end = '1' then
            capturing_frame_end <= '1';
          end if;
        
        else
          --we are waiting for a new frame
          if data_in_start = '1' then
            capturing_frame <= '1';
          end if;
          if data_in_end = '1' then
            --we shouldn't get an end here, error
            error_unexpected_end <= '1';
          end if;
        end if;
      end if;
    end if;
  end process fill_fifo_proc;

  fifo_word <= write_word;
  write_fifo_proc: process (clk_EB,reset_async) is
  begin  -- process write_fifo_proc
    if reset_async = '1' then
      fifo_wr <= '0';
    elsif clk_EB'event and clk_EB = '1' then  -- rising clock edge
      fifo_wr <= '0';
      if capturing_frame = '0' then
        --wait for new data
        write_word <= "00"&x"00000000";
        word_byte_sr <= x"1";
      else
        if delay_data_in_valid = '1' then
          --Shift in the next word
          write_word(31 downto 0) <= delay_data_in & write_word(31 downto 8);

          --Keep track of start and end words (we'll probably throw this away next)
          if word_byte_sr(0) = '1' then
            --This is the only time for a start word
            write_word(32) <= delay_data_in_start;
          end if;
          
          -- write when we have filled our 32bit word
          if word_byte_sr(3) = '1' or delay_data_in_end = '1' then
            fifo_wr <= '1';
            --This should happen at the end of the 32 bit word, but if it comes
            --earlier in error, then we finish this 32bit word to get in sync again
            write_word(33) <= delay_data_in_end;
          end if;

          --update the word_byte_sr
          word_byte_sr <= word_byte_sr(2 downto 0)&word_byte_sr(3);
        end if;
      end if;
    end if;
  end process write_fifo_proc;

  counter_1: entity work.counter
    port map (
      clk         => clk_EB,
      reset_async => reset_async,
      reset_sync  => '0',
      enable      => '1',
      event       => error_unexpected_start,
      count       => monitor.unexpected_start_count,
      at_max      => open);
  counter_2: entity work.counter
    port map (
      clk         => clk_EB,
      reset_async => reset_async,
      reset_sync  => '0',
      enable      => '1',
      event       => error_unexpected_end,
      count       => monitor.unexpected_end_count,
      at_max      => open);

end architecture behavioral;
