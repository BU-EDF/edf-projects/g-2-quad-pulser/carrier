library IEEE;
use IEEE.std_logic_1164.all;

use work.types.all;

package event_builder_CTRL is

  type eb_control_t is record
    reset_sync        : std_logic;
    board_enable : std_logic_vector(3 downto 0);
    reset_block_count : std_logic;
    reset_byte_count  : std_logic;
    reset_board_count : std_logic;
    enable            : std_logic;
  end record eb_control_t;
  constant DEFAULT_EB_control : eb_control_t := (reset_sync => '0',
                                                 board_enable => x"0",
                                                 reset_block_count => '0',
                                                 reset_byte_count  => '0',
                                                 reset_board_count => '0',
                                                 enable => '0');
  type ADCFrameEB_Monitor_t is record
    unexpected_start_count : std_logic_vector(31 downto 0);
    unexpected_end_count   : std_logic_vector(31 downto 0);
  end record ADCFrameEB_Monitor_t;

  type ADCFrameDMA_Monitor_t is record
    state : std_logic_vector(3 downto 0);
    frame_size : std_logic_vector(23 downto 0); -- new
    buffer_count : std_logic_vector(31 downto 0); --new
    size_error_count : std_logic_vector(31 downto 0); -- new
  end record ADCFrameDMA_Monitor_t;

  type ADCFrame_Monitor_t is record
    DAQ_FIFO_out_Count : std_logic_vector(16 downto 0);--13 downto 0);
    DAQ_FIFO_in_Count  : std_logic_vector(16 downto 0);--13 downto 0);
    DAQ_FIFO_full      : std_logic;
    DAQ_FIFO_empty     : std_logic;
    EB  : ADCFrameEB_Monitor_t;
    DMA : ADCFrameDMA_Monitor_t;
  end record ADCFrame_Monitor_t;

  type ADCFrame_Monitors_t is array (3 downto 0) of ADCFrame_Monitor_t;
  
  type eb_monitor_t is record
    board_enable     : std_logic_vector( 3 downto  0);
    block_rate       : std_logic_vector(31 downto  0);
    data_rate        : std_logic_vector(31 downto  0);
    block_count      : std_logic_vector(31 downto  0);
    byte_count       : std_logic_vector(31 downto  0);
    idle_block_count : std_logic_vector(31 downto  0);
    enable           : std_logic;
    dma_ready        : std_logic;
    dma_fifo_open    : std_logic_vector(31 downto 0);
    board_has_data   : std_logic_vector(3 downto 0); -- new
    state            : std_logic_vector(3 downto 0);
    ADCFrame         : ADCFrame_Monitors_t;
    board_frame_count : slv32_array_t(0 to 3);
    board_frame_rate : slv32_array_t(0 to 3);
  end record eb_monitor_t; 
  
end package event_builder_CTRL;

  
