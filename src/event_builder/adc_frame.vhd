library ieee;
use ieee.std_logic_1164.all;
use work.event_builder_CTRL.all;


entity adc_frame is
  generic (
    ADC_NUMBER           : integer := 0;
    DMA_BLOCK_PWR_OF_TWO : integer := 8);
  port (
    clk_EB                 : in  std_logic;
    reset_async            : in  std_logic;
    data_in                : in  std_logic_vector(7 downto 0);
    data_in_start          : in  std_logic;
    data_in_end            : in  std_logic;
    data_in_valid          : in  std_logic;    
    clk_dma                : in  std_logic;
    frame_spill_id         : out std_logic_vector(15 downto 0);
    frame_size_left        : out std_logic_vector(23 downto 0);
    dma_block_request      : in  std_logic;
    dma_block_request_size : in  std_logic_vector(DMA_BLOCK_PWR_OF_TWO downto 0);
    dma_block_data         : out std_logic_vector(31 downto 0);
    dma_block_data_valid   : out std_logic;
    dma_block_data_last    : out std_logic;
    monitor                : out ADCFrame_Monitor_t);

end entity adc_frame;

architecture behavioral of adc_frame is

  component adc_frame_EB is
    port (
      clk_EB        : in  std_logic;
      reset_async   : in  std_logic;
      data_in       : in  std_logic_vector(7 downto 0);
      data_in_start : in  std_logic;
      data_in_end   : in  std_logic;
      data_in_valid : in  std_logic;
      fifo_word     : out std_logic_vector(33 downto 0);
      fifo_wr       : out std_logic;
      monitor       : out ADCFrameEB_Monitor_t);
  end component adc_frame_EB;
  
  component HS_data_fifo is
    port (
      rst           : IN  STD_LOGIC;
      wr_clk        : IN  STD_LOGIC;
      rd_clk        : IN  STD_LOGIC;
      din           : IN  STD_LOGIC_VECTOR(33 DOWNTO 0);
      wr_en         : IN  STD_LOGIC;
      rd_en         : IN  STD_LOGIC;
      dout          : OUT STD_LOGIC_VECTOR(33 DOWNTO 0);
      full          : OUT STD_LOGIC;
      empty         : OUT STD_LOGIC;
      valid         : OUT STD_LOGIC;
--      rd_data_count : OUT STD_LOGIC_VECTOR(16 DOWNTO 0);
--      wr_data_count : OUT STD_LOGIC_VECTOR(16 DOWNTO 0));
      rd_data_count : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      wr_data_count : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));

--      rd_data_count : OUT STD_LOGIC_VECTOR(13 DOWNTO 0);
--      wr_data_count : OUT STD_LOGIC_VECTOR(13 DOWNTO 0));
  end component HS_data_fifo;

  component adc_frame_dma is
    generic (
      ADC_NUMBER           : integer;
      FIFO_SIZE_PWR_OF_TWO : integer;
      DMA_BLOCK_PWR_OF_TWO : integer);--;
--      PREFETCH_WIDTH       : integer);
    port (
      clk                    : in  std_logic;
      reset_async            : in  std_logic;
      fifo_data              : in  std_logic_vector(33 downto 0);
      fifo_data_valid        : in  std_logic;
      fifo_rd                : out std_logic;
      buffered_count         : in  STD_LOGIC_VECTOR(FIFO_SIZE_PWR_OF_TWO-1 DOWNTO 0);
      frame_spill_id         : out std_logic_vector(15 downto 0);
      frame_size_left        : out std_logic_vector(23 downto 0);
      dma_block_request      : in  std_logic;
      dma_block_request_size : in  std_logic_vector(DMA_BLOCK_PWR_OF_TWO downto 0);
      dma_block_data         : out std_logic_vector(31 downto 0);
      dma_block_data_valid   : out std_logic;
      dma_block_data_last    : out std_logic;
      monitor                : out ADCFrameDMA_Monitor_t);
  end component adc_frame_dma;

  constant FIFO_SIZE_PWR_OF_TWO : integer := 14;--17;--14;
  signal fifo_in : std_logic_vector(33 downto 0);
  signal fifo_wr : std_logic;
  signal fifo_rd : std_logic;
  signal fifo_out : std_logic_vector(33 downto 0);
  signal fifo_out_valid : std_logic;
  signal fifo_out_data_count : std_logic_vector(FIFO_SIZE_PWR_OF_TWO-1 downto 0);
  signal fifo_in_data_count : std_logic_vector(FIFO_SIZE_PWR_OF_TWO-1 downto 0);

begin  -- architecture behavioral

  -------------------------------------------------------------------------------
  -- data in clock domain
  -------------------------------------------------------------------------------
  adc_frame_EB_1: entity work.adc_frame_EB
    port map (
      clk_EB        => clk_EB,
      reset_async   => reset_async,
      data_in       => data_in,
      data_in_start => data_in_start,
      data_in_end   => data_in_end,
      data_in_valid => data_in_valid,
      fifo_word     => fifo_in,
      fifo_wr       => fifo_wr,
      monitor       => monitor.EB);

  -------------------------------------------------------------------------------
  -- data-in to DMA clock domain crossing
  -------------------------------------------------------------------------------

  monitor.DAQ_FIFO_out_count(FIFO_SIZE_PWR_OF_TWO-1 downto 0) <= fifo_out_data_count;
  monitor.DAQ_FIFO_in_count (FIFO_SIZE_PWR_OF_TWO-1 downto 0) <= fifo_in_data_count;
  HS_data_fifo_1: entity work.HS_data_fifo
    port map (
      rst           => reset_async,
      wr_clk        => clk_EB,
      rd_clk        => clk_dma,
      din           => fifo_in,
      wr_en         => fifo_wr,
      rd_en         => fifo_rd,
      dout          => fifo_out,
      full          => monitor.DAQ_FIFO_full,
      empty         => monitor.DAQ_FIFO_empty,
      valid         => fifo_out_valid,
      rd_data_count => fifo_out_data_count,
      wr_data_count => fifo_in_data_count);

  -------------------------------------------------------------------------------
  -- DMA clock domain
  -------------------------------------------------------------------------------

  adc_frame_dma_1: entity work.adc_frame_dma
    generic map (
      ADC_NUMBER           => ADC_NUMBER,
      FIFO_SIZE_PWR_OF_TWO => FIFO_SIZE_PWR_OF_TWO,
      DMA_BLOCK_PWR_OF_TWO => DMA_BLOCK_PWR_OF_TWO)--,
--      PREFETCH_WIDTH       => 1)
    port map (
      clk                    => clk_dma,
      reset_async            => reset_async,
      fifo_data              => fifo_out,
      fifo_data_valid        => fifo_out_valid,
      fifo_rd                => fifo_rd,
      buffered_count         => fifo_out_data_count,
      frame_spill_id         => frame_spill_id,
      frame_size_left        => frame_size_left,
      dma_block_request      => dma_block_request,
      dma_block_request_size => dma_block_request_size,
      dma_block_data         => dma_block_data,
      dma_block_data_valid   => dma_block_data_valid,
      dma_block_data_last    => dma_block_data_last,
      monitor                => monitor.DMA);
  
end architecture behavioral;
