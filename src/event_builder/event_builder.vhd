library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

use work.types.all;
use work.event_builder_CTRL.all;

entity event_builder is
  generic (
    ADCBOARD_COUNT : integer := 4;
    DMA_BLOCK_PWR_OF_TWO : integer := 8);
  port (
    -- ADC data stream logic
    clk_EB              : in std_logic;
    reset_async         : in std_logic;
    ADC_data            : in slv8_array_t(ADCBOARD_COUNT-1 downto 0);
    ADC_data_start      : in std_logic_vector(ADCBOARD_COUNT-1 downto 0);
    ADC_data_end        : in std_logic_vector(ADCBOARD_COUNT-1 downto 0);
    ADC_data_valid      : in std_logic_vector(ADCBOARD_COUNT-1 downto 0);
    new_spill           : in std_logic;
    spill_number        : in std_logic_vector(31 downto 0);
    --Interface to local logic (ps_clk domain)
    clk_dma             : in std_logic;
    reset_dma           : in std_logic;
    --Interface to AXI system (ps_clk Domain)
    event_data          : out STD_LOGIC_VECTOR(31 downto  0);
    event_data_ready    : in  STD_LOGIC;
    event_data_open     : in  std_logic_vector(31 downto  0);
    event_data_write    : out STD_LOGIC;
    event_last_word     : out STD_LOGIC;
    control             : in  EB_Control_t;
    monitor             : out EB_Monitor_t);

end entity event_builder;

architecture behavioral of event_builder is

  component adc_frame is
    generic (
      ADC_NUMBER           : integer;
      DMA_BLOCK_PWR_OF_TWO : integer);
    port (
      clk_EB                 : in  std_logic;
      reset_async            : in  std_logic;
      data_in                : in  std_logic_vector(7 downto 0);
      data_in_start          : in  std_logic;
      data_in_end            : in  std_logic;
      data_in_valid          : in  std_logic;
      clk_dma                : in  std_logic;
      frame_spill_id         : out std_logic_vector(15 downto 0);
      frame_size_left        : out std_logic_vector(23 downto 0);
      dma_block_request      : in  std_logic;
      dma_block_request_size : in  std_logic_vector(DMA_BLOCK_PWR_OF_TWO downto 0);
      dma_block_data         : out std_logic_vector(31 downto 0);
      dma_block_data_valid   : out std_logic;
      dma_block_data_last    : out std_logic;
      monitor                : out ADCFrame_Monitor_t);
  end component adc_frame;

  component timed_counter is
    generic (
      timer_count : std_logic_vector;
      DATA_WIDTH  : integer);
    port (
      clk          : in  std_logic;
      reset_async  : in  std_logic;
      reset_sync   : in  std_logic;
      enable       : in  std_logic;
      event        : in  std_logic;
      update_pulse : out std_logic;
      timed_count  : out std_logic_vector(DATA_WIDTH-1 downto 0));
  end component timed_counter;

  component counter is
    generic (
      roll_over   : std_logic;
      end_value   : std_logic_vector(31 downto 0);
      start_value : std_logic_vector(31 downto 0);
      DATA_WIDTH  : integer);
    port (
      clk         : in  std_logic;
      reset_async : in  std_logic;
      reset_sync  : in  std_logic;
      enable      : in  std_logic;
      event       : in  std_logic;
      count       : out std_logic_vector(DATA_WIDTH-1 downto 0);
      at_max      : out std_logic);
  end component counter;
  
  type dma_size_array_t is array (integer range <>) of std_logic_vector(DMA_BLOCK_PWR_OF_TWO downto 0);
  signal frame_spill_id         : slv16_array_t(ADCBOARD_COUNT - 1 downto 0);
  signal frame_size_left        : slv24_array_t(ADCBOARD_COUNT - 1 downto 0);
  signal dma_block_request      : std_logic_vector(ADCBOARD_COUNT - 1 downto 0);  
  signal dma_block_request_size : dma_size_array_t(ADCBOARD_COUNT - 1 downto 0);
  signal dma_block_data         : slv32_array_t(ADCBOARD_COUNT - 1 downto 0);
  signal dma_block_data_valid   : std_logic_vector(ADCBOARD_COUNT - 1 downto 0);
  signal dma_block_data_last    : std_logic_vector(ADCBOARD_COUNT - 1 downto 0);

  type dma_state_t is (DS_INIT,
                       DS_WAIT,
                       DS_READOUT_BOARD,
                       DS_TIMEOUT_BLOCK);
  signal dma_state : dma_state_t;
  
  signal iBoard : integer range ADCBOARD_COUNT-1 downto 0;
  signal board_has_data : std_logic_vector(ADCBOARD_COUNT -1 downto 0);

  signal wrote_block : std_logic;
  signal wrote_word : std_logic;

  signal idle_count : std_logic_vector(31 downto 0);
  signal junk_counter : unsigned(7 downto 0);
  signal reset_idle_counter : std_logic;
  signal idle_dma : std_logic;
  signal idle_block_count_reset : std_logic;

  signal new_board_frame : std_logic_vector(ADCBOARD_COUNT - 1 downto 0);
  
  attribute mark_debug : string;
  attribute mark_debug of dma_state            : signal is "true";   
  attribute mark_debug of dma_block_request    : signal is "true";
  attribute mark_debug of board_has_data       : signal is "true";
  attribute mark_debug of iBoard               : signal is "true";
  attribute mark_debug of frame_size_left      : signal is "true";
  
begin  -- architecture behavioral

  --Process incomming data
  adc_data_proc: for iBoard in ADCBOARD_COUNT -1 downto 0 generate
    adc_frame_1: entity work.adc_frame
      generic map (
        ADC_NUMBER           => iBoard+1,
        DMA_BLOCK_PWR_OF_TWO => 8)
      port map (
        clk_EB                 => clk_EB,
        reset_async            => control.reset_sync,--reset_async,
        data_in                => ADC_data(iBoard),
        data_in_start          => ADC_data_start(iBoard),
        data_in_end            => ADC_data_end(iBoard),
        data_in_valid          => ADC_data_valid(iBoard),
        clk_dma                => clk_dma,
        frame_spill_id         => frame_spill_id(iBoard),
        frame_size_left        => frame_size_left(iBoard),
        dma_block_request      => dma_block_request(iBoard),
        dma_block_request_size => dma_block_request_size(iBoard),
        dma_block_data         => dma_block_data(iBoard),
        dma_block_data_valid   => dma_block_data_valid(iBoard),
        dma_block_data_last    => dma_block_data_last(iBoard),
        monitor                => monitor.ADCFrame(iBoard));
  end generate adc_data_proc;


  size_monitoring: process (clk_dma) is
  begin  -- process size_monitoring
    if clk_dma'event and clk_dma = '1' then  -- rising clock edge
      monitor.board_has_data <= board_has_data;
    end if;
  end process size_monitoring;
  adc_monitor_proc: process (frame_size_left) is
  begin  -- process adc_monitor_proc
    for iBoard in ADCBOARD_COUNT-1 downto 0 loop
      board_has_data(iBoard) <= or_reduce(frame_size_left(iBoard));
    end loop;  -- iBoard
  end process adc_monitor_proc;

  dma_state_mon_proc: process (dma_state) is
  begin  -- process dma_state_mon_proc
    case dma_state is
      when DS_INIT          => monitor.state <= x"0";
      when DS_WAIT          => monitor.state <= x"1";
      when DS_READOUT_BOARD => monitor.state <= x"2";
      when DS_TIMEOUT_BLOCK => monitor.state <= x"3";
      when others           => monitor.state <= x"F";
    end case;
  end process dma_state_mon_proc;
  
  
  monitor.board_enable <= control.board_enable;
  monitor.enable       <= control.enable;
  monitor.dma_ready    <= event_data_ready;
  monitor.dma_fifo_open <= event_data_open;
  dma_builder_SM: process (clk_dma, reset_async) is
  begin  -- process dma_builder_SM
    if reset_async = '1' then           -- asynchronous reset (active high)
      dma_state <= DS_INIT;
    elsif clk_dma'event and clk_dma = '1' then  -- rising clock edge
      idle_dma <= '0';
      idle_block_count_reset <= '0';

      if control.reset_sync = '1' then
        dma_state <= DS_INIT;
      else      
        case dma_state is
          when DS_INIT =>
            --start waiting for the first channel
            dma_state <= DS_WAIT;
            iBoard <= 0;
            idle_block_count_reset <= '1';            
          when DS_WAIT =>
            --We've waited long enough, so we should send an idle block
            if unsigned(idle_count) > x"05F5E100" then
              dma_state <= DS_TIMEOUT_BLOCK;
              junk_counter <= x"FF";
              idle_dma <= '1';
            end if;

            --Wait for data to send
            if control.board_enable(iBoard) = '1' and  board_has_data(iBoard) = '1' then
              --This board is enabled and has data to send. 
              dma_state <= DS_READOUT_BOARD;
              idle_dma <= '0'; -- override a timeout
            else
              --THis board isn't enabled, move to the next board.
              if iBoard = ADCBOARD_COUNT-1 then
                iBoard <= 0;
              else
                iBoard <= iBoard + 1;              
              end if;                
            end if;
            
          when DS_READOUT_BOARD =>
            --readout the board
            if or_reduce(frame_size_left(iBoard)) = '0' then
              -- when this channel has nothing left to read, move to the next channel
              dma_state <= DS_WAIT;
              
              if iBoard = ADCBOARD_COUNT-1 then
                iBoard <= 0;
              else
                iBoard <= iBoard + 1;              
              end if;
            end if;
          when DS_TIMEOUT_BLOCK =>
            if or_reduce(std_logic_vector(junk_counter)) = '0' then
              dma_state <= DS_WAIT;
            else
              junk_counter <= junk_counter -1;
            end if;
          when others => null;
        end case;
      end if;
    end if;
  end process dma_builder_SM;

  dma_builder_proc: process (clk_dma, reset_async) is
  begin  -- process dma_builder_proc
    if reset_async = '1' then           -- asynchronous reset (active high)
      
    elsif clk_dma'event and clk_dma = '1' then  -- rising clock edge
      dma_block_request <= (others => '0');
      event_data_write <= '0';
      event_last_word <= '0';
      wrote_word <= '0';
      wrote_block <= '0';
      new_board_frame <= x"0";
      
      case dma_state is
        when DS_INIT => NULL;
        when DS_WAIT =>
          if board_has_data(iBoard) = '1' and control.board_enable(iBoard) = '1' then
            dma_block_request(iBoard) <= '1';
            dma_block_request_size(iBoard) <= "1"&x"00";
            new_board_frame(iBoard) <= '1';
          end if;          
        when DS_READOUT_BOARD =>
          if dma_block_data_valid(iBoard) = '1' then
            event_data_write <= '1';
            wrote_word <= '1';
            event_data       <= dma_block_data(iBoard);
            if dma_block_data_last(iBoard) = '1' then
              event_last_word <= '1';
              wrote_block <= '1';
              if or_reduce(frame_size_left(iBoard)) = '1' then
                --This dma block is done, request another
                dma_block_request(iBoard) <= '1';
                dma_block_request_size(iBoard) <= "1"&x"00";
              end if;
            end if;
          end if;
        when DS_TIMEOUT_BLOCK =>
          event_data_write <= '1';
          wrote_word <= '1';
          event_data <= x"DEADBEEF";
          if or_reduce(std_logic_vector(junk_counter)) = '0' then
            event_last_word <= '1';
          end if;
        when others => null;
      end case;

    end if;
  end process dma_builder_proc;

  reset_idle_counter <= '0' when dma_state = DS_WAIT else '1';
  counter_fuck_xilinx: entity work.counter
    port map (
      clk         => clk_dma,
      reset_async => '0',
      reset_sync  => wrote_word,
      enable      => '1',
      event       => '1',
      count       => idle_count,
      at_max      => open);
  counter_1: entity work.counter
    port map (
      clk         => clk_dma,
      reset_async => reset_async,
      reset_sync  => idle_block_count_reset,
      enable      => '1',
      event       => idle_dma,
      count       => monitor.idle_block_count,
      at_max      => open);

  
  counter_dma: entity work.counter
    port map (
      clk         => clk_dma,
      reset_async => reset_async,
      reset_sync  => control.reset_block_count,
      enable      => '1',
      event       => wrote_block,
      count       => monitor.block_count,
      at_max      => open);
  block_rate: entity work.timed_counter
    generic map (
      timer_count => x"05F5E100")
    port map (
      clk          => clk_dma,
      reset_async  => reset_async,
      reset_sync   => '0',
      enable       => '1',
      event        => wrote_block,
      update_pulse => open,
      timed_count  => monitor.block_rate);

  board_frame_counter_loop: for iBoardFrame in 0 to 3 generate
    board_frame_counter: entity work.counter
      port map (
        clk         => clk_dma,
        reset_async => reset_async,
        reset_sync  => control.reset_board_count,
        enable      => '1',
        event       => new_board_frame(iBoardFrame),
        count       => monitor.board_frame_count(iBoardFrame),
        at_max      => open);
      block_rate: entity work.timed_counter
    generic map (
      timer_count => x"05F5E100")
    port map (
      clk          => clk_dma,
      reset_async  => reset_async,
      reset_sync   => '0',
      enable       => '1',
      event        => new_board_frame(iBoardFrame),
      update_pulse => open,
      timed_count  => monitor.board_frame_rate(iBoardFrame));
  end generate board_frame_counter_loop;

  counter_2: entity work.counter
    port map (
      clk         => clk_dma,
      reset_async => reset_async,
      reset_sync  => control.reset_byte_count,
      enable      => '1',
      event       => wrote_word,
      count       => monitor.byte_count,
      at_max      => open);
  word_rate: entity work.timed_counter
    generic map (
      timer_count => x"05F5E100",
      DATA_WIDTH  => 30)
    
    port map (
      clk          => clk_dma,
      reset_async  => reset_async,
      reset_sync   => '0',
      enable       => '1',
      event        => wrote_word,
      update_pulse => open,
      timed_count(29 downto  0) => monitor.data_rate(31 downto 2)
      );
  --Each word is 4 bytes, so we bitshift by two to multiply by four
  monitor.data_rate(1 downto 0) <= "00";










  
end architecture behavioral;
