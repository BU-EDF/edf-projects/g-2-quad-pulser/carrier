library IEEE;
use IEEE.std_logic_1164.all;
use work.PMOD_COMM_CTRL.all;


library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity pmod_comm is
  
  port (
    clk_200Mhz    : in  std_logic;
    uart_local_rx : in  std_logic_vector(3 downto 0);
    uart_local_tx : out std_logic_vector(3 downto 0);
    control       : in  pmod_comm_control_t;
    monitor       : out pmod_comm_monitor_t);

end entity pmod_comm;

architecture behavioral of pmod_comm is

  component uart_rx6 is
    port (
      serial_in           : in  std_logic;
      en_16_x_baud        : in  std_logic;
      data_out            : out std_logic_vector(7 downto 0);
      buffer_read         : in  std_logic;
      buffer_data_present : out std_logic;
      buffer_half_full    : out std_logic;
      buffer_full         : out std_logic;
      buffer_reset        : in  std_logic;
      clk                 : in  std_logic);
  end component uart_rx6;

  component uart_tx6 is
    port (
      data_in             : in  std_logic_vector(7 downto 0);
      en_16_x_baud        : in  std_logic;
      serial_out          : out std_logic;
      buffer_write        : in  std_logic;
      buffer_data_present : out std_logic;
      buffer_half_full    : out std_logic;
      buffer_full         : out std_logic;
      buffer_reset        : in  std_logic;
      clk                 : in  std_logic);
  end component uart_tx6;

--
-- UART UART baud rate signals
--
  signal baud_count   : integer range 0 to 67 := 0;
  signal en_16_x_baud : std_logic             := '0';


  signal uart_tx_present : std_logic_vector(3 downto 0);

begin  -- architecture behavioral


  -----------------------------------------------------------------------------------------
-- RS232 (UART) baud rate 
-----------------------------------------------------------------------------------------
--
-- To set serial communication baud rate to 115,200 then en_16_x_baud must pulse 
-- High at 1.84MHz which is every 109 cycles at 200Mhz.

  baud_rate : process(clk_200Mhz)
  begin
    if clk_200Mhz'event and clk_200Mhz = '1' then
      if baud_count = 108 then           -- counts 109 states including zero
        baud_count   <= 0;
        en_16_x_baud <= '1';            -- single cycle enable pulse
      else
        baud_count   <= baud_count + 1;
        en_16_x_baud <= '0';
      end if;        
    end if;
  end process baud_rate;

  boards: for iBoard in 3 downto 0 generate
    monitor.board(iBoard).tx_comm_empty <= not uart_tx_present(iBoard);
    uart_tx6_1: entity work.uart_tx6
      port map (
        data_in             => control.board(iBoard).tx_comm_data,
        en_16_x_baud        => en_16_x_baud,
        serial_out          => uart_local_tx(iBoard),
        buffer_write        => control.board(iBoard).tx_comm_data_valid,
        buffer_data_present => uart_tx_present(iBoard),
        buffer_half_full    => open,
        buffer_full         => open,
        buffer_reset        => '0',
        clk                 => clk_200Mhz);


    uart_rx6_1: entity work.uart_rx6
      port map (
        serial_in           => uart_local_rx(iBoard),
        en_16_x_baud        => en_16_x_baud,
        data_out            => monitor.board(iBoard).rx_comm,
        buffer_read         => control.board(iBoard).rx_comm_read,
        buffer_data_present => monitor.board(iBoard).rx_comm_valid,
        buffer_half_full    => open,
        buffer_full         => open,
        buffer_reset        => '0',
        clk                 => clk_200Mhz);
  end generate boards;
  
end architecture behavioral;
