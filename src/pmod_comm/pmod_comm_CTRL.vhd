library IEEE;
use IEEE.std_logic_1164.all;

package PMOD_COMM_CTRL is

  type BoardUART_Control_t is record
    tx_comm_data          : std_logic_vector(7 downto 0);
    tx_comm_data_valid    : std_logic;

    rx_comm_read          : std_logic;
  end record BoardUART_t;
  constant DEFAULT_BoardUART_Control : BoardUART_Control_t := (tx_comm_data => (others => '0'),
                                                               tx_comm_data_valid => '0',
                                                               rx_comm_read => '0');
  type BoardUART_Control_array_t is array (3 downto 0) of BoardUART_Control_t;

  type pmod_comm_control_t is record
    board : BoardUART_Control_array_t;
  end record pmod_comm_control_t;
  constant DEFAULT_PMOD_COMM_Control : pmod_comm_control_t := (board => (others => DEFAULT_BoardUART_Control));



  type BoardUART_Monitor_t is record
    tx_comm_empty       : std_logic;

    rx_comm             : std_logic_vector(7 downto 0);
    rx_comm_valid       : std_logic;
  end record BoardUART_Monitor_t;

  type BoardUART_Monitor_array_t is array (3 downto 0) of BoardUART_Monitor_t;

  type pmod_comm_monitor_t is record
    board : BoardUART_Monitor_array_t;
  end record pmod_comm_monitor_t
;
  
end package PMOD_COMM_CTRL;
