library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.types.all;
use work.event_builder_ctrl.all;

entity tb_eb is
  generic (
    INJECT_ERROR : boolean := false;
    ALWAYS_FULL_DMA_BLOCK : boolean := true;
    TEST_TIMEOUT  : boolean := true);
  
  port (
    
    clk_200Mhz  : in std_logic;
    clk_100Mhz  : in std_logic;
    reset   : in std_logic);

end entity tb_eb;

architecture behavioral of tb_eb is
  component rx is
    port (
      reset              : in  std_logic;
      clk_200Mhz         : in  std_logic;
      clk_en_20Mhz       : in  std_logic;
      locked             : in  std_logic;
      data               : in  std_logic_vector(8 downto 0);
      data_valid         : in  std_logic;
      uart_rx_data       : out std_logic_vector(6 downto 0);
      uart_rx_data_valid : out std_logic;
      hs_data_out        : out std_logic_vector(7 downto 0);
      hs_data_start      : out std_logic;
      hs_data_end        : out std_logic;
      hs_data_out_valid  : out std_logic);
  end component rx;

  component event_builder is
    generic (
      ADCBOARD_COUNT : integer);
    port (
      clk_EB           : in  std_logic;
      reset_async      : in  std_logic;
      ADC_data         : in  slv8_array_t(ADCBOARD_COUNT-1 downto 0);
      ADC_data_start   : in  std_logic_vector(ADCBOARD_COUNT-1 downto 0);
      ADC_data_end     : in  std_logic_vector(ADCBOARD_COUNT-1 downto 0);
      ADC_data_valid   : in  std_logic_vector(ADCBOARD_COUNT-1 downto 0);
      new_spill        : in  std_logic;
      spill_number     : in  std_logic_vector(31 downto 0);
      clk_dma          : in  std_logic;
      reset_dma        : in  std_logic;
      event_data       : out STD_LOGIC_VECTOR(31 downto 0);
      event_data_ready : in  STD_LOGIC;
      event_data_open  : in  std_logic_vector(31 downto 0);
      event_data_write : out STD_LOGIC;
      event_last_word  : out STD_LOGIC;
      control          : in  EB_Control_t;
      monitor          : out EB_Monitor_t);
  end component event_builder;
  
  signal clk_en_20Mhz       :  std_logic;
  signal locked             :  std_logic;
  signal data               :  std_logic_vector(8 downto 0);
  signal data_valid         :  std_logic;
  signal uart_rx_data       :  std_logic_vector(6 downto 0);
  signal uart_rx_data_valid :  std_logic;
  signal hs_data_out        :  std_logic_vector(7 downto 0);
  signal hs_data_start      :  std_logic;
  signal hs_data_end        :  std_logic;
  signal hs_data_out_valid  :  std_logic;
  signal ADC_data           :  slv8_array_t(4-1 downto 0);
  signal ADC_data_start     :  std_logic_vector(4-1 downto 0);
  signal ADC_data_end       :  std_logic_vector(4-1 downto 0);
  signal ADC_data_valid     :  std_logic_vector(4-1 downto 0);


  
  signal clk_dma                : std_logic;
  signal frame_spill_id         : std_logic_vector(31 downto 0);
  signal event_data             : STD_LOGIC_VECTOR(31 downto 0);
  signal event_data_ready       : STD_LOGIC;
  signal event_data_open        : std_logic_vector(31 downto 0);
  signal event_data_write       : STD_LOGIC;
  signal event_last_word        : STD_LOGIC;
  signal control                : EB_Control_t;
  signal monitor                : EB_Monitor_t;
  
  signal clk20_sr : std_logic_vector(9 downto 0);


  signal counter : integer;
  signal cause_error : std_logic;

begin  -- architecture behavioral

  tb: process (clk_200Mhz, reset) is
    file in_file : text open read_mode is "tx_stream.dat";
    variable my_line : line;
    variable din : std_logic_vector(8 downto 0);
    variable din_valid : std_logic;
  begin  -- process tb
    if reset = '1' then              -- asynchronous reset (active high)
      clk20_sr <= std_logic_vector(to_unsigned(512,10));
      locked <= '1';
      counter <= 0;
      frame_spill_id <= x"00000001";
      event_data_open <= x"ffffffff";
      
      control <= DEFAULT_EB_Control;
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      event_data_ready <= '1';
      control.board_enable <= x"f";
      counter <= counter + 1;

      clk20_sr <= clk20_sr(clk20_sr'left - 1 downto 0)&clk20_sr(clk20_sr'left);
      clk_en_20Mhz <= clk20_sr(0);

      data_valid <= '0';
      if clk_en_20Mhz = '1' then
        if TEST_TIMEOUT then
          data <= "1"&x"3C";
          data_valid <= '1';
        else
          readline(in_file,my_line);
          read(my_line,din);
          data <= din;
          data_valid <= '1';
        end if;       
      end if;

      ADC_data       <= (others => hs_data_out);
      ADC_data_start <= (others => hs_data_start);
      ADC_data_end   <= (others => hs_data_end);
      ADC_data_valid <= (others => hs_data_out_valid);
      
    end if;
  end process tb;

  readout_proc: process (clk_100Mhz, reset) is
  begin  -- process readout_proc
    if reset = '1' then                 -- asynchronous reset (active high)

    elsif clk_100Mhz'event and clk_100Mhz = '1' then  -- rising clock edge
      
    end if;
  end process readout_proc;

  
  rx_1: entity work.rx
    port map (
      reset              => reset,
      clk_200Mhz         => clk_200Mhz,
      clk_en_20Mhz       => clk_en_20Mhz,
      locked             => locked,
      data               => data,
      data_valid         => data_valid,
      uart_rx_data       => uart_rx_data,
      uart_rx_data_valid => uart_rx_data_valid,
      hs_data_out        => hs_data_out,
      hs_data_start      => hs_data_start,
      hs_data_end        => hs_data_end,
      hs_data_out_valid  => hs_data_out_valid);


  event_builder_1: entity work.event_builder
    generic map (
      ADCBOARD_COUNT => 4)
    port map (
      clk_EB           => clk_200Mhz,
      reset_async      => reset,
      ADC_data         => ADC_data,
      ADC_data_start   => ADC_data_start,
      ADC_data_end     => ADC_data_end,  
      ADC_data_valid   => ADC_data_valid,
      new_spill        => '1',
      spill_number     => frame_spill_id,
      clk_dma          => clk_100Mhz,
      reset_dma        => reset,
      event_data       => event_data,
      event_data_ready => event_data_ready,
      event_data_open  => event_data_open,
      event_data_write => event_data_write,
      event_last_word  => event_last_word,
      control          => control,
      monitor          => monitor);
  
end architecture behavioral;
