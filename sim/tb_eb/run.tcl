restart
add_force {/tb_eb/clk_200Mhz} -radix hex {1 0ns} {0 2500ps} -repeat_every 5000ps
add_force {/tb_eb/clk_100Mhz} -radix hex {1 0ns} {0 5000ps} -repeat_every 10000ps
add_force {/tb_eb/reset} -radix hex {1 0ns}
run 10ns
add_force {/tb_eb/reset} -radix hex {0 0ns}
run 30us
run 31ms
