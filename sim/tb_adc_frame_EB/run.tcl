restart
add_force {/tb_adc_frame_EB/clk_200Mhz} -radix hex {1 0ns} {0 2500ps} -repeat_every 5000ps
add_force {/tb_adc_frame_EB/reset} -radix hex {1 0ns}
run 10ns
add_force {/tb_adc_frame_EB/reset} -radix hex {0 0ns}
run 30us
run 31ms
