library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;


entity tb_adc_frame_EB is
  generic (
    INJECT_ERROR : boolean := true);
  port (
    
    clk_200Mhz  : in std_logic;
    reset   : in std_logic);

end entity tb_adc_frame_EB;

architecture behavioral of tb_adc_frame_EB is
  component rx is
    port (
      reset              : in  std_logic;
      clk_200Mhz         : in  std_logic;
      clk_en_20Mhz       : in  std_logic;
      locked             : in  std_logic;
      data               : in  std_logic_vector(8 downto 0);
      data_valid         : in  std_logic;
      uart_rx_data       : out std_logic_vector(6 downto 0);
      uart_rx_data_valid : out std_logic;
      hs_data_out        : out std_logic_vector(7 downto 0);
      hs_data_start      : out std_logic;
      hs_data_end        : out std_logic;
      hs_data_out_valid  : out std_logic);
  end component rx;

  component adc_frame_EB is
    port (
      clk_EB        : in  std_logic;
      reset_async   : in  std_logic;
      data_in       : in  std_logic_vector(7 downto 0);
      data_in_start : in  std_logic;
      data_in_end   : in  std_logic;
      data_in_valid : in  std_logic;
      fifo_word     : out std_logic_vector(33 downto 0);
      fifo_wr       : out std_logic);
  end component adc_frame_EB;
  
  signal clk_en_20Mhz       :  std_logic;
  signal locked             :  std_logic;
  signal data               :  std_logic_vector(8 downto 0);
  signal data_valid         :  std_logic;
  signal uart_rx_data       :  std_logic_vector(6 downto 0);
  signal uart_rx_data_valid :  std_logic;
  signal hs_data_out        :  std_logic_vector(7 downto 0);
  signal hs_data_start      :  std_logic;
  signal hs_data_end        :  std_logic;
  signal hs_data_out_valid  :  std_logic;

  signal clk20_sr : std_logic_vector(9 downto 0);


  signal write_word    : std_logic_vector(33 downto 0);
  signal fifo_wr       : std_logic;

  signal counter : integer;
  signal cause_error : std_logic;
  
begin  -- architecture behavioral

  tb: process (clk_200Mhz, reset) is
    file in_file : text open read_mode is "tx_stream.dat";
    variable my_line : line;
    variable din : std_logic_vector(8 downto 0);
    variable din_valid : std_logic;
  begin  -- process tb
    if reset = '1' then              -- asynchronous reset (active high)
      clk20_sr <= std_logic_vector(to_unsigned(512,10));
      locked <= '1';
      counter <= 0;
      if INJECT_ERROR then
        cause_error <= '1';
      else
        cause_error <= '0';
      end if;
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      counter <= counter + 1;

      clk20_sr <= clk20_sr(clk20_sr'left - 1 downto 0)&clk20_sr(clk20_sr'left);
      clk_en_20Mhz <= clk20_sr(0);

      readline(in_file,my_line);
      read(my_line,din_valid);
      read(my_line,din);
      data <= din;
      data_valid <= din_valid and clk_en_20Mhz;

      if cause_error = '1' and data = "1"&x"BC" and clk_en_20Mhz = '1' then
        cause_error <= '0';
        data <= "0"&x"00";
      end if;
      
    end if;
  end process tb;
  
  rx_1: entity work.rx
    port map (
      reset              => reset,
      clk_200Mhz         => clk_200Mhz,
      clk_en_20Mhz       => clk_en_20Mhz,
      locked             => locked,
      data               => data,
      data_valid         => data_valid,
      uart_rx_data       => uart_rx_data,
      uart_rx_data_valid => uart_rx_data_valid,
      hs_data_out        => hs_data_out,
      hs_data_start      => hs_data_start,
      hs_data_end        => hs_data_end,
      hs_data_out_valid  => hs_data_out_valid);


  adc_frame_EB_1: entity work.adc_frame_EB
    port map (
      clk_EB        => clk_200Mhz,
      reset_async   => reset,
      data_in       => hs_data_out,
      data_in_start => hs_data_start,   
      data_in_end   => hs_data_end,     
      data_in_valid => hs_data_out_valid,
      fifo_word     => write_word,
      fifo_wr       => fifo_wr);
end architecture behavioral;
