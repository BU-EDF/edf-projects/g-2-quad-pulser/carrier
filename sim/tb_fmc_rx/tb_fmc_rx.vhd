library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;


entity tb_fmc_rx is
  port (
    
    clk_200Mhz  : in std_logic;
    reset   : in std_logic);

end entity tb_fmc_rx;

architecture behavioral of tb_fmc_rx is
  component rx is
    port (
      reset              : in  std_logic;
      clk_200Mhz         : in  std_logic;
      clk_en_20Mhz       : in  std_logic;
      locked             : in  std_logic;
      data               : in  std_logic_vector(8 downto 0);
      data_valid         : in  std_logic;
      uart_rx_data       : out std_logic_vector(6 downto 0);
      uart_rx_data_valid : out std_logic;
      hs_data_out        : out std_logic_vector(7 downto 0);
      hs_data_start      : out std_logic;
      hs_data_end        : out std_logic;
      hs_data_out_valid  : out std_logic);
  end component rx;

  signal clk_en_20Mhz       :  std_logic;
  signal locked             :  std_logic;
  signal data               :  std_logic_vector(8 downto 0);
  signal data_valid         :  std_logic;
  signal uart_rx_data       :  std_logic_vector(6 downto 0);
  signal uart_rx_data_valid :  std_logic;
  signal hs_data_out        :  std_logic_vector(7 downto 0);
  signal hs_data_out_valid  :  std_logic;

  signal clk20_sr : std_logic_vector(9 downto 0);
begin  -- architecture behavioral

  tb: process (clk_200Mhz, reset) is
    file in_file : text open read_mode is "tx_stream.dat";
    variable my_line : line;
    variable din : std_logic_vector(8 downto 0);
    variable din_valid : std_logic;
  begin  -- process tb
    if reset = '1' then              -- asynchronous reset (active high)
      clk20_sr <= std_logic_vector(to_unsigned(512,10));
      locked <= '1';
      
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      clk20_sr <= clk20_sr(clk20_sr'left - 1 downto 0)&clk20_sr(clk20_sr'left);
      clk_en_20Mhz <= clk20_sr(0);

      readline(in_file,my_line);
      read(my_line,din_valid);
      read(my_line,din);
      data <= din;
      data_valid <= din_valid;
    end if;
  end process tb;
  
  rx_1: entity work.rx
    port map (
      reset              => reset,
      clk_200Mhz         => clk_200Mhz,
      clk_en_20Mhz       => clk_en_20Mhz,
      locked             => locked,
      data               => data,
      data_valid         => data_valid,
      uart_rx_data       => uart_rx_data,
      uart_rx_data_valid => uart_rx_data_valid,
      hs_data_out        => hs_data_out,
      hs_data_start      => hs_data_start,
      hs_data_end        => hs_data_end,
      hs_data_out_valid  => hs_data_out_valid);
end architecture behavioral;
