restart

add_force {/interface_test/ps_clk} -radix hex {1 0ns} {0 5000ps} -repeat_every 10000ps
add_force {/interface_test/enable_counter} -radix hex {0 0ns}
add_force {/interface_test/reset_async} -radix hex {0 0ns}
add_force {/interface_test/event_data_ready} -radix hex {1 0ns}
add_force {/interface_test/control.streaming_enable} -radix hex {0 0ns}
add_force {/interface_test/control.streaming_oneshot} -radix hex {0 0ns}
add_force {/interface_test/control.stream_period} -radix hex {0000ffff 0ns}
add_force {/interface_test/control.packet_counter_end} -radix hex {0003 0ns}
add_force {/interface_test/enable_counter} -radix hex {00000000 0ns}

remove_force {/interface_test/enable_counter}

run 1us
remove_force {/interface_test/enable_counter}

add_force {/interface_test/control.streaming_oneshot} -radix hex {1 0ns}
run 10ns
add_force {/interface_test/control.streaming_oneshot} -radix hex {0 0ns}

run 1us
add_force {/interface_test/control.streaming_enable} -radix hex {1 0ns}
run 10ms
