library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity tb_adc_frame is
  generic (
    INJECT_ERROR : boolean := false;
    ALWAYS_FULL_DMA_BLOCK : boolean := false);
  
  port (
    
    clk_200Mhz  : in std_logic;
    clk_100Mhz  : in std_logic;
    reset   : in std_logic);

end entity tb_adc_frame;

architecture behavioral of tb_adc_frame is
  component rx is
    port (
      reset              : in  std_logic;
      clk_200Mhz         : in  std_logic;
      clk_en_20Mhz       : in  std_logic;
      locked             : in  std_logic;
      data               : in  std_logic_vector(8 downto 0);
      data_valid         : in  std_logic;
      uart_rx_data       : out std_logic_vector(6 downto 0);
      uart_rx_data_valid : out std_logic;
      hs_data_out        : out std_logic_vector(7 downto 0);
      hs_data_start      : out std_logic;
      hs_data_end        : out std_logic;
      hs_data_out_valid  : out std_logic);
  end component rx;

  component adc_frame is
    generic (
      ADC_NUMBER : integer;
      DMA_BLOCK_PWR_OF_TWO : integer);
    port (
      clk_EB                 : in  std_logic;
      reset_async            : in  std_logic;
      data_in                : in  std_logic_vector(7 downto 0);
      data_in_start          : in  std_logic;
      data_in_end            : in  std_logic;
      data_in_valid          : in  std_logic;
      clk_dma                : in  std_logic;
      frame_spill_id         : out std_logic_vector(15 downto 0);
      frame_size_left        : out std_logic_vector(23 downto 0);
      dma_block_request      : in  std_logic;
      dma_block_request_size : in  std_logic_vector(DMA_BLOCK_PWR_OF_TWO downto 0);
      dma_block_data         : out std_logic_vector(31 downto 0);
      dma_block_data_valid   : out std_logic;
      dma_block_data_last    : out std_logic);
  end component adc_frame;
  
  signal clk_en_20Mhz       :  std_logic;
  signal locked             :  std_logic;
  signal data               :  std_logic_vector(8 downto 0);
  signal data_valid         :  std_logic;
  signal uart_rx_data       :  std_logic_vector(6 downto 0);
  signal uart_rx_data_valid :  std_logic;
  signal hs_data_out        :  std_logic_vector(7 downto 0);
  signal hs_data_start      :  std_logic;
  signal hs_data_end        :  std_logic;
  signal hs_data_out_valid  :  std_logic;

  signal clk_dma                : std_logic;
  signal frame_spill_id         : std_logic_vector(15 downto 0);
  signal frame_size_left        : std_logic_vector(23 downto 0);
  signal dma_block_request      : std_logic;
  signal dma_block_request_size : std_logic_vector(8 downto 0);
  signal dma_block_data         : std_logic_vector(31 downto 0);
  signal dma_block_data_valid   : std_logic;
  signal dma_block_data_last    : std_logic;

  constant DMA_BLOCK_SIZE : std_logic_vector(23 downto 0) := x"000100";
  
  signal clk20_sr : std_logic_vector(9 downto 0);


  signal write_word    : std_logic_vector(33 downto 0);
  signal fifo_wr       : std_logic;

  signal counter : integer;
  signal cause_error : std_logic;

  signal in_transfer : std_logic;
begin  -- architecture behavioral

  tb: process (clk_200Mhz, reset) is
    file in_file : text open read_mode is "tx_stream.dat";
    variable my_line : line;
    variable din : std_logic_vector(8 downto 0);
    variable din_valid : std_logic;
  begin  -- process tb
    if reset = '1' then              -- asynchronous reset (active high)
      clk20_sr <= std_logic_vector(to_unsigned(512,10));
      locked <= '1';
      counter <= 0;
      if INJECT_ERROR then
        cause_error <= '1';
      else
        cause_error <= '0';
      end if;
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      counter <= counter + 1;

      clk20_sr <= clk20_sr(clk20_sr'left - 1 downto 0)&clk20_sr(clk20_sr'left);
      clk_en_20Mhz <= clk20_sr(0);

      data_valid <= '0';
      if clk_en_20Mhz = '1' then
        readline(in_file,my_line);
        read(my_line,din);
        data <= din;
        data_valid <= '1';
      end if;

      if cause_error = '1' and data = "1"&x"BC" and clk_en_20Mhz = '1' then
        cause_error <= '0';
        data <= "0"&x"00";
      end if;
      
    end if;
  end process tb;

  readout_proc: process (clk_100Mhz, reset) is
  begin  -- process readout_proc
    if reset = '1' then                 -- asynchronous reset (active high)
      in_transfer <= '0';
    elsif clk_100Mhz'event and clk_100Mhz = '1' then  -- rising clock edge
      dma_blocK_request <= '0';
      if in_transfer = '1' then
        if dma_block_data_last = '1' then
          in_transfer <= '0';
        end if;
      else
        if ALWAYS_FULL_DMA_BLOCK then
          --every dma block is 256 bytes (extra words are 0xDEADBEEF)
          if or_reduce(frame_size_left) = '1' then
            dma_block_request_size <= DMA_BLOCK_SIZE(8 downto 0);
            dma_block_request <= '1';
            in_transfer <= '1';          
          end if;
        else
          -- the last dma block will be the size left of valid data
          if or_reduce(frame_size_left) = '1' then
            if unsigned(frame_size_left) > unsigned(DMA_BLOCK_SIZE) then
              dma_block_request_size <= DMA_BLOCK_SIZE(8 downto 0);
              dma_block_request <= '1';
              in_transfer <= '1';
            else
              dma_block_request_size <= frame_size_left(8 downto 0);
              dma_block_request <= '1';
              in_transfer <= '1';
            end if;
          end if;
        end if;
      end if;
    end if;
  end process readout_proc;

  
  rx_1: entity work.rx
    port map (
      reset              => reset,
      clk_200Mhz         => clk_200Mhz,
      clk_en_20Mhz       => clk_en_20Mhz,
      locked             => locked,
      data               => data,
      data_valid         => data_valid,
      uart_rx_data       => uart_rx_data,
      uart_rx_data_valid => uart_rx_data_valid,
      hs_data_out        => hs_data_out,
      hs_data_start      => hs_data_start,
      hs_data_end        => hs_data_end,
      hs_data_out_valid  => hs_data_out_valid);


  adc_frame_2: entity work.adc_frame
    generic map (
      ADC_NUMBER => 1,
      DMA_BLOCK_PWR_OF_TWO => 8)
    port map (
      clk_EB                 => clk_200Mhz,
      reset_async            => reset,
      data_in                => hs_data_out,
      data_in_start          => hs_data_start,   
      data_in_end            => hs_data_end,     
      data_in_valid          => hs_data_out_valid,
      clk_dma                => clk_100Mhz,
      frame_spill_id         => frame_spill_id,
      frame_size_left        => frame_size_left,
      dma_block_request      => dma_block_request,
      dma_block_request_size => dma_block_request_size,
      dma_block_data         => dma_block_data,
      dma_block_data_valid   => dma_block_data_valid,
      dma_block_data_last    => dma_block_data_last);
  
end architecture behavioral;
