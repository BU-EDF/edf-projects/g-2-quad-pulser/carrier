create_wave_config

add_wave -regexp .*

add_wave -regexp /tb_adc_frame/rx_1/.*
add_wave -regexp /tb_adc_frame/adc_frame_2/.*
add_wave -regexp /tb_adc_frame/adc_frame_2/adc_frame_EB_1/.*
add_wave -regexp /tb_adc_frame/adc_frame_2/adc_frame_dma_1/.*

source tb_adc_frame/run.tcl
