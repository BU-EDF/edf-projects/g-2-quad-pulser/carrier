library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.FREE_RUN_PKG.all;

entity tb_fr is
  port (    
    clk_200Mhz  : in std_logic;
    reset       : in std_logic);

end entity tb_fr;

architecture behavioral of tb_fr is

  component free_run is
    generic (
      USEC_C  : integer;
      MSEC_C  : integer;
      BURST_C : integer);
    port (
      clk     : in  std_logic;
      rst_n   : in  std_logic;
      set     : in  FREE_RUN_t;
      trigger : out std_logic);
  end component free_run;

  signal rst_n   : std_logic;
  signal set     : FREE_RUN_t;
  signal trigger : std_logic;
  signal counter : unsigned(31 downto 0);
  
begin  -- architecture behavioral

  tb: process (clk_200Mhz, reset) is
  begin  -- process tb
    if reset = '1' then              -- asynchronous reset (active high)
      set <= DEFAULT_FREE_RUN;
      set.burst_mask <= x"FFFFFFFF";
      set.burst_spacing <= x"2710";
      set.periodic_delay <= x"0578";
      set.burst_ena <= '1';
      set.enable <= '0';
      counter <= x"00000000";
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      counter <= counter + 1;

      if counter = x"0000000F" then
        set.enable <= '1';        
      end if;
    end if;
  end process tb;

  rst_n <= not reset;

  free_run_1: entity work.free_run
    generic map (
      USEC_C  => 200,
      MSEC_C  => 1000,
      BURST_C => 32)
    port map (
      clk     => clk_200Mhz,
      rst_n   => rst_n,
      set     => set,
      trigger => trigger);
  
end architecture behavioral;
