library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use work.types.all;
use work.global_CTRL.all;

entity tb_global is
  port (
    clk_200Mhz  : in std_logic;
    reset       : in std_logic);

end entity tb_global;

architecture behavioral of tb_global is
  
  signal clk20_sr : std_logic_vector(9 downto 0);



  signal ext_trigger  : std_logic;        -- trigger input
  signal vac_fail     : std_logic;        -- vacuum fail input
  signal permit       : std_logic;        -- accelerator permit input
  signal adc_trigger  : std_logic_vector(4 downto 0);  -- broadcast trigger signal to all ADC boards
  signal reset_spill  : std_logic_vector(4 downto 0);
  signal oob_trigger  : std_logic_vector(4 downto 0);
  signal spill_number : std_logic_vector(31 downto 0);
  signal control      : GLOBAL_Control_t;
  signal monitor      : GLOBAL_Monitor_t;
  signal rst : std_logic;
  

  signal counter : integer;
  signal cause_error : std_logic;

begin  -- architecture behavioral

  tb: process (clk_200Mhz, reset) is
  begin  -- process tb
    if reset = '1' then              -- asynchronous reset (active high)
      counter <= 0;
      rst <= '1';
      ext_trigger <= '0';
      vac_fail    <= '0';
      permit      <= '0';
      control <= DEFAULT_GLOBAL_Control;
    elsif clk_200Mhz'event and clk_200Mhz = '1' then  -- rising clock edge
      counter <= counter + 1;
      case counter is
        when 1  =>
          control.extra_pulses(1).bypass <= '0';
          control.extra_pulses(1).pulse_en <= x"FF";
          control.extra_pulses(1).pulse(0) <= "0"&x"000000";
          control.extra_pulses(1).pulse(1) <= "0"&x"010000";
          control.extra_pulses(1).pulse(2) <= "0"&x"020000";
          control.extra_pulses(1).pulse(3) <= "0"&x"030000";
          control.extra_pulses(1).pulse(4) <= "0"&x"040000";
          control.extra_pulses(1).pulse(5) <= "0"&x"050000";
          control.extra_pulses(1).pulse(6) <= "0"&x"060000";
          control.extra_pulses(1).pulse(7) <= "0"&x"070000";                                                        
        when 10 => rst <= '0';
        when 50 =>
          control.ext_en <= '1';
          control.pulser_ena <= '1';
          control.wfd_ena <= '1';
        when  100000 => ext_trigger <= '1';
        when  100001 => ext_trigger <= '0';
        when  200000 => ext_trigger <= '1';
        when  200002 => ext_trigger <= '0';
        when  300000 => ext_trigger <= '1';
        when  300003 => ext_trigger <= '0';
        when  400000 => ext_trigger <= '1';
        when  400004 => ext_trigger <= '0';
        when  500000 => ext_trigger <= '1';
        when  500005 => ext_trigger <= '0';
        when  600000 => ext_trigger <= '1';
        when  600006 => ext_trigger <= '0';
        when  700000 => ext_trigger <= '1';
        when  700007 => ext_trigger <= '0';
        when  800000 => ext_trigger <= '1';
        when  800008 => ext_trigger <= '0';
        when  900000 => ext_trigger <= '1';
        when  900009 => ext_trigger <= '0';
        when 1000000 => ext_trigger <= '1';
        when 1000010 => ext_trigger <= '0';
        when 1100000 => ext_trigger <= '1';
        when 1100011 => ext_trigger <= '0';
        when 1200000 => ext_trigger <= '1';
        when 1200012 => ext_trigger <= '0';
        when 1300000 => ext_trigger <= '1';
        when 1300013 => ext_trigger <= '0';
        when 1400000 => ext_trigger <= '1';
        when 1400014 => ext_trigger <= '0';
        when 1500000 => ext_trigger <= '1';
        when 1500015 => ext_trigger <= '0';
        when 1600000 => ext_trigger <= '1';
        when 1600016 => ext_trigger <= '0';
        when 1700000 => ext_trigger <= '1';
        when 1700017 => ext_trigger <= '0';
        when 1800000 => ext_trigger <= '1';
        when 1800018 => ext_trigger <= '0';
        when 1900000 => ext_trigger <= '1';
        when 1900019 => ext_trigger <= '0';
        when 2000000 => ext_trigger <= '1';
        when 2000001 => ext_trigger <= '0';
        when 2000002 => ext_trigger <= '1';
        when 2000010 => ext_trigger <= '0';
                     
        when others => null;
      end case;
      
    end if;
  end process tb;




  GLOBAL_1: entity work.GLOBAL
    port map (
      clk          => clk_200Mhz,
      rst          => rst,
      ext_trigger  => ext_trigger,
      vac_fail     => vac_fail,
      permit       => permit,
      adc_trigger  => adc_trigger,
      reset_spill  => reset_spill,
      spill_number => spill_number,
      oob_trigger  => oob_trigger,
      control      => control,
      monitor      => monitor);
  
end architecture behavioral;
