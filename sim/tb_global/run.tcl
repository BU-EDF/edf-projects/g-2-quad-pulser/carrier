restart
add_force {/tb_global/GLOBAL_1/counter_2/local_count} -radix hex {0x0000FFF8 0ns}
add_force {/tb_global/clk_200Mhz} -radix hex {1 0ns} {0 2500ps} -repeat_every 5000ps
add_force {/tb_global/reset} -radix hex {1 0ns}
run 10ns
add_force {/tb_global/reset} -radix hex {0 0ns}
run 10ns
remove_forces { {/tb_global/GLOBAL_1/counter_2/local_count} }
run 30us
run 10ms
