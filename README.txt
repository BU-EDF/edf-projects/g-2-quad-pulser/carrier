#####################
#Building
#####################
Notes:
		Don't source anything.  The makefile takes care of the vivado enviornment

default:
		build everything up to the zynq image files.
make bit:
		build a new bit file, but no zynq stuff
make chipscope:
		Open/build synthesis and allow you to build debug chipscope things
		Make sure you hit save to update the dcp (design check point)
     		After this type "make bit" and you'll get a bit and ltx files for
		chipscope.
make synth/place/route/bit:
                Run the build process up to whichever of these you specified.

make open_project:
		Open the project file built from the build system.

make open_impl:
		Open the implemented design (post route dcp) in vivado

make open_hw:
		Open vivado in the HW manager mode.
		Run this after "make bit" if you want to use your debug cores (they are in ./bit)



menu configs:
     These will be brought up the first time you build the project or if the BD tcl file changes.
     There are two of these, the first is the bootload menuconfig, the second is the rootfs menuconfig.
     Here is a list of options you need to set if these configs come up:

     BOOTLOADER:
	No changes needed.
     ROOTFS:
	Filesystem Packages->base->external-xilinx-toolchain->libstdc++6
	                   ->base/shell->bash->bash
			   ->console/network->dropbear->dropbear
			                              ->dropbear-openssh-sftp-server
