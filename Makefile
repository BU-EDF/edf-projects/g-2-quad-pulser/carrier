#################################################################################
# make stuff
#################################################################################
SHELL=bash
#OUTPUT_MARKUP= 2>&1 | tee ../build/make_log.txt | ccze -A

#################################################################################
# VIVADO stuff
#################################################################################
VIVADO_VERSION=2015.4
#VIVADO_VERSION=2018.2
VIVADO_FLAGS=-notrace -mode batch
VIVADO_SHELL="/work/Xilinx/Vivado/"$(VIVADO_VERSION)"/settings64.sh"
VIVADO_SDK="/work/Xilinx/SDK/"$(VIVADO_VERSION)"/settings64.sh"
VIVADO_PETALINUX="/work/Xilinx/petalinux/petalinux-v"$(VIVADO_VERSION)"-final/settings.sh"
#VIVADO_PETALINUX="/opt/Xilinx/petalinux/"$(VIVADO_VERSION)"/settings.sh"


#################################################################################
# TCL scripts
#################################################################################
BIT_TCL=scripts/Run_bit.tcl
ROUTE_TCL=scripts/Run_route.tcl
PLACEMENT_TCL=scripts/Run_placement.tcl
PRESYNTH_TCL=scripts/Run_presynth.tcl
SYNTHESIS_TCL=scripts/Run_synth.tcl
HWDEF_TCL=scripts/Run_hwdef.tcl
HWD_TCL=scripts/Run_hwd.tcl
SDK_TCL=scripts/Run_SDK.tcl
HW_TCL=scripts/Run_hw.tcl

#################################################################################
# Source files
#################################################################################
FABRIC_PATH=../src
FABRIC_FILES=$(shell find ./src/$(FABRIC_PATH) | grep -e "\.v\|\.xdc")
ARM_PATH=../bd/zynq/hdl/
ARM_FILES=$(shell find ./src/$(ARM_PATH) | grep -e "\.v")
BLOCK_DESIGN=./bd/block_design.tcl
CORES_PATH=../cores
CORES_FILES=$(shell find ./src/$(CORES_PATH) | grep -e "\.dcp|\.xci|\.xdc")

#################################################################################
# Short build names
#################################################################################
PRESYNTH=proj/top.xpr
SYNTH=build/post_synth.dcp
HWDEF=build/top.hwdef
CHIPSCOPE=bit/dbg.ltx 
PLACE=build/post_place.dcp
ROUTE=build/post_route.dcp
BIT=bit/top.bit
HDF_FILE=os/hwd/top.hdf
ZYNQ_OS=zynq_os
ZYNQ_OS_PROJECT_PATH=os/$(ZYNQ_OS)
ZYNQ_OS_PROJECT=$(ZYNQ_OS_PROJECT_PATH)/config.project
BOOT_FILES=$(ZYNQ_OS_PROJECT)/images/linux/BOOT.BIN
REG_MAP=register_map/register_map.pdf
REG_MAP_SOURCE=src/register/register_map.vhd
SW_REG_INTERFACE=os/software/reg_test


CONFIG_BOOTLOADER=CONFIG_BOOTLOADER
CONFIG_ROOTFS=CONFIG_ROOTFS
BUILD_BOOTLOADER=$(ZYNQ_OS_PROJECT_PATH)/images/linux/zynq_fsbl.elf
BUILD_DEVICE_TREE=$(ZYNQ_OS_PROJECT_PATH)/images/linux/system.dtb
BUILD_KERNEL=$(ZYNQ_OS_PROJECT_PATH)/images/linux/vmlinux
BUILD_ROOTFS=$(ZYNQ_OS_PROJECT_PATH)/build/linux/rootfs/targetroot
UPDATE_ROOTFS=UPDATE_ROOTFS
BUILD_UBOOT=$(ZYNQ_OS_PROJECT_PATH)/images/linux/u-boot.bin
STAGING_AREA=STAGING_AREA
PACKAGE_LINUX_IMAGE=$(ZYNQ_OS_PROJECT_PATH)/images/linux/image.elf
#################################################################################
# Behavioral sim
#################################################################################
#build a list of vdb files for a list of vhd files
build_vdb_list = $(patsubst %.vhd,%.vdb,$(subst src/,sim/vhdl/,$(1)))

MISC_SRCS=src/misc/counter.vhd src/misc/timed_counter.vhd
DMA_FIFO_INTERFACE_SRCS=src/waveform_interface/dma_fifo_interface_CTRL.vhd src/waveform_interface/dma_fifo_interface.vhd

TEST_DMA_FIFO_SRCS=$(MISC_SRCS) $(DMA_FIFO_INTERFACE_SRCS) src/waveform_interface/test_CTRL.vhd src/waveform_interface/test.vhd

TEST_DMA_FIFO=TEST_DMA_FIFO
TEST_DMA_FIFO_VDBS:=$(patsubst %.vhd,%.vdb,$(subst src/,sim/vhdl/,$(TEST_DMA_FIFO_SRCS)))



.SECONDARY:

.PHONY: clean bit clean_os hw reg_map_clean list sim_clean CONFIG_BOOTLOADER CONFIG_ROOTFS UPDATE_ROOTFS

all: bit $(BOOT_FILES) 
#$(REG_MAP)

#################################################################################
# Short name rules
#################################################################################
synth     : $(SYNTH)
chipscope : $(CHIPSCOPE)
place     : $(PLACE)
route     : $(ROUTE)
bit       : $(BIT)
reg_map   : $(REG_MAP)

#################################################################################
# Clean
#################################################################################
clean_os:
	@rm -rf os/.metadata  os/SDK.log  os/.Xil 2> /dev/null
	@rm -rf $(ZYNQ_OS_PROJECT_PATH)
#	@rm -rf ./os/top* 2> /dev/null 
#	@rm -rf ./os/os_proj/* 2> /dev/null
#	@rm -rf ./os/os_proj/build 2> /dev/null
	@echo "Cleaning up os"
clean_ip:
	@echo "Cleaning up ip dcps"
	@find ./cores -type f -name '*.dcp' -delete
clean_bd:
	@echo "Cleaning up bd generated files"
	@rm -rf ./bd/zynq/*
#lean: clean_os reg_map_clean sw_clean
clean: clean_os reg_map_clean
	@rm -rf ./build 2> /dev/null
	@rm -rf ./bit/* 2> /dev/null 
	@rm -f ./os/hwd/top.hdf 2> /dev/null
	@rm -f $(PRESYNTH)
	@rm -rf ./proj/*
	@echo "Cleaning up"

distclean: clean_ip clean_bd clean

#################################################################################
# Open vivado
#################################################################################
open_project : $(PRESYNTH)
	@source $(VIVADO_SHELL) &&\
	cd build &&\
	vivado ../$(PRESYNTH)

open_synth : $(SYNTH)
	@source $(VIVADO_SHELL) &&\
	cd build &&\
	vivado post_synth.dcp

open_impl : $(ROUTE)
	@source $(VIVADO_SHELL) &&\
	cd build &&\
	vivado post_route.dcp

open_hw :
	@source $(VIVADO_SHELL) &&\
	cd build &&\
	vivado -source ../$(HW_TCL)

$(CHIPSCOPE) : $(SYNTH)
	@source $(VIVADO_SHELL) &&\
	cd build &&\
	vivado post_synth.dcp

#################################################################################
# Documentation
#################################################################################
$(REG_MAP) : $(REG_MAP_SOURCE)
	@cd register_map &&\
	make
reg_map_clean :
	@cd register_map &&\
	make --no-print-directory clean
	@echo "Cleaning up register map"

#################################################################################
# Behav sim
#################################################################################
sim_clean :
	@cd sim && rm -rf xsim.dir vhdl webtalk* xelab* xvhdl* *.log *.jou
	@echo cleaning up sim directory

$(TEST_DMA_FIFO) : $(TEST_DMA_FIFO_VDBS)
	@mkdir -p sim/ && \
	source $(VIVADO_SHELL) &&\
	cd sim &&\
	xelab -debug typical interface_test -s interface_test
	source $(VIVADO_SHELL) &&\
	cd sim &&\
	xsim interface_test -gui -t ./dma_fifo_test/setup.tcl

#################################################################################
# FPGA building
#################################################################################
$(PRESYNTH) : $(CORES_FILES) $(FABRIC_FILES) $(BLOCK_DESIGN)
	@rm -f ./build/make_log.txt 
	mkdir -p build && \
	set -o pipefail && \
	source $(VIVADO_SHELL) &&\
	cd build &&\
	vivado $(VIVADO_FLAGS)  -source ../$(PRESYNTH_TCL) -tclargs $(CORES_PATH) $(FABRIC_PATH) $(OUTPUT_MARKUP) 


$(SYNTH) : $(PRESYNTH) $(SYNTHESIS_TCL)
	@mkdir -p build && \
	set -o pipefail && \
	source $(VIVADO_SHELL) &&\
	cd build &&\
	vivado $(VIVADO_FLAGS)  -source ../$(SYNTHESIS_TCL) -tclargs $< $(OUTPUT_MARKUP) 

#rules for the post synth steps.  These are shared between all the steps
$(HWDEF) : $(SYNTH)  $(HWDEF_TCL)
	@source $(VIVADO_SHELL) &&\
	set -o pipefail && \
	cd build && \
	vivado $(VIVADO_FLAGS) -source ../$(HWDEF_TCL) -tclargs $< $(OUTPUT_MARKUP) 

$(PLACE) : $(HWDEF)  $(PLACEMENT_TCL)
	@source $(VIVADO_SHELL) &&\
	set -o pipefail && \
	cd build && \
	vivado $(VIVADO_FLAGS) -source ../$(PLACEMENT_TCL) -tclargs $< $(OUTPUT_MARKUP) 

$(ROUTE) : $(PLACE)  $(ROUTE_TCL)
	@source $(VIVADO_SHELL) &&\
	set -o pipefail && \
	cd build && \
	vivado $(VIVADO_FLAGS) -source ../$(ROUTE_TCL) -tclargs $< $(OUTPUT_MARKUP) 

$(BIT) : $(ROUTE)  $(BIT_TCL)
	@source $(VIVADO_SHELL) &&\
	set -o pipefail && \
	cd build && \
	vivado $(VIVADO_FLAGS) -source ../$(BIT_TCL) -tclargs $< $(OUTPUT_MARKUP) 

#################################################################################
# ARM building
#################################################################################
$(HDF_FILE) : $(BIT) $(HWD_TCL) 
	@echo "Building HDF File and resetting zynq os"
	@mkdir -p os/hwd && \
	source $(VIVADO_SHELL) &&\
	cd build && \
	vivado $(VIVADO_FLAGS) -source ../$(HWD_TCL) -tclargs $<

RECONFIG_ZYNQ : $(HDF_FILE)
	@echo "Creating fresh OS project"
	@rm -rf $(ZYNQ_OS_PROJECT_PATH)
	@cd os &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-create --type project --name $(ZYNQ_OS) --template zynq --force
	@cd $(ZYNQ_OS_PROJECT_PATH) &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-config --get-hw-description ../hwd -c kernel

$(ZYNQ_OS_PROJECT) : $(HDF_FILE)
	@echo "Creating fresh OS project"
	@rm -rf $(ZYNQ_OS_PROJECT_PATH)
	@cd os &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-create --type project --name $(ZYNQ_OS) --template zynq --force
	@cd $(ZYNQ_OS_PROJECT_PATH) &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-config --get-hw-description ../hwd --oldconfig


$(CONFIG_BOOTLOADER): $(ZYNQ_OS_PROJECT)
	@cd $(ZYNQ_OS_PROJECT_PATH) &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-config --get-hw-description ../hwd 

$(CONFIG_ROOTFS): $(ZYNQ_OS_PROJECT) 
	@cd $(ZYNQ_OS_PROJECT_PATH) &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-config -c rootfs
	@cp -a $(ZYNQ_OS_PROJECT_PATH)/subsystems/linux/configs/rootfs/config $(ZYNQ_OS_PROJECT_PATH)_mods/configs/rootfs/config



$(BUILD_BOOTLOADER): $(ZYNQ_OS_PROJECT)
	@echo "Building bootloader"
	@cd $(ZYNQ_OS_PROJECT_PATH) &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-build -c bootloader

$(BUILD_DEVICE_TREE): $(ZYNQ_OS_PROJECT) $(ZYNQ_OS_PROJECT_PATH)_mods/device-tree/pl_dma.dtsi $(ZYNQ_OS_PROJECT_PATH)_mods/device-tree/system-top.dts
	@cp $(ZYNQ_OS_PROJECT_PATH)_mods/device-tree/pl_dma.dtsi $(ZYNQ_OS_PROJECT_PATH)/subsystems/linux/configs/device-tree/
	@cp $(ZYNQ_OS_PROJECT_PATH)_mods/device-tree/system-top.dts $(ZYNQ_OS_PROJECT_PATH)/subsystems/linux/configs/device-tree/
	@echo "Building device-tree"
	@cd $(ZYNQ_OS_PROJECT_PATH) &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-build -c device-tree

#$(BUILD_KERNEL): $(ZYNQ_OS_PROJECT) $(ZYNQ_OS_PROJECT_PATH)/subsystems/linux/configs/kernel/config
$(BUILD_KERNEL): $(ZYNQ_OS_PROJECT)
	@echo "Building kernel"
	@cd $(ZYNQ_OS_PROJECT_PATH) &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-build -c kernel

#os/zynq_os_mods/configs/rootfs/config : $(CONFIG_ROOTFS)

$(BUILD_ROOTFS): $(ZYNQ_OS_PROJECT) $(ZYNQ_OS_PROJECT_PATH)_mods/configs/rootfs/config
	@echo "Building rootfs"
	@cp -a $(ZYNQ_OS_PROJECT_PATH)_mods/configs/rootfs/config $(ZYNQ_OS_PROJECT_PATH)/subsystems/linux/configs/rootfs/config
	@cd $(ZYNQ_OS_PROJECT_PATH) &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-build -c rootfs

$(UPDATE_ROOTFS): $(BUILD_ROOTFS) 
	@echo "Adding modifications to rootfs"
	@cd $(ZYNQ_OS_PROJECT_PATH)_mods &&\
	make PRE_PACKAGE

$(BUILD_UBOOT): $(ZYNQ_OS_PROJECT)
	@echo "Building u-boot"
	@cd $(ZYNQ_OS_PROJECT_PATH) &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-build -c u-boot

#$(PACKAGE_LINUX_IMAGE) : $(ZYNQ_OS_PROJECT) $(BUILD_BOOTLOADER) $(BUILD_DEVICE_TREE) $(BUILD_KERNEL) $(UPDATE_ROOTFS) $(BUILD_UBOOT)
$(PACKAGE_LINUX_IMAGE) : $(ZYNQ_OS_PROJECT) $(BUILD_BOOTLOADER) $(BUILD_DEVICE_TREE) 
	make $(BUILD_KERNEL)
	make $(UPDATE_ROOTFS)
	make $(BUILD_UBOOT)
	@echo "Packaging images"
	@cd $(ZYNQ_OS_PROJECT_PATH) &&\
	source $(VIVADO_PETALINUX) && \
	petalinux-build -x package

$(BOOT_FILES): $(PACKAGE_LINUX_IMAGE)
	@cd $(ZYNQ_OS_PROJECT_PATH) &&\
	source $(VIVADO_PETALINUX) && \
	source $(VIVADO_SHELL) && \
	source $(VIVADO_SDK) && \
	petalinux-package --boot --format BIN --fsbl images/linux/zynq_fsbl.elf --fpga images/linux/top.bit  --uboot --force

#################################################################################
# ARM software
#################################################################################
$(SW_REG_INTERFACE) : 
	cd os/software &&\
	make
sw_clean :
	@cd os/software &&\
	make --no-print-directory clean
	@echo "Cleaning up reg interface software"

#################################################################################
# Help 
#################################################################################

#list magic: https://stackoverflow.com/questions/4219255/how-do-you-get-the-list-of-targets-in-a-makefile
list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$' | column
#	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'
#	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$' | xargs

#################################################################################
# Sim
#################################################################################
define TB_RULE =
	set -o pipefail &&\
	source $(VIVADO_SHELL) && \
	cd sim &&\
	xvhdl $@/$@.vhd	$(OUTPUT_MARKUP)
	@mkdir -p sim/ && \
	source $(VIVADO_SHELL) &&\
	cd sim &&\
	xelab -debug typical $@ -s $@ $(OUTPUT_MARKUP)
	source $(VIVADO_SHELL) &&\
	cd sim &&\
	xsim $@ -gui -t $@/setup.tcl 
endef

#build the vdb file from a vhd file
sim/vhdl/%.vdb : src/%.vhd 
	@echo "Building $@ from $<"
	@mkdir -p sim/vhdl && \
	source $(VIVADO_SHELL) && \
	cd sim &&\
	xvhdl ../$< $(OUTPUT_MARKUP)
	@cd sim && mkdir -p $(subst src,vhdl,$(dir $<)) 
	@cd sim && ln -f -s $(PWD)/sim/xsim.dir/work/$(notdir $@) $(subst src,vhdl,$(dir $<)) 

IPCORES=$(foreach  file,$(shell find ./cores | grep "netlist.vhdl"),../$(file))

TB_IPCORES : 
	@echo "Building sim files for IPCores"
	source $(VIVADO_SHELL) && \
	cd sim && \
	xvhdl $(IPCORES)

TB_MISC_VDBS=$(call build_vdb_list, src/misc/counter.vhd src/misc/types.vhd src/misc/timed_counter.vhd)

TB_FMC_RX_VDBS=$(TB_MISC_VDBS) $(call build_vdb_list, src/fmc_comm/rx/rx.vhd) 
tb_fmc_rx : $(TB_FMC_RX_VDBS)
	$(TB_RULE)

TB_ADC_FRAME_EB_VDBS=$(TB_FMC_RX_VDBS) $(call build_vdb_list, src/event_builder/adc_frame_EB.vhd) 
tb_adc_frame_EB : $(TB_ADC_FRAME_EB_VDBS)
	$(TB_RULE)

TB_ADC_FRAME_VDBS=TB_IPCORES $(TB_ADC_FRAME_EB_VDBS) $(call build_vdb_list, src/event_builder/adc_frame_dma.vhd src/event_builder/adc_frame.vhd) 
tb_adc_frame : $(TB_ADC_FRAME_VDBS)
	$(TB_RULE)

TB_EB_VDBS=TB_IPCORES $(TB_ADC_FRAME_VDBS) $(call build_vdb_list, src/event_builder/event_builder_CTRL.vhd src/event_builder/event_builder.vhd) 
tb_eb : $(TB_EB_VDBS)
	$(TB_RULE)

TB_FR_VDBS=$(call build_vdb_list, src/global/free_run_PKG.vhd src/global/free_run.vhd) 
tb_fr : $(TB_FR_VDBS)
	$(TB_RULE)

TB_GLOBAL_VDBS=$(call build_vdb_list, src/misc/types.vhd src/misc/counter.vhd  src/misc/timed_counter.vhd src/global/free_run_PKG.vhd src/global/free_run.vhd src/global/global_CTRL.vhd src/global/extra_pulses/extra_pulses.vhd src/global/global.vhd) 
tb_global : $(TB_GLOBAL_VDBS)
	$(TB_RULE)
