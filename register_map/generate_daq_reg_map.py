#!/usr/bin/python

import os
import argparse


def GetMasks(line):
    bit_mask_r=0x0
    bit_mask_w=0x0
    bit_mask_a=0x0
    bit_mask_i=0x0
    #handle simple lines with one bit on them
    if line[1].find("..") == -1:                
        mask = 1 << int(line[1])        
        if line[2].find("r") != -1:
            bit_mask_r = (bit_mask_r & ~mask) + mask
        if line[2].find("w") != -1:
            bit_mask_w = (bit_mask_w & ~mask) + mask
        if line[2].find("a") != -1:
            bit_mask_a = (bit_mask_a & ~mask) + mask
        if line[2].find("i") != -1:
            bit_mask_i = (bit_mask_i & ~mask) + mask
        #handle lines that have bit ranges in them.
    else:
        sub_line = line[1].split("..")
        low_bound = min(int(sub_line[1]),int(sub_line[0]))
        high_bound = max(int(sub_line[1]),int(sub_line[0]))+1
        for ibit in range(low_bound,high_bound):
            mask = 1 << ibit        
            if line[2].find("r") != -1:
                bit_mask_r = (bit_mask_r & ~mask) + mask
            if line[2].find("w") != -1:
                bit_mask_w = (bit_mask_w & ~mask) + mask
            if line[2].find("a") != -1:
                bit_mask_a = (bit_mask_a & ~mask) + mask
            if line[2].find("i") != -1:
                bit_mask_i = (bit_mask_i & ~mask) + mask
    return bit_mask_r,bit_mask_w,bit_mask_a,bit_mask_i

#process a register address and its entries
def GenEntry(out_file,entries):
    if len(entries) == 0:
        return

    reg_r = int(0)
    reg_w = int(0)
    reg_a = int(0)
    reg_i = int(0)

    #scan each of the bit lines and record which attributes (r/w/a) they have
    for iLine in range(2,len(entries)):
        line = entries[iLine].split()
        if len(line) > 1:
            bit_mask_r,bit_mask_w,bit_mask_a,bit_mask_i = GetMasks(line)
            reg_r = reg_r | bit_mask_r
            reg_w = reg_w | bit_mask_w
            reg_a = reg_a | bit_mask_a
            reg_i = reg_i | bit_mask_i
#        if len(line) > 1:
#            #handle simple lines with one bit on them
#            if line[1].find("..") == -1:                
#                mask = 1 << int(line[1])        
#                if line[2].find("r") != -1:
#                    reg_r = (reg_r & ~mask) + mask
#                if line[2].find("w") != -1:
#                    reg_w = (reg_w & ~mask) + mask
#                if line[2].find("a") != -1:
#                    reg_a = (reg_a & ~mask) + mask
#                if line[2].find("i") != -1:
#                    reg_i = (reg_i & ~mask) + mask
#                #handle lines that have bit ranges in them.
#            else:
#                sub_line = line[1].split("..")
#                low_bound = min(int(sub_line[1]),int(sub_line[0]))
#                high_bound = max(int(sub_line[1]),int(sub_line[0]))+1
#                for ibit in range(low_bound,high_bound):
#                    mask = 1 << ibit        
#                    if line[2].find("r") != -1:
#                        reg_r = (reg_r & ~mask) + mask
#                    if line[2].find("w") != -1:
#                        reg_w = (reg_w & ~mask) + mask
#                    if line[2].find("a") != -1:
#                        reg_a = (reg_a & ~mask) + mask
#                    if line[2].find("i") != -1:
#                        reg_i = (reg_i & ~mask) + mask

    #Start the first line with the register name (removing the "_" marks)
    reg_name = entries[0]
    latex_line = reg_name.replace('_',' ') + " & "
    #add the hex address in the second column
    reg_address = hex(int("0"+entries[1].replace('"','').replace(';',''),16))
    latex_line = latex_line + reg_address
    # add the color coded bit fields
    for iBit in range(31,-1,-1):
        mask = 0x1 << iBit
        latex_line = latex_line + " & "
        if (reg_r & mask == mask) and (reg_w & mask == mask):
            latex_line = latex_line + "\\cellcolor{cyan} "
        elif (reg_r & mask == mask) and (reg_i & mask == mask):
            latex_line = latex_line + "\\cellcolor{magenta} "
        elif (reg_r & mask == mask):
            latex_line = latex_line + "\\cellcolor{green} "
        elif (reg_w & mask == mask):
            latex_line = latex_line + " \\cellcolor{blue} "
        elif (reg_a & mask == mask):
            latex_line = latex_line + " \\cellcolor{red} "
        latex_line = latex_line + " " + str(iBit)
    latex_line = latex_line + " \\\\ \\hline\n"
    out_file.write(latex_line)
    #add each bit with its description on the next line
    for iLine in range(2,len(entries)):
        line = entries[iLine].split()
        
        latex_line = " & " + " ".join(line[1:3]) + " &  \\multicolumn{32}{|l|}{" + " ".join(line[3:]) + "} \\\\ \\hline\n"
        out_file.write(latex_line)
        if len(line) > 2:
            print reg_name + "." + line[3] + " " + line[2] + " " + line[1]
    
#    print entries[0], hex(reg_r)[2:].zfill(8), hex(reg_w)[2:].zfill(8), hex(reg_a)[2:].zfill(8)










parser = argparse.ArgumentParser(description='Generate latex register map')

parser.add_argument('file', type=str, help='input vhdl file')
args = parser.parse_args()



vhdl_file = open(args.file)
lines = vhdl_file.readlines()

#open the output file
out_file_name = os.path.basename(args.file)
if out_file_name.find('.vhd') == -1:
    out_file_name = out_file_name + ".tex"
else:
    out_file_name = out_file_name.replace('.vhd','.tex')
print out_file_name
out_file = open(out_file_name,'w+')


startLine = 0
endLine = len(lines)

iLine = 0
while iLine < len(lines):
    if lines[iLine].find("-- Address space") != -1:
        startLine = iLine
    if lines[iLine].find("-- Signals") != -1:
        endLine = iLine
        break
    iLine = iLine + 1

out_file.write( "\\documentclass{standalone}\n")
out_file.write( "\\usepackage{color}\n")
out_file.write( "\\usepackage{colortbl}\n")
out_file.write( "\\begin{document}\n")
out_file.write( "\\setlength{\\tabcolsep}{1.45pt}\n")
out_file.write( "% This sets 32 centered fields\n")
out_file.write( "\\begin{tabular}{|c|c|*{32}{c|}}  \n")
out_file.write( "  \\hline\n")
out_file.write( " Register & address & \multicolumn{32}{|c|}{} \\\\ \\hline\n")


row = []
for iLine in range(startLine,endLine):
    line = lines[iLine]

    if line.find("constant") != -1:
        GenEntry(out_file,row)
        row = [line.split()[1],line.split()[7]]
    elif len(row) > 0:
        if line.find("----") > 0:
            GenEntry(out_file,row)
            row = []
        else:
            row.append(line)

out_file.write("  \\hline\n")
out_file.write("\\end{tabular}\n")
out_file.write("\\end{document}\n")
