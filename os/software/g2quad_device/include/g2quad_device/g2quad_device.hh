#ifndef __G2QUAD_DEVICE_HPP__
#define __G2QUAD_DEVICE_HPP__

//For tool device base class
#include <BUTool/CommandList.hh>

#include <BUTool/DeviceFactory.hh>
#include <BUTool/helpers/register_helper.hh>

#include <string>
#include <vector>

#include <g2quad/g2quad.hh>


namespace BUTool{
  
  class G2QUADDevice: public CommandList<G2QUADDevice> , public RegisterHelper{
  public:
    G2QUADDevice(std::vector<std::string> arg);
    ~G2QUADDevice();
  private:
    g2quad * hw_interface;
    std::string remoteAddress;
    std::string carrierTableName;
    std::string ADCBoardTableName;

    //Implementation of read/write/myMatchRegex code
    std::vector<std::string> myMatchRegex(std::string regex);
    uint32_t    RegReadAddress(uint32_t addr);
    uint32_t    RegReadRegister(std::string const & addr);
    void        RegWriteAction(std::string const & addr);
    void        RegWriteAddress(uint32_t addr,uint32_t data);
    void        RegWriteRegister(std::string const & addr, uint32_t data);
	        
    uint32_t    GetRegAddress(std::string const & reg);
    uint32_t    GetRegMask(std::string const & reg);
    uint32_t    GetRegSize(std::string const & reg);
    std::string GetRegMode(std::string const & reg);
    std::string GetRegPermissions(std::string const & reg);
    std::string GetRegDescription(std::string const & reg);
    std::string GetRegDebug(std::string const & reg);



    //Here is where you update the map between string and function
    void LoadCommandList();

    CommandReturn::status StatusDisplay(std::vector<std::string>,std::vector<uint64_t>);
    
    //Add new command functions here           

    CommandReturn::status InitBoardLink(std::vector<std::string>,std::vector<uint64_t>);
    CommandReturn::status ADCBoardCommand(std::vector<std::string>,std::vector<uint64_t>);
    CommandReturn::status ConfigADCs(std::vector<std::string>,std::vector<uint64_t>);
    CommandReturn::status AlignADCChannel(std::vector<std::string>,std::vector<uint64_t>);
    //    CommandReturn::status CaptureADCChannel(std::vector<std::string>,std::vector<uint64_t>);

    CommandReturn::status ReadI2C(std::vector<std::string>,std::vector<uint64_t>);
    CommandReturn::status WriteI2C(std::vector<std::string>,std::vector<uint64_t>);
    
    CommandReturn::status LinkTest(std::vector<std::string>,std::vector<uint64_t>);

    CommandReturn::status ConfigureFreeRun(std::vector<std::string>,std::vector<uint64_t>);
    CommandReturn::status EnableFreeRun(std::vector<std::string>,std::vector<uint64_t>);
    CommandReturn::status DisableFreeRun(std::vector<std::string>,std::vector<uint64_t>);

    CommandReturn::status PulserConfig(std::vector<std::string>,std::vector<uint64_t>);
    CommandReturn::status ProgramFlash(std::vector<std::string>, std::vector<uint64_t>); //added 06/29/2018
    CommandReturn::status TrigPulse(std::vector<std::string>, std::vector<uint64_t>); //07/26/2018
    CommandReturn::status ResetPulser(std::vector<std::string>, std::vector<uint64_t>); //07/26/2018  
    //Add new command (sub command) auto-complete files here
    //    std::string autoComplete_Help(std::vector<std::string> const &,std::string const &,int);
    std::string autoComplete_StatusTables(std::vector<std::string> const & line,std::string const & currentToken ,int state);

  };
  RegisterDevice(G2QUADDevice,
		 "g2quad",
		 "server-address",
		 "q",
		 "g2quad",
		 "connection ip"
		 ); //Register G2QUADDevice with the DeviceFactory  
}

#endif
