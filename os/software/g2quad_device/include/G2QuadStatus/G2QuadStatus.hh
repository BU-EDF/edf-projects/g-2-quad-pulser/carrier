#ifndef __G2QUAD_STATUS_HH__
#define __G2QUAD_STATUS_HH__

#include <g2quad/g2quad.hh>
#include <g2quad/g2quad_Exception.hh>
#include <BUTool/helpers/StatusDisplay/StatusDisplay.hh>

namespace BUTool{
  class G2QuadStatus: public StatusDisplay {
  public:
    G2QuadStatus(g2quad * _hw):hw(NULL){
      if(_hw == NULL){
	BUException::G2QUAD_BAD_ARGS e;
	e.Append("Bad pointer for G2QUAD status table\n");
	throw e;
      }
      hw = _hw;
      SetVersion(hw->GetSVNVersion());
    }
  private:
    void Process(std::string const & singleTable);
    g2quad * hw;
  };

}
#endif
