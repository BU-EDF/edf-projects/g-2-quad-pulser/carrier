#include <g2quad_device/g2quad_device.hh>

CommandReturn::status BUTool::G2QUADDevice::PulserConfig(std::vector<std::string> /*sArg*/,std::vector<uint64_t> iArg){
  if(iArg.size() < 6){
    return CommandReturn::BAD_ARGS;
  }else if(iArg.size() < 8){
    hw_interface->ConfigurePulser(iArg[0],iArg[1],
				  iArg[2],iArg[3],
				  iArg[4],iArg[5]);
  }else{
    hw_interface->ConfigurePulser(iArg[0],iArg[1],
				  iArg[2],iArg[3],
				  iArg[4],iArg[5],
				  iArg[6],iArg[7]);    
  }
  return CommandReturn::OK;
}
