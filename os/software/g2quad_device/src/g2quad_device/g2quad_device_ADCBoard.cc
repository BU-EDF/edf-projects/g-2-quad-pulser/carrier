#include <g2quad_device/g2quad_device.hh>

#include <iostream>
#include <BUTool/helpers/parseHelpers.hh>
CommandReturn::status BUTool::G2QUADDevice::ADCBoardCommand(std::vector<std::string> strArg,std::vector<uint64_t> intArg){ 
  if(strArg.size() < 2){
    return CommandReturn::BAD_ARGS;
  }
  if((0 == intArg[0])||(intArg[0] > 4)){
    return CommandReturn::BAD_ARGS;
  }

  //Build a command string
  std::string command = strArg[1];
  for(size_t iArg = 2; iArg < strArg.size();iArg++){
    command += " ";
    command += strArg[iArg];
  }
  command+="\n";

  //Issue command and print the response.
  std::vector<std::string> const response = hw_interface->ADCCommand(intArg[0],command);
  for(size_t iWord = 0; iWord < response.size();iWord++){
    std::cout << response[iWord] << std::endl;
  }
  return CommandReturn::OK;  
}
CommandReturn::status BUTool::G2QUADDevice::ConfigADCs(std::vector<std::string> /*strArg*/,std::vector<uint64_t> intArg){
  if (intArg.size() != 1){
    return CommandReturn::BAD_ARGS;
  }  
  hw_interface->ConfigADCs(intArg[0]);
  return CommandReturn::OK;
}

CommandReturn::status BUTool::G2QUADDevice::AlignADCChannel(std::vector<std::string> /*strArg*/,std::vector<uint64_t> intArg){
  if (intArg.size() != 2){
    return CommandReturn::BAD_ARGS;
  }  
  hw_interface->AlignADCChannel(intArg[0],intArg[1]);
  return CommandReturn::OK;
}

CommandReturn::status BUTool::G2QUADDevice::InitBoardLink(std::vector<std::string> /*srtArg*/,std::vector<uint64_t> intArg){
  if (intArg.size() != 1){
    return CommandReturn::BAD_ARGS;
  }
  hw_interface-> InitBoardLink(intArg[0]);
  return CommandReturn::OK;
}

CommandReturn::status BUTool::G2QUADDevice::ProgramFlash(std::vector<std::string> strArg, std::vector<uint64_t> intArg){
   if (intArg.size() != 2){
     return CommandReturn::BAD_ARGS;
   }
   hw_interface->ProgramFlash(intArg[0],strArg[1].c_str(),true);
   return CommandReturn::OK;
}
CommandReturn::status BUTool::G2QUADDevice::ResetPulser(std::vector<std::string> strArg, std::vector<uint64_t> /*intArg*/){
   std::vector<size_t> boardList; 
   if(0==strArg.size()){
      boardList.push_back(1);
      boardList.push_back(2);
      boardList.push_back(3);
      boardList.push_back(4);
      //      hw_interface->reset_monitered_pulser(boardList);
   } else{
      boardList = parseList(strArg[0]);
      if(0==boardList.size()){
         return CommandReturn::BAD_ARGS;
      }
      //      hw_interface->reset_monitered_pulser(boardList);
   }
  
   return CommandReturn::OK;
}
