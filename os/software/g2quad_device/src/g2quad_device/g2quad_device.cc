/* 
Rev log
===============================
* 2018-05-07 (Dan Gastler)
- Making new version of g2quad device with netreg interface code

* 2018-06-29 (Lina Lin)
- added the code for program flash
*/
#include <BUException/ExceptionBase.hh>
#include <boost/regex.hpp>
#include <g2quad_device/g2quad_device.hh>
#include <stdlib.h>
#include <G2QuadStatus/G2QuadStatus.hh>
#include <readline/readline.h> //for rl_insert_text
using namespace BUTool;

G2QUADDevice::G2QUADDevice(std::vector<std::string> arg)
  : CommandList<G2QUADDevice>("G2QUAD"),
  RegisterHelper(UPPER),
  hw_interface(NULL){

  if(0 == arg.size()){
    std::exception e;
    throw e;
  }
  
  std::string address = arg[0];
  std::string table_name;
  //parse args
  if(arg.size() > 1){
    table_name = arg[1];
  }else{
    if(NULL != getenv("G2QUAD_ADDRESS_TABLE")){
      table_name = getenv("G2QUAD_ADDRESS_TABLE");
    }
  }
  
  hw_interface = new g2quad(table_name,address);
  //setup commands
  LoadCommandList();


}

G2QUADDevice::~G2QUADDevice(){
  if (hw_interface != NULL){
    delete hw_interface;
  }
}

void G2QUADDevice::LoadCommandList(){
  // general commands (Launcher_commands)
  AddCommand("read",&G2QUADDevice::Read,
	     "Read address\n",
	     &G2QUADDevice::RegisterAutoComplete);
  AddCommand("write",&G2QUADDevice::Write,
	     "write data to address\n",
	     &G2QUADDevice::RegisterAutoComplete);
  AddCommand("names",&G2QUADDevice::ListRegs,
	     "list regs\n" \
	     "    optioon D: debug\n"		\
	     "    option V: describe\n"			\
	     "    option H: help\n",			\
	     &G2QUADDevice::RegisterAutoComplete);

  //  AddCommand("readADC",&G2QUADDevice::ADCRead,
//	     "Read ADC board address\n");
//  AddCommand("writeADC",&G2QUADDevice::ADCWrite,
//	     "write data to ADC board address\n");
  AddCommand("configADCs",&G2QUADDevice::ConfigADCs,
	     "configure the ADCs on the specified board\n");
  AddCommand("alignADC",&G2QUADDevice::AlignADCChannel,
	     "align the adc samples for the board & channel specified\n");
  AddCommand("initBoard",&G2QUADDevice::InitBoardLink,
	     "initialize rx link on board\n");
  AddCommand("programFlash", &G2QUADDevice::ProgramFlash, //added 06/29/2018
        "program the flash on board\n");
  AddCommand("reset_pulser", &G2QUADDevice::ResetPulser, //added 02/27/2018
        "reset pulser\n"\
        "Usage:\n"\
        "    reset_pulser \n"\
        "    reset_pulser boardID\n"\
        "    reset_pulser boardID-boardID\n"\
        "    reset_pulser boardID,boardID,...");
  AddCommand("status",&G2QUADDevice::StatusDisplay,
             "Write status display to screen\n"    \
             "  Usage:\n"                          \
             "  status <level> <table>\n",
	     &G2QUADDevice::autoComplete_StatusTables);
  AddCommand("trig_pulse", &G2QUADDevice::TrigPulse,  //added 07/24/2018
         "send one user pulser\n");
  AddCommand("fr_config",&G2QUADDevice::ConfigureFreeRun,
	     "configure the free-run trigger frequency (default 100hz)\n" \
	     "  Usage:\n"                                 \
	     "  frconfig <freq(hz)>\n");
  AddCommand("fr_enable",&G2QUADDevice::EnableFreeRun,
	     "Enable the free-run trigger\n" \
	     "  Usage:\n"\
	     "  fr_enable\n");
  AddCommand("fr_disable",&G2QUADDevice::DisableFreeRun,
	     "Disable the free-run trigger\n"	\
	     "  Usage:\n"\
	     "  fr_disable\n");

  //  AddCommand("captureADC",&G2QUADDevice::CaptureADCChannel,
//	     "capture a simple waveform from board/channel\n");
//  AddCommand("link_test",&G2QUADDevice::LinkTest,
//	     "Test a board link\n");

  AddCommand("pulser",&G2QUADDevice::PulserConfig,
             "Configure the ADCBoard pulser (arguments in ns, but truncated to 10ns)\n"\
             "  Usage:\n"                                                               \
             "  pulser boardID         mode\n"                                         \
             "         charge1_start   charge1_duration(ns)\n"                         \
             "         discharge_start discharge_duration(ns)\n"                                \
             "         <charge2_start> <charge2_length>\n");

  AddCommand("comm_adc",&G2QUADDevice::ADCBoardCommand,
             "Issue a command directly to the ADCBoard uC\n"\
             "  Usage:\n"                                   \
             "  comm_adc boardID command\n");
  AddCommand("ri",&G2QUADDevice::ReadI2C,
	     "I2C read on timing board bus"\
	     "  Usage:\n"                  \
	     "  ri i2cAddr regAddr\n");
  AddCommand("wi",&G2QUADDevice::WriteI2C,
	     "I2C write on timing board bus"\
	     "  Usage:\n"                  \
	     "  wi i2cAddr regAddr data\n");
	     
}

static bool IsNumberHelper(const std::string & str){
  bool levelIsNumber = true;
  for(size_t iDigit = 0; iDigit < str.size();iDigit++){
    //Check if char is 0-9,'-',or '.'.
    levelIsNumber = levelIsNumber && (isdigit(str[iDigit]) || str[iDigit] == '-' || str[iDigit] == '.');
  }
  return levelIsNumber;
}

CommandReturn::status BUTool::G2QUADDevice::StatusDisplay(std::vector<std::string> strArg,std::vector<uint64_t> intArg){                                                     
  //Create status display object
  G2QuadStatus * stat = new G2QuadStatus(hw_interface);
  std::ostream& stream = std::cout; 
  if(intArg.size()==0){ 
    //default to level 1 display
    stat->Report(1,stream); 
  } else if(intArg.size()==1){
    //arg 0 should be an integer
    if (IsNumberHelper(strArg[0])){ 
      stat->Report(intArg[0],stream); 
    } else {
      std::cout << "Error: \"" << strArg[0] << "\" is not a valid print level\n"; 
    } 
  } else if(intArg.size() > 1){ 
    //arg 0 should be an integer
    if (IsNumberHelper(strArg[0])){ 
      for(size_t iTable = 1; iTable < strArg.size();iTable++){
	stat->Report(intArg[0],stream,strArg[iTable]);
      } 
    } else {
      std::cout << "Error: \"" << strArg[0] << "\" is not a valid print level\n"; 
    } 
  } 
  return CommandReturn::OK;                                                                 
}
  
//CommandReturn::status BUTool::G2QUADDevice::LinkTest(std::vector<std::string>,std::vector<uint64_t> intArg){
//  int board;
//  switch (intArg.size()) {
//  case 1:    
//    board = intArg[0];
//    break;
//  default:
//    return CommandReturn::BAD_ARGS;
//  }
//  uint32_t tests_failed = 0;
//  uint32_t tests_passed = 0;
//  //check that this is a valid board link
//  hw_interface->CheckBoardLink(board);  
//  for(uint32_t iTest = 0; iTest < 0xfff;iTest++){
//    uint32_t data_wr = iTest;
//    for(size_t iBitShift = 0; iBitShift < 32; iBitShift++){
//      //write test data
//      hw_interface->ADCWrite(board,0xfff0,data_wr);
//      //read test data
//      uint32_t data_rd = hw_interface->ADCRead(board,0xfff0);
//      if(data_rd == data_wr){
//	tests_passed++;
//      }else{
//	printf("%08X != %08X\n",data_rd,data_wr);
//	tests_failed++;
//      }
//      data_wr <<= 1;      
//    }
//  }
//
//  printf("Tests passed: %u\nTests failes: %u\n",tests_passed,tests_failed);
//  
//  return CommandReturn::OK;
//}

std::string BUTool::G2QUADDevice::autoComplete_StatusTables(std::vector<std::string> const & line,std::string const & currentToken ,int state){
  if(1 == line.size()){
    //Parse the status number
    if(0 == currentToken.size()){
    }
  }else if((2 == line.size()) && (currentToken.size() == line[1].size())){ 
    //We've now parsed an FEMB number, but to break into the next token, we need to type a space.
    //Since the user is expecting the tab completion to do this, we do it with the following command 
    rl_insert_text(" "); 
  }else if(2 <= line.size() ){ 
 
    static std::vector<std::string> tableName; 
    static size_t iTable;
 
    //Reload lists if we are just starting out at state == 0 
    if(state == 0){
      std::string partialNameRegex(currentToken);
      partialNameRegex+="*"; 
      tableName = hw_interface->GetTableNames(partialNameRegex);
      iTable = 0;
    }else{ 
      iTable++;
    }
 
    for(;iTable < tableName.size();iTable++){
      //Do the string compare and make sure the token is found a position 0 (the start)
      if(tableName[iTable].find(currentToken) == 0){ 
	return tableName[iTable];
      }
    }
  }
  return std::string("");
}

