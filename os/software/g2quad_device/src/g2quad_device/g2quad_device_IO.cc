#include <g2quad_device/g2quad_device.hh>
#include <stdint.h>
#include <vector>

std::vector<std::string> BUTool::G2QUADDevice::myMatchRegex(std::string regex){
  std::vector<std::string> test = hw_interface->GetNames(regex);
  return test;
}

uint32_t BUTool::G2QUADDevice::RegReadAddress(uint32_t addr){ 
  return hw_interface->Read(addr);
}

uint32_t BUTool::G2QUADDevice::RegReadRegister(std::string const & reg){
  return hw_interface->Read(reg);
}

void BUTool::G2QUADDevice::RegWriteAction(std::string const & reg){
  hw_interface->Write(reg,0x1);
}

void BUTool::G2QUADDevice::RegWriteAddress(uint32_t addr,uint32_t data){
  hw_interface->Write(addr,data);
}

void BUTool::G2QUADDevice::RegWriteRegister(std::string const & reg, uint32_t data){
  hw_interface->Write(reg,data);
}


uint32_t BUTool::G2QUADDevice::GetRegAddress(std::string const & reg){
  return hw_interface->GetItem(reg)->address;
}
uint32_t BUTool::G2QUADDevice::GetRegMask(std::string const & reg){
  return hw_interface->GetItem(reg)->mask;
}
uint32_t BUTool::G2QUADDevice::GetRegSize(std::string const & reg){
  uint32_t mask = hw_interface->GetItem(reg)->mask;
  size_t lowest_bit,highest_bit;
  lowest_bit = highest_bit = -1;
  //Find the lowest bit
  for(size_t i = 0;i<32;i++){
    if(mask & (0x1<<i)){
      lowest_bit = i;
    }
  }
  //Find the highest bit
  for(int i = 31;i>=int(lowest_bit);i--){
    if(mask & (0x1<<i)){
      highest_bit = i;
    }
  }
  return (highest_bit - lowest_bit)+1;
}

std::string BUTool::G2QUADDevice::GetRegMode(std::string const & reg){
  uint8_t mode = hw_interface->GetItem(reg)->mode;
  
  std::string ret;
  if(mode & 0x4){
    ret += " action";
  }else if(mode & 0x8){
    ret += " incr";
  }
  return ret;
}

std::string BUTool::G2QUADDevice::GetRegPermissions(std::string const & reg){
  uint8_t mode = hw_interface->GetItem(reg)->mode;
  
  std::string ret;
  if(mode & 0x1){
    ret += "r";
  }
  if(mode & 0x2){
    ret += "w";
  }
  return ret;
}

std::string BUTool::G2QUADDevice::GetRegDescription(std::string const & reg){
  boost::unordered_map<std::string,std::string>::const_iterator it;
  it = hw_interface->GetItem(reg)->user.find("description");
  if(it != hw_interface->GetItem(reg)->user.end()){
    return it->second;
  }
  return std::string("");
}

std::string BUTool::G2QUADDevice::GetRegDebug(std::string const & reg){

  boost::unordered_map<std::string,std::string> const & user = hw_interface->GetItem(reg)->user;

  std::string ret;
  for( boost::unordered_map<std::string,std::string>::const_iterator it = user.begin();
       it != user.end();
       it++) {
    ret+="   ";
    ret+=it->first.c_str();
    ret+=" = ";
    ret+=it->second.c_str();
    ret+="\n";
  }    
  return ret;
}


  
