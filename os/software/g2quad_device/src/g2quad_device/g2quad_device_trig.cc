#include <g2quad_device/g2quad_device.hh>

CommandReturn::status BUTool::G2QUADDevice::ConfigureFreeRun(std::vector<std::string>,std::vector<uint64_t> iArg){
  if(iArg.size() == 0){
    hw_interface->ConfigureFreeRun(100); //DEfault to 100Hz
  }else{
    if(iArg[0] <= 1000){
      hw_interface->ConfigureFreeRun(iArg[0]);
    }else{
      return CommandReturn::BAD_ARGS;
    }
  }
  return CommandReturn::OK;
}
CommandReturn::status BUTool::G2QUADDevice::EnableFreeRun(std::vector<std::string>,std::vector<uint64_t>){
  hw_interface->EnableFreeRun();
  return CommandReturn::OK;
}
CommandReturn::status BUTool::G2QUADDevice::DisableFreeRun(std::vector<std::string>,std::vector<uint64_t>){
  hw_interface->DisableFreeRun();
  return CommandReturn::OK;
}

CommandReturn::status BUTool::G2QUADDevice::TrigPulse(std::vector<std::string> ,std::vector<uint64_t>){
  //  hw_interface->TrigPulse();
  return CommandReturn::OK;
}
