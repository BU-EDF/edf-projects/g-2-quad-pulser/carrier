#include <g2quad_device/g2quad_device.hh>
#include <inttypes.h> //PRIX64



CommandReturn::status BUTool::G2QUADDevice::ReadI2C(std::vector<std::string> sArg,std::vector<uint64_t> iArg){
  if(2 == sArg.size()){
    printf("0x%02" PRIX64 ".%02" PRIX64 ": 0x%02X\n",
	   iArg[0],iArg[1],
	   hw_interface->ReadI2C("TIMING_SYSTEM.I2C",iArg[0],iArg[1],1));
  }else{
    return CommandReturn::BAD_ARGS;
  }
  return CommandReturn::OK;
}

CommandReturn::status BUTool::G2QUADDevice::WriteI2C(std::vector<std::string> sArg,std::vector<uint64_t> iArg){
  if(3 == sArg.size()){
    hw_interface->WriteI2C("TIMING_SYSTEM.I2C",iArg[0],iArg[1],iArg[2],1);
    printf("Wrote 0x%02" PRIX64 " to 0x%02" PRIX64 ".%02" PRIX64 "\n",
	   iArg[2],
	   iArg[0],iArg[1]);
  }else{
    return CommandReturn::BAD_ARGS;
  }
  return CommandReturn::OK;

}
