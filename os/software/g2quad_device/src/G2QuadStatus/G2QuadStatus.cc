#include <G2QuadStatus/G2QuadStatus.hh>

void BUTool::G2QuadStatus::Process(std::string const & singleTable){  
  //Build G2Quad tables
  std::vector<std::string> G2QuadNames = hw->GetNames("*");
  //process all the nodes and build table structure
  for(std::vector<std::string>::iterator itName = G2QuadNames.begin();
      itName != G2QuadNames.end();
      itName++){
    //Get the list of parameters for this node
    Item const * item = hw->GetItem(*itName);
    boost::unordered_map<std::string,std::string> parameters = item->user;
    //Look for a Status parameter
    if(parameters.find("Status") != parameters.end()){	
      //Check for an enabled argument
      boost::unordered_map<std::string,std::string>::iterator itEnabled = parameters.find("Enabled");
      if(itEnabled != parameters.end()){
	if((itEnabled->second)[0] == '!'){
	  //We hvae the negative of our normal control
	  itEnabled->second = (itEnabled->second).substr(1); //Strip the '!'
	  itEnabled->second = (hw->Read(itEnabled->second) == 0x0) ? "1" : "0";	  
	}else{
	  //Change the value to 0 or 1 depending on if it should be enabled
	  itEnabled->second = (hw->Read(itEnabled->second) == 0x0) ? "0" : "1";
	}
      }
      
      //Check for an table name
      boost::unordered_map<std::string,std::string>::iterator itTable = parameters.find("Table");
      
      std::string tableName = itTable->second;
      //Add this Address to our Tables if it matches our singleTable option, or we are looking at all tables
      if( singleTable.empty() || TableNameCompare(tableName,singleTable)){
	tables[tableName].Add(*itName,
			      hw->Read(*itName),
			      uint32_t(item->mask >> item->offset),
			      parameters);
      }
    }
  }  
}
