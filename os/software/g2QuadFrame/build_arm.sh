#!/bin/bash

VIVADO_VERSION=2015.4
VIVADO_SDK="/opt/Xilinx/SDK/"$VIVADO_VERSION"/settings64.sh"
CXX="source "$VIVADO_SDK" && arm-xilinx-linux-gnueabi-g++"
CXX=$CXX make
