#include <g2frame/g2QuadFrame.hpp>
//includes for read .txt
#include <string> //std::string
#include <fstream>//std::ifstream

#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* strtoul */

#include <iostream>

//parameters
uint32_t *pointerBrd;
uint32_t sizeBrd;
std::vector<uint32_t> entries;

uint32_t data;
std::string line;
char member[10];

//to be used when data has non-numbered lines
void shortData(){
    for (size_t i = 0; i< line.size();i++){
        member[i] = line[i];
    } 
}

//to be used when data has numbered lines
void longData(){
    for (size_t i = 8; i < line.size(); i++){
        member[i-8] = line[i];
    }
}

//read data from file and enter them into the vector
void readData(){
    std::ifstream datafile("event_dump.dat");
    if(datafile.is_open()){

        while (getline(datafile,line)){
            //comment or uncomment the two lines below as needed.
            shortData(); 
            //longData();
            //converts string to hex
            data = strtoul(member, NULL, 0);
            
            //push into the vector
            entries.push_back(data);
        }

        datafile.close();
    }

    else{
        printf("Unable to open file.\n");
    }
    pointerBrd = &entries.front();
    sizeBrd = entries.size();
}

void print(){
    printf("%p\n",&entries.front());
    for(size_t i = 0; i < entries.size(); i++){
        printf("0x%08X \n", entries[i]);
    }
    printf("%p\n",&entries.back());
}

int main(){
    readData();
    // print();
    g2QuadFrame board;
    while(sizeBrd){
        pointerBrd = board.Parser(pointerBrd,sizeBrd);
        sizeBrd = sizeBrd - board.GetSize();
        //stuff to do if BAD event happens.
        if(0 == board.GetID()){
            printf("BAD EVENT\n");
            continue;
        }
        printf("Board spill number 0x%04X \n",board.GetSpillID());
        printf("Board number %d \n", board.GetID());
        printf("Board version: %d\n", board.Getver());
        printf("Channel count: %d \n", board.GetChannelCount());
        for(uint32_t iChannel = 1; iChannel <= board.GetChannelCount(); iChannel++){
            Channel const *channel= board.GetChannel(iChannel);
            printf("Channel version: %d \n", channel->GetVER());
            printf("Channel integral1: %d \n", channel->GetIntegral1());
            printf("Channel integral2: %d \n", channel->GetIntegral2());
            printf("Channel integral3: %d \n", channel->GetIntegral3());
            printf("Channel %u: block count - %u \n", channel->GetChannelNum(), channel->GetBlockCount());
            if(channel->GetBlockCount() > 0){
                for(uint8_t iSample = 1; iSample <= channel->GetBlockCount(); iSample++){
                    BlockSample const *sample = channel->GetBlockSample(iSample);
                    printf("Sample Count: %u \n",sample->GetSampleCount());
                    for(uint16_t isample = 0; isample < sample->GetSampleCount(); isample++){
                        printf("0x%04X \n", *(sample->GetSamplePointer()+isample));
                    }
                }
            }
        }
    }    
    return 0;
}

