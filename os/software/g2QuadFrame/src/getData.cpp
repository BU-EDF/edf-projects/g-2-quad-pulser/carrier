#include <g2frame/g2QuadFrame.hpp>

#include <stdio.h> /* printf, NULL */
#include <stdlib.h>  /* strtoul */
#include <string>  /* std::string */
#include <fstream> /* std::ifstream, std::ofstream  */
#include <tclap/CmdLine.h> /*TCLAP*/

int GetData(const char* file, uint32_t BoardNum, uint32_t ChannelNum){
   std::string line;
   std::vector<uint32_t> entries;
   char member[10];
   std::ifstream datafile(file);
   if(datafile.is_open()){ 
      while(getline(datafile,line)){
         for(size_t iLine=0; iLine<line.size();iLine++){
            member[iLine] = line[iLine];
         }
         uint32_t data = strtoul(member,NULL, 0);
         entries.push_back(data);
      }
      datafile.close();
   } else{
      printf("Unable to open file \n");
   }
   
   uint32_t fileSize = entries.size();
   uint32_t *filePtr = &entries.front();
   g2QuadFrame board;
  
   FILE *samples;
   samples = fopen("samples.dat","w");
   while(fileSize){
      filePtr = board.Parser(filePtr,fileSize);
      fileSize -= board.GetSize();
      if(0 == board.GetID()){
         continue;
      }
      if(BoardNum == board.GetID()){
         if(ChannelNum >=0 && ChannelNum <= board.GetChannelCount()){
            Channel const *channel = board.GetChannel(ChannelNum);
            if(channel->GetBlockCount() > 0){
               for(uint8_t iSample = 1; iSample <= channel->GetBlockCount(); iSample++){
                  BlockSample const *sample = channel->GetBlockSample(iSample);
                  uint32_t timestamp = sample->GetTimestamp();
                  printf("timestamp: %d\n",timestamp); 
                  for(uint16_t iSample = 0; iSample < sample->GetSampleCount(); iSample++){
                     uint16_t samp = *(sample->GetSamplePointer()+iSample);
                     fprintf(samples,"%d    %d \n",timestamp,samp);
                     printf("0x%04X \n", samp);
                     timestamp++;
                  }
               } 
            }
            break;
         }
      }
   }
   return 1;
}

int main(int argc, char *argv[]){
    uint32_t BoardNum = 0;
    uint32_t ChannelNum = 0;
    try{
        TCLAP::CmdLine cmd("Enter the values of BoardNum and ChannelNUm",' ',"1<BoardNum<4 and 1<ChannelNum<8");
        TCLAP::ValueArg<int> boardNum("b",
            "boardNumber",
            "board number",
            true,
            1,
            "integer",
            cmd);
        TCLAP::ValueArg<int> channelNum("c",
            "channelNumber",    
            "channel number",
            true,
            1,
            "integer",
            cmd);
        //Parse the command line arguments
        cmd.parse(argc,argv);
        BoardNum = boardNum.getValue();
        ChannelNum = channelNum.getValue();
    } catch(TCLAP::ArgException &e){
        fprintf(stderr, "Error %s for arg %s\n",e.error().c_str(),e.argId().c_str());
        return -1;
    }
    GetData("event_dump.dat", BoardNum, ChannelNum);
    return 0;
}
