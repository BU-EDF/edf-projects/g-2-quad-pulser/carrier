#include <g2frame/channel.hpp>
  

Channel::Channel(){ 
    Clear();
}

//Getters for the Channel class /////////////////////
uint32_t Channel::GetChannelSize() const{
    return ChannelSize;
}

uint32_t *Channel::GetChannelPointer() const{
    return ChannelPointer;
}


uint32_t Channel::GetBlockCount() const{
    return BlockCount;
}

uint32_t Channel::GetChannelNum() const{
    return ChannelNum;
}

uint32_t Channel::GetChannelSpillID(){
    return ChannelSpillID;
}

uint32_t Channel::GetCrossingCount() const{
    return CrossingCount;
}

uint32_t Channel::GetErrorFlag(){
    return ErrorFlag;
}

uint32_t Channel::GetVER() const{
    return VER;
}

int32_t Channel::GetIntegral1()const{
    return Integral1;
}

int32_t Channel::GetIntegral2()const{
    return Integral2;
}

int32_t Channel::GetIntegral3()const{
    return Integral3;
}
/////////////////////////////////////////////////////


uint32_t *Channel::ChannelParser(uint32_t *ptr,uint32_t size){
    Clear();
    ErrorFlag = 0; 
    ChannelPointer = ptr;
   
    //check size of channel////////////////////////
    if(size < ChannelSize){
        ErrorFlag |= CHANNEL_SIZE_ERROR;
    }
    
    else if(0 == size){
        BUException::G2QF_CHANNEL_ERROR e;
        e.Append("Channel is empty");
        throw e;
    }
    ///////////////////////////////////////////////
   
    //Check if channel starts with 0x5A///////////
    uint32_t check_channel = (*ptr) & 0x000000FF;
    if(check_channel != 0x5A){
        BUException::G2QF_CHANNEL_ERROR e;
        e.Append("Channel first entry does not contain 0x5A");
        throw e;
    }
    //////////////////////////////////////////////
   
    //Save all the info of the channel///////////
    ChannelSize = (*ptr) >> 8;
    ptr++;
    VER = (*ptr) & 0x000000FF;
    Integral1 = ((int32_t)(*ptr)) >> 8;
    ptr++;
    Integral2 = ((int32_t)(*ptr)) >> 8;
    ptr++;
    Integral3 = ((int32_t)(*ptr)) >> 8;
    ptr++;
    CrossingCount = (*ptr) & 0x00FFFFFF;
    ptr++;
    BlockCount = (*ptr) >> 24;
    ChannelNum = ((*ptr) >> 16) & 0x00FF;
    ChannelSpillID = (*ptr) & 0x0000FFFF;
    /////////////////////////////////////////////
    ptr++;
    size = size - 5;
   
    if(0 == size){
        BUException::G2QF_CHANNEL_ERROR e;
        e.Append("Channel does not have any sample block");
        throw e;
    }
    
    if(ChannelSize > 3){//Channel must have samples in it
        //start parsing the block samples
        for(uint32_t iSample = 0; iSample < BlockCount; iSample++){
            BlockSample sample;
            ptr = sample.SampleParser(ptr,size);
            blockSamples.push_back(sample);         
            size = size - (sample.GetSampleCount()/2);
            //Check if channel has an error
            if(sample.GetErrorFlag() > 0){
                ErrorFlag |= BLOCK_SAMPLE_ERROR;
            }
        }
    }else{//channel cannot have samples. BlockCount and CrossingCount should be zero
        if(BlockCount > 0){
            ErrorFlag |= BLOCK_COUNT_ERROR;
        }
        
        if(CrossingCount > 0){
            ErrorFlag |= CROSSING_COUNT_ERROR;
        }
    } 
    
    return ptr;
}

//Get a specific block sample.
BlockSample const *Channel::GetBlockSample(uint8_t blockSampleNum) const{
    if(blockSampleNum > blockSamples.size() || blockSampleNum == 0){
        BUException::G2QF_CHANNEL_ERROR e;
        e.Append("Sample block requested is out of range");
        throw e;
    }

    return &(blockSamples[blockSampleNum-1]);
}

//initializes all variables to zero and clear the blockSamples vector
void Channel::Clear(){
    ChannelPointer = NULL;
    //channel information
    ChannelSize = 0;
    BlockCount = 0;
    ChannelNum = 0;
    CrossingCount = 0;
    ChannelSpillID = 0;
    blockSamples.clear();
}
