#include <g2frame/sample_parser.hpp>

#include <stdio.h> //printf

BlockSample::BlockSample(){
   SamplePointer = 0;
   SampleCount = 0;
   Timestamp = 0;
}

//Getters for BlockSample/////////////////////
uint32_t BlockSample::GetSampleCount() const{
   return SampleCount;
}


uint32_t BlockSample::GetPreSamples() const{
   return PreSamples;
}

uint32_t BlockSample::GetTimestamp() const{
   return Timestamp;
}

uint16_t *BlockSample::GetSamplePointer() const{
   return SamplePointer;
}

uint32_t BlockSample::GetErrorFlag(){
   return ErrorFlag;
}
//////////////////////////////////////////////


uint32_t *BlockSample::SampleParser(uint32_t *ptr,uint32_t size){
   ErrorFlag = 0;
   PreSamples = (*ptr) >> 24;
   SampleCount = (*ptr) & 0x0000FFFF;
   ptr++;

   //Check the size of the block sample///
   if(size < (SampleCount/2)){ 
      ErrorFlag |= BLOCK_SAMPLE_SIZE_ERROR;
   }

   else if(size == 0){
      BUException::G2QF_BLOCK_SAMPLE_ERROR e;
      e.Append("Sample Block does not have any entries");
      throw e;
   }
   ///////////////////////////////////////
   
   //Check block samples information//////
   Timestamp = (*ptr) & 0x00FFFFFF;
   if(Timestamp == 0){
      ErrorFlag |= TIMESTAMP_ERROR;
   }

   if(SampleCount == 0){
      ErrorFlag |= SAMPLE_COUNT_ERROR;
   }
   ///////////////////////////////////////
   
   ptr ++;
   size --;
   SamplePointer = (uint16_t*)ptr;
   //Check if SampleCount is even or not for incrementing purposes///
   
   uint32_t increment =0;

   if(SampleCount%2 == 0){
      increment = SampleCount / 2;
   }
   else{
      increment = (SampleCount/2)+1;
   }
   
   //////////////////////////////////////////////////////////////////
   
   //check if number of samples are less than expected////////
   if(size < increment){
      BUException::G2QF_BLOCK_SAMPLE_ERROR e;
      e.Append("Sample block is missing samples");
      throw e;
   }
   
   ptr  += increment;
   return ptr;
}


