#include <g2frame/g2QuadFrame.hpp>

g2QuadFrame::g2QuadFrame(){
    Clear();
}

uint32_t g2QuadFrame::GetSize(){
    return BoardSize;
}

uint32_t *g2QuadFrame::GetPointer(){
    return BoardPointer;
}

uint32_t g2QuadFrame::GetID(){
    return BoardID;
}

uint32_t g2QuadFrame::GetSpillID(){
    return SpillID;
}

uint32_t g2QuadFrame::GetChannelCount(){
    return ChannelCount;
}

uint32_t g2QuadFrame::GetErrorFlag(){
    return ErrorFlag;
}

uint32_t g2QuadFrame::Getver(){
    return ver;
}

uint32_t *g2QuadFrame::Parser(uint32_t *ptr, uint32_t size){
   ErrorFlag = 0;
   Clear();
   //Check that event starts with 0xA5
   if(((*ptr)& 0x000000FF) != 0xA5){
        ptr++;
        BoardSize = 0;
        BoardID = 0;
        SpillID = 0;
        ChannelCount = 0;
        ErrorFlag |= BAD_EVENT;
        return ptr;
    }
   
    //update the event information
    BoardPointer = ptr;
    BoardSize= ((*ptr)>>8) & 0x00FFFF;
    ptr++;
   
    //update the event information
    BoardPointer = ptr;
    ver= ((*ptr)>>8) & 0x000000FF;
    
    //get BoardID and spill ID
    BoardID = (*ptr) >> 24;
    ptr++;
    SpillID = (*ptr) >> 8 & 0x00FFFF;
    ChannelCount = (*ptr) & 0x000000FF;
    ptr ++;
   
    //check the size of the event////////
    if(BoardSize > size){
        ErrorFlag |= g2QUAD_FRAME_SIZE_ERROR;
    }

    else if( size == 0){
        BUException::G2QF_ERROR e;
        e.Append("G2QuadFrame is empty");
        throw e;
    }
    /////////////////////////////////////

    for(uint32_t iBoard = 0; iBoard < ChannelCount; iBoard++){
        Channel channel;
        ptr = channel.ChannelParser(ptr,size);
        //check Spill ID. should be same for both channels and event
        if(SpillID != channel.GetChannelSpillID()){
            ErrorFlag |= SPILL_ID_ERROR;
        }
      
        //check is any channel number is missing//////////
        if(iBoard != channel.GetChannelNum() - 1){
            ErrorFlag |= CHANNEL_NUM_MISSING;
            return ptr;
        } else{
            Channels.push_back(channel);
        }
     
        //check if any channels has errors/////////
        if(channel.GetErrorFlag() >0){
            ErrorFlag |= CHANNEL_ERROR;
        }
        size = size - channel.GetChannelSize();
    }
    valid = true;
    return ptr;
}

//Get a specific channel from the event
Channel const *g2QuadFrame::GetChannel(uint8_t channelNum){
   if(size_t(channelNum) > Channels.size() || channelNum == 0){
      BUException::G2QF_ERROR e;
      e.Append("Requested channel number is out of range");
      throw e;
   }
   return &Channels[channelNum - 1];
}

//Clears all vectors and initialize the variables back to zero
void g2QuadFrame::Clear(){
   BoardPointer = NULL;
   BoardID = 0;
   BoardSize = 0;
   SpillID = 0;
   ChannelCount = 0;
   valid = false;
   Channels.clear(); //clear Channel
   channels.Clear(); //clear vector
}
