#ifndef CHANNEL_HPP
#define CHANNEL_HPP

#include "sample_parser.hpp"
#include <stdint.h> //uint32_t
#include <vector> //std::vector<uint32_t>

#define BLOCK_SAMPLE_ERROR 0x00000001
#define CHANNEL_SIZE_ERROR 0x00000010
#define BLOCK_COUNT_ERROR 0x00000100
#define CROSSING_COUNT_ERROR 0x00001000

class Channel{
    public:
        //constructor
        Channel();
        
        //Get channel information
        uint32_t GetChannelSize() const;
        uint32_t *GetChannelPointer() const;
      
        uint32_t GetBlockCount() const;
        uint32_t GetChannelNum() const;
        uint32_t GetChannelSpillID();
        uint32_t GetCrossingCount() const;
        uint32_t GetPreSamples() const;
        int32_t GetIntegral1() const;
        int32_t GetIntegral2() const;
        int32_t GetIntegral3() const;
        uint32_t GetVER() const;
        
        uint32_t GetErrorFlag();
        //Channel Parser
        uint32_t *ChannelParser(uint32_t *,uint32_t);
        uint16_t *GetSample(int);
        BlockSample const *GetBlockSample(uint8_t) const;  
        void Clear();
        
    private:
        uint32_t ChannelSize;
        uint32_t *ChannelPointer;
         
        //Channel Informtaion
        uint32_t BlockCount;
        uint32_t ChannelNum;
        uint32_t ChannelSpillID;
        uint32_t SampleCount;
        uint32_t CrossingCount;
        uint32_t PreSamples;
        int32_t Integral1;
        int32_t Integral2;
        int32_t Integral3;
        uint32_t VER;
        
        std::vector<BlockSample> blockSamples;
        uint32_t ErrorFlag;      
};

#endif



