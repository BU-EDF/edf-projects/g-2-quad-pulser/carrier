#ifndef PARSER_EXCEPTIONS_HPP
#define PARSER_EXCEPTIONS_HPP

#include <BUException/ExceptionBase.hh>

namespace BUException{
   ExceptionClassGenerator(G2QF_ERROR, "g2QuadFrame error.")
   ExceptionClassGenerator(G2QF_CHANNEL_ERROR, "g2QuadFrame channel error.")
   ExceptionClassGenerator(G2QF_BLOCK_SAMPLE_ERROR, "g2QuadFrame sample block error.")
}

#endif

