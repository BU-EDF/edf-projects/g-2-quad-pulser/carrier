#ifndef SAMPLE_PARSER_HPP
#define SAMPLEPARSER_HPP

#include <stdint.h> //uint32_t
#include <vector> //std::vector uint16_t
#include "parser_exceptions.hpp"

#define BLOCK_SAMPLE_SIZE_ERROR 0x00000001
#define TIMESTAMP_ERROR 0x00000010
#define SAMPLE_COUNT_ERROR 0x00000100

class BlockSample{
    public:
      //constructor
        BlockSample();
     
        uint32_t *SampleParser(uint32_t*i,uint32_t);
        uint32_t GetSampleCount() const;
        uint32_t GetPreSamples() const;
        uint32_t GetTimestamp() const;
        uint16_t *GetSamplePointer() const;
        uint32_t GetErrorFlag();

    private:
      //Block Sample Information
        uint32_t SampleCount;
        uint32_t PreSamples;
        uint32_t Timestamp;
        uint16_t *SamplePointer;
        uint32_t ErrorFlag;
};


#endif
