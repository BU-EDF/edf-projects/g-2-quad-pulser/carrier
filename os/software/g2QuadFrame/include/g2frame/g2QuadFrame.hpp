#ifndef g2QUADFRAME_HPP
#define g2QUADFRAME_HPP

#include "channel.hpp"

#include <stdint.h> //uint32_t
#include <vector> //std::vector<uint32_t>


#define CHANNEL_ERROR 0x00000001
#define g2QUAD_FRAME_SIZE_ERROR 0x00000010
#define SPILL_ID_ERROR 0x00000100
#define CHANNEL_NUM_MISSING 0x00001000
#define BAD_EVENT 0x00010000
#define BOARD_VERSION_ERROR  0x00100000

class g2QuadFrame{

    public:
        //Constructor
        g2QuadFrame();
        
        //Get Board information
        uint32_t GetSize();
        uint32_t *GetPointer();
        
        uint32_t GetID();
        uint32_t GetSpillID();
        uint32_t GetChannelCount();
        uint32_t GetErrorFlag();
        uint32_t Getreserved();
        uint32_t Getver();    
        
        //Board parser
        uint32_t *Parser(uint32_t*, uint32_t);
        Channel const *GetChannel(uint8_t);
        
    private:
        uint32_t BoardSize;
        uint32_t *BoardPointer;
      
        Channel channels;
        //Board Information
        uint32_t BoardID;
        uint32_t SpillID;
        uint32_t ChannelCount;
        uint32_t reserved;
        uint32_t ver;
      
        std::vector<Channel> Channels;
        void Clear();
        uint32_t ErrorFlag;
        bool valid;
};

#endif

