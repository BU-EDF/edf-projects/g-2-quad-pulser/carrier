#include <stdio.h>
#include <netreg/server.hh>
#include <PLInterface.hh>
#include <arpa/inet.h>
#include <tclap/CmdLine.h>
#include <pl_io.hh>


int main (int argc, char * *argv) {
  PLInterface PL;
  netreg::Server<PLInterface, uint16_t, uint32_t> serv(&PL,
						       hw_read, //From PL_io
						       hw_write); //From PL_io
  std::string unixSocket;
  std::string tcpSocketIP;
  uint16_t tcpSocketPort;
  
  try {
    TCLAP::CmdLine cmd("Zynq PL interface server.",
		       ' ',
		       "PLServer");

    //Unix socket
    TCLAP::ValueArg<std::string> unixFile("u",               //one char flag
					  "unix",            // full flag name
					  "unix socket path",//description
					  false,             //required
					  std::string(""),   //Default is empty
					  "string",          // type
					  cmd);
    
    //TCP socket
    TCLAP::ValueArg<std::string>    tcpIP("t",              //one char flag
					  "tcp",            // full flag name
					  "tcp server ip",  //description
					  false,            //required
					  std::string("0.0.0.0"),  //Default is empty
					  "string",         // type
					  cmd);

    //TCP socket
    TCLAP::ValueArg<int16_t>      tcpPort("p",               //one char flag
					  "port",            // full flag name
					  "tcp server port", //description
					  false,             //required
					  uint16_t(5555),    //Default is empty
					  "uint16_t",        // type
					  cmd);

    //Parse the command line arguments
    cmd.parse(argc,argv);

    unixSocket  = unixFile.getValue();
    tcpSocketIP = tcpIP.getValue();
    tcpSocketPort = tcpPort.getValue();
  } catch (TCLAP::ArgException &e) {
    fprintf(stderr, "Error %s for arg %s\n",
	    e.error().c_str(), e.argId().c_str());
    return 1;
  }

  
  bool serverReady = false;
  if(unixSocket.size() > size_t(0)){
    serv.Listen_unix("/tmp/netreg_test");
    serverReady = true;
  }
  if(tcpSocketIP.size() > size_t(0)){
    struct in_addr sin_addr;
    if(1 == inet_pton(AF_INET,tcpSocketIP.c_str(),&sin_addr)){
      if(!serv.Listen_tcp_inet(sin_addr, (in_port_t)htons(tcpSocketPort))){
	serverReady = true;
      }
    }
  }
  if(serverReady){
    serv.Serve();
  }else{
    printf("Error: No server info given\n.");
  }
  return 0;
}

