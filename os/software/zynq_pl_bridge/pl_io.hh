#ifndef __PL_IO_H__
#define __PL_IO_H__

#include <PLInterface.hh>

#define   ADC_SUBDEVICE_OFFSET 0x1000
#define   ADC_SUBDEVICE_IO_OFFSET 0x100
#define   ADC_SUBDEVICE_READ   0x10
#define   ADC_SUBDEVICE_WRITE  0x20
#define   ADC_COUNT 4

int       hw_read   (PLInterface * PL,
		     uint16_t addr,
		     uint32_t *data);

int       hw_write  (PLInterface *PL,
		     uint16_t addr,
		     uint32_t data);

uint32_t  ADCRead   (PLInterface * PL,
		     int board_id,
		     uint16_t address);

void      ADCWrite  (PLInterface * PL,
		     int board_id,
		     uint16_t address,
		     uint32_t data);

std::vector<std::string> ADCCommand(PLInterface * PL,
				    int board_id,
				    std::string const & cmd);


#endif
