/* 
Rev log
===============================
* 2017-12-22 (Dan Gastler)
Removing g2quad specific things from this class

*/

#include <PLInterface.hh>
#include <exception>

PLInterface::PLInterface():fd(-1),reg_data_out(NULL),reg_data_in(NULL),reg_addr(NULL),reg_rw(NULL),reg_done(NULL){
  //Open /dev/mem for mem-mapping
  fd = open("/dev/mem", O_RDWR);
  if (-1 == fd) {
    printf("Error opening /dev/mem\n");
    std::exception e;
    throw e;
  }
  
  //setup memory mappings
  
  //Data in and out
  reg_data_out = (GPIO *) mmap(NULL,2*sizeof(GPIO),PROT_READ|PROT_WRITE,MAP_SHARED,fd, REG_DATA_HW_ADDR + 0x0);
  reg_data_in  = reg_data_out + 1;  //Since data in is the second io in the AXIGPIO primitive, we use the same mmap

  //Address
  reg_addr     = (GPIO *) mmap(NULL,sizeof(GPIO),PROT_READ|PROT_WRITE,MAP_SHARED,fd, REG_ADDR_HW_ADDR + 0x0);

  //Control
  reg_rw       = (GPIO *) mmap(NULL,2*sizeof(GPIO),PROT_READ|PROT_WRITE,MAP_SHARED,fd, REG_CTRL_HW_ADDR + 0x0);
  reg_done     = reg_rw + 1; //Second GPIO in the AXIGPIO primitive, so we use the same mmap


  //Set the direction of all the ios
  reg_data_out->dir = 0xffffffff; //32bits of output
  reg_data_in->dir  = 0x00000000; //32bits of input
  reg_addr->dir     = 0x0000ffff; //16bits of output
  reg_rw->dir       = 0x00000003; //2 bits of output
  reg_done->dir     = 0x00000000; // 1 bit of input
  
}

PLInterface::~PLInterface(){
  if (reg_data_out) {
    munmap((void*)reg_data_out,2*sizeof(GPIO));
  }
  if (reg_addr) {
    munmap((void*)reg_addr,sizeof(GPIO));
  }
  if (reg_rw) {
    munmap((void*)reg_rw,sizeof(GPIO));
  }

  if(fd != -1){
    close(fd);
  }
  
}

uint32_t PLInterface::Read(uint16_t address){
  //set the address
  reg_addr->data = address;
  //send a read stobe
  reg_rw->data = REG_READ;
  reg_rw->data = 0x0;
  //Wait for op done
  while(0x0 == reg_done->data){}
  //read and return the value
  return reg_data_in->data;  
}

void PLInterface::Write(uint16_t address,uint32_t data){
  //set the address
  reg_addr->data = address;
  //set the output data
  reg_data_out->data = data;
  //send a write stobe
  reg_rw->data = REG_WRITE;
  reg_rw->data = 0x0;
  //Wait for op done
  while(0x0 == reg_done->data){} //This should be trivial for now
  return;
}

