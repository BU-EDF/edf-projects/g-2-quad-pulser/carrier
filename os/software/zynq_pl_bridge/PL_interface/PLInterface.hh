#ifndef __PLINTERFACE_HH__
#define __PLINTERFACE_HH__
/* 
Rev log
===============================
* 2017-12-22 (Dan Gastler)
Refactoring this class into two classes ala AMC13Simple/AMC13.  PLInterface will deal with the hdl to axi zynq interface and be ~generic to zynq projects

*/

#include <stdio.h>
#include <sys/mman.h>
#include <stdint.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <vector>
#include <string>

//#include <PLInterface_Exception.hh>

#define REG_DATA_HW_ADDR 0x41200000
#define REG_ADDR_HW_ADDR 0x41210000
#define REG_CTRL_HW_ADDR 0x41220000

#define REG_READ  0x1
#define REG_WRITE 0x2


//Struct for Xilinx GPIOs
struct GPIO {
  uint32_t data;
  uint32_t dir;
};

class PLInterface {
public:
  PLInterface();
  ~PLInterface();
  uint32_t Read      (uint16_t address);
  void     Write     (uint16_t address, uint32_t data);
  
private:
  int fd;
  GPIO volatile * reg_data_out; 
  GPIO volatile * reg_data_in; 
  GPIO volatile * reg_addr; 
  GPIO volatile * reg_rw; 
  GPIO volatile * reg_done;

};
#endif
