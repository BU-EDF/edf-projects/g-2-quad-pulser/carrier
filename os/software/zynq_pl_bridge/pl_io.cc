#include <pl_io.hh>

int FindBoardNumber(uint16_t addr){
  if((addr >= 1*ADC_SUBDEVICE_OFFSET) && (addr < 2*ADC_SUBDEVICE_OFFSET)){
    return 1;
  }else if((addr >= 2*ADC_SUBDEVICE_OFFSET) && (addr < 3*ADC_SUBDEVICE_OFFSET)){
    return 2;
  }else if((addr >= 3*ADC_SUBDEVICE_OFFSET) && (addr < 4*ADC_SUBDEVICE_OFFSET)){
    return 3;
  }else if((addr >= 4*ADC_SUBDEVICE_OFFSET) && (addr < 5*ADC_SUBDEVICE_OFFSET)){
    return 4;
  }
  return 0;
}

int hw_read(PLInterface * PL, uint16_t addr, uint32_t *data) {
  int boardNumber = FindBoardNumber(addr);
  if(boardNumber){
    addr = addr - boardNumber*ADC_SUBDEVICE_OFFSET;
    *data = ADCRead(PL,boardNumber,addr);
  }else{
    *data = PL->Read(addr);
  }
  return 0;
}

int hw_write(PLInterface *PL, uint16_t addr, uint32_t data) {
  int boardNumber = FindBoardNumber(addr);
  if(boardNumber){
    addr = addr - boardNumber*ADC_SUBDEVICE_OFFSET;
    ADCWrite(PL,boardNumber,addr,data);
  }else{
    PL->Write(addr,data);
  }
  return 0;
}

uint32_t  ADCRead  (PLInterface * PL,int board_id, uint16_t address){
  char command[] = "dr 0000\n";
  snprintf(command+3, //staring position of the address
	   6, //size of the address (4 ascii chars) plus '\n' and the null terminator
	   "%04X\n",address);
  std::vector<std::string> response = ADCCommand(PL,board_id,command);
  
  uint32_t ret = 0;
  if(response.size() > 0){
    std::string reg_value = response[0].substr(7);
    ret = strtoul(reg_value.c_str(),NULL,16);
  }
  return ret;			   
}

void  ADCWrite  (PLInterface * PL,int board_id, uint16_t address, uint32_t data){
  char command[] = "dw 0000 12345678\n";
  snprintf(command+3, //staring position of the address
	   15, //size of the address (4 ascii chars) plus space, plus data (8chars) plus '\n' and the null terminator
	   "%04X %08X\n",address,data);  
  ADCCommand(PL,board_id,command);
}

//Must end in a "\n"
std::vector<std::string> ADCCommand(PLInterface * PL,int board_id, std::string const & cmd){
  
  uint16_t write_address = ADC_SUBDEVICE_IO_OFFSET*board_id + ADC_SUBDEVICE_WRITE;
  uint16_t read_address  = ADC_SUBDEVICE_IO_OFFSET*board_id + ADC_SUBDEVICE_READ;

  
  //Clear text buffer
  uint32_t rd_val;
  do{
    hw_read(PL,read_address,&rd_val);
    //This will read until there are no more valid characters
  }while(rd_val&0x100 );


  
  //Write command to ADCBoard
  for(size_t iChar = 0; iChar < cmd.size();){
    //check if there is buffer space
    hw_read(PL,write_address,&rd_val);
    if(rd_val & 0x100){
      uint8_t val = cmd[iChar]&0x7F;
      hw_write(PL,write_address,val);
      //readout until we get the character back
      do{
	usleep(1000);
	hw_read(PL,read_address,&rd_val);
	//we could check the returned value, but we'll skip that for now
      }while((rd_val&0x100) == 0x000);
      iChar++;
    }
  }

  //Get response
  std::vector<std::string> ret;
  uint32_t val;
  bool processing = true;
  while(processing){
    hw_read(PL,read_address,&val);
    if(val&0x100){
      //Valid data
      switch ( char(val) ) {
      case '\r':
	//ignore
	break;
      case '\n':
	//create new line in ret
	ret.push_back(std::string(""));
	break;
      case '>':
	//we are done processing data, break out of the loop
	processing = false;
	break;
      default:
	//Add character	
	ret.back().push_back((char)(val));
	break;
      }
    }
  }
  return ret;
}
