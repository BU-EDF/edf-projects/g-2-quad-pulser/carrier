#ifndef INC_READWRITE_ERR_H_
#define INC_READWRITE_ERR_H_

#include <errno.h>

static inline int write_e (int fildes, const void *buf, size_t nbyte) {
  ssize_t rwsize = write(fildes, buf, nbyte);
  if (rwsize==0) return -100;
  if (rwsize==-1) return errno;
  if (rwsize!=(ssize_t)nbyte) return -50;
  return 0;
}

static inline int read_e (int fildes, void *buf, size_t nbyte) {
  ssize_t rwsize = read(fildes, buf, nbyte);
  if (rwsize==0) return -100;
  if (rwsize==-1) return errno;
  if (rwsize!=(ssize_t)nbyte) return -50;
  return 0;
}

#endif
