#ifndef INC_NETREG_SERVER_HH_
#define INC_NETREG_SERVER_HH_

#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stddef.h>

#include <vector>

#include "code.hh"

namespace netreg {

typedef enum {
  Endian_Little = 0,
  Endian_Big = 1,
} endian_t;

#if BYTE_ORDER == LITTLE_ENDIAN
const endian_t ENDIAN_HOST = Endian_Little;
#endif
#if BYTE_ORDER == BIG_ENDIAN
const endian_t ENDIAN_HOST = Endian_Big;
#endif

typedef enum {
  Operation_Read = 0,
  Operation_Write = 1,
} operation_t;

template <typename HW_T, typename ADDR_T, typename DATA_T>
class Server {

  typedef struct {
      ADDR_T blocklen;
      ADDR_T stride;
      ADDR_T count;
      uint32_t streamlen;
      ADDR_T addr_start;
  } request_param_t;

  struct client_state {
    void *recv;
    size_t recv_cap;
    size_t recv_len;
    size_t recv_expected;
    uint8_t state;
    uint8_t flags;
    request_param_t req_pa;
    uint8_t status_byte;
    bool status_send;
    void *resp;
    size_t resp_len;
    size_t resp_written;
  };

  struct client_state * *clstates;
  fd_set fdread;
  fd_set fdwrite;
  fd_set fdsockets;
  int fdend;
  int fdlast;
  uint8_t * rcvbuf;
  size_t rcvbuf_size;

public:
  HW_T *hw;
  int (*reg_read)(HW_T *hw, ADDR_T addr, DATA_T *data);
  int (*reg_write)(HW_T *hw, ADDR_T addr, DATA_T data);

  Server (
    HW_T *a_hw,
    int (*a_reg_read)(HW_T *hw, ADDR_T addr, DATA_T *data),
    int (*a_reg_write)(HW_T *hw, ADDR_T addr, DATA_T data)
  ) {
    hw = a_hw;
    reg_read = a_reg_read;
    reg_write = a_reg_write;
    FD_ZERO(&fdsockets);
    FD_ZERO(&fdread);
    FD_ZERO(&fdwrite);
    fdend = 0;
    fdlast = 0;
    rcvbuf = NULL;
    rcvbuf_size = 0;
    clstates = NULL;
  }

private:

#define IN_NETREG_SERVER_HH_

#include "server_client.hh"
#include "server_connection.hh"

#undef IN_NETREG_SERVER_HH_

};

} // namespace netreg

#endif // INC_NETREG_SERVER_HH_

