#ifndef IN_NETREG_CLIENT_HH_
#error
#endif

private:

  int negotiate ( ) {
    int err;
    {
      uint8_t req[sizeof(MAGIC)+1+1+1];
      uint8_t *reqwr = req;
      memcpy(reqwr, MAGIC, sizeof(MAGIC)); reqwr += sizeof(MAGIC);
      uint8_t format = 0;
      format |= (ENDIAN_SERVER)&1 << 0;
      *reqwr = format; reqwr += 1;
      uint8_t addr_size = sizeof(ADDR_T);
      uint8_t data_size = sizeof(DATA_T);
      *reqwr = addr_size; reqwr += 1;
      *reqwr = data_size; reqwr += 1;
      if ((err = write_e(fd, req, reqwr-req))){
	goto error;
      }
      uint8_t respb[4];
      if ((err = read_e(fd, respb, 4))){
	goto error;
      }
      uint32_t resp;
      buf_read_uintNN<uint32_t>(respb, &resp, 1);
      if (resp!=MAGIC_NUM) {
        fprintf(stderr, "Server magic number does not match\n");
      }
      return 0;
    }
    error:
    close(fd);
    return err;
  }

public:
  int Connect_unix (
    const char *path
  ) {
    fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd<0) {
      fprintf(stderr, "Failed to create socket\n");
      return errno;
    }
    size_t path_len = strlen(path)+1;
    struct sockaddr_un * addr;
    addr = (struct sockaddr_un *) calloc(1, sizeof(*addr)+path_len);
    addr->sun_family = AF_UNIX;
    strncpy(&addr->sun_path[0], path, path_len);
    if (connect(fd, (const sockaddr *)addr, sizeof(*addr))) {
      fprintf(stderr, "Failed to connect, err %d %s\n",
        errno, strerror(errno));
      free(addr);
      return errno;
    }
    free(addr);
    int err;
    if ((err = negotiate())) {
      fprintf(stderr, "Error %d %s when verifying server\n",
        err, strerror(err));
      close(fd);
      fd = -1;
      return err;
    }
    return 0;
  }

  int Connect_tcp_inet (
    in_addr address, in_port_t port
  ) {
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd<0) {
      fprintf(stderr, "Failed to create socket\n");
      return errno;
    }
    struct sockaddr_in addr = {0};
    addr.sin_family = AF_INET;
    addr.sin_port = port;
    addr.sin_addr = address;
    if (connect(fd, (const sockaddr *)&addr, sizeof(addr))) {
      fprintf(stderr, "Failed to connect, err %d %s\n",
        errno, strerror(errno));
      return errno;
    }
    int tcp_nodelay = 1;
    setsockopt(fd, SOL_TCP, TCP_NODELAY, &tcp_nodelay, sizeof(tcp_nodelay));
    int err;
    if ((err = negotiate())) {
      fprintf(stderr, "Error %d %s when verifying server\n",
        err, strerror(err));
      close(fd);
      fd = -1;
      return err;
    }
    return 0;
  } 

