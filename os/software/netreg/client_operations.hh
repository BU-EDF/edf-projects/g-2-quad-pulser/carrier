#ifndef IN_NETREG_CLIENT_HH_ 
#error
#endif

public:

  static request_arg_t Op_Read_masked (
				       //    DATA_T *ret, ADDR_T addr, DATA_T mask
 DATA_T *ret, ADDR_T addr, DATA_T & mask
  ) {
    return ((request_arg_t){
      .read = true, .write = false,
      .ret_buf = ret, .ret_buf_size = sizeof(*ret),
      .addr_start = addr,
      .blocklen = 1,
      .stride = 0,
      .count = 1,
      .streamlen = 1,
      .mask = &mask, .mask_size = sizeof(mask),
      .data = 0x0, .data_size = 0,
    });
  }

  static request_arg_t Op_Read_block (
    DATA_T *ret_buf, size_t ret_size,
    ADDR_T addr_start, ADDR_T length
  ) {
    return ((request_arg_t){
      .read = true, .write = false,
      .ret_buf = ret_buf, .ret_buf_size = ret_size,
      .addr_start = addr_start,
      .blocklen = length,
      .stride = 0,
      .count = 1,
      .streamlen = 1,
      .mask = NULL, .mask_size = 0,
      .data = NULL, .data_size = 0,
    });
  }

  static request_arg_t Op_Read_stream (
    DATA_T *ret_buf, size_t ret_size,
    ADDR_T addr, uint32_t length
  ) {
    return ((request_arg_t){
      .read = true, .write = false,
      .ret_buf = ret_buf, .ret_buf_size = ret_size,
      .addr_start = addr,
      .blocklen = 1,
      .stride = 0,
      .count = 1,
      .streamlen = length,
      .mask = NULL, .mask_size = 0,
      .data = NULL, .data_size = 0,
    });
  }

  static request_arg_t Op_Write_masked (
    const DATA_T *data, ADDR_T addr, DATA_T &mask
  ) {
    return ((request_arg_t){
      .read = false, .write = true,
      .ret_buf = NULL, .ret_buf_size = 0,
      .addr_start = addr,
      .blocklen = 1,
      .stride = 0,
      .count = 1,
      .streamlen = 1,
      .mask = &mask, .mask_size = sizeof(mask),
      .data = data, .data_size = sizeof(*data),
    });
  }

  static request_arg_t Op_Write_block (
    DATA_T *data_buf, size_t data_size,
    ADDR_T addr_start, ADDR_T length
  ) {
    return ((request_arg_t){
      .read = false, .write = true,
      .ret_buf = NULL, .ret_buf_size = 0,
      .addr_start = addr_start,
      .blocklen = length,
      .stride = 0,
      .count = 1,
      .streamlen = 1,
      .mask = NULL, .mask_size = 0,
      .data = data_buf, .data_size = data_size,
    });
  }

  static request_arg_t Op_Write_stream (
    DATA_T *data_buf, size_t data_size,
    ADDR_T addr, uint32_t length
  ) {
    return ((request_arg_t){
      .read = false, .write = true,
      .ret_buf = NULL, .ret_buf_size = 0,
      .addr_start = addr,
      .blocklen = 1,
      .stride = 0,
      .count = 1,
      .streamlen = length,
      .mask = NULL, .mask_size = 0,
      .data = data_buf, .data_size = data_size,
    });
  }

