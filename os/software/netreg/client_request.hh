#ifndef IN_NETREG_CLIENT_HH_ 
#error
#endif

private:

/*
The Protocol supports only one kind of operation.  This Operation may be a
  read, a write, or both write and read, and has the following features:

  Masking    - a bit mask selects which bits in each register are modified or
                 read.
  Block      - Operation applies to multiple contiguous registers over a range
                 of addresses.
  Grouping*1 - Operation is performed on multiple blocks of addresses,
                 separated in address space by a specified stride.
  Streaming  - Operation is repeated multiple times to same addresses.

Since these features are often unused, the Protocol only transmits masks,
  block lengths, group count and stride, and streaming length when these differ
  from the default values, which are those for which the presence of the
  corresponding feature has no effect.

A bit field at the start of each request identifies whether the request calls
  for reading or for writing and identifies which feature parameters must be
  sent.  These parameters determine the message length of the complete
  request and response.

When masking is used, the server expects (block length) * (word size) bytes of
  mask data to follow the request parameters.
The data length for an operation is
  (block length) * (group count) * (stream length) * (word size) bytes.
When writing is requested, the server expects the data length of data to follow
  the mask data or request parameters.
After receiving the complete request, the server responds with one byte
  identifying the success or failure status of the requested operation.
If the request was successful, the server responds with the data length of data
  after the status byte.

*1 unsupported and disabled in the current implementation of client and server

*/

// A single function request(reguest_arg_t) provides the interface for
//   performing operations.  This interface is wrapped by helper functions
//   using various combinations of features, defined in "client_operations.hh".
  typedef op_arg_t<ADDR_T, DATA_T> request_arg_t;
  int request_write (request_arg_t p) {
    int err = 0;
    if ( !(p.read || p.write) ) return -1; // argument error
    if ( p.blocklen*p.count*p.streamlen==0 ) {
      return EINVAL; // argument error
    }
    if ( p.mask && p.mask_size < p.blocklen*sizeof(DATA_T) ) {
      return EINVAL; // argument error
    }
    if ( p.read &&
         p.ret_buf_size < p.blocklen*p.count*p.streamlen*sizeof(DATA_T) ) {
      return EINVAL; // argument error
    }
    if ( p.write &&
         p.data_size < p.blocklen*p.count*p.streamlen*sizeof(DATA_T) ) {
      return EINVAL; // argument error
    }

    // Flags byte, addr, blocklen, stride, count, streamlen
    size_t req_buf_size = 1+4*sizeof(ADDR_T)+sizeof(uint32_t);
    // Mask
    if (p.mask) req_buf_size += p.blocklen*sizeof(DATA_T);
    // Data
    req_buf_size += p.blocklen*p.count*p.streamlen*sizeof(DATA_T);
    uint8_t * req = (uint8_t *)calloc(1, req_buf_size);
    uint8_t * reqwr = req;
    uint8_t flags = 0;

    if (p.read)           flags |= OP_FLAG_READ;
    if (p.write)          flags |= OP_FLAG_WRITE;
    if (p.mask)           flags |= OP_FLAG_MASKED;
    if (p.blocklen > 1)   flags |= OP_FLAG_SIZED;
    if (p.stride > 0)     flags |= OP_FLAG_STRIDE;
    if (p.count > 1)      flags |= OP_FLAG_MULT;
    if (p.streamlen > 1)  flags |= OP_FLAG_STREAM;

    *reqwr = flags; reqwr += 1;
    if (flags & OP_FLAG_SIZED) {
      buf_write_uintNN<ADDR_T>(reqwr, &p.blocklen, 1); reqwr += sizeof(ADDR_T);
    }
    if (flags & OP_FLAG_STRIDE) {
      buf_write_uintNN<ADDR_T>(reqwr, &p.stride, 1); reqwr += sizeof(ADDR_T);
    }
    if (flags & OP_FLAG_MULT) {
      buf_write_uintNN<ADDR_T>(reqwr, &p.count, 1); reqwr += sizeof(ADDR_T);
    }
    if (flags & OP_FLAG_STREAM) {
      buf_write_uintNN<uint32_t>(reqwr, &p.streamlen, 1);
      reqwr += sizeof(uint32_t);
    }
    buf_write_uintNN<ADDR_T>(reqwr, &p.addr_start, 1); reqwr += sizeof(ADDR_T);
    if (flags & OP_FLAG_MASKED) {
      buf_write_uintNN<DATA_T>(reqwr, p.mask, p.blocklen);
      reqwr += p.blocklen*sizeof(DATA_T);
    }
    if (flags & OP_FLAG_WRITE) {
      buf_write_uintNN<DATA_T>(reqwr, p.data, p.blocklen*p.count*p.streamlen);
      reqwr += p.blocklen*p.count*p.streamlen*sizeof(DATA_T);
    }
    if ((err = write_e(fd, req, reqwr-req))){
      goto error;
    }
    error:
    free(req);
    return err;
  }

  int response_read (request_arg_t p) {
    int err;
    // Data
    size_t resp_buf_size = p.blocklen*p.count*p.streamlen*sizeof(DATA_T);
    uint8_t * resp = (uint8_t *)calloc(1, resp_buf_size);
    uint8_t response;
    if ((err = read_e(fd, &response, 1))) { goto error;}
    if (p.read) {
      if ((err = read_e(fd, resp, p.blocklen*p.count*p.streamlen*sizeof(DATA_T))
         )){
        goto error;
      }
      buf_read_uintNN<DATA_T>(resp, p.ret_buf, p.blocklen*p.count*p.streamlen);
    }
    error:
    free(resp);
    return err;
  }

public:

  int Transact (request_arg_t p) {
    int err;
    if ((err = request_write(p))) return err;
    if ((err = response_read(p))) return err;
    return 0;
  }

  int TransactMult (request_arg_t *p, size_t n) {
    int tcp_nodelay;
    tcp_nodelay = 0;
    setsockopt(fd, SOL_TCP, TCP_NODELAY, &tcp_nodelay, sizeof(tcp_nodelay));
    int err;
    fd_set readfds, writefds;
    FD_ZERO(&readfds);
    FD_ZERO(&writefds);
    size_t isend = 0, irecv = 0;
    while ( (isend < n) || (irecv < n) ) {
      FD_SET(fd, &readfds);
      FD_SET(fd, &writefds);
      int ready = pselect(fd+1, &readfds, &writefds, NULL, NULL, NULL);
      if (ready<0) return errno;
      if ( isend < n && FD_ISSET(fd, &writefds) ) {
        if ((err = request_write(p[isend]))) return err;
        isend ++;
      }
      if ( irecv < n && FD_ISSET(fd, &readfds) ) {
        if ((err = response_read(p[irecv]))) return err;
        irecv ++;
      }
    }
    tcp_nodelay = 1;
    setsockopt(fd, SOL_TCP, TCP_NODELAY, &tcp_nodelay, sizeof(tcp_nodelay));
    return 0;
  }

