#ifndef INC_NETREG_CODE_HH_
#define INC_NETREG_CODE_HH_

namespace netreg {

static const uint8_t MAGIC[] = {0x4e, 0x52, 0x45, 0x47};
static const uint32_t MAGIC_NUM = 0x4e524547;

static const uint8_t OP_FLAG_READ        = 1<<0;
static const uint8_t OP_FLAG_WRITE       = 1<<1;
static const uint8_t OP_FLAG_MASKED      = 1<<2;
static const uint8_t OP_FLAG_SIZED       = 1<<3;
static const uint8_t OP_FLAG_STRIDE      = 1<<4;
static const uint8_t OP_FLAG_MULT        = 1<<5;
static const uint8_t OP_FLAG_STREAM      = 1<<6;

static const uint8_t STATUS_SUCCESS      = 0;
static const uint8_t STATUS_EOF          = 255;
static const uint8_t STATUS_BADREQUEST   = 1;
static const uint8_t STATUS_UNSUPPORTED  = 2;

}

#endif // INC_NETREG_CODE_HH_

