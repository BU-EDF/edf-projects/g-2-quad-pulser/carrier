#include <stdint.h>
#include <stdio.h>
#include <netreg/client.hh>
#include <arpa/inet.h>

#define UINTBITS_(bits) uint##bits##_t
#define UINTBITS(bits) UINTBITS_(bits)
#define PRIHEXNBITS_(bits) PRIHEX_##bits
#define PRIHEXNBITS(bits) PRIHEXNBITS_(bits)
#define PRIHEX_8 "2"
#define PRIHEX_16 "4"
#define PRIHEX_32 "8"
#define PRIHEX_64 "16l"

#define PRIADDR PRIHEXNBITS(ADDRBITS)
#define PRIDATA PRIHEXNBITS(DATABITS)
#define ADDR_T UINTBITS(ADDRBITS)
#define DATA_T UINTBITS(DATABITS)

int main (int argc, char * *argv) {

  if (argc < 3) {
    fprintf(stderr, "Usage: %s server-address reg-address[=data]\n", argv[0]);
    exit(1);
  }

  ADDR_T addr;
  DATA_T data, data_old;
  long long addr_scan, data_scan;
  int scanned1, scanned2;
  int matches = sscanf(argv[2], "%Li%n=%Li%n",
    &addr_scan, &scanned1, &data_scan, &scanned2);
  if ( !matches ||
       (matches == 1 && scanned1 != (int)strlen(argv[2])) ||
       (matches == 2 && scanned2 != (int)strlen(argv[2])) ) {
    fprintf(stderr, "Unrecognized input\n");
    exit(1);
  }
  addr = addr_scan;
  data = data_scan;
  if ((long long)addr != addr_scan) {
    fprintf(stderr, "Invalid address number\n");
    exit(1);
  }
  if ((long long)data != data_scan) {
    fprintf(stderr, "Invalid value number\n");
    exit(1);
  }

  netreg::Connection<ADDR_T, DATA_T, netreg::Endian_Little> conn;
  int err;
  char *addrstr = argv[1];
  if (addrstr[0] >= '0' && addrstr[0] <= '9') {
    char *portsep = strrchr(addrstr, ':');
    in_port_t servport;
    struct in_addr servaddr;
    if (!portsep) {
      servport = htons(5555);
      servaddr = {.s_addr = inet_addr(addrstr)};
    } else {
      servport = htons(atoi(portsep+1));
      *portsep = '\0';
      servaddr = {.s_addr = inet_addr(addrstr)};
    }
    err = conn.Connect_tcp_inet(servaddr, servport);
  } else {
    err = conn.Connect_unix("/tmp/netreg_test");
  }
  if (err) {
    fprintf(stderr, "Error %d %s in Connect\n", err, strerror(err));
    return err;
  }

  conn.Read_block(&data_old, sizeof(data), addr, 1);
  printf("0x%0" PRIADDR "x: 0x%0" PRIDATA "x", addr, data_old);
  if (matches==2) {
    conn.Write_block(&data, sizeof(data), addr, 1);
    conn.Read_block(&data, sizeof(data), addr, 1);
    printf(" -> 0x%0" PRIDATA "x", data);
  }
  printf("\n");
  return 0;
}

