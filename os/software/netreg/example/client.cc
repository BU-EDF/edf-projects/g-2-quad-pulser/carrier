#include <stdint.h>
#include <stdio.h>
#include <netreg/client.hh>
#include <arpa/inet.h>

int main (int argc, char * *argv) {

  if (argc < 2) {
    fprintf(stderr, "Usage: %s address\n", argv[0]);
    exit(1);
  }

  netreg::Connection<uint16_t, uint32_t, netreg::Endian_Little> conn;
  int err;
  char *addrstr = argv[1];
  if (addrstr[0] >= '0' && addrstr[0] <= '9') {
    char *portsep = strrchr(addrstr, ':');
    in_port_t servport;
    struct in_addr servaddr;
    if (!portsep) {
      servport = htons(5555);
      servaddr = {.s_addr = inet_addr(addrstr)};
    } else {
      servport = htons(atoi(portsep+1));
      *portsep = '\0';
      servaddr = {.s_addr = inet_addr(addrstr)};
    }
    err = conn.Connect_tcp_inet(servaddr, servport);
  } else {
    err = conn.Connect_unix("/tmp/netreg_test");
  }
  if (err) {
    fprintf(stderr, "Error %d %s in Connect\n", err, strerror(err));
    return err;
  }
  uint16_t addr = 0xabcd;
  uint32_t value;
  conn.Read_masked(&value, addr, 0x10010110);
  addr = 0x1234;
  conn.Read_masked(&value, addr, 0x10010110);
  conn.Read_block(&value, sizeof(value), addr, 1);
  conn.Read_stream(&value, sizeof(value), addr, 1);
  size_t val_buf_size = sizeof(uint32_t)*16;
  uint32_t * val_buf = (uint32_t *)malloc(val_buf_size);
  conn.Read_block(val_buf, val_buf_size, addr, 16);
  conn.Read_stream(val_buf, val_buf_size, addr, 16);
  addr = 0xabcd;
  value = 0x1234abcd;
  conn.Write_masked(&value, addr, 0x10010110);
  addr = 0x1234;
  conn.Write_masked(&value, addr, 0x10010110);
  conn.Write_block(&value, sizeof(value), addr, 1);
  conn.Write_stream(&value, sizeof(value), addr, 1);
  for (int i = 0; i < 16; i++) {
    val_buf[i] = 0;
    for (int j = 0; j < 8; j++) {
      val_buf[i] |= ((i+j)%16)<<(4*j);
    }
  }
  for (int i = 0; i < 16; i++) {
    fprintf(stderr, "0x%08x\n", val_buf[i]);
  }
  conn.Write_block(val_buf, val_buf_size, addr, 16);
  conn.Read_block(val_buf, val_buf_size, addr, 16);
  for (int i = 0; i < 16; i++) {
    fprintf(stderr, "0x%08x\n", val_buf[i]);
  }
  conn.Write_stream(val_buf, val_buf_size, addr, 16);
  conn.Read_stream(val_buf, val_buf_size, addr, 16);
  free(val_buf);
  if (argc == 3) {
    int n = atoi(argv[2]);
    for (int i = 0; i < n; i++) {
      conn.Read_masked(&value, addr, 0x10010110);
    }
  }
  if (argc == 4) {
    int n = atoi(argv[2]);
    int npt = atoi(argv[3]);
    netreg::op_arg_t<uint16_t, uint32_t> * ops = (
      netreg::op_arg_t<uint16_t, uint32_t> * )
      malloc(npt*sizeof(*ops));
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < npt; j++) {
        ops[j] = conn.Op_Read_masked(&value, addr++, 0x10010110);
      }
      conn.TransactMult(ops, npt);
    }
    free(ops);
  }
  return 0;
}

