#include <stdio.h>
#include <netreg/server.hh>
#include <arpa/inet.h>

uint32_t server_memory[1<<16] = {0};

struct HW_t {
  // nothing is needed for this example
};

int hw_read(HW_t * hw, uint16_t addr, uint32_t *data) {
  *data = server_memory[addr];
  //fprintf(stderr, "Read at %04x: %08x\n", addr, *data);
  return 0;
}

int hw_write(HW_t *hw, uint16_t addr, uint32_t data) {
  server_memory[addr] = data;
  //fprintf(stderr, "Write at %04x: %08x\n", addr, data);
  return 0;
}

int main (int argc, char * *argv) {
  HW_t hw;
  netreg::Server<HW_t, uint16_t, uint32_t> serv(&hw, hw_read, hw_write);
  serv.Listen_unix("/tmp/netreg_test");
  if (argc > 1) {
    char *addrstr = argv[1];
    char *portsep = strrchr(addrstr, ':');
    in_port_t servport;
    struct in_addr servaddr;
    if (!portsep) {
      servport = htons(5555);
      servaddr = {.s_addr = inet_addr(addrstr)};
    } else {
      servport = htons(atoi(portsep+1));
      *portsep = '\0';
      servaddr = {.s_addr = inet_addr(addrstr)};
    }
    serv.Listen_tcp_inet(servaddr, servport);
  }
  serv.Serve();
  return 0;
}

