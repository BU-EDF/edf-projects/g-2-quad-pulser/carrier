
  template <typename data_t>
  void buf_write_uintNN (uint8_t *buf, const data_t *d, size_t n) {
      memcpy(buf, d, n*sizeof(data_t));
  }

  template <typename data_t>
  void buf_read_uintNN (const uint8_t *buf, data_t *d, size_t n) {
      memcpy(d, buf, n*sizeof(data_t));
  }

// Define conditions for reading size information according to flags and
//   default values when size information is not needed
#define SIZEFLAGS \
  RR( (flags & OP_FLAG_SIZED ), ADDR_T,   blocklen  , 1) \
  RR( (flags & OP_FLAG_STRIDE), ADDR_T,   stride    , 0) \
  RR( (flags & OP_FLAG_MULT  ), ADDR_T,   count     , 1) \
  RR( (flags & OP_FLAG_STREAM), uint32_t, streamlen , 1) \
  RR( (true)                  , ADDR_T,   addr_start, 0)
// Macro RR is given three different definitions in the following procedures,
//   corresponding to reuse of the conditions, names, sizes, and defaults
//   as needed

  void req_param_size (
      uint8_t flags,
      size_t *ret_size
  ) {
    size_t req_len = 0;
// Check the condition for counting up the length of part of the request
//   containing size information
#define RR( cond, type, name, default)          \
    if (cond) { req_len += sizeof(type); }
SIZEFLAGS
#undef RR
    *ret_size = req_len;
  }

  void req_param_read (
      uint8_t flags,
      const uint8_t *buf,
      request_param_t *p
  ) {
    *p = (request_param_t){
// Initialize sizes to default values
#define RR( cond, type, name, default) \
      .name = default,
SIZEFLAGS
#undef RR
    };
    const uint8_t * reqrd = buf;
// Read sizes from the received request
#define RR( cond, type, name, default)                  \
    if (cond) {                                         \
      buf_read_uintNN<type>(reqrd, & p->name, 1);       \
      reqrd += sizeof(type);                            \
    }
SIZEFLAGS
#undef RR
  }
#undef SIZEFLAGS

  void req_data_size (
    uint8_t flags,
    request_param_t p,
    size_t *ret_size
  ) {
    size_t req_len = 0;
    if (flags & OP_FLAG_MASKED) req_len += p.blocklen*sizeof(DATA_T);
    if (flags & OP_FLAG_WRITE)
      req_len += p.blocklen*p.count*p.streamlen*sizeof(DATA_T);
    *ret_size = req_len;
  }

  int req_serve (uint8_t flags, request_param_t p, DATA_T *req_buf,
    size_t req_buf_size, DATA_T * *ret_resp_buf, size_t *ret_resp_len) {
    int err = 0;
{
    DATA_T * datrd = req_buf;
    
    if ( p.count!=1 ) {
      err = -STATUS_UNSUPPORTED; // UNIMPLEMENTED
      goto error;
    }
    
    if ( ( p.count!=1 ) ||
         ( p.blocklen!=1 && p.streamlen !=1 ) ||
         ( (flags & OP_FLAG_MASKED) && (p.blocklen!=1 || p.streamlen!=1) )
        ) {
      err = -STATUS_UNSUPPORTED; // DISALLOWED
      goto error;
    }

    size_t req_len = 0;
    req_data_size(flags, p, &req_len);
    if (req_len > req_buf_size) {
      err = -STATUS_BADREQUEST; goto error; }
    DATA_T * mask = 0;
    DATA_T * data = 0;
    if (flags & OP_FLAG_MASKED) {
      mask = datrd;
      datrd += p.blocklen;
    }
    if (flags & OP_FLAG_WRITE) {
      data = datrd;
      datrd += p.blocklen*p.count*p.streamlen;
    }
    if (flags & OP_FLAG_WRITE) {
      for (uint32_t stream_i = 0; stream_i < p.streamlen; stream_i++) {
//        for (ADDR_T block_i = 0; block_i < p.count; block_i++) {
          for (ADDR_T word_i = 0; word_i < p.blocklen; word_i++) {
            ADDR_T addr = p.addr_start + word_i;
            DATA_T writew = data[word_i + p.blocklen*p.count*stream_i];
            if (flags & OP_FLAG_MASKED) {
              DATA_T maskw = mask[word_i];
              DATA_T readw = 0;
              reg_read(hw, addr, &readw);
              writew = (readw&(~maskw))|(writew&maskw);
            }
            reg_write(hw, addr, writew);
          }
//        }
      }
    }

    if (flags & OP_FLAG_READ) {
      size_t res_len = p.blocklen*p.count*p.streamlen*sizeof(DATA_T);
      DATA_T * wres = (DATA_T *)realloc(*ret_resp_buf, res_len);
      *ret_resp_len = res_len;
      *ret_resp_buf = wres;
      for (uint32_t stream_i = 0; stream_i < p.streamlen; stream_i++) {
//        for (ADDR_T block_i = 0; block_i < p.count; block_i++) {
          for (ADDR_T word_i = 0; word_i < p.blocklen; word_i++) {
            ADDR_T addr = p.addr_start + word_i;
            DATA_T readw = 0;
            reg_read(hw, addr, &readw);
            if (flags & OP_FLAG_MASKED) {
              DATA_T maskw = mask[word_i];
              readw = (readw&maskw);
            }
            wres[word_i + p.blocklen*p.count*stream_i] = readw;
          }
//        }
      }
    } else {
      *ret_resp_len = 0;
    }
}
    error:
    return err;
  }

  static const size_t neg_req_size = sizeof(MAGIC)+1+1+1;
  static const size_t neg_resp_size = 4;

  int negotiate2 (const uint8_t *req_buf, uint8_t *resp_buf) {
    int err;
    {
      const uint8_t * reqrd = req_buf;
      if (memcmp(MAGIC, reqrd, sizeof(MAGIC))) {
        fprintf(stderr, "Client failed header word test\n");
        return -1;
      }
      reqrd += sizeof(MAGIC);
      uint8_t format = 0;
      format |= (ENDIAN_HOST)&1 << 0;
      uint8_t client_format;
      client_format = *reqrd; reqrd += 1;
      if (format != client_format) {
        fprintf(stderr, "Client failed format test\n");
        return -1;
      }
      uint8_t addr_size = sizeof(ADDR_T);
      uint8_t data_size = sizeof(DATA_T);
      uint8_t client_addr_size;
      uint8_t client_data_size;
      client_addr_size = *reqrd; reqrd++;
      client_data_size = *reqrd; reqrd++;
      if ( (client_addr_size != addr_size) ||
           (client_data_size != data_size) ) {
        fprintf(stderr, "Client failed sizes test\n");
        return -1;
      }
      uint32_t resp = MAGIC_NUM;
      memcpy(resp_buf, &resp, 4);
      return 0;
    }
    return err;
  }

  int handle_client (struct client_state *c) {
    int err = 0;
    switch (c->state) {
      // State 0: New client.  Need to check compatibility.
      case (0): {
        c->recv_expected = neg_req_size;
        c->state = 5;
      } break;
      // State 5: Client is ready to be checked for compatibility
      case (5): {
        c->resp_len = neg_resp_size;
        c->resp = realloc(c->resp, c->resp_len);
        err = negotiate2((uint8_t *)c->recv, (uint8_t *)c->resp);
        c->state = 1;
        c->recv_expected = 1;
      } break;
      // State 1: New request has been received; Flags are ready for reading
      case (1): {
        c->flags = *(uint8_t *)c->recv;
        req_param_size(c->flags, & c->recv_expected);
        c->state = 2;
      } break;
      // State 2: Sizes are ready for reading
      case (2): {
        req_param_read(c->flags, (uint8_t *)c->recv, & c->req_pa);
        req_data_size(c->flags, c->req_pa, & c->recv_expected);
        c->state = 3;
      } break;
      // State 3: Data, if any, are ready for reading.
      // If no data, nothing has happened since transition from State 2.
      case (3): {
        err = req_serve(c->flags, c->req_pa, (DATA_T *)c->recv, c->recv_len,
          (DATA_T * *)&c->resp, &c->resp_len);
        if (err>0) return err;
        uint8_t clierr = -err;
        c->status_byte = clierr;
        c->status_send = true;
        // Success: Ready for next request.
        c->state = 1;
        c->recv_expected = 1;
      } break;
    }
    return err;
  }

  int client_do_read(struct client_state *c, int fd) {
    if (!c->recv_expected) return 0;
    if (c->recv_cap != c->recv_expected) {
      c->recv = realloc(c->recv, c->recv_expected);
    }
    ssize_t rdlen = read(fd,
      (uint8_t *)c->recv + c->recv_len,
      c->recv_expected - c->recv_len);
    if (rdlen<0) return errno;
    if (rdlen==0) return -STATUS_EOF;
    c->recv_len += rdlen;
    return 0;
  }

  int client_do_write(struct client_state *c, int fd) {
    if (!c->resp_len) return 0;
    ssize_t wrlen = write(fd,
      (uint8_t *)c->resp + c->resp_written,
      c->resp_len - c->resp_written);
    if (wrlen<0) return errno;
    if (wrlen==0) return -STATUS_EOF;
    c->resp_written += wrlen;
    return 0;
  }

  int handle_read (int fd) {
    struct client_state * c = clstates[fd];
    int err;
    if ((err = client_do_read(c, fd))){ return err; }
    if (c->recv_len==c->recv_expected && !c->status_send && !c->resp_len) {
      do {
        if ((err = handle_client(c))) {return err;}
        c->recv_len = 0;
        c->resp_written = 0;
      } while (!c->recv_expected && !c->status_send && !c->resp_len);
      if (c->resp_len || c->status_send) FD_SET(fd, &fdwrite);
    }
    return 0;
  }

  int handle_write (int fd) {
    struct client_state * c = clstates[fd];
    if (c->status_send) {
      ssize_t wrlen = write(fd, &c->status_byte, 1);
      if (wrlen==0) return -STATUS_EOF;
      if (wrlen<0) return errno;
      c->status_send = false;
    }
    if (c->resp_len == c->resp_written) {
      FD_CLR(fd, &fdwrite);
      c->resp_len = 0;
      c->resp_written = 0;
      return 0;
    }
    return client_do_write(c, fd);
  }

