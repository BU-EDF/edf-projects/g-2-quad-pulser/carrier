#ifndef IN_NETREG_SERVER_HH_
#error
#endif

private:

public:

int Listen_unix (
		 const char *path
		 ) {
  int sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
  if (sockfd<0) {
    fprintf(stderr, "Failed to create socket\n");
  }
  if (sockfd>=FD_SETSIZE) {
    close(sockfd);
    return ENFILE;
  }
  size_t path_len = strlen(path)+1;
  struct sockaddr_un * addr;
  addr = (struct sockaddr_un *) calloc(1, sizeof(*addr)+path_len);
  addr->sun_family = AF_UNIX;
  strncpy(&addr->sun_path[0], path, path_len);
  fprintf(stderr, "Binding to %s\n", path);
  if (bind(sockfd, (const sockaddr *)addr, sizeof(*addr))) {
    fprintf(stderr, "Failed to bind, err %d %s\n", errno, strerror(errno));
    free(addr);
    return errno;
  }
  free(addr);
  if (-1 == listen(sockfd, 1)) {
    fprintf(stderr, "Failed to listen, err %d %s\n", errno, strerror(errno));
    return errno;
  }
  FD_SET(sockfd, &fdsockets);
  FD_SET(sockfd, &fdread);
  if (sockfd>=fdend) fdend = sockfd+1;
  int sock_rcvbuf_size;
  socklen_t argsize = sizeof(sock_rcvbuf_size);
  getsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, (void*)&sock_rcvbuf_size,
	     &argsize);
  if (size_t(sock_rcvbuf_size) > rcvbuf_size) {
    rcvbuf_size = sock_rcvbuf_size;
    rcvbuf = (uint8_t*)realloc(rcvbuf, rcvbuf_size);
  }
  return 0;
}

int Listen_tcp_inet (
		     struct in_addr address, in_port_t port
		     ) {
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd<0) {
    fprintf(stderr, "Failed to create socket\n");
    return errno;
  }
  if (sockfd>=FD_SETSIZE) {
    close(sockfd);
    return ENFILE;
  }
  struct sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_port = port;
  addr.sin_addr = address;
  fprintf(stderr, "Binding to %d.%d.%d.%d port %d\n",
	  ((uint8_t*)&address)[0],
	  ((uint8_t*)&address)[1],
	  ((uint8_t*)&address)[2],
	  ((uint8_t*)&address)[3],
	  ntohs(port)
	);
  if (bind(sockfd, (const sockaddr *)&addr, sizeof(addr))) {
    fprintf(stderr, "Failed to bind, err %d %s\n", errno, strerror(errno));
    return errno;
  }
  if (-1 == listen(sockfd, 1)) {
    fprintf(stderr, "Failed to listen, err %d %s\n", errno, strerror(errno));
    return errno;
  }
  FD_SET(sockfd, &fdsockets);
  FD_SET(sockfd, &fdread);
  if (sockfd>=fdend) fdend = sockfd+1;
  int sock_rcvbuf_size;
  socklen_t argsize = sizeof(sock_rcvbuf_size);
  getsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, (void*)&sock_rcvbuf_size,
	     &argsize);
  if (size_t(sock_rcvbuf_size) > rcvbuf_size) {
    rcvbuf_size = sock_rcvbuf_size;
    rcvbuf = (uint8_t*)realloc(rcvbuf, rcvbuf_size);
  }
  return 0;
}

void Serve ( ) {
  while (1) {
    int err = 0;
    fd_set fdread_ready = fdread;
    fd_set fdwrite_ready = fdwrite;
    int nready = pselect(fdend, &fdread_ready, &fdwrite_ready,
			 NULL, NULL, NULL);
    if (nready==0) continue;
    if (nready==-1) {
      fprintf(stderr, "Error %d %s\n", errno, strerror(errno));
      continue;
    }

    fdlast = (fdlast+1)%fdend;
    while ( !FD_ISSET(fdlast, &fdread_ready) &&
	    !FD_ISSET(fdlast, &fdwrite_ready) ) fdlast = (fdlast+1)%fdend;

    if (FD_ISSET(fdlast, &fdsockets)) {
      // Ready socket is a listening socket
      int sockfd = fdlast;
      sockaddr_storage clientaddr_buf;
      socklen_t clientaddr_len = sizeof(clientaddr_buf);
      int clientfd = accept(sockfd,
        (sockaddr *)&clientaddr_buf, &clientaddr_len);
      if (clientfd>=FD_SETSIZE) {
	close(clientfd);
	clientfd=-1;
	errno = ENFILE;
      }
      if (clientfd==-1) {
	fprintf(stderr, "Failed to accept, error %d %s\n",
		errno, strerror(errno));
	continue;
      }
      struct sockaddr * clientaddr = (struct sockaddr *)&clientaddr_buf;
      switch (clientaddr->sa_family) {
        case (AF_UNIX): {
          fprintf(stderr, "Accepted client from socket\n");
        } break;
        case (AF_INET): {
          struct sockaddr_in * addr = (struct sockaddr_in *)clientaddr;
          fprintf(stderr, "Accepted client at %d.%d.%d.%d port %d\n",
	    ((uint8_t*)&addr->sin_addr)[0],
	    ((uint8_t*)&addr->sin_addr)[1],
	    ((uint8_t*)&addr->sin_addr)[2],
	    ((uint8_t*)&addr->sin_addr)[3],
	    ntohs(addr->sin_port)
	  );
        } break;
        default: {
          fprintf(stderr, "Accepted client from socket\n");
        } break;
      }
      // Turn off TCP Delay if it is supported
      // If the client is not from a TCP socket or the option is not supported,
      // this will safely and silently fail (ENOPROTOOPT).
      int tcp_nodelay = 1;
      setsockopt(clientfd, SOL_TCP, TCP_NODELAY,
        &tcp_nodelay, sizeof(tcp_nodelay));
      FD_SET(clientfd, &fdread);
      if (clientfd>=fdend) {
	fdend = clientfd+1;
	clstates = (struct client_state * *)
	  realloc(clstates, fdend*sizeof(*clstates));
      }
      clstates[clientfd] = (struct client_state *)
	calloc(1, sizeof(**clstates));
      continue;
    } 

    // Ready socket is a client
    int clientfd = fdlast;
    if (FD_ISSET(fdlast, &fdwrite_ready)) {
      err = handle_write(clientfd);
    } else {
      err = handle_read(clientfd);
    }
    if (err) {
      if (err > 0) {
        fprintf(stderr, "Server error %d %s when handling client, closing\n",
          err, strerror(err));
      } else {
        const char *status = "Unknown";
        if (-err == STATUS_EOF) status = "End of file";
        if (-err == STATUS_BADREQUEST) status = "Malformed request";
        if (-err == STATUS_UNSUPPORTED) status = "Unsupported operation";
        fprintf(stderr, "Client is disconnected: %s\n", status);
      }
      close(clientfd);
      FD_CLR(clientfd, &fdread);
      FD_CLR(clientfd, &fdwrite);
      free(clstates[clientfd]);
    }
  }
}

