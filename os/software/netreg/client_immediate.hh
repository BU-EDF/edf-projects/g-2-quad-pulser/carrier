#ifndef IN_NETREG_CLIENT_HH_ 
#error
#endif

public:

  int Read_masked (
    DATA_T *ret, ADDR_T addr, DATA_T mask
  ) {
    return Transact(Op_Read_masked(ret, addr, mask));
  }

  int Read_block (
    DATA_T *ret_buf, size_t ret_size,
    ADDR_T addr_start, ADDR_T length
  ) {
    return Transact(Op_Read_block(ret_buf, ret_size, addr_start, length));
  }

  int Read_stream (
    DATA_T *ret_buf, size_t ret_size,
    ADDR_T addr, uint32_t length
  ) {
    return Transact(Op_Read_stream(ret_buf, ret_size, addr, length));
  }

  int Write_masked (
    const DATA_T *data, ADDR_T addr, DATA_T mask
  ) {
    return Transact(Op_Write_masked(data, addr, mask));
  }

  int Write_block (
    DATA_T *data_buf, size_t data_size,
    ADDR_T addr_start, ADDR_T length
  ) {
    return Transact(Op_Write_block(data_buf, data_size, addr_start, length));
  }

  int Write_stream (
    DATA_T *data_buf, size_t data_size,
    ADDR_T addr, uint32_t length
  ) {
    return Transact(Op_Write_stream(data_buf, data_size, addr, length));
  }

