#ifndef INC_NETREG_CLIENT_HH_
#define INC_NETREG_CLIENT_HH_

#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdio.h>

#include "code.hh"

namespace netreg {

typedef enum {
  Endian_Little = 0,
  Endian_Big = 1,
} endian_t;

#if BYTE_ORDER == LITTLE_ENDIAN
const endian_t ENDIAN_HOST = Endian_Little;
#endif
#if BYTE_ORDER == BIG_ENDIAN
const endian_t ENDIAN_HOST = Endian_Big;
#endif

template <typename ADDR_T, typename DATA_T>
struct op_arg_t {
  bool read;
  bool write;
  DATA_T *ret_buf;
  size_t ret_buf_size;
  ADDR_T addr_start;
  ADDR_T blocklen;
  ADDR_T stride;
  ADDR_T count;
  uint32_t streamlen;
  const DATA_T *mask;
  size_t mask_size;
  const DATA_T *data;
  size_t data_size;
};


#include "readwrite_err.h"

template <typename ADDR_T, typename DATA_T, endian_t ENDIAN_SERVER>
class Connection {
public:
  int fd;
  Connection() {
    fd = -1;
  }
private:
  template <typename data_t>
  void buf_write_uintNN (uint8_t *buf, const data_t *d, size_t n) {
    if (sizeof(data_t)==1 || ENDIAN_HOST==ENDIAN_SERVER) {
      memcpy(buf, d, n*sizeof(data_t));
      return;
    }
    for (size_t i = 0; i < n; i++) {
      for (size_t j = 0; j < sizeof(data_t); j++) {
        buf[i*sizeof(data_t)+j] = ((uint8_t*)&d[i])[sizeof(data_t)-j-1];
      }
    }
  }

  template <typename data_t>
  void buf_read_uintNN (const uint8_t *buf, data_t *d, size_t n) {
    if (sizeof(data_t)==1 || ENDIAN_HOST==ENDIAN_SERVER) {
      memcpy(d, buf, n*sizeof(data_t));
      return;
    }
    for (size_t i = 0; i < n; i++) {
      for (size_t j = 0; j < sizeof(data_t); j++) {
        ((uint8_t*)&d[i])[sizeof(data_t)-j-1] = buf[i*sizeof(data_t)+j];
      }
    }
  }

#define IN_NETREG_CLIENT_HH_

#include "client_connection.hh"
#include "client_request.hh"
#include "client_operations.hh"
#include "client_immediate.hh"

#undef IN_NETREG_CLIENT_HH_

};

} // namespace netreg

#endif // INC_NETREG_CLIENT_HH_

