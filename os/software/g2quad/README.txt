===============================================================================
== Zynq pl register map
===============================================================================
This library deals with the BU's register map interface on a zynq fpga.
This software can talk over a TCP or Unix sockets to a server running on the Zynq.
This code also includes the software to convert names into register address/bit masks.

===============================================================================
== Build instructions (linux native, this probably means you)
===============================================================================
After checking out the BUTool source:
  $ ./setup.sh
  $ source env.sh
  $ make

===============================================================================
== Build and Install instructions (BU zynq petalinux builds)
===============================================================================
After checking out the BUTool source:
  $ PLATFORM=petalinux ./setup.sh
  $ source env.sh
  $ make
  $ make install

You may need to update env.sh to set the working prefix for current zynq projects
You will have to change env.sh's MAKEFLAGS and PROJECT_ROOT directories for your checkout
  $ source env.sh 
  $ make
  $ make install
This will build the directory structure for the zynq in ./install/
You should copy its contents to "/" on the zynq

===============================================================================
== Bug/issue tracking: 
===============================================================================
email bugs and issues to: dgastler@bu.edu

