#ifndef __G2QUAD_EXCEPTION_HH__
#define __G2QUAD_EXCEPTION_HH__ 1

#include <BUException/ExceptionBase.hh>

namespace BUException{           
  //Exceptions for Tool
  ExceptionClassGenerator(TRANSACTION_ERROR,"Error during transaction\n");
  ExceptionClassGenerator(SERVER_CONN_ERROR,"Server connection error\n");
  ExceptionClassGenerator(ADC_OUT_OF_RANGE,"ADC board number out of range\n")
  ExceptionClassGenerator(ADC_NO_LOCK,"ADC Board rx not locked\n")
  ExceptionClassGenerator(ADC_CHANNEL_OUT_OF_RANGE,"ADC Channel number out of range\n")
  ExceptionClassGenerator(ADC_CHIP_OUT_OF_RANGE,"ADC Chip number out of range\n")
  ExceptionClassGenerator(G2QUAD_BAD_ARGS,"Bad argument(s)\n")
  ExceptionClassGenerator(PROGRAM_FLASH_ERROR,"Error while programming the flash\n")
  ExceptionClassGenerator(I2C_ERROR,"Error during i2c transaction\n")
}




#endif
