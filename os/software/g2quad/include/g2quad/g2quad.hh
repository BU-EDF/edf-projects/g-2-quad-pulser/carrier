#ifndef __G2QUAD_HH__
#define __G2QUAD_HH__
/* 
Rev log
===============================
* 2017-12-22 (Dan Gastler)
File creation.  Separating the PLInterface code that interfaces with the hdl register map via AXI GPIOs from the higher level g2quad specific firmware. 

*/
#include <stdio.h>
#include <sys/mman.h>
#include <stdint.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <vector>
#include <string>

#include <g2quad/g2quad_Exception.hh>
#include <AddressTable/AddressTable.hh>
#include <netreg/client.hh>

class g2quad{
public:
  g2quad(std::string const & table_file, std::string const & server_name);
  //  ~g2quad();
  std::vector<std::string> GetNames(std::string regex = "*") {return addr_table.GetNames(regex);};
  Item const * GetItem         (std::string const & name){return addr_table.GetItem(name);}
  std::vector<std::string> GetTableNames(std::string const & regex){return addr_table.GetTables(regex);}   
  uint32_t Read                (uint16_t address);
  void     Write               (uint16_t address, uint32_t data);
  uint32_t Read                (std::string const & address);
  void     Write               (std::string const & address, uint32_t data);
  uint32_t ADCRead             (int board_id, uint16_t address);
  void     ADCWrite            (int board_id, uint16_t address, uint32_t data);
//  uint32_t ADCRead             (int board_id, std::string const & address);
//  void     ADCWrite            (int board_id, std::string const & address, uint32_t data);
  uint32_t ReadI2C(std::string const & base_address ,uint8_t i2c_addr,uint16_t address, uint8_t byte_count);
  void     WriteI2C(std::string const & base_address,uint8_t i2c_addr,uint16_t address, uint32_t data, uint8_t byte_count,bool ignore_error = false);
  
  std::string GetADCBoardBase    (int board_id);
  std::string GetADCBoardADCBase (int board_id,int ADC);
  void     InitBoardLink         (int board_id);
  void     CheckBoardLink        (int board_id);
  void     CheckBoardRange       (int board_id);
  void     CheckADCRange         (int board_id);
  
  bool     ResetBoardConnection(int board_id);

  void     ADCConfigRegWrite   (int board_id,int ADC,uint8_t reg_addr,uint16_t data);
  void     ConfigADCs          (int board_id);
  void     AlignADCChannel     (int board_id, int channel);

  void     ConfigureFreeRun(uint16_t period /*ms*/);
  void     EnableFreeRun();
  void     DisableFreeRun();
  void     TrigPulse(); //added 07/24/18

  void     ConfigurePulser(uint8_t  boardID,          uint32_t mode,
			   uint32_t charge1_start,    uint32_t charge1_length,
			   uint32_t discharge_start,  uint32_t discharge_length,
			   uint32_t charge2_start =0, uint32_t charge2_length =0);
  
  int      GetSVNVersion(){return Version;}
  void ProgramFlash(int board_id, char const* file,bool printUpdate=false); //aded 06/29/2018
  void reset_monitered_pulser(std::vector<size_t> boardList); //added 07/27/2018
  //  std::vector<uint16_t> ReadoutADCChannel(int board_id, int channel);
  void convertToHex(std::vector<uint8_t> data, uint8_t ByteCount);
  std::vector<std::string> ADCCommand(int board_id, std::string const & cmd);
private:
  g2quad(); // do not implement


  static const int Version;

  bool RPCMode;
  std::vector<bool>     subDeviceValid;
  std::vector<uint16_t> subDeviceReadAddr;
  std::vector<uint16_t> subDeviceWriteAddr;
    
  AddressTable addr_table;  
  std::string server;
  netreg::Connection<uint16_t, uint32_t, netreg::Endian_Little> conn;

  void LoadIHexFile(std::vector <uint8_t>&data, const char* file); // added 06/29/18
  void ParseIHexLine(std::string line, std::vector <uint8_t> &data, uint32_t &upperAddr);
  void WriteFlash(std::vector<uint8_t> const &data, int board_id,bool printUpdate);               // added 06/29/18
};
#endif
