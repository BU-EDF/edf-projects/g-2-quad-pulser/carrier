#ifndef __ADDRESSTABLE_HPP__
#define __ADDRESSTABLE_HPP__

#include <string>
#include <vector>
#include <boost/unordered_map.hpp>
#include <map>

#include <stdint.h>

#include <AddressTable/AddressTable_ItemConversion.hh>

class Item{
public:
  enum ModeMask{EMPTY=0x0,READ = 0x1,WRITE =0x2, ACTION = 0x4};
  std::string name;
  uint16_t address;
  uint32_t mask;
  uint8_t  offset;
  uint8_t  mode; // r :0, w :1, a:2, i:3
  boost::unordered_map<std::string,std::string> user;   
  ItemConversion *sc_conv;
  int deviceID; //0 for none/primary, -1 for bad
};

class SubDevice{
public:
  std::string device;
  int deviceID;
  std::vector<std::pair<uint16_t,uint16_t> > address_range; //low,high
};

class AddressTable{
 public:
  AddressTable(std::string const & addressTableName);
  ~AddressTable();
  Item const *              GetItem     (std::string const &);
  std::vector<Item const *> GetTagged   (std::string const & tag);
  std::vector<std::string>  GetNames    (std::string const & regex = "*");
  std::vector<std::string>  GetAddresses(uint16_t lower, uint16_t upper);
  std::vector<std::string>  GetTables   (std::string const &regex);
  int                       GetDeviceFromAddress(uint16_t address);
  uint16_t                  GetDeviceSubAddress(int,uint16_t);
 private:  
  //default constructor is forbidden 
  AddressTable();
  //preventcopying
  AddressTable( const AddressTable & );
  AddressTable& operator=(const AddressTable &);

  int fileLevel;
  void LoadFile(std::string const &,
		std::string const & prefix = "",
		uint16_t offset=0,
		int currentDeviceID = -1);
  void ProcessLine(std::string const &,
		   size_t,
		   std::string const & prefix = "",
		   uint16_t offset=0,int currentDeviceID = -1);
  //Ds of entries
  //req  req     req  req  tokenized
  //name address mask mode user
  void AddEntry(Item *);
  //Map of address to Items (master book-keeping; delete from here)
  std::map<uint32_t,std::vector<Item*> > addressItemMap;
  //Map of names to Items
  std::map<std::string,Item*> nameItemMap;
  static const char * SC_key_conv;

  //List of devices
  std::vector<SubDevice*> deviceList;
  std::vector<std::pair<uint16_t,int> > deviceRangeList;
  int GetSubDeviceID(std::string const &);
  void UpdateSubDeviceRanges(SubDevice * parent,
			     SubDevice * child,
			     uint16_t low,
			     uint16_t high);
};
#endif
