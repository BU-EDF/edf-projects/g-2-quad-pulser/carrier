#include <g2quad/g2quad.hh>

void     g2quad::ConfigureFreeRun(uint16_t frequency /*Hz*/){
  uint32_t period = uint16_t(1000.0/frequency);
  Write("TRIGGER.FREE_RUN.PERIOD",period);  
}
void     g2quad::EnableFreeRun(){
  //Needs a rising edge of enable
  Write("TRIGGER.FREE_RUN.ENABLE",0);
  Write("TRIGGER.FREE_RUN.ENABLE",1);
}
void     g2quad::DisableFreeRun(){
  Write("TRIGGER.FREE_RUN.ENABLE",0);
}

//added 07/24/2018
void     g2quad::TrigPulse(){
  uint32_t burstModeTemp = Read("TRIGGER.FREE_RUN.BURST_MODE");
  uint32_t EnableValueTemp = Read("TRIGGER.FREE_RUN.ENABLE");
  Write("TRIGGER.FREE_RUN.BURST_MODE", 0);
  Write("TRIGGER.FREE_RUN.ENABLE", 0);
  Write("TRIGGER.FREE_RUN.TRIG", 1);
  Write("TRIGGER.FREE_RUN.BURST_MODE",burstModeTemp);
  Write("TRIGGER.FREE_RUN.ENABLE",EnableValueTemp);;
}
