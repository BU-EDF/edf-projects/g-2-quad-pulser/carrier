#include <g2quad/g2quad.hh>

uint32_t g2quad::ReadI2C(std::string const & base_address ,uint8_t i2c_addr,uint16_t address, uint8_t byte_count){
  //This is an incredibly inefficient version of this function since it does a dozen or so UDP transactions.
  //This is done to be generic, but it could be hard-coded by assuming address offsets and bit maps and done in one write and one read.

  //Set the I2c address
  Write(base_address+".I2C_ADDR",i2c_addr);
   //Set type of read
  Write(base_address+".RW",1);
   //Set address
  Write(base_address+".REG_ADDR",address);
  //Set read size
  Write(base_address+".BYTE_COUNT",byte_count);
  //Wait for the last transaction to be done
  while(Read(base_address+".DONE") == 0x0){usleep(1000);}
  //Run transaction
  Write(base_address+".RUN",0x1);
  //Wait for the last transaction to be done
  while(Read(base_address+".DONE") == 0x0){usleep(1000);}
  if(Read(base_address+".ERROR")){
    printf("%s 0x%08X\n",(base_address+".ERROR").c_str(),Read(base_address+".ERROR"));
    //Reset the I2C firmware
    Write(base_address+".RESET",1);
    Write(base_address+".RESET",0);    
    char trans_info[] = "rd @ 0xFFFF";
    sprintf(trans_info,"rd @ 0x%04X",address&0xFFFF);
    BUException::I2C_ERROR e;
    e.Append("I2C Error on ");
    e.Append(base_address.c_str());
    e.Append(trans_info);
    throw e;
  }
  return Read(base_address+".RD_DATA");
}
void     g2quad::WriteI2C(std::string const & base_address,uint8_t i2c_addr,uint16_t address, uint32_t data, uint8_t byte_count,bool ignore_error){
  //This is an incredibly inefficient version of this function since it does a dozen or so UDP transactions.
  //This is done to be generic, but it could be hard-coded by assuming address offsets and bit maps and done in one write and one read.

    //Set the I2c address
  Write(base_address+".I2C_ADDR",i2c_addr);
   //Set type of read
  Write(base_address+".RW",0);
  //Set address
  Write(base_address+".REG_ADDR",address);
  //Set read size
  Write(base_address+".BYTE_COUNT",byte_count);
  //Send data to write
  Write(base_address+".WR_DATA",data);
  //Wait for the last transaction to be done
  while(Read(base_address+".DONE") == 0x0){usleep(1000);}
  //Run transaction
  Write(base_address+".RUN",0x1);

  //Wait for the last transaction to be done
  while(Read(base_address+".DONE") == 0x0){usleep(1000);}

  if(!ignore_error && Read(base_address+".ERROR")){
    //Reset the I2C firmware
    Write(base_address+".RESET",1);
    Write(base_address+".RESET",0);    
    char trans_info[] = "wr 0xFFFFFFFF @ 0xFFFF";
    sprintf(trans_info,"wr 0x%08X @ 0x%04X",data,address&0xFFFF);
    BUException::I2C_ERROR e;
    e.Append("I2C Error on ");
    e.Append(base_address.c_str());
    e.Append(trans_info);
    throw e;
  }
}
