#include <g2quad/g2quad.hh>
#include <arpa/inet.h>

g2quad::g2quad(std::string const & table_file,
	       std::string const & server_name):addr_table(table_file){
  int netreg_error = 0;
  //Check if this is a unix socket or a IPV4 address
  struct in_addr sin_addr;
  if(1 == inet_pton(AF_INET,server_name.c_str(),&sin_addr)){    
    if((netreg_error = conn.Connect_tcp_inet(sin_addr,(in_port_t)htons(uint16_t(5555))))){
      BUException::SERVER_CONN_ERROR e;
      e.Append(server_name.c_str());
      throw e;
    }
  }else{
    //Not an IP address, try unix socket
    if((netreg_error = conn.Connect_unix(server_name.c_str()))){
      BUException::SERVER_CONN_ERROR e;
      e.Append(server_name.c_str());
      throw e;
    }
  }
  subDeviceValid.resize(4,false); //Should do nothing except for the first run
  subDeviceReadAddr.resize(4,0);
  subDeviceWriteAddr.resize(4,0);

  RPCMode = true;
}

uint32_t g2quad::Read(uint16_t address){
  //  fprintf(stderr,"0x%04X ",address);
  uint32_t value;
  //Figure out what device/subdevice this is.
  int deviceID = addr_table.GetDeviceFromAddress(address);
  if((0 == deviceID) || RPCMode){
    //Main board
    if(0 != conn.Read_masked(&value,address,0xffffffff)){
      BUException::TRANSACTION_ERROR e;
      throw e;
    }
  }else if(deviceID <= 4){
    printf("Slow READ\n");
    //ADC Board = device ID
    value = ADCRead(deviceID,address);
  }else{
    //Unknown device
    BUException::TRANSACTION_ERROR e;
    e.Append("Unknown sub-device\n");
    throw e;    
  }
  //  fprintf(stderr,"0x%08X\n",value);
  return value;
}

void g2quad::Write(uint16_t address,uint32_t data){
  //  fprintf(stderr,"0x%04X 0x%08X\n",address,data);
  //Figure out what device/subdevice this is.
  int deviceID = addr_table.GetDeviceFromAddress(address);
  if((0 == deviceID) || RPCMode){
    //Main board
    if(0 != conn.Write_masked(&data,address,0xffffffff)){
      BUException::TRANSACTION_ERROR e;
      throw e;
    }
  }else if(deviceID <= 4){
    printf("Slow Write\n");
    //ADC Board = device ID
    ADCWrite(deviceID,address,data);
  }else{
    //Unknown device
    BUException::TRANSACTION_ERROR e;
    e.Append("Unknown sub-device\n");
    throw e;    
  }  

}

uint32_t g2quad::Read(std::string const & address){
  uint32_t val =0;
  Item const * item = addr_table.GetItem(address);
  if((0 == item->deviceID) || RPCMode){
    //Main board
    conn.Read_masked(&val,item->address,item->mask);
    val >>= item->offset;
  }else if(item->deviceID <= 4){
    //ADC Board = device ID
    val = ADCRead(item->deviceID,
		  addr_table.GetDeviceSubAddress(item->deviceID,item->address));    
    val &= item->mask;
    val >>= item->offset;
  }else{
    //Unknown device
    BUException::TRANSACTION_ERROR e;
    e.Append("Unknown sub-device\n");
    throw e;    
  }  
  
  return val;
} 
void     g2quad::Write(std::string const & address, uint32_t data){
  Item const * item = addr_table.GetItem(address);
  if((0 == item->deviceID) || RPCMode){
    //Main board
    data <<= item->offset;  
    conn.Write_masked(&data,item->address,item->mask);
  }else if(item->deviceID <= 4){
    //ADC Board = device ID

    //read
    uint32_t val = ADCRead(item->deviceID,
			   addr_table.GetDeviceSubAddress(item->deviceID,item->address));
    //modify
    val &= ~(item->mask);
    val |= (data << item->offset) &  (item->mask);
    //write
    ADCWrite(item->deviceID,
	     addr_table.GetDeviceSubAddress(item->deviceID,item->address),
	     val);
  }else{
    //Unknown device
    BUException::TRANSACTION_ERROR e;
    e.Append("Unknown sub-device\n");
    throw e;    
  }  





}

