#include <g2quad/g2quad.hh>

void g2quad::reset_monitered_pulser(std::vector<size_t> boardList){
   for(size_t iboard = 0; iboard < boardList.size(); iboard++){
      try{
         CheckBoardLink((int)boardList[iboard]);
         std::stringstream ss;
         ss << "ADCBOARD.";
         ss << boardList[iboard];
         std::string const & base = ss.str();
         Write((base+".ED.ARM"),1);
         Write((base+".FP_PULSER.RESET_INHIBIT"),1);
      } catch(BUException::ADC_NO_LOCK & e){}
   }
}
