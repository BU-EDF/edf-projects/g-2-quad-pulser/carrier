#include <g2quad/g2quad.hh>

void g2quad::ConfigurePulser(uint8_t boardID,          uint32_t mode,
			    uint32_t charge1_start,   uint32_t charge1_length,
			    uint32_t discharge_start, uint32_t discharge_length,
			    uint32_t charge2_start,   uint32_t charge2_length){

  CheckBoardRange(boardID);
  std::stringstream ss;
  ss << "ADCBOARD." << int(boardID) << ".FP_PULSER.";
  std::string const & base = ss.str();

  if(mode > 1){
    BUException::G2QUAD_BAD_ARGS e;
    e.Append("Invalid mode.\n");      
    throw e;        
  }

  uint32_t charge_step1_start;
  uint32_t charge_step1_end;
  uint32_t charge_step2_start;
  uint32_t charge_step2_end;
  uint32_t discharge_end;

  //figure out the real parameters
  if(0 == mode){
    //units of arguments are ns, registers are 10ns
    charge_step1_start = charge1_start/10;
    charge_step2_end = (charge1_start + charge1_length)/10;
    discharge_end   = (discharge_start + discharge_length)/10;
    discharge_start = discharge_start/10;
    charge_step1_end =0;
    charge_step2_start =0;
  }else{
    //units of arguments are ns, registers are 10ns
    charge_step1_start = (charge1_start                 )/10;
    charge_step1_end   = (charge1_start + charge1_length)/10;
    charge_step2_start = (charge2_start                 )/10;
    charge_step2_end   = (charge2_start + charge2_length)/10;
    
    discharge_end   = (discharge_start + discharge_length)/10;
    discharge_start = discharge_start/10;
  }
  
  //Write proposed parameters
  Write((base+"PROPOSED.ENABLE_2STEP"),    mode);
  Write((base+"PROPOSED.CHARGE_START"),    charge_step1_start);
  Write((base+"PROPOSED.CHARGE_END"),      charge_step2_end);
  Write((base+"PROPOSED.STEP2_START"),     charge_step2_start);
  Write((base+"PROPOSED.STEP1_END"),       charge_step1_end);
  Write((base+"PROPOSED.DISCHARGE_START"), discharge_start);
  Write((base+"PROPOSED.DISCHARGE_END"),   discharge_end);

  //CHeck status
  uint32_t status = Read((base+"STATUS"));
  if(0 == status){
    //Things are good, enable
    Write((base+"ENABLE"),1);
    //CHeck that the enabled worked
    uint32_t enabled = Read((base+"ENABLED"));
    if(0 == enabled){
      BUException::G2QUAD_BAD_ARGS e;
      e.Append("Unable to enable pulser\n");      
      throw e;    
    }
  }else{ 
    BUException::G2QUAD_BAD_ARGS e;
    std::stringstream ss;
    ss << "Proposed parameters did not pass error checking: " << std::hex << Read((base+"ERROR")) << "\n";
    e.Append(ss.str());      
    uint32_t errorCode = Read(base+"ERROR");
    char const* const ERROR_FLAGS[7] = {"charge_start or charge2_ start is zero\n",\
                                     "charge_start > 100000ns\n",\
                                     "charge1_duration is >800000 or <5000\n",\
                                     "charge1_end-charge2_start is > 2000\n",\
                                     "charge2_duration is <5000 or >300000000\n",\
                                     "discharge_start-charge2_end is >2000000\n",\
                                     "discharge_duration is >2000000 or <500000\n"};
    for(size_t iBit=0; iBit<7;iBit++){
        if(errorCode&(1<<iBit)){
            e.Append(ERROR_FLAGS[iBit]);
            fprintf(stderr,"%s",ERROR_FLAGS[iBit]);
        } 
    }
    throw e;        
  }
 }
