#include <g2quad/g2quad.hh>
#include <exception>

#include <string>       /* std::string */
#include <stdio.h>      /* printf, NULL */
#include <iostream>     /* std::cout */
#include <stdint.h>     /* uint32_t */
#include <stdlib.h>     /* strtoul */
#include <vector>       /* std::vector*/
#include <fstream> /* std::ifstream, std::ofstream, std::fstream  */

#define MIN_LINE_LENGTH 10 
#define MAX_LINE_LENGTH 266

void g2quad::ParseIHexLine(std::string line, std::vector <uint8_t> &data, uint32_t &upperAddr){
   if(line.empty()){
      BUException::PROGRAM_FLASH_ERROR e;
      e.Append("line is empty \n");
      throw e;
   }
   else if(line[0] != ':'){
      BUException::PROGRAM_FLASH_ERROR e;
      e.Append("BAD Line in file - does not start with ':'\n");
      e.Append(line.c_str());
      throw e;
   }else{
      line.erase(0,1);
   }
    
   if(line.size()<MIN_LINE_LENGTH){
      BUException::PROGRAM_FLASH_ERROR e;
      e.Append("BAD Line in file - incomplete intel hex format\n");
      e.Append(line.c_str());
      throw e;
    
   }else if(line.size()>MAX_LINE_LENGTH){
      BUException::PROGRAM_FLASH_ERROR e;
      e.Append("BAD Line in file - intel hex format not supported, contains too many characters\n");
      e.Append(line.c_str());
      throw e;
   }else if (line.size()&1){
      BUException::PROGRAM_FLASH_ERROR e;
      e.Append("BAD Line in file - uneven number of characters\n");
      e.Append(line.c_str());
      throw e;
   }else {
      char const *entry = line.c_str();
      uint32_t checksum = 0, line_sum = 0, byte_count = 0, address = 0, record_type = 0;
      
      //does the checksum of each Intel Hex Line
      uint32_t byte = 0;
      for(uint32_t iSum = 0; iSum < line.size()-2; iSum+=2){
         sscanf(entry+iSum, "%2x", &byte);
         line_sum += byte;
      }
      sscanf(entry+line.size()-2,"%2x",&checksum);
      //the content of the line is cancelled out through addition to its
      //two's complement which is the checksum if all data is intact
      line_sum += checksum; 
      
      if(0xFF&line_sum){
         //sum of data and checksum is not zero!!
         line_sum = 0xFF & line_sum;
         BUException::PROGRAM_FLASH_ERROR e;
         e.Append("BAD Line in file - content inconsistent with checksum.\n");
         throw e;
      } else{
         line_sum = 0;
      }
         
      sscanf(entry,"%2x %4x %2x ", &byte_count, &address, &record_type);
      if(4 == record_type){ //record_type 04 happens when upperAddr needs to be updated.
         upperAddr = 0;
         sscanf(entry+8,"%4x", &upperAddr);
         upperAddr = upperAddr << 16;
      } else if(0 == record_type){ //record_type 00 contains the data. 
         address = upperAddr+address;
         if(data.size() < (address+byte_count)){
            data.resize(address+byte_count,0xFF);
         }
         for(uint32_t iAddr = 0; iAddr < byte_count; iAddr++){
            uint32_t tempData = 0;
            sscanf(entry+(iAddr*2)+8,"%02x",&tempData);
            data[address+iAddr] = (uint8_t)tempData;
         }
         
      }
#ifdef DEBUG
   printf("check_sum is: %d \n",checksum);
   printf("byte_count:0x%02X address:0x%08X record_type:0x%02X \n",byte_count, address, record_type);
   printf("    data is: ");
   if(4 != record_type){
      for(uint32_t iaddr = 0 ; iaddr < byte_count; iaddr++){
         printf("%c%c %02X\n",*(entry+8+(iaddr*2)),*(entry+8+(iaddr*2)+1),data[iaddr+address]);
      }
   }
   printf("\n");
#endif
   }
}

void g2quad::LoadIHexFile(std::vector <uint8_t>&data, const char* file){
   std::string line;
   std::ifstream datafile(file);
   uint32_t upperAddr = 0;
   if(datafile.is_open()){
      while(getline(datafile, line)){
         //remove \r if exists (only for windows file)
         if(line[line.size()-1] == '\r'){
            line.erase(line[line.size()-1]);
         }
         ParseIHexLine(line, data, upperAddr);
      }
   } else{
      BUException::PROGRAM_FLASH_ERROR e;
      e.Append("Unable to open file ");
      e.Append(file);
      e.Append("\n");
      throw e;
   }
}

#define UPDATE_PERIOD 60
void g2quad::WriteFlash(std::vector<uint8_t> const &data,int board_id,bool printUpdate){
  ADCCommand(board_id,"\n");
  ADCCommand(board_id,"\n");
  ADCCommand(board_id,"\n");
  ADCCommand(board_id,"fi\n");
  ADCCommand(board_id,"fi\n");
  
  //First, erase flash
  if(printUpdate){
    fprintf(stderr,"Erasing flash\n");
  }
  ADCCommand(board_id,"fe\n");
  ADCCommand(board_id,"faw 000000\n");
  if(printUpdate){
    fprintf(stderr,"Flash erased\n");
    fprintf(stderr,"Starting programming\n");
  }
  //Write new data
  size_t posInData = 0;
  size_t posInDataUpdate = data.size()/UPDATE_PERIOD;
  
  int counter = 0;
  if(0 == data.size()){
    BUException::PROGRAM_FLASH_ERROR e;
    e.Append("data is empty. Nothing to write to the flash\n");
    throw e;
  }
   
  if(printUpdate){
    //Print update status bar
    fprintf(stderr,"[");      
    for(size_t iChar = 0; iChar < UPDATE_PERIOD;iChar++){
      fprintf(stderr," ");      
    }
    fprintf(stderr,"]\n");      
    fprintf(stderr," ");
  }
  const size_t cmdSize=26;
  char command[cmdSize+1];
  memset(command,0,cmdSize+1);//NULL terminator out command
  
  for(uint32_t iData = 0; iData < data.size();iData += 8){   
    char * cmdPtr      = command;
    int    cmdSizeLeft = cmdSize;
    int    wrCount     = 0;
    
    wrCount = snprintf(command, cmdSizeLeft, "fww %x ", counter);
    cmdPtr      += wrCount;
    cmdSizeLeft -= wrCount;
    for(int32_t count = 7; count >= 0; count--){      
      if(data.size() <= (iData+count)){
      	wrCount = snprintf(cmdPtr, cmdSizeLeft,"%02X", 0xFF);
      }else{
	      wrCount = snprintf(cmdPtr, cmdSizeLeft,"%02X",data[iData+count]);
      }
      
      cmdPtr      += wrCount;
      cmdSizeLeft -= wrCount;
      posInData++;
    }
      
    snprintf(cmdPtr,cmdSizeLeft,"\n");
    ADCCommand(board_id,command);
    counter++;
    if(16 == counter){
      counter = 0;
      ADCCommand(board_id,"fw\n");
    }


    if(printUpdate){
      if(posInData > posInDataUpdate){
	   fprintf(stderr,"=");
	   posInDataUpdate+=data.size()/UPDATE_PERIOD;
      }
    }
  }
   
  //Finish the block we are working on
  while(counter != 0){
    snprintf(command, cmdSize, "fww %2x FFFFFFFFFFFFFFFF\n",uint8_t(counter));
    ADCCommand(board_id,command);
    counter++;
    if(16 == counter){
      ADCCommand(board_id,"fw\n");
      counter = 0;
    }   
  }
  if(printUpdate){
    fprintf(stderr,"\n");
  }
}

void g2quad::ProgramFlash(int board_id,char const* file,bool printUpdate){
   std::vector <uint8_t> data;
   LoadIHexFile(data, file);
   WriteFlash(data,board_id,printUpdate);
}

#define HEX_CASE "X"
//update upperAddr when lowerAddr is back 0x0000
void updateUpperAddr(uint16_t &upperAddr,FILE *hexFile){
   uint8_t tempAdd = upperAddr & 0xFF;
   uint8_t tempAdd1 = (upperAddr & 0xFF00)>>8;
   uint8_t checksum = 0x0 -((6+tempAdd+tempAdd1)&0xFF);
   fprintf(hexFile,":02000004%04" HEX_CASE "%02" HEX_CASE "\n",upperAddr,checksum);
   upperAddr++;
}

//Take in a vector and turn it into a intel hex file.
void g2quad::convertToHex(std::vector<uint8_t> data, uint8_t ByteCount){
   if(0==data.size()){
      BUException::PROGRAM_FLASH_ERROR e;
      e.Append("Vector to convert into IntelHex is empty :(\n");
      throw e;
   }
   
   if(0==ByteCount){
      BUException::PROGRAM_FLASH_ERROR e;
      e.Append("ByteCount 0 is invalid -__-\n");
      throw e;
   }
   
   uint8_t checkByteCount = 0;
   int dataCount = 0;
   uint16_t lowerAddr = 0x0000;
   uint16_t upperAddr = 0x0000;
   uint8_t checksum = 0;
   
   int dataSize = 266;
   char dataStr[dataSize];
   memset(dataStr,0,dataSize);//NULL terminator out command
   char *dataPtr = dataStr;
   
   FILE *hexFile;
   hexFile = fopen("hexFile.txt","w");
   for(size_t iData=0; iData<data.size();iData++){
      if(checkByteCount == ByteCount){ //construct the Intel Hex line and save it to a file
         checksum = 0x0 -(checksum&0xFF); 
         fprintf(hexFile,":%02" HEX_CASE "%04" HEX_CASE "00",ByteCount,(lowerAddr-ByteCount)&0xFFFF);
         fprintf(hexFile,"%s%02" HEX_CASE "\n",dataStr,checksum);
         checksum = 0;
         checkByteCount = 0;
         dataCount = 0;
         dataSize = 266;
         memset(dataStr,0,dataSize);//NULL terminator out command
         dataPtr = dataStr;
      }
      //update upperAddr
      if(0==lowerAddr ){
         updateUpperAddr(upperAddr,hexFile);
      }
      
      //update checksum for new Intel hex line
      if(0==checkByteCount){
         uint8_t tempAdd = lowerAddr & 0xFF;
         uint8_t tempAdd1 = (lowerAddr & 0xFF00)>>8;
         checksum = ByteCount +tempAdd + tempAdd1;
      }
      
      dataCount = snprintf(dataPtr,dataSize,"%02" HEX_CASE,data[iData]);
      dataPtr += dataCount;
      dataSize -= dataCount;
      
      checksum += data[iData];
      lowerAddr++;
      checkByteCount++;
   }
   
   //Add in the last two lines of the Intel Hex and save it the file.
   checksum = checksum - ByteCount + checkByteCount;
   checksum = 0x0 -(checksum&0xFF);
   lowerAddr = (lowerAddr-checkByteCount)&0xFFFF;
   fprintf(hexFile,":%02" HEX_CASE "%04" HEX_CASE "00%s%02" HEX_CASE "\n",checkByteCount,lowerAddr,dataStr,checksum);
   fprintf(hexFile,":00000001FF\n"); //Intel Hex line to end a file.
   fclose(hexFile);
}
