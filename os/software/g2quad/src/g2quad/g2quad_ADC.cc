/* 
Rev log
===============================
* 2017-12-22 (Dan Gastler)
- File created.
- Splitting code for g2quad specific things from the more generic PL Interface code
- Added the ADCConfig code to setup ADCs 1&2 on each board (still no bit phase lock-on)
* 2018-01-03 (Dan Gastler
- Added lock code
- Added ADCConfigRegWrite to simplify the ADC reg writes
- Fixed initboard to not leave receive channel in data reset
*/

#include <g2quad/g2quad.hh>
#include <exception>


uint32_t  g2quad::ADCRead  (int board_id, uint16_t address){
  CheckBoardRange(board_id);
  
  char command[] = "dr 0000\n";
  snprintf(command+3, //staring position of the address
	   6, //size of the address (4 ascii chars) plus '\n' and the null terminator
	   "%04X\n",address);
  std::vector<std::string> response = ADCCommand(board_id,command);

  uint32_t ret = 0;
  if(response.size() > 0){
    std::string reg_value = response[0].substr(7);
    ret = strtoul(reg_value.c_str(),NULL,16);
  }
  return ret;			   
}

void  g2quad::ADCWrite  (int board_id, uint16_t address, uint32_t data){
  CheckBoardRange(board_id);
  char command[] = "dw 0000 12345678\n";
  snprintf(command+3, //staring position of the address
	   15, //size of the address (4 ascii chars) plus space, plus data (8chars) plus '\n' and the null terminator
	   "%04X %08X\n",address,data);  
  ADCCommand(board_id,command);
}


//Must end in a "\n"
std::vector<std::string> g2quad::ADCCommand(int board_id, std::string const & cmd){
  CheckBoardLink(board_id);
  
  uint16_t write_address = subDeviceWriteAddr[board_id-1];
  uint16_t read_address  = subDeviceReadAddr[board_id-1];

  
  //Clear text buffer
  while(Read(read_address)&0x100 ){
    //This will read until there are no more valid characters
  }
  
  //Write command to ADCBoard
  for(size_t iChar = 0; iChar < cmd.size();){
    //check if there is buffer space
    if(Read(write_address) & 0x100){
      Write(write_address,cmd[iChar]&0x7F);
      //readout until we get the character back
      while((Read(read_address)&0x100) == 0x000){
	usleep(1000);
	//we could check the returned value, but we'll skip that for now
      }
      iChar++;
    }
  }

  //Get response
  std::vector<std::string> ret;
  uint32_t val;
  bool processing = true;
  while(processing){
    val = Read(read_address);
    if(val&0x100){
      //Valid data
      switch ( char(val) ) {
      case '\r':
	//ignore
	break;
      case '\n':
	//create new line in ret
	ret.push_back(std::string(""));
	break;
      case '>':
	//we are done processing data, break out of the loop
	processing = false;
	break;
      default:
	//Add character
	if(0 == ret.size()){
	  ret.push_back(std::string());
	}
	ret.back().push_back((char)(val));
	break;
      }
    }
  }
  return ret;
}

void g2quad::CheckBoardRange(int board_id){

  if ((board_id < 1) || (board_id > 4)){
    BUException::ADC_OUT_OF_RANGE e;
    e.Append("Bad ADC Board number");
    throw e;    
  }

}

void  g2quad::InitBoardLink(int board_id){
  CheckBoardRange(board_id);
  //Get read and write address for the ADC board
  std::stringstream ss;
  ss << "LINK_CTRL." << board_id << ".COMM.TX_DATA";
  uint16_t write_address = subDeviceWriteAddr[board_id-1] = addr_table.GetItem(ss.str())->address;
  ss.str("");
  ss << "LINK_CTRL." << board_id << ".COMM.RX_DATA";
  subDeviceReadAddr[board_id-1] = addr_table.GetItem(ss.str())->address;

  //REset the transcievers. 
  ResetBoardConnection(board_id);


  //Force the ADC board to listen to the HDMI uart
  Write(write_address,char(0x1d));
  usleep(10000);
  Write(write_address,char(0x1d));
  usleep(10000);
  Write(write_address,char(0x1d));
  usleep(10000);
  //Clear any pending commands
  ADCCommand(board_id,"\n");
  usleep(10000);
  ADCCommand(board_id,"\n");
  usleep(10000);
  ADCCommand(board_id,"\n");
  usleep(100000);

  //Clear text buffer
  while(Read(subDeviceReadAddr[board_id-1])&0x100 ){
    //This will read until there are no more valid characters
  }
  
  //Set this as configured. (board_id-1 is valid since checkBoard range already called)
  subDeviceValid[board_id-1] = true;   
}


void  g2quad::CheckBoardLink(int board_id){

  std::stringstream ss;
  ss << "LINK_CTRL." << board_id << ".IO.RX_LOCKED";

  CheckBoardRange(board_id);
  if(!(Read(ss.str()))){
    //We are not connected, try reconnecting
    if(!ResetBoardConnection(board_id)){
      BUException::ADC_NO_LOCK e;
      char msg[] = "Board 0";
      msg[6] = '0' + (board_id&0x7);
      e.Append(msg);
      throw e;
    }
  }  
}

bool g2quad::ResetBoardConnection(int board_id){
  CheckBoardRange(board_id);

  //We now have a valid board ID 
  std::stringstream ssPwr,ssIO;
  ssPwr << "PWR_EN." << board_id;
  ssIO << "LINK_CTRL." << board_id << ".IO";


  int errorCount = 0;
  for(int iPwrCycle = 0; iPwrCycle < 4;iPwrCycle++){
    //Power cycle the board
    Write(ssPwr.str(),0x0);
    
    // we are off, stay off long enough to force the FPGA off
    usleep(100000);
    //Reenable the power
    Write(ssPwr.str(),0x1);
    usleep(3000000); //sleep to wait for the board to power up

    //Reset the carrier links
    Write(ssIO.str(),0xF);
    usleep(10000);
    Write(ssIO.str(),0xB);
    usleep(10000);
    Write(ssIO.str(),0x3);
    usleep(10000);
    Write(ssIO.str(),0x2);
    usleep(10000);
    Write(ssIO.str(),0x0);  
    
    //Wait up to 5 seconds for the board to come up.
    int attempts = 0;
    bool keepTrying = true;
    do{
      if(attempts > 500){  // 10seconds/200ms
	BUException::ADC_NO_LOCK e;
	e.Append(ssPwr.str().c_str());
	e.Append(" failed to lock!\n");
	throw e;
      }
      usleep(100000);
      attempts++;
      //reset the rx link
      Write(ssIO.str(),0x3);
      usleep(10000);
      Write(ssIO.str(),0x2);
      usleep(10000);
      Write(ssIO.str(),0x0);  
      usleep(100000);

      keepTrying = false;
      for(size_t iLocked = 0; iLocked < 10;iLocked++){
	if(0 == Read(ssIO.str()+".RX_LOCKED")){
	    keepTrying = true;
	  }
	  usleep(10000);
      }
      
      }while(keepTrying);

    //reset the error counter
    Write(ssIO.str()+".RX_CODE_ERROR",1);
    Write(ssIO.str()+".RX_DISPARITY_ERROR",1);
    usleep(1000000); //Wait a bit for errors to accumulate.
    errorCount =  Read(ssIO.str()+".RX_CODE_ERROR");
    errorCount += Read(ssIO.str()+".RX_DISPARITY_ERROR");
    if(0 == errorCount){
      break;
    }
  }

  if(errorCount){
    //Non zero error count after several tries.
    BUException::ADC_NO_LOCK e;
    e.Append(ssPwr.str().c_str());
    e.Append(" failed to lock!\n");
    throw e;    
  }
  
//  //We are
//
//  //reset evertying  
//  Write(ssIO.str(),0xF);  
//  usleep(10000); //sleep 10ms
//
//  //Enable TX clock output  
//  Write(ssIO.str(),0xB);
//  usleep(100000); //sleep 10ms (lockon should be much faster than this)
//
//  //Enable TX data output
//  Write(ssIO.str(),0x3);
//  usleep(100000); //sleep 10ms (should be ~20 * 10 200Mhz clock ticks: ~1uS)
//
//  //Enable RX clock lockon
//  Write(ssIO.str(),0x2);
//  usleep(100000); //sleep 10ms (lockon should be quick)
//
//  //Enable RX data lockon
//  Write(ssIO.str(),0x0);
//  usleep(200000); //sleep 20ms (should be ~20*10*100Mhz clock ticks ~2uS)


//  //back to no resets
//  Write(addressIO, 0);
//  usleep(200000); //sleep 20ms (should be ~20*10*100Mhz clock ticks ~2uS)

  //Return if the RX is now locked
  return Read(ssIO.str()+".RX_LOCKED");
}

void g2quad::CheckADCRange(int ADC){
  if( (ADC < 1) || (ADC > 2)){
    BUException::ADC_CHIP_OUT_OF_RANGE e;
    e.Append("Bad ADC chip number");
    throw e;        
  }
}

std::string g2quad::GetADCBoardBase(int board_id){
  CheckBoardRange(board_id);
  std::string adcBase("ADCBOARD.0");
  adcBase[9] += char(board_id&0x7); //Add 1-4 to char '0' to get char '1' - '4'
  return adcBase;
}

std::string g2quad::GetADCBoardADCBase(int board_id, int ADC){
  std::string adcBase = GetADCBoardBase(board_id);
  CheckADCRange(ADC);
  adcBase+=".ADC0";
  adcBase[14] += char(ADC&0x7); //Add 1-4 to char '0' to get char '1' - '4'
  return adcBase;
}

void g2quad::ADCConfigRegWrite(int board_id,int ADC,uint8_t reg_addr,uint16_t data){
  //Validate board/ADC numbers and get the address table name
  std::string adc = GetADCBoardADCBase(board_id,ADC);
  
  Write(adc+".CONFIG.DATA",data);
  Write(adc+".CONFIG.ADDRESS",reg_addr);
  Write(adc+".CONFIG.WRITE",1);
  //  usleep(100000);//sleep 1ms
}

void g2quad::ConfigADCs(int board_id){
  //=========================================================
  //ADC 1&2 config
  for(int iADC = 1; iADC <= 2; iADC++){

    //Validate the board/ADC number and get the address table base name
    std::string adc_reg = GetADCBoardADCBase(board_id,iADC);

    //Disable ADC capture
    Write(adc_reg+".CONTROL.ENABLE",0);
    
    //reset the ADC config
    Write(adc_reg+".CONFIG.RESET",1);
    usleep(100000);//sleep 1ms
    
    //power down
    ADCConfigRegWrite(board_id,iADC,0x0,0x001); //Write 0x001 (11-bits) to register 0x0 (5-bits)
    //  ADCWrite(board_id,ADDR_ADC_1_CONFIG,0x11); 
    //    usleep(100000);//sleep 1ms

    //power up
    ADCConfigRegWrite(board_id,iADC,0x0,0x000); //Write 0x000 (11-bits) to register 0x0 (5-bits)
    //  ADCWrite(board_id,ADDR_ADC_1_CONFIG,0x1); 
    //    usleep(100000);//sleep 1ms

    //input clock buffer gain (default)
    ADCConfigRegWrite(board_id,iADC,0x4,0x000); //Write 0x000 (11-bits) to register 0x4 (5-bits)
    //  ADCWrite(board_id,ADDR_ADC_1_CONFIG,0x40001); 
    //    usleep(100000);//sleep 1ms

    //Sample config
    uint16_t data = ((1   << 10) |  // unsigned samples
		     (0x0 <<  5));  // test pattern
    ADCConfigRegWrite(board_id,iADC,0xA,data);
    //    usleep(100000);//sleep 1ms

    //Fixed sample value
    uint16_t fixed_sample = 0;
    data = (fixed_sample&0x7FF); //LSB11 bits
    ADCConfigRegWrite(board_id,iADC,0xB,data);
    //    usleep(100000);//sleep 1ms

    //Fixed sample and gain control
    data = ((0x0 << 8) |  // Gain control
	    (fixed_sample >> 11)); //MSB 
    ADCConfigRegWrite(board_id,iADC,0xC,data);
    //    usleep(100000);//sleep 1ms


    //misc data config
    data = ((0x1 <<  0) | // two wire interface
	    (0x0 <<  1) | // ddr /sdr bit clock
	    (0x0 <<  2) | // 14/16 bit serialize
	    (0x0 <<  4) | // capture clock edge
	    (0x0 <<  5) | // coarse gain enable
	    (0x1 <<  6) | // msb or lsb first
	    (0x0 <<  7) | // byte/bit wise
	    (0x1 << 10)); // override bit
    ADCConfigRegWrite(board_id,iADC,0xD,data);
    //    usleep(100000);//sleep 1ms

    //misc data config
    data = ((0x3 <<  0) | // lvds current double
	    (0x0 <<  2) | // lvds current settings
	    (0x5 <<  6)); // lvds internal termination bit
    ADCConfigRegWrite(board_id,iADC,0x10,data);
    //    usleep(100000);//sleep 1ms

    //misc data config
    data = ((0x5 <<  0) | // lvds output termination
	    (0x0 <<  9)); // byte wise/bitwise output
    ADCConfigRegWrite(board_id,iADC,0x11,data);
    //    usleep(100000);//sleep 1ms
    
    //=========================================================
    //Enable ADCs for data taking
    Write(adc_reg+".CONTROL.ENABLE",1);
  }

}

void g2quad::AlignADCChannel(int board_id, int channel){
  //validate board_id
  CheckBoardRange(board_id);

  //Validate channel
  if( (channel < 1) || (channel > 8)){
    BUException::ADC_CHANNEL_OUT_OF_RANGE e;
    e.Append("From AlignADCCHannel");
    throw e;        
  }

  //Get control address for this channel
  //  uint16_t adc_address;
  int iADC = 0;
  if(channel > 4){
    channel -= 4; //Use channel number 1-4 on ADC 2 instead of channel number 5-8
    iADC = 2;
  }else{
    iADC = 1;
  }
  std::string chBase = GetADCBoardADCBase(board_id,iADC);
  chBase+=".CH0";
  chBase[18]+= char(channel & 0xf);
  /* printf("%s\n",chBase.c_str()); */
//  uint16_t sample_address = (adc_address +  //Correct adc offset
//			     0x30        +  //Offset to channels
//			     channel);      //offset to channel
//  uint16_t bitslip_address = (adc_address +  //Correct adc offset
//			      0x10);         //Offset to bitslip reg

  //Set the ADC to pattern mode
  ADCConfigRegWrite(board_id,iADC,0xa,0xa0);  
  
  //begin bitslip on the LSBs link of the ADC channel
  // Do this by setting the lsb of the ADC test pattern to '1' and align bit zero to that.
  ADCConfigRegWrite(board_id,iADC,0xb,0x1);
  ADCConfigRegWrite(board_id,iADC,0xc,0x0);  

  for(int iSlip = 0; iSlip < 6; iSlip++){
    //take a sample
    Write(chBase+".CAPTURE",0x1);
    uint32_t sample = Read(chBase+".SAMPLE");//ADCRead(board_id,sample_address);
    //    sample >>= 16; //Read the sample and shift it to bit zero    
    //printf("%d: 0x%08X\n",iSlip,sample&0x3f);
    if ((sample&0x3f) != 0x001){
      //We are in the wrong alignment, do a bitslip
      Write(chBase+".BITSLIP_A",0x1);
      //      ADCWrite(board_id,bitslip_address,0x1<<((channel-1)*2));
    }else{
      //We are aligned, break
      break;
    }
  }

  //begin bitslip on the MSBs link of the ADC channel
  // Do this by setting bit 6 of the ADC test pattern to '1' and align bit six to that.
  ADCConfigRegWrite(board_id,iADC,0xb,0x40);
  ADCConfigRegWrite(board_id,iADC,0xc,0x0);  

  for(int iSlip = 0; iSlip < 6; iSlip++){
    Write(chBase+".CAPTURE",0x1);
    //ADCWrite(board_id,sample_address,0x1);
    uint32_t sample = Read(chBase+".SAMPLE");//ADCRead(board_id,sample_address);
    //uint32_t sample = ADCRead(board_id,sample_address);
    //    sample >>=22; //Read and shift bit 6 (located at bit 22) down to bit 0
    sample >>=6; //Read and shift bit 6 (located at bit 22) down to bit 0
    /* printf("%d: 0x%08X\n",iSlip,sample&0x3f); */
    if ((sample&0x3f) != 0x001){
      //We are in the wrong alignment, do a bitslip
      Write(chBase+".BITSLIP_B",0x1);
      //      ADCWrite(board_id,bitslip_address,0x1<<(((channel-1)*2)+1));
    }else{
      //We are aligned, break
      break;
    }
  }

  
  //Test configuration by shifting a bit through all 12 positions on the ADC side and see if it locks on correctly.
  //MAKE THIS MORE ROBUST
    //MAKE THIS MORE ROBUST
    //MAKE THIS MORE ROBUST
    //MAKE THIS MORE ROBUST
    //MAKE THIS MORE ROBUST
    //MAKE THIS MORE ROBUST
    //MAKE THIS MORE ROBUST
    //MAKE THIS MORE ROBUST
  //quick print test
  for(int i = 0; i < 12; i++){
    uint16_t test_value = 0x1 << i;

    //Fixed sample value
    ADCConfigRegWrite(board_id,iADC,0xB, (test_value & 0x7FF) );
    //    usleep(100000);//sleep 1ms

    ADCConfigRegWrite(board_id,iADC,0xC, (test_value & 0x800) >>11);
    //    usleep(100000);//sleep 1ms

    Write(chBase+".CAPTURE",0x1);
    //ADCWrite(board_id,sample_address,0x1);

    uint16_t read_value = Read(chBase+".SAMPLE");//ADCRead(board_id,sample_address);
    //    uint16_t read_value = ADCRead(board_id,sample_address)>>16;
    //    usleep(100000);//sleep 1ms
    if(test_value != (read_value&0xFFF)){
      //printf("%03X %03X(%d)\n",test_value,read_value&0xFFF,read_value >> 12);   
      BUException::TRANSACTION_ERROR e; 
      e.Append("ADC channel not aligned properly.\n");
      throw e;
    }
  }

  //Set the ADC to pattern back to normal
  //ADCConfigRegWrite(board_id,iADC,0xa,0x00);  //2s complement
  ADCConfigRegWrite(board_id,iADC,0xa,0x200);      //unsigned
  
}


////std::vector<uint16_t> g2quad::ReadoutADCChannel(int board_id, int channel){
////  //validate board_id
////  CheckBoardRange(board_id);
////
////  //Validate channel
////  if( (channel < 1) || (channel > 8)){
////    BUException::ADC_CHANNEL_OUT_OF_RANGE e;
////    e.Append("From AlignADCCHannel");
////    throw e;        
////  }
////
////  //Get to the correct base address for readout
////  uint16_t adc_address;
////  if(channel > 4){
////    adc_address = ADDR_ADC_2_BASIC_READOUT;
////    channel -= 4; //Use channel number 1-4 on ADC 2 instead of channel number 5-8
////  }else{
////    adc_address = ADDR_ADC_1_BASIC_READOUT;
////  }
////  //Add channel offset to ADC basic readout.
////  adc_address += channel;
////
////  //Start a capture
////  ADCWrite(board_id,adc_address,0x2);
////  usleep(100000);//sleep 1ms
////
////  //Wait for capture to finish
////  printf("Wait for capture to finish\n");
////  while(ADCRead(board_id,adc_address) & 0x20){ //wait for empty flag to turn off
////    usleep(100000);//sleep 1ms
////  }
////
////  printf("Capture complete. Reading out.\n");
////  std::vector<uint16_t> data;
////  while( ! (ADCRead(board_id,adc_address) & 0x20) ){ // Wait for empty flag to turn on
////    uint32_t reg = ADCRead(board_id,adc_address);
////    if( reg & 0x100000 ){ // the FIFO had valid data on the output
////      data.push_back((reg>>8)&0xFFF); // get the 12 bit sample out of the 32 reg
////    }    
////    //Ack that we read this.  This gets the next word
////    ADCWrite(board_id,adc_address,0x10);
////  }
////  return data;
////}
