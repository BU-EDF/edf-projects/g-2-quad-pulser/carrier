#include <g2quad/g2quad.hh>
#include <tclap/CmdLine.h> /*TCLAP*/
#include <string> /*std::string*/
#include <stdio.h> /*printf*/

int main(int argc, char* argv[]){
   std::string file_name = "";
   std::string address = "";
   std::string table_name = "";
   int board_id = 0;
   try {
      TCLAP::CmdLine cmd("Code for programming the flash",' ', "program_flash v1.0");
      //-------------------------------------------------------
      TCLAP::ValueArg<std::string> fileName("f",              //one char flag
                     "file_name",         // full flag name
                     "name of file",//description
                     true,            //required
                     std::string(""),  //Default is empty
                     "string",         // type
                     cmd);
                     
      TCLAP::ValueArg<int> boardID("b",              //one char flag
                    "BoardID",         // full flag name
                     "ID number of the board",//description
                     true,            //required
                     0,  //Default is empty
                     "integer",         // type
                     cmd);
                     
      TCLAP::ValueArg<std::string> ADDRESS("a",              //one char flag
                     "address",         // full flag name
                     "address",//description
                     true,            //required
                     std::string(""),  //Default is empty
                     "string",         // type
                     cmd);
                  
      TCLAP::ValueArg<std::string> tableName("t",              //one char flag
                     "table_name",         // full flag name
                     "table name",//description
                     false,            //required
                     std::string(""),  //Default is empty
                     "string",         // type
                     cmd);
      
      //Parse the command line arguments
      cmd.parse(argc,argv);
      file_name = fileName.getValue();
      address = ADDRESS.getValue();
      table_name = tableName.getValue();
      board_id = boardID.getValue();
     
   } catch(TCLAP::ArgException &e) {
      fprintf(stderr, "Error %s for arg %s\n",e.error().c_str(), e.argId().c_str());
      return -1;
   }


   //parse args
   if("" == table_name){
     if(NULL != getenv("G2QUAD_ADDRESS_TABLE")){
         table_name = getenv("G2QUAD_ADDRESS_TABLE");
     }
   }

   g2quad *hw_interface = new g2quad(table_name,address);
   printf("   success\n");
   hw_interface-> InitBoardLink(board_id);
   hw_interface->CheckBoardLink(board_id);
   hw_interface->ProgramFlash(board_id,file_name.c_str(),true);
   return 0; 

}
