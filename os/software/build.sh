#!/bin/bash

#BUILD TOOL AND DEVICE
export PLATFORM=linux-native

cd BUTool
./setup.sh
source env.sh
make clean
make
cd ..
cd g2quad
./setup.sh
source env.sh
make clean
make
cd ..
cd g2quad_device
./setup.sh
source env.sh
make clean
make
cd ..
