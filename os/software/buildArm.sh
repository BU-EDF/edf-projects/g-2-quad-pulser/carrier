#!/bin/bash

#BUILD PL bridge
cd zynq_pl_bridge
make clean
make
cd ..
#BUILD zynq_daq
cd zynq_daq
make clean
make
cd ..

#BUILD TOOL AND DEVICE
export PLATFORM=petalinux

cd BUTool
./setup.sh
source env.sh
make clean
make
make install
cd ..
cd g2quad
./setup.sh
source env.sh
make clean
make
make install
cd ..
cd g2quad_device
./setup.sh
source env.sh
make clean
make
make install
cd ..
