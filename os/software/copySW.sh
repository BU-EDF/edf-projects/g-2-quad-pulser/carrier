#!/bin/bash
#IP=192.168.10.209
IP=192.168.10.236

scp -r \
    BUTool/install/work/* \
    g2quad/install/work/* \
    g2quad_device/install/work/* \
    zynq_pl_bridge/pl_server \
    zynq_daq/shutdown_dma_ctrl zynq_daq/client_test \
    zynq_daq/dma_ctrl zynq_daq/dma_test zynq_daq/memtest \
    root@$IP:/work
