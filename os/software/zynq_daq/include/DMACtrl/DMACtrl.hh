#ifndef __DMA_CTRL_HH__
#define __DMA_CTRL_HH__

#include <stdio.h> //size_t
#include <stdint.h> //uint32_t
#include <list> // std::list
#include <queue> //std::queue

#include <sys/un.h>

#include <SM/SMController.hh>

#include <DMAMesg/DMAMesgServer.hh>

#include <sys/select.h>

class DMACtrl : public SMController {
public:
  DMACtrl(size_t _maxFrameSize,size_t _frameBufferCount);
  void Run();
private:
  DMACtrl();//do not implement
  void ProcessClient();
  int TimeoutCheck();
  int BlockOnDMA(uint32_t * ptr);
  
  //SMController interface
  //Frame interface to users
  int maxFrameSize;
  size_t frameBufferCount;
  int userFrame;
  std::list<Frame_t> frames;
  uint32_t throwAwaySpace[DMA_BLOCK_SIZE];
  std::queue<int> emptyBlocks;
  
  uint32_t * startPtr;
  int iEnd;
  int iFrame;

  //Socket interface
  DMAMesgServer unSock;
  
  //DMA interface
  int dmaFD;

  bool runClient;
  bool runServer;

  //stat counters
  uint32_t statReturnedFrameCount;    
  uint32_t statDeliveredFrameCount;
  uint32_t statAvailableRequestCount;
  uint32_t statBlockOnDMACount;
  uint32_t statBlockOnDMACheckReturn;
  uint32_t statValidHeader;
  uint32_t statBoard1Count;
  uint32_t statBoard2Count;
  uint32_t statBoard3Count;
  uint32_t statBoard4Count;
  uint32_t statBoardUnknownCount;
  uint32_t statFinishedFrames;
  uint32_t statBlockThrownAway;
  uint32_t statIdleBlockCount;
  
  //error counters
  uint32_t errorFrameTooBig;
  uint32_t errorBadDMAInFrame;
  uint32_t errorFrameIncomplete;
  uint32_t errorBadFrameStartMagic;
  uint32_t errorBadDMAAtFrameStart;
  uint32_t errorBadReturnFrame;
};
#endif
