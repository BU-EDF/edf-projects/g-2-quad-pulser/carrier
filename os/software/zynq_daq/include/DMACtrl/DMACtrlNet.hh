#ifndef __DMA_CTRL_NET_HH__
#define __DMA_CTRL_NET_HH__

#include <stdio.h> //size_t
#include <stdint.h> //uint32_t

#include <sys/select.h>

#define DMA_BLOCK_SIZE 256

class DMACtrlNet {
public:
  DMACtrlNet(size_t _maxFrameSize);
  void Run();
private:
  DMACtrlNet();//do not implement
  void ProcessClient();
  int TimeoutCheck();
  int BlockOnDMA(uint32_t * ptr);
  
  //Frame interface to users
  int maxFrameSize;
  
  uint32_t throwAwaySpace[DMA_BLOCK_SIZE];  
  
  uint32_t * dataPtr;
  int frameBufferCount;
  int iFrame;

  sigset_t ignore_mask;
  sigset_t pending_mask;
  
  //DMA interface
  int dmaFD;

  bool runClient;
  bool runServer;

  int listenFD;
  int clientFD;

};
#endif
