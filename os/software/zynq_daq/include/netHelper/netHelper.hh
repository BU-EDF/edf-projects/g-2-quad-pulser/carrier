#ifndef __NETHELPER__
#define __NETHELPER__

#include <stdint.h>

void setShutdown(bool _shutdown);

int32_t writeN(int socketFD,
	       uint8_t * ptr,
	       uint32_t size);

int32_t readN(int socketFD,
	       uint8_t * ptr,
	       uint32_t maxSize);

#endif
