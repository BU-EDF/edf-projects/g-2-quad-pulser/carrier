#ifndef __DMA_MESG_SERVER_HH__
#define __DMA_MESG_SERVER_HH__

#include <string>
#include <DMAMesg/DMAMesgComm.hh>
#include <sys/select.h>
#include <sys/un.h> //sockaddr_un

class DMAMesgServer{
public:
  DMAMesgServer();
  ~DMAMesgServer();
  bool Open(std::string const & _path);
  void Close();
  std::string const & GetPath();

  bool ListenForClient(int timeout_ms /*ms*/);
  void CloseClient();
  
  int SendMessage(message_t * mesg);
  int SendMessage(uint16_t type, uint8_t * mesgData, size_t mesgDataSize);

  int MessagePending();
  int GetMessage(message_t * & mesg);

private:
  std::string path;
  //interace for users to get frame pointers
  int listenFD;
  int clientFD;  //This server only ever has one client.
  struct sockaddr_un sa_unix;
  struct sockaddr_un sa_client;
  int fdEnd; //for pselect
  fd_set fdReadMask; //for pselect
};
#endif
