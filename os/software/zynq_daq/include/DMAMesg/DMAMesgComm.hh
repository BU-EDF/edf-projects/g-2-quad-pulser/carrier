#ifndef __DMA_COMM_HH__
#define __DMA_COMM_HH__

#include <stdio.h> //size_t
#include <stdint.h> //uint32_t

#define DMA_BLOCK_SIZE 256
#define G2QUAD_DATA_SOF (0xA5)

struct Frame_t{
  int32_t  data_block;
  uint32_t size; //32bit words
  uint32_t boardID;
  uint32_t spillID;
};

struct Setup_t{
  uint32_t memorySize; // bytes
  uint32_t blockSize; // uint32_t words
  char path[]; //NULL TERMINATED
};

struct Status_t{
  uint32_t type: 8;
  uint32_t reserved: 24;
  uint32_t data;
};

struct DMAStat_t{
  uint32_t ver: 8;
  uint32_t reserved: 24;
  uint32_t errorFrameTooBig;
  uint32_t errorBadDMAInFrame;
  uint32_t errorFrameIncomplete;
  uint32_t errorBadFrameStartMagic;
  uint32_t errorBadDMAAtFrameStart;
  uint32_t errorBadReturnFrame;
  uint32_t statReturnedFrameCount;
  uint32_t statDeliveredFrameCount;
  uint32_t statAvailableRequestCount;
  uint32_t statBlockOnDMACount;
  uint32_t statBlockOnDMACheckReturn;
  uint32_t statValidHeader;
  uint32_t statBoard1Count;
  uint32_t statBoard2Count;
  uint32_t statBoard3Count;
  uint32_t statBoard4Count;
  uint32_t statBoardUnknownCount;
  uint32_t statFinishedFrames;
  uint32_t statBlockThrownAway;
  uint32_t statIdleBlockCount;
  uint32_t statEmptyBlockCount;
  uint32_t statActiveFrameCount;
};

//Message Types
#define MT_FRAME 0 
#define MT_CLOSE 1
#define MT_SHUTDOWN 2
#define MT_REQUEST_FRAME 3
#define MT_GET_SM_INFO 4
#define MT_SM_INFO 5
#define MT_GET_AVAILABLE 6
#define MT_GET_DMA_STATS 7
#define MT_UNKNOWN 8

struct message_t {
  uint16_t size; //bytes
  uint16_t type;
  uint8_t  data[];
};

#endif
