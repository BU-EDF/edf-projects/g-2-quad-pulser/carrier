#include <stdint.h>
#include <sys/types.h>
#include <DMAMesg/DMAMesgComm.hh>

namespace DMAMesgCommon{
  int  SendMessage(int fd,uint16_t type, uint8_t * mesgData, size_t mesgDataSize /*bytes*/);
  int  SendMessage(int fd,message_t * mesg);
  int  MessagePending(int fd); //negative is error, 0 false, positive message pending
  int  GetMessage(int fd,message_t * & mesg);
  void SetShutdown(bool _shutdown);
}
