#ifndef __DMA_MESG_CLIENT_HH__
#define __DMA_MESG_CLIENT_HH__

#include <string>
#include <DMAMesg/DMAMesgComm.hh>

class DMAMesgClient{
public:
  DMAMesgClient();
  bool Connect(std::string const & path);
  void SetShutdown(bool _shutdown);
  void Disconnect();

  int SendMessage(message_t * mesg);
  int SendMessage(uint16_t type, uint8_t * mesgData, size_t mesgDataSize);

  int MessagePending();
  int GetMessage(message_t * & mesg);

  Setup_t const * GetSMInfo();
private:
  bool RequestSMInfo();
  int fd;
  Setup_t * SMInfo;
};
#endif
