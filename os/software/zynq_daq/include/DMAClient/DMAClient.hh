#ifndef __DMA_CLIENT_HH__
#define __DMA_CLIENT_HH__

#include <DMAMesg/DMAMesgClient.hh>
#include <SM/SMClient.hh>

class DMAClient{
public:
  DMAClient();
  ~DMAClient();
  int Connect();
  void Disconnect();

  int32_t GetAvailableFrames();
  DMAStat_t GetStats();
  
  Frame_t GetFrame();
  uint32_t const * GetFrameData(int32_t iDataBlock);
  uint32_t const * GetFrameData(Frame_t const & frame);  
  void ReturnFrame(Frame_t * frame);
private:
  DMAMesgClient * dmaMsg;
  SMClient * sm;
};
#endif
