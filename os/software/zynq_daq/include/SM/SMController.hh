#ifndef __SM_CONTROLLER_HPP__
#define __SM_CONTROLLER_HPP__ 1

#include <string>

class SMController{
public:
  SMController();
  ~SMController();
  bool Running();
  int  SetShareName(std::string const & name);
  std::string GetShareName();
  int  SetGroupName(std::string const & name);
  std::string GetGroupName();

  int SetSize(size_t size);
  int GetSize();

protected:
  void Create();
  void Destroy();
  char * GetMemory();
private:
  //Shared memory info
  int fdSM;
  std::string shareName;
  std::string groupName; //group given read permission for shared mm

  char * sharedMemory;
  size_t sharedMemorySize;//bytes
};

#endif
