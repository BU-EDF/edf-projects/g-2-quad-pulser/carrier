#ifndef __SM_CLIENT_HPP__
#define __SM_CLIENT_HPP__ 1

#include <string> //std::string
#include <stdint.h> //size_t

class SMClient{
public:
  SMClient();
  ~SMClient();
  void Connect(std::string const & name,size_t size);
  bool IsConnected();
  void Disconnect();
  char * GetMemory();
  size_t GetMemorySize();
private:
  int fdSM;
  std::string shareName;
  char * sharedMemory;
  size_t sharedMemorySize;  
};
#endif
