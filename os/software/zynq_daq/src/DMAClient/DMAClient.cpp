#include <DMAClient/DMAClient.hh>

#include <signal.h> //struct sigaction, sigaction, raise
#include <string.h> //memset

static bool running = false;
static DMAMesgClient * dmaMTemp = NULL;
static bool caughtSignal = false;
static void signal_handler(int sig){
  if(sig == SIGINT){    
    //remove SIGINT from signal_handler
    struct sigaction sa;
    sa.sa_handler = SIG_DFL;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGINT, &sa, NULL);    

    if(NULL != dmaMTemp){
      dmaMTemp->SetShutdown(true);
    }

    caughtSignal = true;
    running = false;
  }
}


DMAClient::DMAClient():dmaMsg(NULL),
		       sm(NULL){
  
}

DMAClient::~DMAClient(){
  Disconnect();
}

int DMAClient::Connect(){
  //Check that we aren't already connected.
  Disconnect();

  //Connect to the DMA message server
  dmaMsg = new DMAMesgClient;
  if(NULL == dmaMsg){
    fprintf(stderr,"DMAMesgClient allocation failed\n");
    return -1;
  }
  
  //Connect to the socket
  char const dmaMsgPath[] = "/tmp/pl_dma.socket";
  if(!dmaMsg->Connect(dmaMsgPath)){
    fprintf(stderr,"Error connecting to dma message bridge: %s\n",dmaMsgPath);
    return -2;
  }

  //We are connected, connect to the shared memory
  sm = new SMClient;
  if(NULL == sm){
    fprintf(stderr,"SMClient allocation failed\n");
    return -3;    
  }

  try{
    //Throws exception on error.
    sm->Connect(dmaMsg->GetSMInfo()->path,
		dmaMsg->GetSMInfo()->memorySize);
  }catch(std::exception & e){
    dmaMsg->Disconnect();
    delete dmaMsg;
    throw;
  }
  
  return 0;
}


void DMAClient::Disconnect(){
  //close the SM first
  //(we should update the DMACtrl to automatically return outstanding blocks when the client disconnects)
  if(NULL != sm){
    delete sm;
    sm = NULL;
  }

  //Close the dma message client
  if(NULL != dmaMsg){
    dmaMsg->Disconnect();
    delete dmaMsg;
    dmaMsg = NULL;
  }
}

Frame_t DMAClient::GetFrame(){
  Frame_t retFrame = {.data_block =-1,
		      .size=0,
		      .boardID=0,
		      .spillID=0};

  //we are going to setup a signal handler for SIGINT to break out loop if issued.
  //To do this we will replace any existin sigaction with our own and restore it after.
  //TODO.  Consider what happens when other signals show up... Should we catch all signals(not just SIGNINT), fix ourselves and then re-raise the signal? 
  struct sigaction existingSigAction;
  struct sigaction newSigAction;
  memset(&existingSigAction,0,sizeof(existingSigAction));
  memset(&newSigAction,0,sizeof(newSigAction));
  
  newSigAction.sa_handler = &signal_handler;

  //Install the new handler and cache the old one
  running = true;
  caughtSignal = false;
  dmaMTemp = dmaMsg; //give the signal handler access to the shutdown method
  if(0 != sigaction(SIGINT,
		    &newSigAction,
		    &existingSigAction)){
    //Failed to install signal handler, something is wrong.
    return retFrame;
  }

  
  //Request a frame
  try{
    if(0 == dmaMsg->SendMessage(MT_REQUEST_FRAME,NULL,0)){
      message_t * response = NULL;
      //Wait for a response.
      //If there is a signal (except for user SIGINT), keep going if we get a signal
      //, but if SIGINT, then running is false and readN's shutdown will be called on our socket. 
      while(running && (NULL == response)){
	if(0 == dmaMsg->GetMessage(response)){
	  if(MT_FRAME == response->type){
	    //We got a frame, put it in retFrame.
	    memcpy(&retFrame,response->data,sizeof(retFrame));
	    running = false; //break out
	  }else{
        //invalid message type
        return retFrame;
      }
	}else{
        //Failed to get message
        return retFrame;
    }
      }
    }
  }catch(std::exception & e){
    //make sure running and dmaMTemp are restored
    running = false;
    dmaMTemp = NULL;
    //restore old signal handler
    sigaction(SIGINT,&existingSigAction,NULL);
  }

  //make sure running and dmaMTemp are restored
  running = false;
  dmaMTemp = NULL;  
  //restore old signal handler
  sigaction(SIGINT,&existingSigAction,NULL);
  if(caughtSignal){
    //We caught a signal while waiting.
    //We need to re-raise the signal now that the original signal handling has been restored
    caughtSignal = false;
    raise(SIGINT);
  }
  return retFrame;	  
}
uint32_t const * DMAClient::GetFrameData(Frame_t const & frame){
  return GetFrameData(frame.data_block);
}
uint32_t const * DMAClient::GetFrameData(int32_t iDataBlock){  
  if(iDataBlock < 0){
    //out of rang
    return NULL;
  }
  int offset = iDataBlock*(dmaMsg->GetSMInfo()->blockSize);
  if( (offset + dmaMsg->GetSMInfo()->blockSize) >= dmaMsg->GetSMInfo()->memorySize){
    //This block is outside of our range.
    return NULL;
  }
  uint32_t * ptr = ((uint32_t *) sm->GetMemory()) + offset;
  return ptr;
}
void DMAClient::ReturnFrame(Frame_t * frame){
  if(NULL == frame){
    return;
  }
  dmaMsg->SendMessage(MT_FRAME,(uint8_t*)frame,sizeof(Frame_t));
}

int32_t DMAClient::GetAvailableFrames(){
  int32_t availableFrames = -1;

  
  //This is copied from GetFrame(), we should make it a function
  struct sigaction existingSigAction;
  struct sigaction newSigAction;
  memset(&existingSigAction,0,sizeof(existingSigAction));
  memset(&newSigAction,0,sizeof(newSigAction));
  newSigAction.sa_handler = &signal_handler;
  //Install the new handler and cache the old one
  running = true;
  caughtSignal = false;
  dmaMTemp = dmaMsg; //give the signal handler access to the shutdown method
  if(0 != sigaction(SIGINT,
		    &newSigAction,
		    &existingSigAction)){
    //Failed to install signal handler, something is wrong.
    return 0;
  }

  //Request available blocks
  try{
    if(0 == dmaMsg->SendMessage(MT_GET_AVAILABLE,NULL,0)){
      message_t * response = NULL;
      //Wait for a response.
      //If there is a signal (except for user SIGINT), keep going if we get a signal
      //, but if SIGINT, then running is false and readN's shutdown will be called on our socket. 
      while(running){
        if(0 == dmaMsg->GetMessage(response)){
	  //Got a valid message.
	  if(MT_GET_AVAILABLE == response->type){
	    //Got a MG_GET_AVAILABLE message
	    Status_t msg;
	    //Copy in the message
	    if( (sizeof(msg) + sizeof(message_t)) == response->size){
	      //valid size
	      memcpy(&msg,response->data,sizeof(msg));
	      availableFrames = (int32_t)msg.data;
	    }else{
	      //invalid size
	      availableFrames = -1;
	    }
	    running = false; //break out
	  }else{
	    //Wrong message type
	    availableFrames = -1;
	    running = false;
	  }
        }else{
	  //failed to get a message
	  availableFrames = -1;
	  running = false;
        }
      }
    }
  }catch(std::exception & e){
    availableFrames = -1;
  }
    
  //make sure running and dmaMTemp are restored
  running = false;
  dmaMTemp = NULL;  
  //restore old signal handler
  sigaction(SIGINT,&existingSigAction,NULL);
  if(caughtSignal){
    //We caught a signal while waiting.
    //We need to re-raise the signal now that the original signal handling has been restored
    caughtSignal = false;
    raise(SIGINT);
  }
  return availableFrames;	  

}
DMAStat_t DMAClient::GetStats(){
  DMAStat_t stats;
  stats.ver = 0x0;

  //This is copied from GetFrame(), we should make it a function
  struct sigaction existingSigAction;
  struct sigaction newSigAction;
  memset(&existingSigAction,0,sizeof(existingSigAction));
  memset(&newSigAction,0,sizeof(newSigAction));
  newSigAction.sa_handler = &signal_handler;
  //Install the new handler and cache the old one
  running = true;
  caughtSignal = false;
  dmaMTemp = dmaMsg; //give the signal handler access to the shutdown method
  if(0 != sigaction(SIGINT,
		    &newSigAction,
		    &existingSigAction)){
    //Failed to install signal handler, something is wrong.    
    return stats;
  }

  //Request available blocks
  try{
    if(0 == dmaMsg->SendMessage(MT_GET_DMA_STATS,NULL,0)){
      message_t * response = NULL;
      //Wait for a response.
      //If there is a signal (except for user SIGINT), keep going if we get a signal
      //, but if SIGINT, then running is false and readN's shutdown will be called on our socket. 
      while(running){
        if(0 == dmaMsg->GetMessage(response)){
	  //Got a valid message.
	  if(MT_GET_DMA_STATS == response->type){
	    //Got a MG_GET_DMA_STATS message
	    //Copy in the message
	    if( (sizeof(stats) + sizeof(message_t)) == response->size){
	      //valid size
	      memcpy(&stats,response->data,sizeof(stats));
	    }else{
	      //invalid size
	      stats.ver = 0;
	    }
	    running = false; //break out
	  }else{
	    //Wrong message type
	    stats.ver = 0;
	    running = false;
	  }
        }else{
	  //failed to get a message
	  stats.ver = 0;
	  running = false;
        }
      }
    }
  }catch(std::exception & e){
    stats.ver = 0;
  }
    
  //make sure running and dmaMTemp are restored
  running = false;
  dmaMTemp = NULL;  
  //restore old signal handler
  sigaction(SIGINT,&existingSigAction,NULL);
  if(caughtSignal){
    //We caught a signal while waiting.
    //We need to re-raise the signal now that the original signal handling has been restored
    caughtSignal = false;
    raise(SIGINT);
  }
  return stats;	  
}
  

