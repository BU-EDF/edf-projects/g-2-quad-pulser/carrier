#include <stdio.h>
#include <DMAMesg/DMAMesgClient.hh>
#include <DMAMesg/DMAMesgComm.hh>


int main(){

  DMAMesgClient dmaMB;
  //Connect to the socket
  if(!dmaMB.Connect("/tmp/pl_dma.socket")){
    fprintf(stderr,"Error connecting to dma message bridge\n");
    return -1;
  }
  dmaMB.SendMessage(MT_SHUTDOWN,NULL,0);
  dmaMB.Disconnect();
  return 0;  
}
