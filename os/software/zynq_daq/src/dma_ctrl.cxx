//#include <DMACtrl/DMACtrl.hh>
#include <DMACtrl/DMACtrlNet.hh>

#define BLOCK_COUNT_PWR_OF_TWO 3
#define BLOCK_DATA_COUNT_WIDTH 8
int main(){
  //  DMA_Controller controller(0x209A,128);//DMA_BLOCK_SIZE*32,128);
  //  DMA_Controller controller(0x209A,50);//DMA_BLOCK_SIZE*32,128);
  //  DMACtrl controller(0x209A,16);//DMA_BLOCK_SIZE*32,128);
  size_t maxFrameSizeInBytes = (4*3)+(8*(4*(6+(0x1<<BLOCK_COUNT_PWR_OF_TWO)*(2+0.5*(0x1<<BLOCK_DATA_COUNT_WIDTH)))));
  
  DMACtrlNet controller(maxFrameSizeInBytes/sizeof(uint32_t));//DMA_BLOCK_SIZE*32,128);
  fprintf(stderr,"DMA Ctrl Run\n");
  controller.Run();
  fprintf(stderr,"DMA Ctrl End\n");
  return 0;
}
