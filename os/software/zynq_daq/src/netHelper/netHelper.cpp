#include <netHelper/netHelper.hh>

#include <cstdio>
#include <errno.h>

//read/write
#include <unistd.h>
#include <sys/utsname.h>
#include <sys/select.h>
#include <sys/time.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/un.h>  // for AF_LOCAL
#include <fcntl.h> //For fcntl and misc

//CheckLocal
#include <net/if.h>
#include <sys/ioctl.h>


static bool netHelperShutdown = false;
void setShutdown(bool _shutdown){
  netHelperShutdown = _shutdown;
}


int32_t writeN(int socketFD,
	       uint8_t * ptr,
	       uint32_t size)
{
  int32_t ret = 0;

  //From Unix Network Programming: Stevens.
  uint8_t * writePtr = ptr;
  int32_t nLeft = size;
  
  int32_t nWritten = 0;

  //Loop over what is left to write
  while(nLeft >0)
    {
      //take a shot at writing the data
      if( (nWritten = write(socketFD,writePtr,nLeft)) <=0)
	//Process any errors
	{
	  if(errno == EINTR)
	    {
	      nWritten = 0;
	    }
	  else
	    {
	      ret = -errno;
	      break;
	    }
	}
      //remote the transmitted data from the send "queue" and keep going
      nLeft -= nWritten;
      writePtr += nWritten;
    }
  if(ret >= 0)
    ret = size;
  return ret;
}

int32_t readN(int socketFD,
	      uint8_t * ptr,
	      uint32_t readSize)
{
  //Read readSize bytes from the socket
  //If return != readSize, the socket died

  //From Unix Network Programming: Stevens.
  int32_t nLeft = readSize;
  int32_t nRead = 0;
  uint8_t * readPtr = ptr;
  
  while(nLeft >0)
    {
      //Read some data
      if( (nRead = read(socketFD,readPtr,nLeft)) < 0)
	//process errors
	{
	  if(errno == EINTR)
	    {
	      nRead = 0;
	      if(netHelperShutdown){
		break;
	      }
	    }
	  else
	    {
	      return -1; //error
	    }
	}
      else if (nRead == 0)
	{
	  break; /* EOF */
	}

      //move along in our read to queue for our read
      nLeft -= nRead;
      readPtr += nRead;
    }  
  return (readSize - nLeft);
}


bool SetNonBlocking(int &fd,bool value)
{
  //Get the previous flags
  int currentFlags = fcntl(fd,F_GETFL,0);
  if(currentFlags < 0)
    {
      return(false);
    }
  //Make the socket non-blocking
  if(value)
    {
      currentFlags |= O_NONBLOCK;
    }
  else
    {
      currentFlags &= ~O_NONBLOCK;
    }

  int currentFlags2 = fcntl(fd,F_SETFL,currentFlags);
  if(currentFlags2 < 0)
    {
      return(false);
    }
  return(true);
}


