/**
 * Proof of concept offloaded memcopy using AXI Direct Memory Access v7.1
 */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/mman.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>

//for signals
#include <signal.h>


#include <sys/shm.h> //shared memory
#include <stdlib.h> //abort

#include <tclap/CmdLine.h>

#define BLOCK_SIZE   (256)
#define BLOCK_COUNT  (64)

void update_block(int & iBlock){
  iBlock++;
  if(size_t(iBlock) >= (BLOCK_COUNT-1)){
    iBlock = 0;
  }
}


void printEvent(uint32_t * data, size_t size){
  for(size_t i = 0; i < size;i++){
    printf("%06zu: 0x%08X\n",i,data[i]);
  }
}


static volatile int run;
struct timespec lastTimeSpec;
static volatile uint8_t ctrlCCount;
//Signal handler to catch Ctrl-C
static void signalHandler(int signal)
{
  if(signal == SIGINT)
    {
      printf("Shutting down\n");
      //Cause the main loop to end
      run = 0;
      //Keep track of ctrl-Cs 
      ctrlCCount++;
      //We have taken away the right of every linux user to kill our program with Ctrl-C
      //If something is going wrong, we should accept their wishes after several Ctrl-Cs and kill our program
      if(ctrlCCount > 2)
	{
	  printf("Next Ctrl-C kills\n");
	  //Stop catching the Ctrl-C signal and let the user kill
	  sigset_t signalMask;              
	  sigemptyset (&signalMask);                        
	  sigaddset (&signalMask, SIGINT);                  
	  //install the new signal mask                     
	  sigprocmask(SIG_UNBLOCK, &signalMask, NULL);
	} 
    }
  else if(signal == SIGALRM)
    {
      //Reset the ctrl-C count 
      ctrlCCount = 0;
      alarm(3);
    }
}


int main(int argc, char ** argv) {

  uint32_t prescale = 0x400; //Prescale for printing events
  try {
    TCLAP::CmdLine cmd("DMA Test app",' ',"ZYNQ DMA test");
    TCLAP::ValueArg<uint32_t> printPrescale("p",
					    "prescale",
					    "prescale num",
					    false,
					    0x400,
					    "unsigned integer",
					    cmd);
    //Parse the command line arguments
    cmd.parse(argc,argv);

    prescale = printPrescale.getValue();
  } catch (TCLAP::ArgException &e) {
    fprintf(stderr, "Error %s for arg %s\n",
	    e.error().c_str(), e.argId().c_str());
    return 0;
  }


  //==============================================================                      
  //Setup signal handler                            
  //==============================================================                      
  sigset_t signalMask,emptySignalMask;              
  struct sigaction sSigAction;                      
  memset (&sSigAction, 0, sizeof(sSigAction));      
                                
  //Setup a signal handler & sigmask to capture CTRL-C                                  
  sSigAction.sa_handler = signalHandler;            
  if ( sigaction(SIGINT, &sSigAction, NULL)  < 0 )  
    {           
      printf("Error in sigaction (%d): %s\n",errno,strerror(errno));                    
      return -2;
    }           
  if ( sigaction(SIGALRM, &sSigAction, NULL)  < 0 )  
    {           
      printf("Error in sigaction (%d): %s\n",errno,strerror(errno));                    
      return -2;
    }           
  sigemptyset(&signalMask);                        
  sigaddset (&signalMask, SIGINT);                  
  sigaddset (&signalMask, SIGALRM);                  
                
  //install the new signal mask                     
  if (sigprocmask(SIG_UNBLOCK, &signalMask, &emptySignalMask) < 0)                        
    {           
      printf("Error in sigprocmask (%d): %s\n",errno,strerror(errno));                  
      return -3;
    }           


  
  
  size_t destination_allocated_size = (BLOCK_COUNT)*BLOCK_SIZE*4 ;  //size in bytes

  //create a shared memory for clients
  int fdSM = shm_open("zynq_test",
		      O_CREAT | O_RDWR | O_SYNC,
		      S_IRUSR | S_IWUSR | S_IRGRP);  // rw for creator, r for group, nothing for others 
  if (fdSM == -1) {
    //throw exception
    abort(); //for now
  }
  // Size the shared memory and get memmap a pointer to it
  if(-1 == ftruncate(fdSM, destination_allocated_size)){
    //clean up fd
    close(fdSM);
    //throw exception
    abort(); //for now
  }
  uint32_t * sharedMemory = (uint32_t *) mmap(NULL,
					      destination_allocated_size,
					      PROT_READ | PROT_WRITE,
					      MAP_SHARED | MAP_LOCKED,
					      fdSM,
					      0);
  if(MAP_FAILED == sharedMemory){
    //clean up fd
    close(fdSM);
    //throw exception
    abort();//for now
  }  

  

  //open our device
  int fd = open("/dev/pl_dma",O_RDONLY);
  if(fd < 0){
    printf("Err(%d) open: %s\n",errno,strerror(errno));
    return -1;
  }

  run = 1;
  int iBlock = 0;
  int startBlock = -1;
  uint32_t event_number = 0;
  while(run){
    uint32_t * data = sharedMemory + (iBlock*BLOCK_SIZE);
    size_t data_size = BLOCK_SIZE * sizeof(uint32_t);
    //    printf("Waiting for spill\n");

    //Block in read for beginning of packet
    int ret = read(fd,(char *) data,data_size);
    if(0 == ret){
      printf("\nTimeout!\n\n");
    }else if(ret > 0){
      //we have data, check that it is a valid first block
      if(0xA5 == (data[0]&0xFF)){	
	//we have a valid header, get size
	int size_left = (data[0] >> 8); //number of 32bit words
	size_left -= BLOCK_SIZE;        //remove the number of words we already read

	//get the rest of the data
	startBlock = iBlock; //keep track of the block where this event started
	update_block(iBlock);//get a new block to write to
	data = sharedMemory + (iBlock*BLOCK_SIZE); //update our data pointer
	
	while(size_left > 0){
	  //Keep reading blocks until we get the last one (size_left <= 0)
	  ret = read(fd,(char *) data,data_size);

	  if(0 == ret){
	    printf("\nTimeout!\n\n");
	  }else if(ret > 0){
	    size_left -= BLOCK_SIZE;
	    update_block(iBlock);//get a new block to write to
	    data = sharedMemory + (iBlock*BLOCK_SIZE); //update our data pointer	    
	  }else{
	    //Bad data
	    iBlock = startBlock; //go back to the first block we started with
	    startBlock = -1; //indicate that we hvae a bad start block
	    printf("Bad data block\n");
	    break;
	  }
	}
	//	event_number = (data[1]>>8)&0xFFFF;
	//	if(prescale == (event_number & ((prescale<<1)-1))){
	if(event_number >= prescale){
	  //We've finished this data
	  data = sharedMemory + (startBlock*BLOCK_SIZE); //update our data pointer	
	  printf("\tGot spill %u, board %d, size: %u\n",
		 (data[1]>>8)&0xFFFF,
		 (data[1]>>24)&0xFF,
		 (data[0]>>8)&0xFFFFFF);
	  int transfer_block_count = iBlock - startBlock;	
	  if(transfer_block_count < 0){
	    transfer_block_count+=BLOCK_COUNT;
	  }
	  printf("\t%d blocks\n",transfer_block_count);
	  printEvent(data,(data[0]>>8)&0xFFFFFF);
	  event_number = 0;
	}else{
	  event_number++;
	}
      }	    
    }else{
      printf("Bad start block\n");
      startBlock = -1;
    }
  }
  close(fd);
  close(fdSM);
  return 0;
}
