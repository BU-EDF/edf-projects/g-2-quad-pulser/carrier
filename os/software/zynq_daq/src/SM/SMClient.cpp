#include <SM/SMClient.hh>
#include <exception>

#include <fcntl.h>  // for O_RDONLY

#include <sys/mman.h> //for mmap

#include <stdexcept> // for std::invalid_argument

#include <unistd.h> //close

#include <stdio.h>

SMClient::SMClient():
  fdSM(-1),
  sharedMemory( (char*) MAP_FAILED),
  sharedMemorySize(0){  
}

SMClient::~SMClient(){
  if(IsConnected()){
    Disconnect();
  }
};

void SMClient::Connect(std::string const & name,size_t size){    
  //open the shared memory device
  if(-1 == (fdSM = shm_open(name.c_str(), O_RDONLY,0))){
    //error opening shm device
    throw std::invalid_argument(name.c_str());
  }

  if(0 == size){
    throw std::invalid_argument("Zero size\n");
  }
  sharedMemorySize = size;
  
  //map the shared memory
  sharedMemory = (char *) mmap(NULL,
			       sharedMemorySize,
			       PROT_READ,
			       MAP_SHARED,
			       fdSM,
			       0);
  if(MAP_FAILED == sharedMemory){
    //mmap failed
    close(fdSM);
    throw std::invalid_argument("Map failed\n");
  }

  //everythign is good
}

bool SMClient::IsConnected(){
  bool ret = false;
  if((-1 != fdSM) && (MAP_FAILED != sharedMemory)){
    ret = true;
  }else{
    ret = false;
  }
  return ret;
}

void SMClient::Disconnect(){
  if(MAP_FAILED != sharedMemory){
    //mmap failed
    munmap(sharedMemory,sharedMemorySize);
    sharedMemory=(char*)MAP_FAILED;
    sharedMemorySize = 0;
  }
  if(-1 != fdSM){
    close(fdSM);
    fdSM = -1;
  }
}

char * SMClient::GetMemory(){
  if(!IsConnected()){
    return NULL;
  }
  return sharedMemory;
}

size_t SMClient::GetMemorySize(){
  if(!IsConnected()){
    return 0;
  }
  return sharedMemorySize;
}
