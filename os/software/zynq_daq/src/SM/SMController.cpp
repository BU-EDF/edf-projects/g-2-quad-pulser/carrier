#include <SM/SMController.hh>

#include <fcntl.h>     //open 2
#include <sys/types.h> //open 2
#include <sys/stat.h>  //open 2
#include <unistd.h>    //close 2
#include <sys/shm.h> //shared memory
#include <sys/mman.h> //mmap
#include <errno.h> // for errno
#include <grp.h>   //group id syscalls and types

#include <stdlib.h> //abort

#include <stdio.h> //printf

#include <string.h> //memset

SMController::SMController():fdSM(-1),shareName(""),groupName(""),sharedMemory(NULL),sharedMemorySize(0){
  
}

bool        SMController::Running(){
  return (fdSM > 0);
};

SMController::~SMController(){
  Destroy();
}


std::string SMController::GetShareName(){
  return shareName;
};
int         SMController::SetShareName(std::string const & name){
  if(Running()){
    return -1;
  }
  shareName = name;
  return 0;
}


std::string SMController::GetGroupName(){
  return groupName;
}
int         SMController::SetGroupName(std::string const & name){
  if(Running()){
    return -1;
  }
  groupName = name;
  return 0;
}

int         SMController::GetSize(){
  return sharedMemorySize;
}
int         SMController::SetSize(size_t size){
  if(Running()){
    return -1;
  }
  sharedMemorySize = size;
  return 0;
}



void        SMController::Create(){
  //=============================================================================
  //Start-up checks
  //=============================================================================

  //Check that we aren't running
  if(Running()){
    //Throw exception
    abort();//abort for now
  }

  //Check that the share name exists
  if(0 == shareName.size()){
    //Throw exception
    abort();
  }  
  
  //=============================================================================
  //create the shared memory file descriptor and set permissions on it
  //=============================================================================

  //Open the fd
  fdSM = shm_open(shareName.c_str(),
		  O_CREAT | O_RDWR,
		  S_IRUSR | S_IWUSR | S_IRGRP);  // rw for creator, r for group, nothing for others  
  if (fdSM == -1) {
    //throw exception
    abort(); //for now
  }

  //Get the group ID we should be using
  struct group * group_id = getgrnam(groupName.c_str());
  if(NULL == group_id){
    //Clean up fd
    close(fdSM);
    //throw exception
    abort(); //for now
  }

  // change the roup ownership of the shared memory fd
  if(-1 == fchown(fdSM,-1,group_id->gr_gid)){
    //clean up fd
    close(fdSM);
    //throw exception
    abort();//for now
  }

  //=============================================================================
  // Size the shared memory and get memmap a pointer to it
  //=============================================================================
  if(-1 == ftruncate(fdSM, sharedMemorySize)){
    //clean up fd
    close(fdSM);
    //throw exception
    abort(); //for now
  }

  sharedMemory = (char *) mmap(0,
			       sharedMemorySize,
			       PROT_READ | PROT_WRITE,
			       MAP_SHARED| MAP_POPULATE,
			       fdSM,
			       0);
  if(MAP_FAILED == sharedMemory){
    //clean up fd
    close(fdSM);
    //throw exception
    abort();//for now
  }
  memset(sharedMemory,0,sharedMemorySize);
  printf("Mapped %zu bytes @ %p\n",sharedMemorySize,sharedMemory);
}

void        SMController::Destroy(){
  if(!Running()){
    return;
  }

  //create an exception for if we have multiple errors
  bool error = false;
  
  //umap our memory
  if( -1 == munmap(sharedMemory,sharedMemorySize) ){
    //update the exception, set error to true so we know to throw it.
    error = true;
  }

  //close the shared memory file
  if( -1 == close(fdSM)) {
    //update the exception and set error to true
    error = true;
  }

  if(error){
    //throw exception
    abort();//for now
  }
}

char *      SMController::GetMemory(){
  if(Running()){
    return sharedMemory;
  }
  return NULL;
}
