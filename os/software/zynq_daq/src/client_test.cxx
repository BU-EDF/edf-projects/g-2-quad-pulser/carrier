#include <stdlib.h>
#include <stdint.h>

#include <signal.h> //signals


#include <tclap/CmdLine.h>

//#include <g2frame/g2QuadFrame.hpp>

#include <stdio.h> // for FILE *

#include <DMAClient/DMAClient.hh>

#include <unistd.h> // for sleep

#include <inttypes.h> //for PRI macros

#include <g2frame/g2QuadFrame.hpp> //for frame parser.

static volatile bool running = false;

volatile int writeFrame = 0;

void signal_handler(int sig){
  if(sig == SIGINT){
    //remove SIGINT from signal_handler
    struct sigaction sa;
    sa.sa_handler = SIG_DFL;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGINT, &sa, NULL);    
    
    running = false;
  }else if(sig == SIGUSR1){
    writeFrame = 4; //write four frames.
  }
}
    

void printFrame(Frame_t & frame, uint32_t const * memory,bool full = false){
  printf("Board %01u, spill %08u\n",frame.boardID,frame.spillID);
  printf("Size: %u @ %d\n",frame.size,frame.data_block);

  size_t printSize = frame.size;
  if(!full){
    //we aren't printing the full frame, just the header
    if(printSize >3){
      printSize = 3;
    }
  }
  
  for(size_t i = 0; i < printSize;i++){
    printf("  0x%04X: 0x%08X\n",i,memory[i]);
  }
}

void dumpToFile(FILE * outfile,uint32_t const * memory,size_t frameSize){
  for(size_t i = 0; i < frameSize;i++){
    fprintf(outfile,"0x%08X\n", memory[i]);
  }
}


int main(int argc, char** argv){

  //signal handling
  struct sigaction sa;
  sa.sa_handler = signal_handler;
  sigemptyset(&sa.sa_mask);
  sigaction(SIGINT, &sa, NULL);
  sigaction(SIGUSR1, &sa, NULL);


  uint32_t prescale = 0x400; //Prescale for printing events
  bool availableTest = false;
  bool printDump = false;
  try {
    TCLAP::CmdLine cmd("DMA Test app",' ',"ZYNQ DMA test");
    TCLAP::ValueArg<uint32_t> printPrescale("p",
					    "prescale",
					    "prescale num",
					    false,
					    0x400,
					    "unsigned integer",
					    cmd);
    TCLAP::SwitchArg printAvailable("a",
				    "available_test",
				    "",
				    cmd,
				    false
				    );
    TCLAP::SwitchArg printDumpArg("d",
				  "print_dump",
				  "",
				  cmd,
				  false);
    
    //Parse the command line arguments
    cmd.parse(argc,argv);

    availableTest = printAvailable.getValue();
    prescale = printPrescale.getValue();
    printDump = printDumpArg.getValue();
  } catch (TCLAP::ArgException &e) {
    fprintf(stderr, "Error %s for arg %s\n",
	    e.error().c_str(), e.argId().c_str());
    return 0;
  }


  char const filename[] = "event_dump.dat";
  FILE * outFile = fopen(filename,"w");
  if(NULL == outFile){
    fprintf(stderr,"Error opening file: %s\n",filename);
    return -1;
  }

  //Connect to the running waveform daemon and get setup for frames
  DMAClient dmaC;
  if(dmaC.Connect()){
    fprintf(stderr,"Error connecting to dma message bridge\n");
    return -1;
  }
  
  uint32_t event_number = 1;
  uint32_t frame_number = 1;
  running = true;


  g2QuadFrame parser;

  size_t const histXBins = 100;
  size_t const histYBins = 5;

  uint32_t const histXBinWidth = 100000;
  uint32_t monitoring[histXBins][histYBins];
  memset(monitoring,0,histXBins*histYBins*sizeof(uint32_t));

  uint32_t errorCount = 0;
  
  while(running){

    //Available test
    if(availableTest){
      if(dmaC.GetAvailableFrames()<0){
	printf("Error. GetAvailable Frame is -1\n");
      }else{
	uint32_t available = (uint32_t)dmaC.GetAvailableFrames();
	printf("%" PRIu32 " frames available.\n",available);
      }
      sleep(1);
    }
    
    
    //Get a frame
    Frame_t newFrame = dmaC.GetFrame();    
    //Check that the frame returned a valid data block
    if(-1 == newFrame.data_block){
      printf("Got bad frame\n");
      running = false;
    }else{

      parser.Parser(dmaC.GetFrameData(newFrame),newFrame.size);
      size_t eventBin = parser.GetSpillID()/histXBinWidth;
      size_t boardID = parser.GetID();
      if(boardID > 4){
	boardID = 0;
      }
      
      monitoring[eventBin][boardID]++;

      if(parser.GetErrorFlag()){
	errorCount++;
      }

      
      //Write events to disk if enabled.
      if(writeFrame > 0){
	//Dump file to disk
	dumpToFile(outFile,
		   dmaC.GetFrameData(newFrame), //Get a (uint32_t) pointer to the data for this frame
		   newFrame.size); //Size of the frame in uint32_t-s. 
	writeFrame--;
	if(0 == writeFrame){
	  //flush the last of this burst to disk
	  fflush(outFile);
	}
      }

      //Print prescaled events to the screen
      if(event_number >= prescale){
	//print pre-scaled events
	printf("Captured frame: %u\n",frame_number);
	printFrame(newFrame,dmaC.GetFrameData(newFrame),printDump);

	printf("\n (err: %u) Frame# 0x%08X Spill ID 0x%08X Bin: %04zu: ",errorCount,frame_number,parser.GetSpillID(),eventBin);
	for(size_t iID = 0; iID < histYBins;iID++){
	  printf(" 0x%08X",monitoring[eventBin][iID]);
	}
	printf("\n");
	
	event_number = 1;

	//Get the DMA stats
	DMAStat_t stats = dmaC.GetStats();
	if(stats.ver != 0){
	  printf("\n");
	  printf("  errorFrameTooBig          : %08X\n",stats.errorFrameTooBig          );
	  printf("  errorBadDMAInFrame        : %08X\n",stats.errorBadDMAInFrame        );
	  printf("  errorFrameIncomplete      : %08X\n",stats.errorFrameIncomplete      );
	  printf("  errorBadFrameStartMagic   : %08X\n",stats.errorBadFrameStartMagic   );
	  printf("  errorBadDMAAtFrameStart   : %08X\n",stats.errorBadDMAAtFrameStart   );
	  printf("  errorBadReturnFrame       : %08X\n",stats.errorBadReturnFrame       );
	  printf("  statReturnedFrameCount    : %08X\n",stats.statReturnedFrameCount    );
	  printf("  statDeliveredFrameCount   : %08X\n",stats.statDeliveredFrameCount   );
	  printf("  statAvailableRequestCount : %08X\n",stats.statAvailableRequestCount );
	  printf("  statBlockOnDMACount       : %08X\n",stats.statBlockOnDMACount       );
	  printf("  statBlockOnDMACheckReturn : %08X\n",stats.statBlockOnDMACheckReturn );
	  printf("  statValidHeader           : %08X\n",stats.statValidHeader           );
	  printf("  statBoard1Count           : %08X\n",stats.statBoard1Count           );
	  printf("  statBoard2Count           : %08X\n",stats.statBoard2Count           );
	  printf("  statBoard3Count           : %08X\n",stats.statBoard3Count           );
	  printf("  statBoard4Count           : %08X\n",stats.statBoard4Count           );
	  printf("  statBoardUnknownCount     : %08X\n",stats.statBoardUnknownCount     );
	  printf("  statFinishedFrames        : %08X\n",stats.statFinishedFrames        );
	  printf("  statBlockThrownAway       : %08X\n",stats.statBlockThrownAway       );
	  printf("  statIdleBlockCount        : %08X\n",stats.statIdleBlockCount        );
	  printf("  statEmptyBlockCount       : %08X\n",stats.statEmptyBlockCount       );
	  printf("  statActiveFrameCount      : %08X\n",stats.statActiveFrameCount      );

	}else{
	  printf("\nBad stats\n");
	}
	
      }else{
	event_number++;
      }
      frame_number++;
      	         
      //Return the data so it can be used for another frame. 
      dmaC.ReturnFrame(&newFrame);
    }
  }
  fprintf(stderr,"End\n");
  dmaC.Disconnect(); //Disconnect from the daemon. 
  fclose(outFile);
  return 0;  
}
