#include <DMAMesg/DMAMesgServer.hh>
#include <DMAMesg/DMAMesgCommon.hh>

#include <stdlib.h> //malloc,free
#include <string.h> //memset
#include <sys/ioctl.h> // ioctl
#include <sys/socket.h> //SOCK_AF
#include <errno.h>

#include <unistd.h> //open,close

#include <netHelper/netHelper.hh>

DMAMesgServer::DMAMesgServer():listenFD(-1),
			       clientFD(-1)
{
  //zero structs
  memset(&sa_unix,0x00,sizeof(sa_unix));
  memset(&sa_unix,0x00,sizeof(sa_client));
  
  fdEnd = -1;
  FD_ZERO(&fdReadMask);  
}

DMAMesgServer::~DMAMesgServer(){
  Close();
}

bool DMAMesgServer::Open(std::string const & _path){
  //open the unix socket
  listenFD = socket(AF_UNIX,SOCK_STREAM,0);
  if(listenFD < 0){
    fprintf(stderr,"Error: server socket creation failed\n");
    //throw std::invalid_argument("server socket creation");
    return false;
  }

  path = _path;
  //Build the saddr struct
  sa_unix.sun_family = AF_UNIX;
  size_t socketPathLen = strlen(path.c_str())+1; //+1 gives assures us a NULL terminated string
  if(socketPathLen > sizeof(sa_unix.sun_path)){
    fprintf(stderr,
	    "Error: unix socket path is too long %d %d\n",
	    socketPathLen,
	    sizeof(sa_unix.sun_path));
    //throw std::invalid_argument("Invalid unix socket path");
    close(listenFD);
    path.clear();
    return false;
  }
  strncpy(sa_unix.sun_path,path.c_str(),socketPathLen);


  //Bind the listen struct_un to our socket
  if(bind(listenFD, (const sockaddr *) &sa_unix,sizeof(sa_unix))){
    fprintf(stderr,"Error: bind to unix socket failed, %s(%d)\n",strerror(errno),errno);
    //    throw std::invalid_argument("Listen socket failed\n");
    close(listenFD);
    path.clear();
    return false;
  }

  
  //We are ready to listen, but we will wait for clients later
  if(-1 == listen(listenFD,1)){
    fprintf(stderr,"Error: Listen faile with %s(%d)\n",strerror(errno),errno);
    close(listenFD);
    path.clear();
    return false;
  }

  
  //setup pselect mask for listen socket
  FD_ZERO(&fdReadMask);
  FD_SET(listenFD,&fdReadMask);
  fdEnd = listenFD+1;
  return true;
}

void DMAMesgServer::CloseClient(){
  if(clientFD >= 0){
    close(clientFD);
    clientFD = -1;
  }
  memset(&sa_unix,0x00,sizeof(sa_client));  
}
void DMAMesgServer::Close(){
  CloseClient();
  
  if(listenFD >= 0){
    close(listenFD);
    listenFD = -1;
  }

  FD_ZERO(&fdReadMask);
  fdEnd = -1;

  if(path.size()){
    unlink(path.c_str());
  }
}

bool DMAMesgServer::ListenForClient(int timeout_ms /*ms*/){
  bool ret = false;
  struct timespec timeout = {0,timeout_ms*1000*1000}; //ms
  fd_set fdReadReady = fdReadMask; //FDs to listen for
  
  int nReady = pselect(fdEnd,&fdReadReady,NULL,NULL,&timeout,NULL); 
  if(nReady != 0){
    //get client    
    socklen_t sa_client_len = sizeof(sa_client);
    clientFD = accept(listenFD,(sockaddr *)&sa_client,&sa_client_len);
    if(-1 != clientFD){
      ret = true;
    }      
  }
  return ret;
}

int DMAMesgServer::SendMessage(uint16_t type, uint8_t * mesgData, size_t mesgDataSize){
  return DMAMesgCommon::SendMessage(clientFD,type,mesgData,mesgDataSize);
}

int DMAMesgServer::SendMessage(message_t * mesg){
  return SendMessage(mesg->type,mesg->data,size_t(mesg->size)-sizeof(message_t));
}


int DMAMesgServer::MessagePending(){
  return DMAMesgCommon::MessagePending(clientFD);
}
int DMAMesgServer::GetMessage(message_t * & mesg){
  return DMAMesgCommon::GetMessage(clientFD,mesg);
}



			    
