#include <DMAMesg/DMAMesgCommon.hh>
#include <stdlib.h> //malloc,free
#include <string.h> //memset
#include <sys/ioctl.h> // ioctl
#include <sys/socket.h> //SOCK_AF
#include <errno.h>

#include <unistd.h> //open,close

#include <netHelper/netHelper.hh>

void DMAMesgCommon::SetShutdown(bool _shutdown){
  setShutdown(_shutdown);
}

int DMAMesgCommon::SendMessage(int fd,uint16_t type, uint8_t * mesgData, size_t mesgDataSize  /*bytes*/){
    //Check FD
  if(fd < 0){
    return -1;
  }

  //Check size
  if((mesgDataSize + sizeof(message_t)) > 0xFFFF){
    return -2; //invalid size
  }

  uint16_t messageSize = mesgDataSize + sizeof(message_t);
  int ret;

  //Write size
  if(-1 == (ret = writeN(fd,(uint8_t*)&messageSize,sizeof(messageSize)))){
    return -4; //error
  }

  //Write type
  if(-1 == (ret = writeN(fd,(uint8_t*)&type,sizeof(type)))){
    return -3; //error
  }
  
  //Write data
  if((mesgDataSize > 0) && mesgData != NULL){
    if(-1 == (ret = writeN(fd,mesgData,mesgDataSize))){
      return -5; //error
    }
  }

#ifdef DEBUG
  fprintf(stderr,
	  "SentMessage of type 0x%04X\n"	\
	  "               size 0x%04X\n",type,messageSize);
#endif
  
  return 0;  
}
int DMAMesgCommon::SendMessage(int fd,message_t * mesg){
  return SendMessage(fd,mesg->type,mesg->data,size_t(mesg->size)-sizeof(message_t));
}


int DMAMesgCommon::MessagePending(int fd){

  //Check if the socket is readable.
  fd_set tempFDMask;
  FD_ZERO(&tempFDMask);
  FD_SET(fd,&tempFDMask);
  timeval zeroTimeout = {0,0}; //Do not sleep waiting for read ready
  select(fd+1,&tempFDMask,NULL,NULL,&zeroTimeout);
  if(! FD_ISSET(fd,&tempFDMask) ){
    //Socket is not readble, no data ready.
    return 0;
  }

  
  //The socket is read ready
 
  
  //Check if there is a message's worth of data in the buffer;
  message_t temp;
  
  size_t rd_len = 0;
  ioctl(fd,FIONREAD,&rd_len);
  if(0 == rd_len){
    //The socket is read ready, but there is no data.
    return -1;
  }else if(rd_len < sizeof(temp)){
    //There isn't even enough data for a message
    return 0;
  }

  //There is a message in the buffer, but let's peek to see if all of it is there.
  ssize_t ret = recv(fd,&(temp.size),sizeof(temp.size),MSG_PEEK);
  if(ret < -1){
    return 0;
  }
  if(rd_len < temp.size){
    //The message isn't fully there, wait.
    return 0;
  }
  return 1;
}
int DMAMesgCommon::GetMessage(int fd,message_t * & mesg){
#ifdef DEBUG
  fprintf(stderr,"GetMessage start\n");
#endif
  if(fd < 0){
    return -1;
  }
  
  //Get message_t with a Setup_t inside of it.
  message_t temp;
  temp.size=0;
  mesg = NULL;

  //get Size
  int rd_ret = 0;
  if(sizeof(temp.size) != (rd_ret = readN(fd,(uint8_t*)&(temp.size),sizeof(temp.size)))){
    return -2; //error
  }

  //Size is good, so we allocate a buffer for it.
  mesg = (message_t *) malloc(temp.size);
  if(NULL == mesg){
    return -3;
  }  
  mesg->size = temp.size;

  
  //Get the rest of the mesg  
  if(-1 == readN(fd,(uint8_t*)&(mesg->type),mesg->size - sizeof(mesg->size))){
    //error
    free(mesg);
    mesg = NULL;
    return -2; //error
  }

#ifdef DEBUG
  fprintf(stderr,
	  "GotMessage of type 0x%04X\n"	\
	  "              size 0x%04X\n",
	  mesg->type,
	  mesg->size);
#endif


  //Everything is OK.
  return 0;
}



			    
