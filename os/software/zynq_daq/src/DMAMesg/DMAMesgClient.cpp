#include <DMAMesg/DMAMesgClient.hh>
#include <DMAMesg/DMAMesgCommon.hh>
#include <DMAMesg/DMAMesgComm.hh>

#include <stdlib.h>
#include <stdint.h>

#include <unistd.h>
#include <sys/socket.h>
#include <errno.h> 
#include <sys/un.h>   


DMAMesgClient::DMAMesgClient():fd(-1),
		   SMInfo(NULL){
}

void DMAMesgClient::SetShutdown(bool _shutdown){
  DMAMesgCommon::SetShutdown(_shutdown);
}

int DMAMesgClient::SendMessage( uint16_t type, uint8_t *mesgData, size_t mesgDataSize){
  return DMAMesgCommon::SendMessage(fd,type,mesgData,mesgDataSize);
}

int DMAMesgClient::GetMessage( message_t * & mesg_out){
  return DMAMesgCommon::GetMessage(fd,mesg_out);
}
int DMAMesgClient::SendMessage(message_t * mesg){
  return SendMessage(mesg->type,mesg->data,size_t(mesg->size)-sizeof(message_t));
}


bool DMAMesgClient::Connect(std::string const & path){
  SetShutdown(false);
  
  fd = socket(AF_UNIX, SOCK_STREAM, 0);
  if (fd<0) {
    fprintf(stderr, "Failed to create socket\n");
    return errno;
  }

  size_t path_len = path.size()+1;
  struct sockaddr_un * addr;
  addr = (struct sockaddr_un *) calloc(1, sizeof(*addr)+path_len);
  if(NULL == addr){
    return -1;
  }
  addr->sun_family = AF_UNIX;
  strncpy(&addr->sun_path[0], path.c_str(), path_len);
  if (connect(fd, (const sockaddr *)addr, sizeof(*addr))) {
    fprintf(stderr, "Failed to connect, err %d %s\n",
	    errno, strerror(errno));
    free(addr);
    return errno;
  }
  free(addr);

  //We are now connected, get the SMInfo
  if(!RequestSMInfo()){
    //this failed...
    Disconnect();
    return false;
  }
  
  return true;
}

void DMAMesgClient::Disconnect(){
  //if we are connected, send a close message
  if(fd > 0){
    SendMessage(MT_CLOSE,NULL,0);    
  }
  //Invalidate SMinfo if valid
  if(NULL != SMInfo){
    free(SMInfo);
    SMInfo = NULL;
  }
  //close the fd
  close(fd);
  fd = -1;
}

Setup_t const * DMAMesgClient::GetSMInfo(){
  return SMInfo;
}

bool DMAMesgClient::RequestSMInfo(){
  //Make sure our socket is empty.
  
  //Write a request frame
  int request = SendMessage(MT_GET_SM_INFO,
			    NULL,0);

  if(request != 0){
    return false;
  }

  //Get response
  message_t * response = NULL;
  int tries = 10;
  while(tries > 0){
    //Get the message
    if(0 != GetMessage(response)){
      //error
      return false;
    }
    //See if it is the correct kind
    if(response->type != MT_SM_INFO){
      //wrong type, keep trying
      free(response);
      response = NULL;
      tries--;
    }else{
      break;
    }
  }

  if(NULL == response){
    //failed
    return false;
  }

  SMInfo= (Setup_t *) malloc(response->size - sizeof(response));
  if(NULL == SMInfo){
    free(response);
    return false;
  }
  memcpy(SMInfo,response->data,response->size - sizeof(response));

  free(response);
  
  //Everything is good.
  return true;
}
