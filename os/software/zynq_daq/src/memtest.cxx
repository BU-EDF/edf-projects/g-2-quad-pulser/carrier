#include <fcntl.h>     //open 2
#include <sys/types.h> //open 2
#include <sys/stat.h>  //open 2
#include <unistd.h>    //close 2
#include <sys/shm.h> //shared memory
#include <sys/mman.h> //mmap
#include <errno.h> // for errno
#include <grp.h>   //group id syscalls and types

#include <stdlib.h> //abort

#include <stdio.h> //printf

#include <string.h> //memset


int allocateZeroAndDeallocate(size_t byteCount){
  fprintf(stderr,"Trying %zu\n",byteCount);

  int fd = shm_open("test",
		  O_CREAT | O_RDWR,
		  S_IRUSR | S_IWUSR | S_IRGRP);  // rw for creator, r for group, nothing for others  
  if (fd == -1) {
    //throw exception
    return -1;
  }
  if(-1 == ftruncate(fd, byteCount)){
    //clean up fd
    close(fd);
    //throw exception
    return -1;
  }

  char * sharedMemory = (char *) mmap(NULL,
				      byteCount,
				      PROT_READ | PROT_WRITE,
				      MAP_SHARED ,
				      fd,
				      0);
  if(MAP_FAILED == sharedMemory){
    //clean up fd
    close(fd);
    //throw exception
    return -1;
  }
  sleep(1);
  struct stat statbuf;
  if(-1 != fstat(fd,&statbuf)){
    fprintf(stderr,"size %ld\n",statbuf.st_size);
    fprintf(stderr,"block size %ld\n",statbuf.st_blksize);
    fprintf(stderr,"block count %ld\n",statbuf.st_blocks);
  }

  memset(sharedMemory,0,byteCount);
  fprintf(stderr,"Mapped %zu bytes @ %p\n",byteCount,sharedMemory);


  //umap our memory
  if( -1 == munmap(sharedMemory,byteCount) ){
    //update the exception, set error to true so we know to throw it.
    return -1;
  }

  
  //close the shared memory file
  if( -1 == close(fd)) {
    //update the exception and set error to true
    return -1;
  }

  if( -1 == shm_unlink("test")) {
    //update the exception and set error to true
    return -1;
  }

  fprintf(stderr,"\n\n\n");
  
  return 0;
}

int normal(size_t byteCount){
  char * buffer = (char*) malloc(byteCount);
  if(NULL == buffer){
    return -1;
  }
  sleep(1);
  memset(buffer,0,byteCount);
  fprintf(stderr,"malloced %zu bytes @ %p\n",byteCount,buffer);
  free(buffer);
  return 0;
}

int main(){

  for(size_t i = 8;i < 32;i++){
    size_t size = 0x1<<i;
    if(-1 == allocateZeroAndDeallocate(size)){
      break;
    }
//    if(-1 == normal(size)){
//      break;
//    }
  }
  return 0;
}
