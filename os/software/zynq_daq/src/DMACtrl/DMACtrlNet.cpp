#include <DMACtrl/DMACtrlNet.hh>
#include <errno.h>
#include <string.h> // for strerror
#include <unistd.h> //for read

#include <fcntl.h> // for O_*

#include <exception>
#include <stdexcept> // for std::invalid_argument

#include <algorithm>

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#include <DMAMesg/DMAMesgComm.hh>

//for SIGTERM watch
#include <signal.h>
#include <stddef.h>


const uint8_t fakeData[] = {0xA5,0x33,0x00,0x00,0x01,0x00,0x00,0x04,0x08,0xCE,0x12,0x00,0x5A,0x06,0x00,0x00,0x01,0x23,0xFF,0xFF,0x00,0x3D,0xFF,0xFF,0x00,0x26,0xFF,0xFF,0x00,0x00,0x00,0x0C,0xCE,0x12,0x01,0x00,0x5A,0x06,0x00,0x00,0x01,0x45,0xFE,0xFF,0x00,0x1B,0xFE,0xFF,0x00,0x25,0xFE,0xFF,0x00,0x00,0x00,0x04,0xCE,0x12,0x02,0x00,0x5A,0x06,0x00,0x00,0x01,0xB9,0x00,0x00,0x00,0xD1,0x00,0x00,0x00,0xDA,0x00,0x00,0x00,0x00,0x00,0x04,0xCE,0x12,0x03,0x00,0x5A,0x06,0x00,0x00,0x01,0xF9,0x01,0x00,0x00,0x0B,0x02,0x00,0x00,0x06,0x02,0x00,0x00,0x00,0x00,0x04,0xCE,0x12,0x04,0x00,0x5A,0x06,0x00,0x00,0x01,0x3E,0xFA,0xFF,0x00,0x38,0xFA,0xFF,0x00,0x30,0xFA,0xFF,0x00,0x00,0x00,0x04,0xCE,0x12,0x05,0x00,0x5A,0x06,0x00,0x00,0x01,0x86,0xFE,0xFF,0x00,0x8A,0xFE,0xFF,0x00,0x91,0xFE,0xFF,0x00,0x00,0x00,0x04,0xCE,0x12,0x06,0x00,0x5A,0x06,0x00,0x00,0x01,0x7D,0xFD,0xFF,0x00,0x40,0xFD,0xFF,0x00,0x49,0xFD,0xFF,0x00,0x00,0x00,0x04,0xCE,0x12,0x07,0x00,0x5A,0x06,0x00,0x00,0x01,0x93,0x05,0x00,0x00,0x95,0x05,0x00,0x00,0x8E,0x05,0x00,0x00,0x00,0x00,0x04,0xCE,0x12,0x08,0x00};


void error(char const *msg) {
  perror(msg);
  exit(1);
}


ssize_t writen(int fd,void const *vptr, size_t n)
{
  size_t nleft;
  ssize_t nwritten;
  char const *ptr;

  ptr = (char const * ) vptr;
  nleft = n;
  while (nleft > 0) {

    
    //check that the socket isn't dead
    fd_set tempFDMask;
    FD_ZERO(&tempFDMask);
    FD_SET(fd,&tempFDMask);
    timeval zeroTimeout = {0,0}; //Do not sleep waiting for read ready
    select(fd+1,&tempFDMask,NULL,NULL,&zeroTimeout);
    if(FD_ISSET(fd,&tempFDMask)){
      return -1;
    }

    


    if ( (nwritten = write(fd, ptr, nleft)) <= 0) {
      if (nwritten < 0 && errno == EINTR)
	nwritten = 0;   /* and call write() again */
      else
	return (-1);    /* error */
    }

    nleft -= nwritten;
    ptr += nwritten;
  }
  return (n);
}



static const char DMA_device_name[] = "/dev/pl_dma"; 

#define FRAME_SIZE_WORD 0
#define FRAME_ID_WORD 1
#define FRAME_SPILL_WORD 2

DMACtrlNet::DMACtrlNet(size_t _maxFrameSize):
  maxFrameSize(_maxFrameSize), //in 32bit words
  dataPtr(NULL),
  dmaFD(-1){

  //open the DMA device
  dmaFD = open(DMA_device_name,O_RDONLY);
  if(dmaFD < 0){
    fprintf(stderr,"Err(%d) open: %s\n",errno,strerror(errno));
    throw std::invalid_argument("dma driver open");
  }


  //Adjust maxFrameSize to be an integer number of blocks
  int page_size = sysconf(_SC_PAGESIZE);  // in bytes
  maxFrameSize /= (page_size/sizeof(uint32_t));
  maxFrameSize++;
  maxFrameSize *= (page_size/sizeof(uint32_t));

  //Compute buffer size in bytes which is a multiple of the DMABLOCKSIZE
  frameBufferCount = 512;
  iFrame = 0;
  size_t bufferSize = maxFrameSize;
  //  bufferSize *= sizeof(uint32_t); //go to bytes
  //  sendPtr = new uint32_t[bufferSize];
  bufferSize *= frameBufferCount;
  dataPtr = new uint32_t[bufferSize];

  
  runClient = false;
  runServer = false;
}


void DMACtrlNet::Run(){
  runServer = true;  


  /* 
   * socket: create the parent socket 
   */
  listenFD = socket(AF_INET, SOCK_STREAM, 0);
  if (listenFD < 0) 
    error("ERROR opening socket");

  /* setsockopt: Handy debugging trick that lets 
   * us rerun the server immediately after we kill it; 
   * otherwise we have to wait about 20 secs. 
   * Eliminates "ERROR on binding: Address already in use" error. 
   */
  int optval = 1;
  setsockopt(listenFD, SOL_SOCKET, SO_REUSEADDR, 
	     (const void *)&optval , sizeof(int));

  /*
   * build the server's Internet address
   */
  struct sockaddr_in serveraddr; /* server's addr */

  bzero((char *) &serveraddr, sizeof(serveraddr));

  /* this is an Internet address */
  serveraddr.sin_family = AF_INET;

  /* let the system figure out our IP address */
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);

  /* this is the port we will listen on */
  serveraddr.sin_port = htons((unsigned short)9999);

  /* 
   * bind: associate the parent socket with a port 
   */
  if (bind(listenFD, (struct sockaddr *) &serveraddr, 
	   sizeof(serveraddr)) < 0) 
    error("ERROR on binding");

  /* 
   * listen: make this socket ready to accept connection requests 
   */
  if (listen(listenFD, 0) < 0) /* allow 0 requests to queue up */ 
    error("ERROR on listen");



  //block SIGTERM
  //  sigset_t ignore_mask, pending_mask;

  sigemptyset (&ignore_mask);
  sigaddset (&ignore_mask, SIGTERM);
  sigprocmask (SIG_SETMASK, &ignore_mask, NULL);

  
  while(runServer){
    fd_set tempFDMask;
    FD_ZERO(&tempFDMask);
    FD_SET(listenFD,&tempFDMask);
    timeval zeroTimeout = {0,0}; //Do not sleep waiting for read ready
    select(listenFD+1,&tempFDMask,NULL,NULL,&zeroTimeout);
    if(FD_ISSET(listenFD,&tempFDMask) ){
      //Socket is not readble, no data ready.
      clientFD = accept(listenFD, NULL,NULL);
      fprintf(stderr,"New client\n");
      if (clientFD < 0) {
	error("ERROR on accept");
      }else {
	runClient = true;
	ProcessClient();
	close(clientFD);
	fprintf(stderr,"Client closed\n");
      }
    }else{
      //Check that we haven't gotten sigterm
      sigpending (&pending_mask);
      if (sigismember (&pending_mask, SIGTERM)) {
	runClient=false;
	runServer=false;
      }else{
	//make sure we don't fill up our buffers by reading out our 1Hz junk DMAs
	//until we get a client
	read(dmaFD,throwAwaySpace,DMA_BLOCK_SIZE*sizeof(uint32_t));
      }
    }
  }
  close(listenFD);
}


int DMACtrlNet::BlockOnDMA(uint32_t * ptr){
  //because of the xilinx dma driver not being able to terminate an in-progress dma transaction,
  //we have to have the PL side have an idle DMA transferr at the polling rate.
  //If no valid DMA transaction has happened in the last 1s, then the PL will send us a junk one.
  //This will just have DEADBEEFs in it.
  //These allow us to address any other bookkeeping (user interface) instead of blocking for the next event.
  //This won't affect data taking as we dont' start valid DMAs until we have a full valid frame (multiple blocks), so we should never send an invalid block int he middle of a frame.
  //If we are waiting for a new frame and we get an invalid block, it won't have the correct magic word at the beginning and we'll just drop it and ignore it.
  //If we do get out of sync, we will end up with at most, one accepted bad event, and then missing data until the next full valid frame gets sent.



  //check that the socket isn't dead
  fd_set tempFDMask;
  FD_ZERO(&tempFDMask);
  FD_SET(clientFD,&tempFDMask);
  timeval zeroTimeout = {0,0}; //Do not sleep waiting for read ready
  select(clientFD+1,&tempFDMask,NULL,NULL,&zeroTimeout);
  if(FD_ISSET(clientFD,&tempFDMask)){
    runClient = false;
    return -1;
  }

  //Check that we haven't gotten sigterm
  sigpending (&pending_mask);
  if (sigismember (&pending_mask, SIGTERM)) {
    runClient=false;
    runServer=false;
    return -1;
  }
  
  
  //block in dma (blocks for at most 1s)
  return read(dmaFD,ptr,DMA_BLOCK_SIZE*sizeof(uint32_t));

}

void DMACtrlNet::ProcessClient(){
  while(runClient){
    if(iFrame >= frameBufferCount){
      iFrame = 0;
    }
    uint32_t * framePtr = dataPtr + iFrame*maxFrameSize;
    uint32_t * sendPtr  = framePtr;
    iFrame++;

    uint32_t size = 0;
    //read out the first block
    int dmaReturn = BlockOnDMA(framePtr);
    if(dmaReturn > 0){
      //copy the writePtr into the framePtr so if we get invalid data, we can go back to the write ptr and start over. 	
      //We got data in our first block, check if it has the magic word.
      if(G2QUAD_DATA_SOF == (framePtr[FRAME_SIZE_WORD]&0xFF)){
	//We have a valid header, grab the size (bits 31 downto 8)
	size = framePtr[FRAME_SIZE_WORD]>>8;

	if(int(size) > maxFrameSize){
	  //frame is too big, punt on this data
	  fprintf(stderr,"Frame too big! %d > %d\n",size,maxFrameSize);
	  continue;
	}

	//Check if this is a valid board ID
	int boardID = framePtr[FRAME_ID_WORD]>>24;
	if((boardID < 0) || (boardID > 4)){
	  fprintf(stderr,"Bad frame number: %d skipping\n",boardID);
	  continue;
	}

	
	  
	//frame size makes sense, continue reading
	int sizeLeft = int(size) - DMA_BLOCK_SIZE; //Get size left to readout.
	//Move past the data we have and get ready for the next DMA block
	framePtr += DMA_BLOCK_SIZE;

	bool error = false;
	
	//read out the rest of the frame
	while(sizeLeft > 0){
	  //read out the next DMA block
	  dmaReturn = BlockOnDMA(framePtr);//read(dmaFD,framePtr,DMA_BLOCK_SIZE);
	  if(dmaReturn > 0){
	    //Block is good, keep going
	    sizeLeft -= DMA_BLOCK_SIZE;
	    framePtr += DMA_BLOCK_SIZE;
	  }else{
	    //this was a bad block, note it and continue
	    error = true;
	    break;
	  }
	}

	//Done with readout of this frame
	if(!error){
	  //wait for next frame
#ifdef DEBUG_MODE
	  fprintf(stderr,"Event ID  : %u\n",(sendPtr[2]>>8)&0xFFFF);
	  fprintf(stderr,"Board ID  : %u\n",(sendPtr[1]>>24)&0xFF);
	  fprintf(stderr,"Event size: %uw(%ub)\n",(sendPtr[0]>>8)&0xFFFFFF,((sendPtr[0]>>8)&0xFFFFFF)*4);

	  for(size_t iWord = 0; iWord < size;){
	    fprintf(stderr,"     ");
	    for(size_t iPrintWord = iWord;
		(iPrintWord < size) && (iPrintWord < iWord+4);
		iPrintWord++){
	      fprintf(stderr," 0x%08X",sendPtr[iPrintWord]);
	    }
	    fprintf(stderr,"\n");
	    iWord += 4;
	  }
#endif	  
	  //	  int sizeLeft = sizeof(uint32_t) * size;
	  int sendSize = sizeof(uint32_t) * size;

	  if(G2QUAD_DATA_SOF != (sendPtr[0]&0xFF)){
	    fprintf(stderr,"Bad SOF: 0x%08X\n",sendPtr[0]);
	  }

	  
#ifdef DEBUG_MODE
	  fprintf(stderr,"Sending %d bytes\n",sendSize);
#endif
	  if(writen(clientFD,sendPtr,sendSize) < 0){
	    //if(writen(clientFD,fakeData,sizeof(fakeData)) < 0){
	    perror("TCP write error");
	    runClient = false;
	  }
#ifdef DEBUG_MODE
	  fprintf(stderr,"Sent\n");
#endif
	}else{
	  fprintf(stderr,"Bad data. (possibly client disconnect)\n");
	}
      }else{
	if(0xdeadbeef == framePtr[0]){
	  //This is a idle word
	}else{
	  //bad data!
	  fprintf(stderr,"Bad data start 0x%08X\n",framePtr[0]);
	}
      }
    }
  }
  return;
}
