#include <DMACtrl/DMACtrl.hh>
#include <errno.h>
#include <string.h> // for strerror
#include <unistd.h> //for read

#include <fcntl.h> // for O_*

#include <exception>
#include <stdexcept> // for std::invalid_argument

#include <algorithm>

static const char DMA_device_name[] = "/dev/pl_dma"; 
static const char client_socket_path[] = "/tmp/pl_dma.socket";

#define FRAME_SIZE_WORD 0
#define FRAME_ID_WORD 1
#define FRAME_SPILL_WORD 2

DMACtrl::DMACtrl(size_t _maxFrameSize,size_t _frameBufferCount):
  SMController(),
  maxFrameSize(_maxFrameSize), //in 32bit words
  frameBufferCount(_frameBufferCount),
  userFrame(0),
  startPtr(NULL),
  dmaFD(-1){

  //open the DMA device
  dmaFD = open(DMA_device_name,O_RDONLY);
  if(dmaFD < 0){
    fprintf(stderr,"Err(%d) open: %s\n",errno,strerror(errno));
    throw std::invalid_argument("dma driver open");
  }

  if(!unSock.Open(client_socket_path)){
    throw std::invalid_argument("can't open dmaMesgServer\n");
  }
  
  SetShareName("g2quad");
  //  SetGroupName("g2quad");
  SetGroupName("root");

  //Adjust maxFrameSize to be an integer number of blocks
  int page_size = sysconf(_SC_PAGESIZE);  // in bytes
#ifdef DEBUG
  printf("Page size: %d bytes\n",page_size);
  printf("Original maxFrameSize 0x%X (0x%X bytes)\n",maxFrameSize,maxFrameSize*sizeof(uint32_t));
#endif
  maxFrameSize /= (page_size/sizeof(uint32_t));
  maxFrameSize++;
  maxFrameSize *= (page_size/sizeof(uint32_t));
#ifdef DEBUG
  printf("Original aligned maxFrameSize 0x%X (0x%X bytes)\n",maxFrameSize,maxFrameSize*sizeof(uint32_t));
#endif  

  //Compute buffer size in bytes which is a multiple of the DMABLOCKSIZE
  size_t bufferSize = maxFrameSize*(frameBufferCount);
  bufferSize *= sizeof(uint32_t); //go to bytes
#ifdef DEBUG
  printf("Buffer size: 0x%X * 0x%X = 0x%X bytes\n",maxFrameSize,frameBufferCount,bufferSize);
#endif
  SetSize(bufferSize);
  Create();
  startPtr = (uint32_t*) GetMemory();
  //Fill our queue of indexes.
  for(size_t iIndex = 0; iIndex < frameBufferCount;iIndex++){
    emptyBlocks.push(iIndex);
#ifdef DEBUG
    printf("Adding block %zu\n",iIndex);
#endif
  }

#ifdef DEBUG  
  for(size_t iBlock = 0; iBlock < frameBufferCount;iBlock++){
    uint32_t * ptr = (startPtr + (iBlock*maxFrameSize));
    printf("%zu(%zu) %p\n",iBlock,frameBufferCount,ptr);
    *ptr = 0xdeadbeef;
    *(ptr + maxFrameSize-1) = 0xabadcafe;
  }
  printf("Block count: %zu\n",frameBufferCount);
#endif
  
  runClient = false;
  runServer = false;
  
  errorFrameTooBig          = 0;       
  errorBadDMAInFrame        = 0;     
  errorFrameIncomplete      = 0;   
  errorBadFrameStartMagic   = 0;
  errorBadDMAAtFrameStart   = 0;
  errorBadReturnFrame       = 0;

  statReturnedFrameCount    = 0;    
  statDeliveredFrameCount   = 0;   
  statAvailableRequestCount  = 0;  
  statBlockOnDMACount       = 0;       
  statBlockOnDMACheckReturn = 0; 
  statValidHeader           = 0;           
  statBoard1Count           = 0;           
  statBoard2Count           = 0;             
  statBoard3Count           = 0;           
  statBoard4Count           = 0;           
  statBoardUnknownCount     = 0;     
  statFinishedFrames        = 0;        
  statBlockThrownAway       = 0;       
  statIdleBlockCount        = 0;
}


void DMACtrl::Run(){
  runServer = true;  
  while(runServer){
    if(unSock.ListenForClient(10)){

      //Process this client. 
      fprintf(stdout,"Client connected\n");
      runClient = true;      
      ProcessClient();  // WE'll be in this call for a long time. 

      //The client has ended, close the fd
      unSock.CloseClient();
      fprintf(stdout,"Client disconnected\n");      
    }else{
      //make sure we don't fill up our buffers by reading out our 1Hz junk DMAs
      //until we get a client
      read(dmaFD,throwAwaySpace,DMA_BLOCK_SIZE*sizeof(uint32_t));
    }
  }
}

int DMACtrl::TimeoutCheck(){
  int ret = 0;

  static bool frameRequested = false;
  bool smInfoRequested = false;
  bool availableSizeRequested = false;
  bool dmaStatsRequested = false;
  //check if there is a message from the client.  
  message_t * clientMsg;

  int pendingMessage = unSock.MessagePending();
  
  if(-1 == pendingMessage){
    //The socket has been closed on the other end.
    //We need to stop.
    ret = -1;
    frameRequested = 0;
  }else if (pendingMessage == 1){
    if(0 == unSock.GetMessage(clientMsg)){
#ifdef DEBUG
      fprintf(stderr,"Got message of type: %d\n",clientMsg->type);
#endif
      switch (clientMsg->type){
      //===================================================
      case MT_CLOSE:
	//The client is disconnecting.
	//We need to stop.
	ret = -1;
	frameRequested = 0;
	break;
      //===================================================
      case MT_SHUTDOWN:
	//shutdown this program the next chance we get to aviod DMA interrupt issues.
	ret = -1;
	runServer = false;
	frameRequested = 0;
	break;
      //===================================================
      case MT_FRAME:
	ret = 0; //No need to shut down
	//User is returning a frame
	if(0 == userFrame){
	  fprintf(stderr,"NO frames lent out\n");
	  errorBadReturnFrame++;
	}else{
	  Frame_t * frame = (Frame_t*)clientMsg->data;
	  //Search through the outstanding frames for this block
	  bool foundFrame = false;
	  for(std::list<Frame_t>::iterator itFrame = frames.begin();
	      itFrame != frames.end();
	      itFrame++){
	    if(itFrame->data_block == frame->data_block){
	      //Found the Frame

	      //Clear the header of it before returning it to empty queue	      
	      if(frame->data_block != -1){
		uint32_t * framePtr = startPtr+((frame->data_block) * maxFrameSize);
		framePtr[0] = 0;
		framePtr[1] = 0;
		framePtr[2] = 0;
	      }
	      
	      emptyBlocks.push(frame->data_block);
	      frames.erase(itFrame);
	      foundFrame = true;
	      statReturnedFrameCount++;
	      if(userFrame > 0){
		userFrame--;
	      }
	      break;
	    }
	  }
	  if(!foundFrame){
	    fprintf(stderr,"Bad return frame %d\n",
		    frame->data_block);
	    errorBadReturnFrame++;
	  }
	}	
	break;
      //===================================================
      case MT_REQUEST_FRAME:
	ret = 0; //Things are good
	frameRequested = true;
	break;
      //===================================================
      case MT_GET_SM_INFO:	
	ret = 0;
	smInfoRequested = true;
	frameRequested = false; //I don't know why this would be true if we get a message, but due to the transactional relateionship with the client, we are going to set this to false. 
	break;
      //===================================================
      case MT_GET_AVAILABLE:
	ret = 0;
	smInfoRequested = false;
	frameRequested = false;
	availableSizeRequested = true;
	break;
      case MT_GET_DMA_STATS:
	ret = 0;
	smInfoRequested = false;
	frameRequested = false;
	availableSizeRequested = false;
	dmaStatsRequested = true;
	break;
	
      //===================================================
      case MT_UNKNOWN:
    ret = 0;
    fprintf(stderr,"Unknown message\n");
    unSock.SendMessage(MT_UNKNOWN,NULL,0);
    break;
      //===================================================
      default:
	ret = 0;
	fprintf(stderr,"Unknown message type %d\n",clientMsg->type);
	break;
      }

      //free clientMsg memory
      free(clientMsg);
    }
  }

#ifdef DEBUG
  fprintf(stderr,"Timeout check: smInfo (%s) request (%s)\n",
	  smInfoRequested ? "Y":"N",
	  frameRequested ? "Y":"N");
#endif
  
  //Respond to client if needed
  if(smInfoRequested){
    smInfoRequested = false; //Why? 2019-01-21
    //Allocate and fill an Setup_t structure with the servers SM info
    size_t smInfoSize= sizeof(Setup_t) + GetShareName().size()+1;
    Setup_t * smInfo = (Setup_t *) calloc(smInfoSize,sizeof(uint8_t)); //allocate and zero
    smInfo->memorySize = GetSize();
    smInfo->blockSize = maxFrameSize;
    memcpy(smInfo->path,GetShareName().c_str(),GetShareName().size());    
    unSock.SendMessage(MT_SM_INFO,(uint8_t*) smInfo,smInfoSize);
    free(smInfo);    
  }else if(frameRequested){
    //There is no frame sent out to the users
    if(frames.size() > 0){
#ifdef DEBUG
      fprintf(stderr,"Sending frame: %d\n",frames.front().data_block);
#endif
      unSock.SendMessage(MT_FRAME,(uint8_t*)&(frames.front()),sizeof(Frame_t));
      userFrame++;
      statDeliveredFrameCount++;
      frameRequested = false;
    }
  }else if(availableSizeRequested){
    Status_t available;
    available.type = 0;
    //Compute the number of frames available (frames minus the existing outstanding)
    available.data = frames.size() - userFrame;
    //printf("frames.size(): %d and userFrame: %d \n",frames.size(),userFrame);
    unSock.SendMessage(MT_GET_AVAILABLE,(uint8_t *) &available,sizeof(available));
#ifdef DEBUG
    fprintf(stderr,"frames.size(): %d and userFrame: %d\n",frames.size(),userFrame);
#endif
    statAvailableRequestCount++;
  }else if(dmaStatsRequested){
    DMAStat_t stats;
    stats.ver=2;
    stats.reserved = 0;
    stats.errorFrameTooBig           = errorFrameTooBig;          
    stats.errorBadDMAInFrame         = errorBadDMAInFrame;        
    stats.errorFrameIncomplete       = errorFrameIncomplete;      
    stats.errorBadFrameStartMagic    = errorBadFrameStartMagic;   
    stats.errorBadDMAAtFrameStart    = errorBadDMAAtFrameStart;   
    stats.errorBadReturnFrame        = errorBadReturnFrame;       
    stats.statReturnedFrameCount     = statReturnedFrameCount;    
    stats.statDeliveredFrameCount    = statDeliveredFrameCount;   
    stats.statAvailableRequestCount  = statAvailableRequestCount;  
    stats.statBlockOnDMACount        = statBlockOnDMACount;       
    stats.statBlockOnDMACheckReturn  = statBlockOnDMACheckReturn; 
    stats.statValidHeader            = statValidHeader;           
    stats.statBoard1Count            = statBoard1Count;           
    stats.statBoard2Count            = statBoard2Count;           
    stats.statBoard3Count            = statBoard3Count;           
    stats.statBoard4Count            = statBoard4Count;           
    stats.statBoardUnknownCount      = statBoardUnknownCount;     
    stats.statFinishedFrames         = statFinishedFrames;        
    stats.statBlockThrownAway        = statBlockThrownAway;       
    stats.statIdleBlockCount         = statIdleBlockCount;
    stats.statEmptyBlockCount        = emptyBlocks.size();
    stats.statActiveFrameCount       = frames.size();
    unSock.SendMessage(MT_GET_DMA_STATS,(uint8_t *) &stats,sizeof(stats));

  }
  return ret;
}

int DMACtrl::BlockOnDMA(uint32_t * ptr){
  //because of the xilinx dma driver not being able to terminate an in-progress dma transaction,
  //we have to have the PL side have an idle DMA transferr at the polling rate.
  //If no valid DMA transaction has happened in the last 1s, then the PL will send us a junk one.
  //This will just have DEADBEEFs in it.
  //These allow us to address any other bookkeeping (user interface) instead of blocking for the next event.
  //This won't affect data taking as we dont' start valid DMAs until we have a full valid frame (multiple blocks), so we should never send an invalid block int he middle of a frame.
  //If we are waiting for a new frame and we get an invalid block, it won't have the correct magic word at the beginning and we'll just drop it and ignore it.
  //If we do get out of sync, we will end up with at most, one accepted bad event, and then missing data until the next full valid frame gets sent.

  statBlockOnDMACount++;
  //Do any book-keeping
  int timeoutCheckReturn = 0;
  if((timeoutCheckReturn = TimeoutCheck()) < 0){
    //Something happened and we want to shutdown our readout
    runClient = false;  // stop the loop in process client
    statBlockOnDMACheckReturn++;
    return timeoutCheckReturn;
  }
  //block in dma (blocks for at most 1s)
  return read(dmaFD,ptr,DMA_BLOCK_SIZE*sizeof(uint32_t));

}

void DMACtrl::ProcessClient(){
  while(runClient){

    //Find the next block of free space that is big enough.
    int freeSpace = maxFrameSize;
    if(emptyBlocks.empty()){
      freeSpace = 0;      
    }

    //Do the readout if we can
    if(freeSpace >= maxFrameSize){
      
      
      uint32_t * framePtr = startPtr+(emptyBlocks.front()*maxFrameSize);
      
      //we have space for a frame, build a structure for it.
      Frame_t frame = {emptyBlocks.front(),0,0,0};

      //read out the first block
      int dmaReturn = BlockOnDMA(framePtr);
      if(dmaReturn > 0){
#ifdef DEBUG
	fprintf(stderr,"FramePtr: %p\n",framePtr);
#endif	
	//copy the writePtr into the framePtr so if we get invalid data, we can go back to the write ptr and start over. 	
	//We got data in our first block, check if it has the magic word.
	if(G2QUAD_DATA_SOF == (framePtr[FRAME_SIZE_WORD]&0xFF)){
	  statValidHeader++;
	  //We have a valid header, grab the size (bits 31 downto 8)
	  frame.size = framePtr[FRAME_SIZE_WORD]>>8;

	  if(int(frame.size) > maxFrameSize){
	    //frame is too big, punt on this data
	    errorFrameTooBig++;
	    continue;
	  }

	  //Set board ID and spill ID
	  frame.spillID = (framePtr[FRAME_SPILL_WORD]>>8)&0xFFFF;
	  frame.boardID = (framePtr[FRAME_ID_WORD]>>24)&0xF;

	  switch (frame.boardID){
	  case 1:
	    statBoard1Count++;
	    break;
	  case 2:
	    statBoard2Count++;
	    break;
	  case 3:
	    statBoard3Count++;
	    break;
	  case 4:
	    statBoard4Count++;
	    break;
	  default:
	    statBoardUnknownCount++;
	    break;
	  };
	  
	  
	  //frame size makes sense, continue reading
	  int sizeLeft = int(frame.size) - DMA_BLOCK_SIZE; //Get size left to readout.
	  //Move past the data we have and get ready for the next DMA block
	  framePtr += DMA_BLOCK_SIZE;

	  //read out the rest of the frame
	  while(sizeLeft > 0){
	    //read out the next DMA block
	    dmaReturn = BlockOnDMA(framePtr);//read(dmaFD,framePtr,DMA_BLOCK_SIZE);
#ifdef DEBUG
	    fprintf(stderr,"FramePtr: %p\n",framePtr);
#endif
	    if(dmaReturn > 0){
	      //Block is good, keep going
	      sizeLeft -= DMA_BLOCK_SIZE;
	      framePtr += DMA_BLOCK_SIZE;
	    }else{
	      //this was a bad block, note it and continue
	      errorBadDMAInFrame++;
	      frame.data_block = -1;
	      frame.size = 0;	      
	      break;
	    }
	  }

	  //Done with readout of this frame
	  if(-1 != frame.data_block){
	    //This was a good frame
	    statFinishedFrames++;
	    
	    //update the write points
	    emptyBlocks.pop();
	    
	    //Update the frame's pointer to be shm offset
	    //save the frame for the user	    
#ifdef DEBUG
	    fprintf(stderr,"Board %01u, spill %08u\n",frame.boardID,frame.spillID);
	    fprintf(stderr,"Size: %u @ %d\n",frame.size,frame.data_block);
#endif
	    frames.push_back(frame);
#ifdef DEBUG
	    fprintf(stderr,"SHM start: %p\n",startPtr);
	    fprintf(stderr,"emptyBlocks: %zu\n",emptyBlocks.size());
	    if(!emptyBlocks.empty()){
	      fprintf(stderr,"  write block: %d\n",emptyBlocks.front());
	    }	
#endif
	    //wait for next frame
	  }else{
	    //Something went wrong, so don't update the write pointer.
	    errorFrameIncomplete++;
	  }
	  
	}else{
	  //Bad magic word at beginning of frame
	  if(0xDEADBEEF == framePtr[0]){
	    statIdleBlockCount++;
	  }else{
	    errorBadFrameStartMagic++;
	  }
	}
      }else{
	//bad block
	errorBadDMAAtFrameStart++;
      }
    }else{
      //Throw away data, but keep the DMAs going and check for shutdowns and disconnects
      statBlockThrownAway++;
      BlockOnDMA(throwAwaySpace);      
    }
  }
}
