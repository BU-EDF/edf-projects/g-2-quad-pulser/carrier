#!/bin/bash

#resize devtmpfs to allow for bigger shared memory mappings
#We should figure out how to check this since mmap is happy to map bigger than it is allowed
#and we only find out when we page in memory over the limit. 
echo "Updating devtmpfs size to 64M" 
mount -o remount,size=256M devtmpfs

#add the dma driver
insmod /work/pl_dma.ko

#start the dma control server
cp /work/dma_ctrl /tmp/
/tmp/dma_ctrl &
#/work/dma_test -p 0 &

#start the register server
/work/pl_server &
