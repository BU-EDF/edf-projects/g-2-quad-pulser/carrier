# Non-project mode
# collect files
# run synthesis

source ../scripts/settings.tcl
source ../scripts/helpers.tcl


open_project $outputDir/../proj/$top.xpr
#read_checkpoint $outputDir/pre_synth.dcp
#link_design -top $top -part $FPGA_part

#################################################################################
# STEP#2: run synthesis, report utilization and timing estimates, write checkpoint design
#################################################################################
synth_design -top $top -part $FPGA_part -flatten rebuilt
#write_edif $outputDir/post_synth.edf
write_checkpoint -force $outputDir/post_synth
report_timing_summary -file $outputDir/post_synth_timing_summary.rpt
report_power -file $outputDir/post_synth_power.rpt


