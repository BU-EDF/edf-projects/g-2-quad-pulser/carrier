# Non-project mode
# Generate bistream files

source ../scripts/settings.tcl


set osDir ../os
set hwDescriptionDir $osDir/hwd


#export hw description
read_checkpoint $outputDir/post_route.dcp
link_design -top $top -part $FPGA_part

set hwdef_file $outputDir/$top.hwdef
set bit_file ../bit/$top.bit
set hdf_file $hwDescriptionDir/$top.hdf

set cmd_write_sysdef "write_sysdef -hwdef $hwdef_file -bitfile $bit_file -file $hdf_file -force"

eval $cmd_write_sysdef
