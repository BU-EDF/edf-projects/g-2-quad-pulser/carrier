# Non-project mode
# run placement
source ../scripts/settings.tcl


#
#Generate hwdef for BD
#
read_checkpoint $outputDir/post_synth.dcp
link_design -top $top -part $FPGA_part

write_hwdef -file $outputDir/$top.hwdef -force

