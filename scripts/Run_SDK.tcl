# Non-project mode
# Generate route
source ../scripts/settings.tcl


set osDir ./
set hwDescriptionDir $osDir/hwd


set hw_name $top
set bsp_name $top
set fsbl_name $top
append hw_name "_hw"
append bsp_name "_bsp"
append fsbl_name "_fsbl"

#
# STEP#6:
#
set script_path [ file dirname [ file normalize [ info script ] ] ]
puts $script_path

sdk set_workspace $osDir
puts $osDir
puts $hwDescriptionDir/$top.hdf

sdk create_hw_project  -name $hw_name   -hwspec $hwDescriptionDir/$top.hdf

#for some reason, this version breaks when building a bsp, but will build one automatically if it doesn't exist when you do create_app_project for a FSBL
sdk create_app_project -name $fsbl_name -hwproject $hw_name -proc ps7_cortexa9_0 -os standalone -lang C -app {Zynq FSBL}

sdk build_project -type all
exit
