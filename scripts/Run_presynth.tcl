# Non-project mode
# collect files
# run synthesis

source ../scripts/settings.tcl
source ../scripts/helpers.tcl
source ../scripts/timestamp.tcl



#############################################################
# Setup the top
#############################################################
set top_file ../src/top.vhd
#set the file added for the primary timing constraints
set top_xdc_file ../src/top.xdc

puts "Setting top level to $top"



#################################################################################
# STEP#0: define output directory area.
#################################################################################
file mkdir $outputDir

#
# STEP# 0.6: build project for project mode editing
#set projectDir ../proj/$top
set projectDir ../proj/
file mkdir $projectDir
if {[file isfile $projectDir/$top.xpr]} {
    puts "Re-creating project file."
} else {
    puts "Creating project file."
}
create_project -force -part $FPGA_part $top $projectDir
set_property target_language VHDL [current_project]

#################################################################################
# STEP#1: setup design sources and constraints
#################################################################################


#add top file
read_vhdl $top_file

#rebuild the timestamp version file
build_fw_version ../src/


set new_folder [lindex $argv 0]
puts "Adding files from directory: $new_folder "


#regenerate the block design
source ../bd/block_design.tcl

puts "Generating Zynq BD wrapper"
make_wrapper -files [get_files ../bd/zynq/zynq.bd] -top


#add board files
set bd_file ""
set bd_list [ glob-r ../bd *.bd ]
for {set j 0} {$j < [llength $bd_list ] } {incr j} {    
    read_bd [lindex $bd_list $j]
    open_bd_design [lindex $bd_list $j]
    set bd_file [lindex $bd_list $j]
    generate_target all [get_files [lindex $bd_list $j]]

    #make a wrapper file for the block design
    make_wrapper -files [get_files $bd_file] -top -import -force
    set bd_wrapper_vhdl_file [file dirname $bd_file]
    append bd_wrapper_vhdl_file "/hdl/"
    append bd_wrapper_vhdl_file [file rootname [file tail $bd_file]]
    append bd_wrapper_vhdl_file "_wrapper.vhd"
    puts "$bd_wrapper_vhdl_file"
    read_vhdl [glob $bd_wrapper_vhdl_file]
    
    #Add block design hdl
###    set bd_vhdl_file [file dirname $bd_file]
###    append bd_vhdl_file "/hdl/"
###    append bd_vhdl_file [file rootname [file tail $bd_file]]
###    append bd_vhdl_file ".vhd"
###    puts "$bd_vhdl_file"
###    read_vhdl [glob $bd_vhdl_file]
###
###    puts [lindex $bd_list $j]
}


#############################################################
#read in all vhd and v files from the design
#############################################################
for {set i 1} {$i < $argc} {incr i} {
    set new_folder [lindex $argv $i]
    puts "Adding files from directory: $new_folder "

    set vhdl_list [ glob-r $new_folder *.vhd ]
    for {set j 0} {$j < [llength $vhdl_list ] } {incr j} {
	read_vhdl [lindex $vhdl_list $j]
	puts [lindex $vhdl_list $j]
    }

    set xdc_list [ glob-r $new_folder *.xdc ]
    for {set j 0} {$j < [llength $xdc_list ] } {incr j} {
        read_xdc [lindex $xdc_list $j]
        puts [lindex $xdc_list $j]
    }
    
    set library_list [ glob-r $new_folder *library.* ]
    for {set j 0} {$j < [llength $library_list ] } {incr j} {
	set lib_path [file dirname [lindex $library_list $j]]
	set lib_name [string range [file tail [lindex $library_list $j]] 8 1000]
	set_property library $lib_name [ get_files [ glob $lib_path/*.vhd ]]
	puts "set_property library $lib_name [ get_files [ glob $lib_path/*.vhd ]]    "
    }						     
}


#############################################################
#read in the IPCores directory
#############################################################
set new_folder [lindex $argv 0]
set xci_list [ glob-r $new_folder *.xci ]
set bd_xci_list  [ glob-r "../bd/" *.xci ]
#set xci_list [ glob-r $new_folder *.xci ] [ glob-r "../bd/" *.xci ]
#append xci_list " "
#append xci_list $bd_xci_list
for {set j 0} {$j < [llength $xci_list ] } {incr j} {
    set xci_file [lindex $xci_list $j]

    set dcp_filename [file rootname $xci_file]
    append dcp_filename ".dcp"
    
    set ip_name [file rootname [file tail $xci_file]]    
    puts "\n\nLoading $ip_name from $xci_file"

    read_ip $xci_file
    set load_ip_checkpoint [get_property GENERATE_SYNTH_CHECKPOINT [get_ips $ip_name]]
    if $load_ip_checkpoint {
	if [file exists $dcp_filename ] {
	    if { [file mtime $dcp_filename] > [file mtime $xci_file] } {
		puts "Found $dcp_filename for $xci_file."
		read_checkpoint $dcp_filename
	    } else {
		puts "$dcp_filename is older than $xci_file . Rebuilding"
		generate_target -force {all} [get_files $xci_file]
		synth_ip [get_ips $ip_name]
	    }
	} else {
	    puts "Can\'t find $dcp_filename for $xci_file . Rebuilding"
	    generate_target -force {all} [get_files $xci_file]
	    synth_ip [get_ips $ip_name]
	}
    } else {
	if [file exists $dcp_filename ] {
	    read_checkpoint $dcp_filename
	}
    }
    puts "\n\n"
#    generate_target {all} [get_files [lindex $xci_list $j]]

}

#############################################################
#add constraint files
#############################################################
read_xdc $top_xdc_file

#write_checkpoint -force $outputDir/pre_synth

