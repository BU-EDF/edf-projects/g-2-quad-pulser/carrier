# Non-project mode
# run placement
source ../scripts/settings.tcl


#
# STEP#3: run placement and logic optimization, report utilization and timing
#estimates, write checkpoint design
#
read_checkpoint $outputDir/post_synth.dcp
link_design -top $top -part $FPGA_part
opt_design
power_opt_design
place_design
phys_opt_design
write_checkpoint -force $outputDir/post_place
report_timing_summary -file $outputDir/post_place_timing_summary.rpt
