# Non-project mode
# Generate bistream files

source ../scripts/settings.tcl


set osDir ../os
set hwDescriptionDir $osDir/hwd


#
# STEP#5: generate a bitstream and export HW description
#
read_checkpoint $outputDir/post_route.dcp
link_design -top $top -part $FPGA_part
write_debug_probes -force ../bit/$top.ltx
write_bitstream -force ../bit/$top.bit
